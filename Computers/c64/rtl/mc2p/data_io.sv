//
// data_io.v
//
// data_io for the MiST board
// http://code.google.com/p/mist-board/
//
// Copyright (c) 2014 Till Harbaum <till@harbaum.org>
//
// This source file is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This source file is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

module data_io_c64 #(parameter STRLEN=0 ) 
(
    input             clk_sys,

    // Global SPI clock from ARM. 24MHz
    input             SPI_SCK,
    input             SPI_SS2,
    input             SPI_DI,
    output reg         SPI_DO,
    
// parameter STRLEN and the actual length of conf_str have to match
    input [(8*STRLEN)-1:0] conf_str,
    //external data in to the microcontroller
    input  [7:0] data_in,
    //output reg  [7:0]     config_buffer_o[15:0],  // 15 bytes for general use
    output reg mc_ack,
    input reset,
    input menu_in,
    output reg [31:0]status,
    
    output reg          img_mounted, //rising edge if a new image is mounted

    // ARM -> FPGA download
    input             ioctl_force_erase,
    output reg        ioctl_download = 0, // signal indicating an active download
    output reg        ioctl_erasing = 0,  // signal indicating an active erase
    output reg  [7:0] ioctl_index,        // menu index used to upload the file
    output reg        ioctl_wr = 0,
    output reg [24:0] ioctl_addr,
    output reg  [7:0] ioctl_dout
);


///////////////////////////////   DOWNLOADING   ///////////////////////////////

reg  [7:0] data_w;
reg [24:0] addr_w;
reg        rclk   = 0;

localparam UIO_FILE_TX      = 8'h53;
localparam UIO_FILE_TX_DAT  = 8'h54;
localparam UIO_FILE_INDEX   = 8'h55;

reg  [7:0] cmd;
reg  [4:0] cnt;
reg  [9:0] byte_cnt;   // counts bytes
reg  [7:0] ACK = 8'd75; // letter K - 0x4b

// SPI MODE 0 : incoming data on Rising, outgoing on Falling
    always@(negedge SPI_SCK, posedge SPI_SS2, posedge reset) 
    begin
    
                if (reset) mc_ack <= 0;
    
        
                //each time the SS goes down, we will receive a command from the SPI master
                else if (SPI_SS2) // not selected
                    begin
                        SPI_DO <= 1'bZ;
                        byte_cnt <= 9'd0;
                    end
                else
                    begin
                            
                            if (cmd == 8'h10 ) //command 0x10 - send the data to the microcontroller
                                SPI_DO <= data_in[~cnt[2:0]];
                                
                            else if (cmd == 8'h00 ) //command 0x00 - ACK
                                SPI_DO <= ACK[~cnt[2:0]];
                            
                            //else if (acmd == 8'h61 ) //command 0x61 - echo the pumped data
                            //  SPI_DO <= sram_data_s[~cnt[2:0]];           
                    
                    
                            else if(cmd == 8'h14) //command 0x14 - reading config string
                                begin
                                
                                 if (STRLEN == 0 || menu_in == 1'b1) //if we dont have a str, just send the first byte as 00
                                        SPI_DO <= 1'b0;
                                    else if(byte_cnt < STRLEN + 1 ) // returning a byte from string
                                        SPI_DO <= conf_str[{STRLEN - byte_cnt,~cnt[2:0]}];
                                    else
                                        SPI_DO <= 1'b0;

                                
                                    mc_ack <= 1;
                                
                                        
                                end 
                        
                
                            if(cnt[2:0] == 7) byte_cnt <= byte_cnt + 9'd1;
                            
                    end
    end

        
reg  [4:0] cnf_byte;

// data_io has its own SPI interface to the io controller
always@(posedge SPI_SCK, posedge SPI_SS2) 
begin

    reg  [6:0] sbuf;

    reg [24:0] addr;

    if ( SPI_SS2 ) 
    begin
        cnt <= 0;
        cnf_byte <= 4'd15;
        img_mounted <= 1'b0;
    end
    else 
    begin
    
        rclk <= 0;

        // don't shift in last bit. It is evaluated directly
        // when writing to ram
        if(cnt != 15) sbuf <= { sbuf[5:0], SPI_DI};

        // increase target address after write
        if(rclk) addr <= addr + 1'd1;

        // count 0-7 8-15 8-15 ... 
        if(cnt < 15) cnt <= cnt + 1'd1;
            else cnt <= 8;

        // finished command byte
        if(cnt == 7) 
        begin 
            cmd <= {sbuf, SPI_DI};
            
                // command 0x61: start the data streaming
                if(sbuf[6:0] == 7'b0110000 && SPI_DI == 1'b1)
                begin
                    ioctl_download <= 1'b1;
                end
                
                // command 0x62: end the data streaming
                if(sbuf[6:0] == 7'b0110001 && SPI_DI == 1'b0)
                begin
                    ioctl_download <= 1'b0;
                    
                    if (ioctl_index == 8'h91) img_mounted <= 1'b1; //signal to the 1541 interface
                        
                end
        end
        
        if (cnt == 15)
        begin
        
                // command 0x60: stores a configuration byte
                if (cmd == 8'h60)
                begin
                        //bytes 15 to 11 are file_size

                        //config_buffer_o[cnf_byte] <= {sbuf, SPI_DI}; //sbuf[6:0]
                        cnf_byte <= cnf_byte - 1'd1;
            
                        addr <= 0;
                        
                        //boot = index 0
                        //prg = index 8'h02  
                        //tap = index 8'h42
                        //CRT = intex 8'h82
                        //rom = index 8'h03
                        //D64 = intex 8'h91
                       
                        //extension bytes order [11], [10], [9]
                        if({sbuf, SPI_DI} == "p" && cnf_byte == 4'b1011) //byte 11
                            ioctl_index <= 8'h02; // PRG image                      
                        
                        if({sbuf, SPI_DI} == "r" && cnf_byte == 4'b1011) //byte 11
                            ioctl_index <= 8'h03; // ROM image     

                        if({sbuf, SPI_DI} == "t" && cnf_byte == 4'b1011) //byte 11
                            ioctl_index <= 8'h42; // TAP image
                            
                        if({sbuf, SPI_DI} == "c" && cnf_byte == 4'b1011) //byte 11
                            ioctl_index <= 8'h82; // CRT image
                                                        
                        if({sbuf, SPI_DI} == "d" && cnf_byte == 4'b1011) //byte 11
                            ioctl_index <= 8'h00; // initial DAT (boot)

                        if({sbuf, SPI_DI} == "6" && cnf_byte == 4'b1010) //byte 10
                            ioctl_index <= 8'h91; // D64 image
                   
                end
                
                // command 0x15: stores the status word (menu selections)
                if (cmd == 8'h15)
                begin
                    case (cnf_byte) 
                                        
                        4'd15: status[31:24] <={sbuf, SPI_DI};
                        4'd14: status[23:16] <={sbuf, SPI_DI};
                        4'd13: status[15:8]  <={sbuf, SPI_DI};
                        4'd12: status[7:0]   <={sbuf, SPI_DI};
                        
                    endcase
                    
                    cnf_byte <= cnf_byte - 1'd1;

                end
                
                // command 0x61: Data Pump
                if (cmd == 8'h61) 
                begin
                    addr_w <= addr;
                    data_w <= {sbuf, SPI_DI};
                    rclk <= 1;
                end
            
                
/*              // prepare/end transmission
                if((cmd == UIO_FILE_TX) && (cnt == 15)) begin
                    // prepare 
                    if(SPI_DI) begin
                        addr <= 0;
                        ioctl_download <= 1; 
                    end else begin
                        addr_w <= addr;
                        ioctl_download <= 0;
                    end
                end

                // command 0x54: UIO_FILE_TX
                if((cmd == UIO_FILE_TX_DAT) && (cnt == 15)) begin
                    addr_w <= addr;
                    data_w <= {sbuf, SPI_DI};
                    rclk <= 1;
                end

                // expose file (menu) index
                if((cmd == UIO_FILE_INDEX) && (cnt == 15)) ioctl_index <= {sbuf, SPI_DI};
*/
            end
        end
end

reg  [24:0] erase_mask;
wire [24:0] next_erase = (ioctl_addr + 1'd1) & erase_mask;

always@(posedge clk_sys) begin
    reg        rclkD, rclkD2;
    reg        old_force = 0;
    reg  [6:0] erase_clk_div;
    reg [24:0] end_addr;

    rclkD    <= rclk;
    rclkD2   <= rclkD;
    ioctl_wr <= 0;

    if(rclkD & ~rclkD2) begin
        ioctl_dout <= data_w;
        ioctl_addr <= addr_w;
        ioctl_wr   <= 1;
    end

/*
    if(ioctl_download) begin
        old_force     <= 0;
        ioctl_erasing <= 0;
    end else begin

        old_force <= ioctl_force_erase;
        if(ioctl_force_erase & ~old_force) begin
            ioctl_addr    <= 'h1FFFF;
            erase_mask    <= 'h1FFFF;
            end_addr      <= 'h10002;
            erase_clk_div <= 1;
            ioctl_erasing <= 1;
        end else if(ioctl_erasing) begin
            erase_clk_div <= erase_clk_div + 1'd1;
            if(!erase_clk_div) begin
                if(next_erase == end_addr) ioctl_erasing <= 0;
                else begin
                    ioctl_addr <= next_erase;
                    ioctl_dout <= 0;
                    ioctl_wr   <= 1;
                end
            end
        end
    end
    */

end

endmodule
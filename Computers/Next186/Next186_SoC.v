/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/
//============================================================================
//
//  Multicore 2+ Top by Victor Trucco
//
//============================================================================

//`default_nettype none

module Next186_SoC(

// Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAM (IS61WV20488FBLL-10)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM (W9825G6KH-6)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io        = 1'bz,
    inout wire  ps2_data_io       = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o         = 1'bZ,
    output wire sd_sclk_o         = 1'bZ,
    output wire sd_mosi_o         = 1'bZ,
    input wire  sd_miso_i,

    // Joysticks
    output wire joy_clock_o       = 1'b1,
    output wire joy_load_o        = 1'b1,
    input  wire joy_data_i,
    output wire joy_p7_o          = 1'b1,

    // Audio
    output      AUDIO_L,
    output      AUDIO_R,
    input wire  ear_i,
    output wire mic_o             = 1'b0,

    // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

    //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
   
    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output wire   SPI_nWAIT         = 1'b1, // '0' to hold the microcontroller data streaming

    inout [31:0] GPIO,

    output LED                      = 1'b1 // '0' is LED on
);


//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------
assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign stm_rst_o    = 1'b0; //no microcontroler
assign stm_rx_o = 1'bZ;

//no SRAM for this core
assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

joystick_serial  joystick_serial 
(
    .clk_i           ( clk_25 ),
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

// ROM Data Pump
//reg [7:0] osd_s = 8'b11111111;
//PumpSignal PumpSignal (clk_18, ~pll_locked, ioctl_download, osd_s);

//-- END defaults -------------------------------------------------------

    wire CLK44100x256;
    assign SDRAM_CKE = 1'b1;
    wire [3:0] IO;

    wire [7:0] leds_s; 
    assign LED = leds_s[1]; 

    reg [5:0] vga_r_s;
    reg [5:0] vga_g_s;
    reg [5:0] vga_b_s;

    assign VGA_R = vga_r_s[5:1];
    assign VGA_G = vga_g_s[5:1]; 
    assign VGA_B = vga_b_s[5:1];
    
    wire SDR_CLK;
    wire clk_25;
    dd_buf sdrclk_buf
    (
        .datain_h(1'b1),
        .datain_l(1'b0),
        .outclock(SDR_CLK),
        .dataout(SDRAM_CLK)
    );
    
    system sys_inst
    (
        .CLK_50MHZ(clock_50_i),
        
        .VGA_R(vga_r_s),
        .VGA_G(vga_g_s),
        .VGA_B(vga_b_s),
        .VGA_HSYNC(VGA_HS),
        .VGA_VSYNC(VGA_VS),
        .frame_on(),
        
        .clk_25(clk_25),
        
        .sdr_CLK_out(SDR_CLK),
        .sdr_n_CS_WE_RAS_CAS({SDRAM_nCS, SDRAM_nWE, SDRAM_nRAS, SDRAM_nCAS}),
        .sdr_BA(SDRAM_BA),
        .sdr_ADDR(SDRAM_A),
        .sdr_DATA(SDRAM_DQ),
        .sdr_DQM({SDRAM_DQMH, SDRAM_DQML}),
        
        .LED(leds_s), 
        
        .BTN_RESET(~btn_n_i[4]), // boton de RESET en placa principal K3
        .BTN_NMI(1'b0), // ~BTN_WEST), // anulo NMI, no me hace falta
        
        .RS232_DCE_RXD(stm_tx_i),
        .RS232_DCE_TXD(stm_rx_o),
        .RS232_EXT_RXD(),
        .RS232_EXT_TXD(),
        
        .SD_n_CS(sd_cs_n_o),
        .SD_DI(sd_mosi_o),
        .SD_CK(sd_sclk_o),
        .SD_DO(sd_miso_i),
        
        .AUD_L(AUDIO_L),
        .AUD_R(AUDIO_R),
        
        .PS2_CLK1(ps2_clk_io),
        .PS2_CLK2(ps2_mouse_clk_io),
        .PS2_DATA1(ps2_data_io),
        .PS2_DATA2(ps2_mouse_data_io),
        
        .RS232_HOST_RXD(),
        .RS232_HOST_TXD(),
        .RS232_HOST_RST(),
        
        .GPIO(), //{IO, GPIO}), // jepalza, anulados
        
        .I2C_SCL(),//I2C_SCLK), // JEPALZA, ANULADOS
        .I2C_SDA() //I2C_SDAT)
    );
    
endmodule
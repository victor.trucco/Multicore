--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-- altera message_off 10306

library ieee;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.ALL;
use IEEE.numeric_std.all;

entity gen_rom is

	generic 
	(
		INIT_FILE  : string  := "";
		ADDR_WIDTH : natural := 14;
		DATA_WIDTH : natural := 8
	);

	port 
	(
		wrclock   : in  std_logic;
		wraddress : in  std_logic_vector((ADDR_WIDTH - 1) downto 0) := (others => '0');
		data	    : in  std_logic_vector((DATA_WIDTH - 1) downto 0) := (others => '0');
		wren      : in  std_logic := '0';

		rdclock   : in  std_logic;
		rdaddress : in  std_logic_vector((ADDR_WIDTH - 1) downto 0);
		q         : out std_logic_vector((DATA_WIDTH - 1) downto 0);
		cs        : in  std_logic := '1'
	);

end gen_rom;

architecture rtl of gen_rom is

	subtype word_t is std_logic_vector((DATA_WIDTH-1) downto 0);
	type memory_t is array(2**ADDR_WIDTH-1 downto 0) of word_t;

	shared variable ram : memory_t;
	
	attribute ram_init_file : string;
	attribute ram_init_file of ram : variable is INIT_FILE;
	
	signal q0 : std_logic_vector((DATA_WIDTH - 1) downto 0);
	

begin

	q<= q0 when cs = '1' else (others => '1');

	-- WR Port
	process(wrclock) begin
		if(rising_edge(wrclock)) then 
			if(wren = '1') then
				ram(to_integer(unsigned(wraddress))) := data;
			end if;
		end if;
	end process;

	-- RD Port
	process(rdclock) begin
		if(rising_edge(rdclock)) then 
			 q0 <= ram(to_integer(unsigned(rdaddress)));
		end if;
	end process;

end rtl;

/*
    
Z88 FPGA core by
Frédéric Requin
Thierry Peycru
(C) copyright 2014-2021

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

/*
  
   Multicore 2 / Multicore 2+
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///============================================================================
//
//  Multicore 2 Top by Victor Trucco
//
//============================================================================

`default_nettype none

module z88_mc2_top
(
 // Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAMs (AS7C34096)
    output reg [18:0]sram_addr_o  = 18'b0000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output reg sram_we_n_o     = 1'b1,
    output reg sram_oe_n_o     = 1'b1,
        
    // SDRAM    (H57V256)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io          = 1'bz,
    inout wire  ps2_data_io         = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o           = 1'b1,
    output wire sd_sclk_o           = 1'b0,
    output wire sd_mosi_o           = 1'b0,
    input wire  sd_miso_i,

    // Joysticks
    input wire  joy1_up_i,
    input wire  joy1_down_i,
    input wire  joy1_left_i,
    input wire  joy1_right_i,
    input wire  joy1_p6_i,
    input wire  joy1_p9_i,
    input wire  joy2_up_i,
    input wire  joy2_down_i,
    input wire  joy2_left_i,
    input wire  joy2_right_i,
    input wire  joy2_p6_i,
    input wire  joy2_p9_i,
    output wire joyX_p7_o           = 1'b1,

    // Audio
    output        AUDIO_L,
    output        AUDIO_R,
    input wire  ear_i,
    output wire mic_o                   = 1'b0,

        // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

        // HDMI
    output wire [7:0]tmds_o         = 8'b00000000,

        //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
        
    inout wire  stm_b8_io, 
    inout wire  stm_b9_io,

    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2
);


localparam CONF_STR = {
    "P,Z88.dat;", 
//  "P,oz47b.rom;",
//  "P,ozs1.epr;",
    "S,DAT,Alternative ROM...;",
    "T6,Reset;", 
    "V,v1.30." 
};

assign  stm_rst_o           = 1'bz;

wire ioctl_downl;
wire [7:0]ioctl_index;
wire [31:0]status;
wire ioctl_wr;
wire [24:0]ioctl_addr;
wire [7:0]ioctl_dout;

wire clk_sys, clk_snd, clk_vga, clk_27,clk_dvi;
wire pll_locked;
pll pll(
    .inclk0(clock_50_i),
    .c0(clk_sys),
    .c1(clk_vga),
    //.c2(SDRAM_CLK),
    .c3(clk_dvi),
    .locked(pll_locked)
    );

    


    // ========================================================================
    // Reset de-bouncing
    // ========================================================================
    
    reg  [6:0] r_rst_n;
    reg  [6:0] r_flap_n;
    reg        r_rst;
    reg        r_flap;
    wire       w_clk_ena;
    wire       w_bus_ph;

    always@(posedge clk_sys) begin : RESET_FLAP_CC
        r_rst_n  <= { r_rst_n[5:0],  btn_n_i[4]  };
        r_flap_n <= { r_flap_n[5:0], btn_n_i[1] };
        r_rst    <= (r_rst_n[6:2]  == 5'b00000 | ioctl_downl) ? 1'b1 : 1'b0;
        r_flap   <= (r_flap_n[6:2] == 5'b00000) ? 1'b1 : 1'b0;
    end

    // ========================================================================
    // Z88 instance
    // ========================================================================
    
    wire  [7:0] w_kbd_val;
    
    wire        w_ram_ce_n;
    wire        w_ram_oe_n;
    wire        w_ram_we_n;
    wire  [1:0] w_ram_be_n;
    wire [18:0] w_ram_addr;
    wire [15:0] w_ram_wdata;
    wire [15:0] w_ram_rdata;
    
    wire        w_rom_ce_n;
    wire        w_rom_oe_n;
    wire  [1:0] w_rom_be_n;
    wire [18:0] w_rom_addr;
    wire [15:0] w_rom_rdata;
    
    wire        w_vga_fr_tgl;
    wire        w_vga_hs;
    wire        w_vga_vs;
    wire        w_vga_de;
    wire [11:0] w_vga_rgb;
    
    z88_top
    #(
        .RAM_DATA_WIDTH (8),
        .ROM_DATA_WIDTH (8),

        .RAM_ADDR_MASK  (32'h00007FFF) // 32 KB
 //       .RAM_ADDR_MASK  (32'h0007FFFF) // 512 KB

    )
    the_z88
    (
        .rst        (r_rst),
        .clk        (clk_sys),
        .clk_ena    (w_clk_ena),
        .bus_ph     (w_bus_ph),
        .flap_sw    (r_flap),
        
        .kb_matrix  (r_kb_matrix_p2),
        .kbd_val    (w_kbd_val),
        
        .ram_ce_n   (w_ram_ce_n),
        .ram_oe_n   (w_ram_oe_n),
        .ram_we_n   (w_ram_we_n),
        .ram_be_n   (w_ram_be_n),
        .ram_addr   (w_ram_addr),
        .ram_wdata  (w_ram_wdata),
        .ram_rdata  (w_ram_rdata),
        
        .rom_ce_n   (w_rom_ce_n),
        .rom_oe_n   (w_rom_oe_n),
        .rom_be_n   (w_rom_be_n),
        .rom_addr   (w_rom_addr),
        .rom_rdata  (w_rom_rdata),
        
        .vga_fr_tgl (w_vga_fr_tgl),
        .vga_hs     (w_vga_hs),
        .vga_vs     (w_vga_vs),
        .vga_de     (w_vga_de),
        .vga_rgb    (w_vga_rgb)
    );
    
    // 512 KB SRAM :
    // -------------
//    assign sram_oe_n_o         = w_ram_oe_n;
//    assign sram_we_n_o         = w_ram_we_n;
//    assign sram_addr_o         = w_ram_addr;

//    assign sram_data_io     = (!w_ram_we_n) ? w_ram_wdata[7:0] : 8'hZZ;
//    assign w_ram_rdata[7:0] = sram_data_io;

    //RAM and ROM shared
  //  assign sram_oe_n_o       = w_ram_oe_n & w_rom_oe_n;
  //  assign sram_we_n_o       = w_ram_we_n & (~ioctl_wr);
  //  assign sram_addr_o       = (ioctl_downl)? ioctl_addr[23:0] :  (~w_rom_oe_n) ? { 1'b0, w_rom_addr[17:0]} : {1'b1, w_ram_addr[17:0]};

//    assign sram_data_io      = (ioctl_downl)? ioctl_dout : (!w_ram_we_n) ? w_ram_wdata[7:0] : 8'hZZ;
//    assign w_ram_rdata[15:0] = { 8'h00, sram_data_io };
//    assign w_rom_rdata[15:0] = { 8'h00, sram_data_io };




    // 512 KB SRAM ROM:
    // -------------
    assign sram_data_io     = (ioctl_downl) ? ioctl_dout : 8'hZZ;
    assign w_rom_rdata[7:0] = sram_data_io;


   always@(posedge clk_sys) 
   begin
        sram_oe_n_o <= w_rom_oe_n;
        sram_addr_o <= (ioctl_downl)? ioctl_addr[18:0] : w_rom_addr[18:0] ;
        sram_we_n_o <= ~ioctl_wr;
   end



    // 32k BRAM 
    reg [7:0] ram_blk [0:32767];
    assign w_ram_rdata[7:0] = ram_blk[w_ram_addr[14:0]];

    always@(posedge clk_sys) 
    begin 

        if (!w_ram_we_n && !w_ram_ce_n) 
        begin
            ram_blk[w_ram_addr[14:0]] <= w_ram_wdata[7:0];
        end

    end

    // 512 KB Flash :
    // --------------
//   assign FL_RST_N          = 1'b1;
//   assign FL_CE_N           = w_rom_ce_n;
//   assign FL_OE_N           = w_rom_oe_n;
//   assign FL_WE_N           = 1'b1;
//   assign FL_ADDR[21:0]     = { 3'b0, w_rom_addr[18:0] };
//
//   assign FL_DQ[7:0]        = 8'hZZ;
//   assign w_rom_rdata[15:0] = { 8'h00, FL_DQ[7:0] };


    // VGA output :
    // ------------
    assign VGA_HS = ~w_vga_hs;
    assign VGA_VS = ~w_vga_vs;
    assign VGA_R  = {w_vga_rgb[ 3:0], 1'b0};
    assign VGA_G  = {w_vga_rgb[ 7:4], 1'b0};
    assign VGA_B  = {w_vga_rgb[11:8], 1'b0};

    // ========================================================================
    // PS2 controller
    // ========================================================================

    wire        w_kb_vld_p0;
    wire  [7:0] w_kb_data_p0;
    
    ps2_keyboard the_keyboard
    (
        .rst       (r_rst),
        .clk       (clk_sys),
        .cdac_r    (w_clk_ena & ~w_bus_ph),
        .cdac_f    (w_clk_ena &  w_bus_ph),
        .caps_led  (1'b0),
        .num_led   (1'b0), 
        .disk_led  (1'b0),
        .ps2_kclk  (ps2_clk_io),
        .ps2_kdat  (ps2_data_io),
        .kb_vld    (w_kb_vld_p0),
        .kb_data   (w_kb_data_p0)
    );
    
    reg       r_kb_ext_p1;
    reg       r_kb_brk_p1;
    reg [8:0] r_kb_data_p1;
    reg       r_kb_vld_p1;
    
    always@(posedge r_rst or posedge clk_sys) begin : KB_PRE_DECODE_P1
    
        if (r_rst) begin
            r_kb_ext_p1  <= 1'b0;
            r_kb_brk_p1  <= 1'b0;
            r_kb_data_p1 <= { 1'b0, 8'h00 };
            r_kb_vld_p1  <= 1'b0;
        end
        else begin
            if (w_clk_ena & w_bus_ph & w_kb_vld_p0) begin
                case (w_kb_data_p0[7:4])
                    4'hE : // Extended key
                    begin
                        r_kb_ext_p1 <= 1'b1;
                        r_kb_vld_p1 <= 1'b0;
                    end
                    4'hF : // Key upstroke
                    begin
                        r_kb_brk_p1 <= 1'b1;
                        r_kb_vld_p1 <= 1'b0;
                    end
                    default : // Normal keycode
                    begin
                        // Store key code
                        r_kb_data_p1 <= { r_kb_brk_p1, r_kb_ext_p1 | w_kb_data_p0[7], w_kb_data_p0[6:0] };
                        r_kb_vld_p1  <= 1'b1;
                        // Clear special flags
                        r_kb_brk_p1  <= 1'b0;
                        r_kb_ext_p1  <= 1'b0;
                    end
                endcase
            end
            else begin
                r_kb_vld_p1 <= 1'b0;
            end
        end
    end
    
    reg  [63:0] r_kb_matrix_p2;
    
    always@(posedge r_rst or posedge clk_sys) begin : KB_MATRIX_P2
       
    
        if (r_rst) begin
            
            r_kb_matrix_p2 <= 64'b0;
        end
        else begin
            
            if (r_kb_vld_p1) begin
                case (r_kb_data_p1[7:0])
                    //  A8 column
                    8'h3E: r_kb_matrix_p2[ 0] <= ~r_kb_data_p1[8];  // 8
                    8'h3D: r_kb_matrix_p2[ 1] <= ~r_kb_data_p1[8];  // 7
                    8'h31: r_kb_matrix_p2[ 2] <= ~r_kb_data_p1[8];  // N
                    8'h33: r_kb_matrix_p2[ 3] <= ~r_kb_data_p1[8];  // H
                    8'h35: r_kb_matrix_p2[ 4] <= ~r_kb_data_p1[8];  // Y
                    8'h36: r_kb_matrix_p2[ 5] <= ~r_kb_data_p1[8];  // 6
                    8'h5A: r_kb_matrix_p2[ 6] <= ~r_kb_data_p1[8];  // Enter
                    8'h66: r_kb_matrix_p2[ 7] <= ~r_kb_data_p1[8];  // Del
                    //  A9 column
                    8'h43: r_kb_matrix_p2[ 8] <= ~r_kb_data_p1[8];  // I
                    8'h3C: r_kb_matrix_p2[ 9] <= ~r_kb_data_p1[8];  // U
                    8'h32: r_kb_matrix_p2[10] <= ~r_kb_data_p1[8];  // B
                    8'h34: r_kb_matrix_p2[11] <= ~r_kb_data_p1[8];  // G
                    8'h2C: r_kb_matrix_p2[12] <= ~r_kb_data_p1[8];  // T
                    8'h2E: r_kb_matrix_p2[13] <= ~r_kb_data_p1[8];  // 5
                    8'hF5: r_kb_matrix_p2[14] <= ~r_kb_data_p1[8];  // Up
                    8'h5D: r_kb_matrix_p2[15] <= ~r_kb_data_p1[8];  // \
                    //  A10 column
                    8'h44: r_kb_matrix_p2[16] <= ~r_kb_data_p1[8];  // O
                    8'h3B: r_kb_matrix_p2[17] <= ~r_kb_data_p1[8];  // J
                    8'h2A: r_kb_matrix_p2[18] <= ~r_kb_data_p1[8];  // V
                    8'h2B: r_kb_matrix_p2[19] <= ~r_kb_data_p1[8];  // F
                    8'h2D: r_kb_matrix_p2[20] <= ~r_kb_data_p1[8];  // R
                    8'h25: r_kb_matrix_p2[21] <= ~r_kb_data_p1[8];  // 4
                    8'hF2: r_kb_matrix_p2[22] <= ~r_kb_data_p1[8];  // Down
                    8'h55: r_kb_matrix_p2[23] <= ~r_kb_data_p1[8];  // =
                    //  A11 column
                    8'h46: r_kb_matrix_p2[24] <= ~r_kb_data_p1[8];  // 9
                    8'h42: r_kb_matrix_p2[25] <= ~r_kb_data_p1[8];  // K
                    8'h21: r_kb_matrix_p2[26] <= ~r_kb_data_p1[8];  // C
                    8'h23: r_kb_matrix_p2[27] <= ~r_kb_data_p1[8];  // D
                    8'h24: r_kb_matrix_p2[28] <= ~r_kb_data_p1[8];  // E
                    8'h26: r_kb_matrix_p2[29] <= ~r_kb_data_p1[8];  // 3
                    8'hF4: r_kb_matrix_p2[30] <= ~r_kb_data_p1[8];  // Right
                    8'h4E: r_kb_matrix_p2[31] <= ~r_kb_data_p1[8];  // -
                    //  A12 column
                    8'h4D: r_kb_matrix_p2[32] <= ~r_kb_data_p1[8];  // P
                    8'h3A: r_kb_matrix_p2[33] <= ~r_kb_data_p1[8];  // M
                    8'h22: r_kb_matrix_p2[34] <= ~r_kb_data_p1[8];  // X
                    8'h1B: r_kb_matrix_p2[35] <= ~r_kb_data_p1[8];  // S
                    8'h1D: r_kb_matrix_p2[36] <= ~r_kb_data_p1[8];  // W
                    8'h1E: r_kb_matrix_p2[37] <= ~r_kb_data_p1[8];  // 2
                    8'hEB: r_kb_matrix_p2[38] <= ~r_kb_data_p1[8];  // Left
                    8'h5B: r_kb_matrix_p2[39] <= ~r_kb_data_p1[8];  // ]
                    //  A13 column
                    8'h45: r_kb_matrix_p2[40] <= ~r_kb_data_p1[8];  // 0
                    8'h4B: r_kb_matrix_p2[41] <= ~r_kb_data_p1[8];  // L
                    8'h1A: r_kb_matrix_p2[42] <= ~r_kb_data_p1[8];  // Z
                    8'h1C: r_kb_matrix_p2[43] <= ~r_kb_data_p1[8];  // A
                    8'h15: r_kb_matrix_p2[44] <= ~r_kb_data_p1[8];  // Q
                    8'h16: r_kb_matrix_p2[45] <= ~r_kb_data_p1[8];  // 1
                    8'h29: r_kb_matrix_p2[46] <= ~r_kb_data_p1[8];  // Space
                    8'h54: r_kb_matrix_p2[47] <= ~r_kb_data_p1[8];  // [
                    //  A14 column
                    8'h52: r_kb_matrix_p2[48] <= ~r_kb_data_p1[8];  // "
                    8'h4C: r_kb_matrix_p2[49] <= ~r_kb_data_p1[8];  // ;
                    8'h41: r_kb_matrix_p2[50] <= ~r_kb_data_p1[8];  // ,
                    8'h04: r_kb_matrix_p2[51] <= ~r_kb_data_p1[8];  // Menu (F3)
                    8'h14: r_kb_matrix_p2[52] <= ~r_kb_data_p1[8];  // <> (Ctrl)
                    8'h94: r_kb_matrix_p2[52] <= ~r_kb_data_p1[8];  // <> (Ctrl)
                    8'h0D: r_kb_matrix_p2[53] <= ~r_kb_data_p1[8];  // Tab
                    8'h12: r_kb_matrix_p2[54] <= ~r_kb_data_p1[8];  // LShift
                    8'h05: r_kb_matrix_p2[55] <= ~r_kb_data_p1[8];  // Help (F1)
                    //  A15 column
                    8'h0E: r_kb_matrix_p2[56] <= ~r_kb_data_p1[8];  // £
                    8'h4A: r_kb_matrix_p2[57] <= ~r_kb_data_p1[8];  // /
                    8'h49: r_kb_matrix_p2[58] <= ~r_kb_data_p1[8];  // .
                    8'h58: r_kb_matrix_p2[59] <= ~r_kb_data_p1[8];  // Caps
                    8'h06: r_kb_matrix_p2[60] <= ~r_kb_data_p1[8];  // Index (F2)
                    8'h76: r_kb_matrix_p2[61] <= ~r_kb_data_p1[8];  // Esc
                    8'h11: r_kb_matrix_p2[62] <= ~r_kb_data_p1[8];  // [] (Alt)
                    8'h91: r_kb_matrix_p2[62] <= ~r_kb_data_p1[8];  // [] (Alt)
                    8'h59: r_kb_matrix_p2[63] <= ~r_kb_data_p1[8];  // RShift
                    default: ;                    
                endcase
            end
          
        end
    end
   
    //----ROM--------------------------------------------

    data_io #(.STRLEN(($size(CONF_STR)>>3))) data_io(
    .clk_sys       ( clk_sys      ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),
    .SPI_DO        ( SPI_DO       ),
    
    .data_in            ( osd_s ),
    .conf_str       ( CONF_STR ),
    .status         ( status ),
    
    .ioctl_download( ioctl_downl  ),
    .ioctl_index   ( ioctl_index  ),
    .ioctl_wr      ( ioctl_wr     ),
    .ioctl_addr    ( ioctl_addr   ),
    .ioctl_dout    ( ioctl_dout   )
    );
    
    //--------- ROM DATA PUMP ----------------------------------------------------
    
        reg [15:0] power_on_s   = 16'b1111111111111111;
        reg [7:0] osd_s = 8'b11111111;
        
        wire hard_reset = ~pll_locked;
        
        //--start the microcontroller OSD menu after the power on
        always @(posedge clk_sys) 
        begin
        
                if (hard_reset == 1)
                    power_on_s = 16'b1111111111111111;
                else if (power_on_s != 0)
                begin
                    power_on_s = power_on_s - 1;
                    osd_s = 8'b00111111;
                end 
                    
                
                if (ioctl_downl == 1 && osd_s == 8'b00111111)
                    osd_s = 8'b11111111;
            
        end 
/*
    //-----------------------
    
    assign SDRAM_CLK = clk_sys;
    assign SDRAM_CKE         = 1'b1;

    sdram sdram (
    // interface to the chip
   .sd_data        ( SDRAM_DQ                 ),
   .sd_addr        ( SDRAM_A                  ),
   .sd_dqm         ( {SDRAM_DQMH, SDRAM_DQML} ),
   .sd_cs          ( SDRAM_nCS                ),
   .sd_ba          ( SDRAM_BA                 ),
   .sd_we          ( SDRAM_nWE                ),
   .sd_ras         ( SDRAM_nRAS               ),
   .sd_cas         ( SDRAM_nCAS               ),

   // system interface
   .clk_64         ( clk_sys                  ),
   .clk_8          ( w_clk_ena                ),
   .init           ( !pll_locked              ),

   // cpu/chipset interface
  .din            ( {ioctl_dout, ioctl_dout}  ),
  .addr           ( (ioctl_downl)? ioctl_addr[23:0] :   { 6'b000000, w_rom_addr[18:0] }              ),
  .ds             ( 2'b11                     ),
  .we             ( ioctl_wr                  ),
  .oe             ( w_rom_oe_n                ), INVERTIDO?
  .dout           ( w_rom_rdata[15:0]         )
    );
    */
/*
    assign SDRAM_CLK = clk_sys;
    assign SDRAM_CKE         = 1'b1;

    sdram sdram (
    // interface to the chip
    .sd_data        ( SDRAM_DQ                 ),
    .sd_addr        ( SDRAM_A                  ),
    .sd_dqm         ( {SDRAM_DQMH, SDRAM_DQML} ),
    .sd_cs          ( SDRAM_nCS                ),
    .sd_ba          ( SDRAM_BA                 ),
    .sd_we          ( SDRAM_nWE                ),
    .sd_ras         ( SDRAM_nRAS               ),
    .sd_cas         ( SDRAM_nCAS               ),

    // system interface
    .clk_64         ( clk_sys                  ),
    .clk_8          ( w_clk_ena                ),
    .init           ( ~pll_locked              ),

    // cpu/chipset interface
    .din            ( w_ram_wdata              ),
    .addr           ( { 5'b00000, w_ram_addr } ),
    .ds             ( 2'b11                    ), //only the lower bits
    .we             ( ~(w_ram_we_n|w_ram_ce_n) ),
    .oe             ( ~w_ram_oe_n              ),
    .dout           ( w_ram_rdata              )
    );
*/

 //---- HDMI -----------------------------------------

    wire    [15:0] sound_hdmi_s;
    
    wire [9:0] tdms_r_s;
    wire [9:0] tdms_g_s;
    wire [9:0] tdms_b_s;
    wire [3:0] hdmi_p_s;
    wire [3:0] hdmi_n_s;

    hdmi 
    # (
        .FREQ ( 25833000 ),   //-- pixel clock frequency 
        .FS   ( 48000 ),      //-- audio sample rate - should be 32000, 41000 or 48000 = 48KHz
        .CTS  ( 25833 ),      //-- CTS = Freq(pixclk) * N / (128 * Fs)
        .N    ( 6144 )        //-- N = 128 * Fs /1000,  128 * Fs /1500 <= N <= 128 * Fs /300 (Check HDMI spec 7.2 for details)
    ) inst_dvid
    (
        .I_CLK_PIXEL    ( clk_vga ),
        .I_R            ( { w_vga_rgb[3:1],   w_vga_rgb[3:1],  1'b0 } ),
        .I_G            ( { w_vga_rgb[7:5],   w_vga_rgb[7:5],  1'b0 } ),
        .I_B            ( { w_vga_rgb[11:9],  w_vga_rgb[11:9], 1'b0 } ),
        .I_BLANK        ( ~w_vga_de ), //w_vga_blank ),
        .I_HSYNC        ( w_vga_hs ),
        .I_VSYNC        ( w_vga_vs ),
        
        //-- PCM audio
        .I_AUDIO_ENABLE ( 1'b1 ),
        .I_AUDIO_PCM_L  ( sound_hdmi_s ),
        .I_AUDIO_PCM_R  ( sound_hdmi_s ),
        
        //-- TMDS parallel pixel synchronous outputs (serialize LSB first)
        .O_RED          ( tdms_r_s ),
        .O_GREEN        ( tdms_g_s ),
        .O_BLUE         ( tdms_b_s )
    );
 
    hdmi_out_altera hdmio
    (
      .clock_pixel_i  ( clk_vga ),
      .clock_tdms_i   ( clk_dvi   ),
      .red_i          ( tdms_r_s ),
      .green_i        ( tdms_g_s ),
      .blue_i         ( tdms_b_s ),
      .tmds_out_p     ( hdmi_p_s ),
      .tmds_out_n     ( hdmi_n_s )
    );

    assign tmds_o[7] = hdmi_p_s[2]; //-- 2+     
    assign tmds_o[6] = hdmi_n_s[2]; //-- 2-     
    assign tmds_o[5] = hdmi_p_s[1]; //-- 1+         
    assign tmds_o[4] = hdmi_n_s[1]; //-- 1-     
    assign tmds_o[3] = hdmi_p_s[0]; //-- 0+     
    assign tmds_o[2] = hdmi_n_s[0]; //-- 0- 
    assign tmds_o[1] = hdmi_p_s[3]; //-- CLK+   
    assign tmds_o[0] = hdmi_n_s[3]; //-- CLK-   

    assign sound_hdmi_s =  {16'hFFFF}; 
    
endmodule

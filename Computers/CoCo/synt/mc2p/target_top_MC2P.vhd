--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.pace_pkg.all;
use work.sdram_pkg.all;
use work.video_controller_pkg.all;
use work.project_pkg.all;
use work.platform_pkg.all;
use work.target_pkg.all;

use IEEE.std_logic_unsigned.ALL;

entity target_top is
  port (
     -- Clocks
        clock_50_i          : in    std_logic;

		-- VGA
		vga_r_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';
		

		
        -- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		--STM32
		stm_tx_i				: in  std_logic		:= 'Z'; -- stm TX pin, so, is IN on the slave
		stm_rx_o				: out std_logic		:= 'Z'; -- stm RX pin, so, is OUT on the slave
		stm_rst_o			: out std_logic		:= '0'; -- '0' to hold the microcontroller reset line, to free the SD card
		
		-- SDRAM	(H57V256)
		sdram_ad_o			: out std_logic_vector(12 downto 0);
		sdram_da_io			: inout std_logic_vector(15 downto 0);

		sdram_ba_o			: out std_logic_vector(1 downto 0);
		sdram_dqm_o			: out std_logic_vector(1 downto 0);

		sdram_ras_o			: out std_logic;
		sdram_cas_o			: out std_logic;
		sdram_cke_o			: out std_logic;
		sdram_clk_o			: out std_logic;
		sdram_cs_o			: out std_logic;
		sdram_we_o			: out std_logic;
		

		
		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);
		
		-- SRAMs (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_oe_n_o			: out   std_logic								:= '1';

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: inout   std_logic								:= '1';
		sd_sclk_o			: inout   std_logic								:= '0';
		sd_mosi_o			: inout   std_logic								:= '0';
		sd_miso_i			: inout   std_logic;

        -- Joysticks
		joy_clock_o          : out   std_logic                               := '1';       
		joy_load_o           : out   std_logic                               := '1';       
		joy_data_i           : in   std_logic;       
		joy_p7_o           : out   std_logic                               := '1';       

		GPIO 				: inout std_logic_vector(31 downto 0)


  );

end target_top;

architecture SYN of target_top is

  constant DE1_HAS_BURCHED_PERIPHERAL   : boolean := false;
  constant DE1_TEST_BURCHED_LEDS        : boolean := false;
  constant DE1_TEST_BURCHED_DIPS        : boolean := false;
  constant DE1_TEST_BURCHED_7SEG        : boolean := false;

  signal init       	  : std_logic := '1';
  
  signal clkrst_i       : from_CLKRST_t;
  signal buttons_i      : from_BUTTONS_t;
  signal switches_i     : from_SWITCHES_t;
  signal leds_o         : to_LEDS_t;
  signal inputs_i       : from_INPUTS_t;
  signal flash_i        : from_FLASH_t;
  signal flash_o        : to_FLASH_t;
signal sram_i			    : from_SRAM_t;
signal sram_o			    : to_SRAM_t;	
signal sdram_i        : from_SDRAM_t;
signal sdram_o        : to_SDRAM_t;
signal video_i        : from_VIDEO_t;
  signal video_o        : to_VIDEO_t;
  signal audio_i        : from_AUDIO_t;
  signal audio_o        : to_AUDIO_t;
  signal ser_i          : from_SERIAL_t;
  signal ser_o          : to_SERIAL_t;
  signal project_i      : from_PROJECT_IO_t;
  signal project_o      : to_PROJECT_IO_t;
  signal platform_i     : from_PLATFORM_IO_t;
  signal platform_o     : to_PLATFORM_IO_t;
  signal target_i       : from_TARGET_IO_t;
  signal target_o       : to_TARGET_IO_t;
	
	signal btn_artf_s	: std_logic;
	signal atifact_en_s : std_logic_vector(2 downto 0) := "000";
	
	signal btn_joy_s	: std_logic;
	signal joy_invert_s : std_logic := '0';
	
	signal clk_28M6: std_logic := '0';
	signal clock_dvi_s : std_logic;
	
  signal joy1_up_i : std_logic;
  signal joy1_down_i : std_logic;
  signal joy1_left_i : std_logic;
  signal joy1_right_i : std_logic;
  signal joy1_p6_i : std_logic;
  signal joy1_p9_i : std_logic;

  signal joy2_up_i : std_logic;
  signal joy2_down_i : std_logic;
  signal joy2_left_i : std_logic;
  signal joy2_right_i : std_logic;
  signal joy2_p6_i : std_logic;
  signal joy2_p9_i : std_logic;
component joystick_serial is 
	port (
		  clk_i : in std_logic;
		  joy_data_i : in std_logic;
		 joy_clk_o : out std_logic;
		 joy_load_o : out std_logic;

		 joy1_up_o : out std_logic;
		 joy1_down_o : out std_logic;
		 joy1_left_o : out std_logic;
		 joy1_right_o : out std_logic;
		 joy1_fire1_o : out std_logic;
		 joy1_fire2_o : out std_logic;

		 joy2_up_o : out std_logic;
		 joy2_down_o : out std_logic;
		 joy2_left_o : out std_logic;
		 joy2_right_o : out std_logic;
		 joy2_fire1_o : out std_logic;
		 joy2_fire2_o : out std_logic
	);
  end component; 	
	
begin

  BLK_CLOCKING : block
  begin
  
    clkrst_i.clk_ref <= clock_50_i;
    
     pll : entity work.pll1
		port map
      (
        inclk0  => clock_50_i,
		  c0      => clkrst_i.clk(0), -- 57.2
        c1      => clkrst_i.clk(1), -- 57.2
        c2      => clkrst_i.clk(2), -- 18.432
        c3      => clkrst_i.clk(3),  -- 27
		  c4 		 => clock_dvi_s
      );
  

  end block BLK_CLOCKING;


  joystick_serial_inst : joystick_serial 
  port map (
    clk_i	=>	clkrst_i.clk(2),
	joy_data_i	=>	joy_data_i,
    joy_clk_o       	=>	 joy_clock_o,
    joy_load_o      	=>	 joy_load_o,

    joy1_up_o       	=>	 joy1_up_i,
    joy1_down_o     	=>	 joy1_down_i,
    joy1_left_o     	=>	 joy1_left_i,
    joy1_right_o    	=>	 joy1_right_i,
    joy1_fire1_o    	=>	 joy1_p6_i,
    joy1_fire2_o    	=>	 joy1_p9_i,

    joy2_up_o       	=>	 joy2_up_i,
    joy2_down_o     	=>	 joy2_down_i,
    joy2_left_o     	=>	 joy2_left_i,
    joy2_right_o    	=>	 joy2_right_i,
    joy2_fire1_o    	=>	 joy2_p6_i,
    joy2_fire2_o    	=>	 joy2_p9_i
    );


	
  -- FPGA STARTUP
	-- should extend power-on reset if registers init to '0'
	process (clock_50_i)
		variable count : unsigned(11 downto 0) := (others => '0');
	begin
		if rising_edge(clock_50_i) then
			if count = X"FFF" then
				init <= '0';
			else
				count := count + 1;
				init <= '1';
			end if;
		end if;
	end process;
	
	
	
	process (clkrst_i.clk(0))
	begin
		if rising_edge(clkrst_i.clk(0)) then
			clk_28M6 <= not clk_28M6;
		end if;
	end process;
	
	

  clkrst_i.arst <= init or not (btn_n_i(3) or btn_n_i(4));
	clkrst_i.arst_n <= not clkrst_i.arst;

  GEN_RESETS : for i in 0 to 3 generate

    process (clkrst_i.clk(i), clkrst_i.arst)
      variable rst_r : std_logic_vector(2 downto 0) := (others => '0');
    begin
      if clkrst_i.arst = '1' then
        rst_r := (others => '1');
      elsif rising_edge(clkrst_i.clk(i)) then
        rst_r := rst_r(rst_r'left-1 downto 0) & '0';
      end if;
      clkrst_i.rst(i) <= rst_r(rst_r'left);
    end process;

  end generate GEN_RESETS;
	
  -- buttons - active low
  buttons_i <= '0' & clkrst_i.arst & clkrst_i.arst & clkrst_i.arst;--"0000"; --std_logic_vector(resize(unsigned(not key), buttons_i'length));
  -- switches - up = high
  switches_i <= "00000000"& joy_invert_s & atifact_en_s & "000000"; --std_logic_vector(resize(unsigned(sw), switches_i'length));
  -- leds
 -- ledr <= leds_o(ledr'range);
  
	-- inputs

  GEN_NO_BURCHED_PERIPHERAL : if not DE1_HAS_BURCHED_PERIPHERAL generate
    -- ps/2
    inputs_i.ps2_kclk <= ps2_clk_io;
    inputs_i.ps2_kdat <= ps2_data_io;
    inputs_i.ps2_mclk <= '0';
    inputs_i.ps2_mdat <= '0';
    -- serial
   -- uart_txd <= ser_o.txd;
   -- ser_i.rxd <= uart_rxd;
  end generate GEN_NO_BURCHED_PERIPHERAL;
  
 
		inputs_i.jamma_n.coin(1) <= '1';
		inputs_i.jamma_n.p(1).start <= '1';
		inputs_i.jamma_n.p(1).up <= joy1_up_i;
		inputs_i.jamma_n.p(1).down <= joy1_down_i;
		inputs_i.jamma_n.p(1).left <= joy1_left_i;
		inputs_i.jamma_n.p(1).right <= joy1_right_i; 
		inputs_i.jamma_n.p(1).button <= (others =>joy1_p6_i);

	inputs_i.jamma_n.coin_cnt <= (others => '1');
	inputs_i.jamma_n.coin(2) <= '1';
	inputs_i.jamma_n.p(2).start <= '1';
   inputs_i.jamma_n.p(2).up <= joy2_up_i;
   inputs_i.jamma_n.p(2).down <= joy2_down_i;
	inputs_i.jamma_n.p(2).left <= joy2_left_i;
	inputs_i.jamma_n.p(2).right <= joy2_right_i;
	inputs_i.jamma_n.p(2).button <= (others =>joy2_p6_i);
	inputs_i.jamma_n.service <= '1';
	inputs_i.jamma_n.tilt <= '1';
	inputs_i.jamma_n.test <= '1';
		
	
 

  
  -- static memory
  BLK_SRAM : block
  
		signal btn_reset_s : std_logic := '1';
		signal count_clear : std_logic_vector(18 downto 0) := (others=>'0');
	
  begin
  
				btnres: entity work.debounce
				generic map (
					counter_size_g	=> 16
				)
				port map (
					clk_i				=> clkrst_i.clk(2),
					button_i			=> btn_n_i(3) or btn_n_i(4),
					result_o			=> btn_reset_s
				);


	--		process (btn_reset_s)
	--		begin
	--			if falling_edge(btn_reset_s) then
	--				count_clear <= (others=>'0');
	--			end if;
	--		end process;
				

				
			process (clock_50_i, btn_artf_s)
			begin
		 	
		 		if btn_reset_s='0' then
		 		
		 			if rising_edge (clock_50_i)  then
		 			
		 				sram_addr_o <= count_clear;
						sram_oe_n_o <= '0';
		 				sram_we_n_o <= '0';
		 				count_clear <= count_clear + 1;
		 			
		 			end if;
		 			
		 		else
					
						sram_addr_o <= sram_o.a(sram_addr_o'range);
						sram_i.d <= std_logic_vector(resize(unsigned(sram_data_io), sram_i.d'length));
						
						if sram_o.cs = '1' and sram_o.we = '1' then
							sram_data_io <= sram_o.d(sram_data_io'range); 
						else 
							sram_data_io <= (others => 'Z');
						end if;
						
		--					sram_data_io <= sram_o.d(sram_data_io'range) when (sram_o.cs = '1' and sram_o.we = '1') else (others => 'Z');
						
						sram_oe_n_o <= not sram_o.oe;
						sram_we_n_o <= not sram_o.we;
						
					end if;
--
				end process;
	--	
		 end block BLK_SRAM;
		

  
  
  
  pace_inst : entity work.pace                                            
    port map
    (
    	-- clocks and resets
	  	clkrst_i					=> clkrst_i,

      -- misc inputs and outputs
      buttons_i         => buttons_i,
      switches_i        => switches_i,
      leds_o            => leds_o,
      
      -- controller inputs
      inputs_i          => inputs_i,

     	-- external ROM/RAM
     	flash_i           => flash_i,
      flash_o           => flash_o,
      sram_i        		=> sram_i,
      sram_o        		=> sram_o,
     	sdram_i           => sdram_i,
     	sdram_o           => sdram_o,
  
      -- VGA video
      video_i           => video_i,
      video_o           => video_o,
      
      -- sound
      audio_i           => audio_i,
      audio_o           => audio_o,

      -- SPI (flash)
      spi_i.din         => '0',
      spi_o             => open,
  
      -- serial
      ser_i             => ser_i,
      ser_o             => ser_o,
      
      -- custom i/o
      project_i         => project_i,
      project_o         => project_o,
      platform_i        => platform_i,
      platform_o        => platform_o,
      target_i          => target_i,
      target_o          => target_o
    );

  BLK_CUSTOM_IO : block
  


  begin
  
    custom_io_inst : entity work.custom_io
      port map
      (
        -- GPIO 0 connector
        gpio_0_i          => (others=>'0'),
        gpio_0_o          => open,
        gpio_0_oe         => open,
        gpio_0_is_custom  => open,
        
        -- GPIO 1 connector
        gpio_1_i          => (others=>'0'),
        gpio_1_o          => open,
        gpio_1_oe         => open,
        gpio_1_is_custom  => open,

        -- 7-segment display
        seg7              => open,

        -- SD card
        sd_dat            => sd_miso_i,
        sd_dat3           => sd_cs_n_o,
        sd_cmd            => sd_mosi_o,
        sd_clk            => sd_sclk_o,
        
        -- custom i/o
        project_i         => project_i,
        project_o         => project_o,
        platform_i        => platform_i,
        platform_o        => platform_o,
        target_i          => target_i,
        target_o          => target_o
      );

   

  end block BLK_CUSTOM_IO;
  
   BLK_VIDEO : block
	
		signal scanlines_en_s		: std_logic := '0';
		signal btn_scan_s				: std_logic;
		signal odd_line_s				: std_logic := '0';
		signal vga_r_s					: std_logic_vector( 2 downto 0);
		signal vga_g_s					: std_logic_vector( 2 downto 0);
		signal vga_b_s					: std_logic_vector( 2 downto 0);
		signal vga_r_out_s			: std_logic_vector( 2 downto 0);
		signal vga_g_out_s			: std_logic_vector( 2 downto 0);
		signal vga_b_out_s			: std_logic_vector( 2 downto 0);
		signal vga_hsync_n_s			: std_logic;
		signal vga_vsync_n_s			: std_logic;
		
  begin

	 video_i.clk <= clkrst_i.clk(1);	-- by convention
	 video_i.clk_ena <= '1';
    video_i.reset <= clkrst_i.rst(1);
    
    vga_r_s <= video_o.rgb.r(video_o.rgb.r'left downto video_o.rgb.r'left-2);
    vga_g_s <= video_o.rgb.g(video_o.rgb.g'left downto video_o.rgb.g'left-2);
    vga_b_s <= video_o.rgb.b(video_o.rgb.b'left downto video_o.rgb.b'left-2);
	 vga_hsync_n_s <= video_o.hsync;
    vga_vsync_n_s <= video_o.vsync; 
	 
	 




  	
		btnscl: entity work.debounce
		generic map (
			counter_size_g	=> 16
		)
		port map (
			clk_i				=> clkrst_i.clk(2),
			button_i			=> btn_n_i(1) or btn_n_i(2),
			result_o			=> btn_scan_s
		);
		
		process (btn_scan_s)
		begin
			if falling_edge(btn_scan_s) then
				scanlines_en_s <= not scanlines_en_s;
			end if;
		end process;
		
		vga_r_out_s <=  vga_r_s - 2 when vga_r_s > 1 and scanlines_en_s = '1' and odd_line_s = '1' else vga_r_s;
		vga_g_out_s <=  vga_g_s - 2 when vga_g_s > 1 and scanlines_en_s = '1' and odd_line_s = '1' else vga_g_s;
		vga_b_out_s <=  vga_b_s - 2 when vga_b_s > 1 and scanlines_en_s = '1' and odd_line_s = '1' else vga_b_s;

		
		process(vga_hsync_n_s,vga_vsync_n_s)
		begin
			if vga_vsync_n_s = '0' then
				odd_line_s <= '0';
			elsif rising_edge(vga_hsync_n_s) then
				odd_line_s <= not odd_line_s;
			end if;
		end process;
		

		
		vga_r_o <= vga_r_out_s & "00";
		vga_g_o <= vga_g_out_s & "00";
		vga_b_o <= vga_b_out_s & "00";
		vga_hsync_n_o <= vga_hsync_n_s;
		vga_vsync_n_o <= vga_vsync_n_s; 

				
	
	end block BLK_VIDEO;
	
	
		btnartf: entity work.debounce
		generic map (
			counter_size_g	=> 16
		)
		port map (
			clk_i				=> clkrst_i.clk(2),
			button_i			=> btn_n_i(2),
			result_o			=> btn_artf_s
		);
		
		process (btn_artf_s)
		begin
			if falling_edge(btn_artf_s) then
				if atifact_en_s = "000" then
					atifact_en_s <= "100";
				elsif atifact_en_s = "100" then
					atifact_en_s <= "101";
				elsif atifact_en_s = "101" then
					atifact_en_s <= "110";
				elsif atifact_en_s = "110" then
					atifact_en_s <= "111";
				else
					atifact_en_s <= "000";
				end if;
			end if;
		end process;

		btnjoy: entity work.debounce
		generic map (
			counter_size_g	=> 16
		)
		port map (
			clk_i				=> clkrst_i.clk(2),
			button_i			=> btn_n_i(1),
			result_o			=> btn_joy_s
		);
		
		process (btn_artf_s)
		begin
			if falling_edge(btn_joy_s) then
				joy_invert_s <= not joy_invert_s;
			end if;
		end process;
		
	 BLK_AUDIO : block
	
		signal audio_dac_r_s		: std_logic;
		signal audio_dac_l_s		: std_logic;
		
	begin
		audiooutL: entity work.dac
		generic map (
			msbi_g		=> 7
		)
		port map (
			clk_i		=> clkrst_i.clk(0),
			res_i		=> clkrst_i.rst(1),
			dac_i		=> audio_o.ldata(15 downto 8),
			dac_o		=> audio_dac_l_s
		);
		
		audiooutR: entity work.dac
		generic map (
			msbi_g		=> 7
		)
		port map (
			clk_i		=> clkrst_i.clk(0),
			res_i		=> clkrst_i.rst(1),
			dac_i		=> audio_o.rdata(15 downto 8),
			dac_o		=> audio_dac_r_s
		);
		
		dac_l_o	<= audio_dac_l_s;
		dac_r_o	<= audio_dac_r_s;
	
	end block BLK_AUDIO;
	
  
  
  
  
end SYN;



	
 

		
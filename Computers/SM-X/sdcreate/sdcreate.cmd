@echo off
rem --- 'sdcreate.cmd' v2.5 by KdL (2017.09.18)
rem --- Modified for use on MC2+ (2010.10.15)

if "%~1"=="" title SD-Card preparing tool

:begin
if not exist "OS\MSXDOS2.SYS" (
    echo.&echo 'MSXDOS2.SYS' is missing!
    set TIMEOUT=1&goto quit_0
)
if not exist "OS\COMMAND2.COM" (
    echo.&echo 'COMMAND2.COM' is missing!
    set TIMEOUT=1&goto quit_0
)

for /r %%f in (CORE\*.mcp) do (
  echo DIS_SD> core\%%~nf.ini
)

for /r %%f in (CORE\*.mc2) do (
  echo DIS_SD> core\%%~nf.ini
)

for /r %%f in (CORE\*.ua2) do (
  echo DIS_SD> core\%%~nf.ini
)

for /r %%f in (CORE\*.np1) do (
  echo DIS_SD> core\%%~nf.ini
)

call :title
wmic logicaldisk where drivetype=2 get deviceid, filesystem, volumename, size

rem --- Input parameters
:target
setlocal ENABLEDELAYEDEXPANSION
set TIMEOUT=1
set /p dest=Enter the target drive letter:
if "%dest%"=="" endlocal&goto begin
set dest=%dest:~0,1%:
call :uppercase dest

set label=MSXFAT16
set /p label=Enter a label name (optional):
set label=%label:~0,11%


:smx
set smx=x
set /p smx=Is it for OCM/SM-X/SMX-HB? [y/n]
call :uppercase smx
if not "%smx%"=="Y" (
  if not "%smx%"=="N" (
    goto smx
  )
)

:extras
set extras=Y
set /p extras=Include EXTRAS? [Y/n]
call :uppercase extras
if not "%extras%"=="Y" (
  if not "%extras%"=="N" (
    goto extras
  )
)

:network
set network=Y
set /p network=Include NETWORK? [Y/n]
call :uppercase network
if not "%network%"=="Y" (
  if not "%network%"=="N" (
    goto network
  )
)

rem --- Validate if a Core is needed
if "%smx%"=="N" (
  if not exist "CORE\*.ini" (
      echo.
      echo Core file 'core.mcp' 'core.mc2' 'core.ua2' 'core.np1' not found at '.\core' directory.
      echo Please add a Core file before continue.
      set TIMEOUT=4&goto quit_0
  )
)

rem --- Start of process
call :title
echo ### Ready to format the SD-Card "%label%" (%dest%)
echo.
echo WARNING: ALL EXISTING DATA OF THE TARGET DEVICE WILL BE DESTROYED^^!

:format
set format=x
set /p format=Do you want to continue formating? [y/n]
call :uppercase format
if not "%format%"=="Y" (
  if not "%format%"=="N" (
    goto format
  )
)

if "%format%"=="N" set TIMEOUT=1&goto quit_0

set clean=%cd%\cleanscript.tmp
set dps=%cd%\dpscript.tmp
set partsize=4095

rem --- Create a partition if SD-Card is higher than 4GB
call :title
echo # Formatting...
echo.
echo >%clean% select volume=%dest%
echo >>%clean% clean
diskpart /s %clean% >nul 2>&1

waitfor /T %TIMEOUT% pause >nul 2>nul
del %clean% >nul 2>nul

echo >%dps% select volume=%dest%
echo >>%dps% create partition primary size=%partsize%
echo >>%dps% format fs=fat label="%label%" quick
diskpart /s %dps% >nul 2>&1
md "%dest%\System Volume Information" >nul 2>nul
if exist "%dest%\System Volume Information" goto is_higher

rem --- Create a partition if SD-Card is lower than 4GB
echo >%dps% select volume=%dest%
echo >>%dps% create partition primary
echo >>%dps% format fs=fat label="%label%" quick
diskpart /s %dps% >nul 2>&1

rem --- Finalizing
:is_higher
del %dps% >nul 2>nul

if "%smx%"=="N" (
  echo ## Copying BIOS ...
  echo.
  if exist SDBIOS\OCM-BIOS.DAT (
      waitfor /T %TIMEOUT% pause >nul 2>nul
      if exist "%dest%\System Volume Information" rd /S /Q "%dest%\System Volume Information" >nul 2>nul
      copy SDBIOS\OCM-BIOS.DAT %dest% >nul 2>nul
  )
  echo Done
)

echo.
echo ### Copying system files ...
echo.
copy OS\MSXDOS2.SYS %dest% >nul 2>nul
copy OS\NEXTOR.SYS %dest% >nul 2>nul
copy OS\COMMAND2.COM %dest% >nul 2>nul
copy QHELP\QHELP.BAT %dest%\QHELP.BAT >nul 2>nul

if not exist "%dest%\COMMAND2.COM" (
    call :title
    echo ### CRITICAL ERROR: THE TARGET DRIVE IS NOT FOUND^^!
    del OCM-BIOS.DAT >nul 2>nul
    del MSXDOS2.SYS >nul 2>nul
    del NEXTOR.SYS >nul 2>nul
    del COMMAND2.COM >nul 2>nul
    waitfor /T %TIMEOUT% pause >nul 2>nul
    goto quit_0
)
echo Done

echo.
echo #### Copying SYSTEM ...
echo.
xcopy /Q /H /E /Y SYSTEM\*.* %dest%

if "%extras%"=="Y" (
  echo.
  echo #### Copying EXTRAS ...
  echo.
  xcopy /Q /H /E /Y EXTRAS\*.* %dest%
  copy QHELP\QHELP-extras.BAT %dest%\QHELP.BAT >nul 2>nul
)

if "%network%"=="Y" (
  echo.
  echo #### Copying NETWORK ...
  echo.
  xcopy /Q /H /E /Y NETWORK\*.* %dest%
  copy QHELP\QHELP-network.BAT %dest%\QHELP.BAT >nul 2>nul
)

if "%extras%"=="Y" (
  if "%network%"=="Y" (
    copy QHELP\QHELP-extras-network.BAT %dest%\QHELP.BAT >nul 2>nul
  )
)

if "%smx%"=="N" (
  echo.
  echo ##### Copying CORE ...
  echo.
  xcopy /Y core\*.* %dest%
)

echo.
echo #### Copying OTHER ...
echo.
xcopy /Q /H /E /Y OTHER\*.* %dest%

del %dest%\README

rem --- End of process
echo Done^^!
:quit_0
waitfor /T %TIMEOUT% pause >nul 2>nul
goto quit

:uppercase
set %~1=!%1:a=A!
set %~1=!%1:b=B!
set %~1=!%1:c=C!
set %~1=!%1:d=D!
set %~1=!%1:e=E!
set %~1=!%1:f=F!
set %~1=!%1:g=G!
set %~1=!%1:h=H!
set %~1=!%1:i=I!
set %~1=!%1:j=J!
set %~1=!%1:k=K!
set %~1=!%1:l=L!
set %~1=!%1:m=M!
set %~1=!%1:n=N!
set %~1=!%1:o=O!
set %~1=!%1:p=P!
set %~1=!%1:q=Q!
set %~1=!%1:r=R!
set %~1=!%1:s=S!
set %~1=!%1:t=T!
set %~1=!%1:u=U!
set %~1=!%1:v=V!
set %~1=!%1:w=W!
set %~1=!%1:x=X!
set %~1=!%1:y=Y!
set %~1=!%1:z=Z!
goto:eof

:title
cls
echo Multicore SD Card creator (2023.02.19)
echo ======================================
echo.
goto:eof

:quit
endlocal

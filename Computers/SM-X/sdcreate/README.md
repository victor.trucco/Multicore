SM-X SD CREATE v1.1 (2021.03.31)
================================

This tool allows you to easily create your SD-Card for Multicore machines.

WARNING: USE THIS TOOL AT YOUR OWN RISK AND PROCEED WITH EXTREME CAUTION!
-------------------------------------------------------------------------

Before starting, you can add your Core inside the directory `./core` to be copied
during the process. Also, the core can be copied to SD-Card after the process finalize.

Add any other file you want to add to image on `./extras` directory.

## Windows

### sdcreate.cmd

Any target size lower or higher than 4GB are automatically managed by DiskPart of Windows.
No other external tool is needed to go!

Tested with Microsoft Windows 10.0.15063 64-bit.

1. Insert the SD-Card into the card reader.
2. Run the "sdcreate.cmd" script as Administrator.
3. Enter the target drive letter and a label name.
4. Press a key to finalize the process.

All done!

## Linux

### sdcreate.sh

1. Insert the SD-Card into the card reader.
2. Identify your SD-Card device. Ex: `lsblk | grep /media`
3. Run the "sdcreate.sh" script with "sudo" (Also, use "extras" and/or "network" to add respective folders).

Ex: `sudo ./sdcreate.sh /dev/sdX extras network`

Where "/dev/sdX" is your SD Card without partition number (Ex.: /dev/sdb)

4. Press a key to finalize the process.

All done!

### imgcreate.sh

1. Run "imgcreate.sh" with the image size in MB (Also, use "extras" and/or "network" to add respective folders).

Ex: `./imgcreate.sh 256 extras network`

It'll create an image named "SD_SM-X.img" with the size of 256MB with folders "./extras" and "./network"

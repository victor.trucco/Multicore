#!/bin/bash

IMGFILE=SD_SM-X.img

[ -z "$( which sfdisk )" ] && echo "sfdisk not found!" && exit 1
[ -z "$( which mkfs.msdos )" ] && echo "mkfs.msdos not found!" && exit 1
[ -z "$( which mcopy )" ] && echo "mcopy not found!" && exit 1
[ -z "$( which mattrib )" ] && echo "truncate not found!" && exit 1

echo "SM-X SD Image creator (2023.02.19)"
echo "================================="
echo

echo "This script will create a file 'SD_SM-X.img' to use with Multicore"
echo

echo
while true; do
    read -p "Image size? [in MB] " imgsize
    if [[ "$imgsize" =~ ^[0-9]{2,4}$ ]] && [[ $imgsize -ge 10 ]] && [[ $imgsize -le 2048 ]]; then
      IMGSIZE=$imgsize;
      break;
    fi
    echo "Please only numbers from 10 to 2048";
done

echo
while true; do
    read -p "Include EXTRAS? [Y/n] " yn
    case $yn in
        [Nn]* ) EXTRAS=0; break;;
        [Yy]*|"" ) EXTRAS=1; break;;
        * ) echo "Please answer Yes or No.";;
    esac
done

echo
while true; do
    read -p "Include NETWORK? [Y/n] " yn
    case $yn in
        [Nn]* ) NETWORK=0; break;;
        [Yy]*|"" ) NETWORK=1; break;;
        * ) echo "Please answer Yes or No.";;
    esac
done

SIZEB=$(( $IMGSIZE * 1024 * 1024 ))

echo
echo "# Creating image file"
echo

truncate --size=$SIZEB $IMGFILE

echo
echo "# Creating partition"
echo

NSECTORS=$( expr $SIZEB / 512 - 1 )
echo 1 $NSECTORS 6 | sfdisk -q --force $IMGFILE
sync
sleep 1

echo
echo "## Formatting image"
echo

CYLINDERS=$( expr $NSECTORS / 16065 )
mformat -i $IMGFILE@@512 -v FAT16MSX -t $CYLINDERS -h 255 -n 63 -H 1 -m 248 ::
sleep 1

echo "### Copying BIOS file"
echo
mcopy -i $IMGFILE@@512 sdbios/OCM-BIOS.DAT ::
mattrib -i $IMGFILE@@512 +h ::OCM-BIOS.DAT

echo "#### Copying system files"
echo
mcopy -i $IMGFILE@@512 os/MSXDOS2.SYS ::
mcopy -i $IMGFILE@@512 os/NEXTOR.SYS ::
mcopy -i $IMGFILE@@512 os/COMMAND2.COM ::

mattrib -i $IMGFILE@@512 +s ::MSXDOS2.SYS
mattrib -i $IMGFILE@@512 +s ::NEXTOR.SYS
mattrib -i $IMGFILE@@512 +s ::COMMAND2.COM

mcopy -i $IMGFILE@@512 system/* ::
[[ "$EXTRAS" == "1" ]] && echo "**** Copying EXTRAS" && mcopy -os -i $IMGFILE@@512 extras/* ::
[[ "$NETWORK" == "1" ]] && echo "**** Copying NETWORK" && mcopy -os -i $IMGFILE@@512 network/* ::

echo "**** Copying OTHER" && mcopy -os -i $IMGFILE@@512 other/* ::
mdel -i $IMGFILE@@512 ::README

echo

mcopy -os -i $IMGFILE@@512 qhelp/QHELP.BAT ::QHELP.BAT
[[ "$EXTRAS" == "1" ]] && mcopy -os -i $IMGFILE@@512 qhelp/QHELP-extras.BAT ::QHELP.BAT
[[ "$NETWORK" == "1" ]] && mcopy -os -i $IMGFILE@@512 qhelp/QHELP-network.BAT ::QHELP.BAT
[[ "$EXTRAS" == "1" ]] && [[ "$NETWORK" == "1" ]] && mcopy -os -i $IMGFILE@@512 qhelp/QHELP-extras-network.BAT ::QHELP.BAT

sync

echo "Done"

//============================================================================
//
//  This program is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation; either version 2 of the License, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//============================================================================

///============================================================================
//
//  Multicore 2+ / Neptuno Top by Victor Trucco
//
//============================================================================

`default_nettype none

module CoCo2_mc2p(
    // Clocks
    input wire  clock_50_i,   

    `ifdef NEPTUNO
    // SRAM (IS61WV102416BLL-10TLI)
    output [19:0]sram_addr_o = 20'b00000000000000000000,
    inout  [15:0]sram_data_io = 8'hZZZZ,
    output sram_we_n_o = 1'b1,
    output sram_oe_n_o = 1'b1,
    output sram_ub_n_o = 1'b1,
    output sram_lb_n_o = 1'b1,
    `else
    // SRAM (IS61WV20488FBLL-10)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1, 

    //external buttons
    input wire [4:1]    btn_n_i,
    `endif
        
    // SDRAM (W9825G6KH-6)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io        = 1'bz,
    inout wire  ps2_data_io       = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o         = 1'bZ,
    output wire sd_sclk_o         = 1'bZ,
    output wire sd_mosi_o         = 1'bZ,
    input wire  sd_miso_i,

    // Joysticks
    output wire joy_clock_o       = 1'b1,
    output wire joy_load_o        = 1'b1,
    input  wire joy_data_i,
    output wire joy_p7_o          = 1'b1,

    // Audio
    output      AUDIO_L,
    output      AUDIO_R,
    input wire  ear_i,
    output wire mic_o             = 1'b0,

    // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

    //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
   
    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output wire   SPI_nWAIT         = 1'b1, // '0' to hold the microcontroller data streaming

    inout [31:0] GPIO,

    output LED                      = 1'b1 // '0' is LED on
);
 
//assign LED  =  tape_in;


`include "build_id.v"
localparam CONF_STR = {
  "S1,CCC/ROM,Load Cartridge;",
  "T1,Revome Cartridge;",
  "S2,CAS,Load Cassette;",
  "T6,Stop & Rewind Tape;",
  "OD,Monitor Tape Sound,No,Yes;",
  "OC,Tape Input,File,Line;",


  "OFG,Scanlines,Off,25%,50%,75%;",
  "OH,Scandoubler,On,Off;", 
  "O5,Blend,Off,On;", 

  "O4,Overscan,Hidden,Visible;",
  "O3,Artifact,Enable,Disable;",
  "O2,Artifact Phase,Normal,Reverse;",
 // "ON,Keyboard Layout,PC,CoCo;",
  "OLM,Joystick,Analog Slow Emu,Analog Fast Emu,D-Pad,USB;",
  "OA,Swap Joysticks,Off,On;",
//  "O89,Machine,CoCo2,Dragon32,Dragon64;",
//  "O7,Turbo,Off,On;",
  "T0,Reset;",
  "V,v",`BUILD_DATE
};

//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------
assign LED = 1;
assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign stm_rx_o = 1'bZ;

//no SRAM for this core
assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;

//all the SD reading goes thru the microcontroller for this core
assign sd_cs_n_o = 1'bZ;
assign sd_sclk_o = 1'bZ;
assign sd_mosi_o = 1'bZ;
assign stm_rst_o = 1'bz;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

`ifdef NEPTUNO
    wire [4:1] btn_n_i = 4'b1111;

    neptuno_joydecoder  neptuno_joydecoder (
`else 
    joystick_serial  joystick_serial (
`endif

    .clk_i           ( clock_50_i ),
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

// ROM Data Pump
reg [7:0] osd_s = 8'b11111111;
//PumpSignal PumpSignal (clk_sys, ~pll_locked, ioctl_downl, osd_s);

//-- END defaults -------------------------------------------------------

/////////////////  CLOCKS  ////////////////////////

wire clk_sys;
wire locked;
wire sdram_clk;

pll pll
(
  .inclk0(clock_50_i),
  .c0(clk_sys),
 // .c1(SDRAM_CLK),
  .locked(locked)
);




/////////////////  HPS  ///////////////////////////


wire forced_scandoubler;
wire  [1:0] buttons;
wire [31:0] status;
wire [10:0] ps2_key;
wire        ioctl_download;
wire        ioctl_wr;
wire [15:0] ioctl_addr;
wire  [7:0] ioctl_data;
wire  [7:0] ioctl_index;

wire [15:0] joy1, joy2;
wire [15:0] joya1, joya2;

wire ypbpr;



 /*
mist_io #(.STRLEN($size(CONF_STR)>>3),.PS2DIV(100)) mist_io
(
  .SPI_SCK   (SPI_SCK),
   .CONF_DATA0(CONF_DATA0),
   .SPI_SS2   (SPI_SS2),
   .SPI_DO    (SPI_DO),
   .SPI_DI    (SPI_DI),

  .clk_sys(clk_sys),
  .conf_str(CONF_STR),

  .buttons(buttons),
  .status(status),
  .scandoubler_disable(forced_scandoubler),
   .ypbpr     (ypbpr),
  
  .ioctl_ce(1),
  .ioctl_download(ioctl_download),
  .ioctl_index(ioctl_index),
  .ioctl_wr(ioctl_wr),
  .ioctl_addr(ioctl_addr),
  .ioctl_dout(ioctl_data),
    
  
  .ps2_key(ps2_key),
  .joystick_analog_0(joya1),
   .joystick_analog_1(joya2),

  .joystick_0(joy1), // HPS joy [4:0] {Fire, Up, Down, Left, Right}
  .joystick_1(joy2)

);
*/

data_io #(
    .STRLEN(($size(CONF_STR)>>3)))
data_io(
    .clk_sys       ( clk_sys      ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),
    .SPI_DO        ( SPI_DO       ),
    
    .data_in       ( osd_s & keys_s ),
    .conf_str      ( CONF_STR ),
    .status        ( status ),
    
    .ioctl_download( ioctl_download  ),
    .ioctl_index   ( ioctl_index  ),
    .ioctl_wr      ( ioctl_wr     ),
    .ioctl_addr    ( ioctl_addr   ),
    .ioctl_dout    ( ioctl_data   )
);

/////////////////  RESET  /////////////////////////

wire reset =  (ioctl_download && ioctl_index==1) | machine_select_reset;



////////////////  Console  ////////////////////////

wire sndout;
wire [15:0] sound_pad =  {sndout,sound[11:8], sound[7] ^ (status[13] ? (status[12] ? tape_in : casdout) : 1'b0), sound[6:0], 3'b0};

dac #(16) dac_l (
   .clk_i        (clk_sys),
   .res_n_i      (1      ),
   .dac_i        (sound_pad),
   .dac_o        (AUDIO_L)
);

assign AUDIO_R=AUDIO_L;

wire HBlank;
wire HSync;
wire VBlank;
wire VSync;

wire [7:0] red;
wire [7:0] green;
wire [7:0] blue;

wire [11:0] sound;


wire [9:0] center_joystick_y1 = (status[22:21] == 2'b11) ? joya1[15:8] : ana1_y; //joya1[15:8]; //8'd128 + joya1[15:8];
wire [9:0] center_joystick_x1 = (status[22:21] == 2'b11) ? joya1[7:0]  : ana1_x; //joya1[7:0]; //8'd128 + joya1[7:0];
wire [9:0] center_joystick_y2 = (status[22:21] == 2'b11) ? joya2[15:8] : ana2_y; //joya2[15:8]; //8'd128 + joya2[15:8];
wire [9:0] center_joystick_x2 = (status[22:21] == 2'b11) ? joya2[7:0]  : ana2_x; //joya2[7:0]; //8'd128 + joya2[7:0];
wire vclk;

wire [31:0] coco_joy1 = ~status[10] ? joy2 : joy1;
wire [31:0] coco_joy2 = ~status[10] ? joy1 : joy2;

wire [15:0] coco_ajoy1 = ~status[10] ? {center_joystick_x2[7:0],center_joystick_y2[7:0]} : {center_joystick_x1[7:0],center_joystick_y1[7:0]};
wire [15:0] coco_ajoy2 = ~status[10] ? {center_joystick_x1[7:0],center_joystick_y1[7:0]} : {center_joystick_x2[7:0],center_joystick_y2[7:0]};

wire casdout;
wire cas_relay;

reg dragon64;
reg dragon;
reg [1:0]machineselect_r;
reg machine_select_reset;
reg [15:0]reset_count;

always @(posedge clk_sys)
begin
     machine_select_reset <=1'b0;
     dragon64 <= (status[9:8]==2'b10);
     dragon   <= (status[9:8]!=2'b00);
     
     if (status[0] | status[1] | ~btn_n_i[4] | machineselect_r!=status[9:8]) reset_count<=16'hffff;

     if (reset_count>16'd0) 
     begin
        reset_count<=reset_count - 1'b1;
        machine_select_reset<=1'b1;
     end
     
     machineselect_r<=status[9:8];
end


dragoncoco dragoncoco(
  .clk(clk_sys), // 50 mhz
  .turbo(status[7]&cas_relay),
  .reset(~reset),
  .dragon(dragon),
  .dragon64(dragon64),
  .kblayout(~status[23]),
  .red(red),
  .green(green),
  .blue(blue),

  .hblank(HBlank),
  .vblank(VBlank),
  .hsync(HSync),
  .vsync(VSync),
  .vclk(vclk),
  // input ps2_clk,
  // input ps2_dat,
  .uart_din(1'b0),

  .ps2_key(ps2_key),
  .ioctl_addr(ioctl_addr),
  .ioctl_data(ioctl_data),
  .ioctl_download(ioctl_download),
  .ioctl_index(ioctl_index),
  .ioctl_wr(ioctl_wr),
  .remove_cart(status[1]),
  .casdout(status[12] ? casdout : casdout),
  .cas_relay(cas_relay),
  .artifact_phase(status[2]),
  .artifact_enable(~status[3]),
  .overscan(status[4]),
//  .count_offset(status[8:5]),

  .joy_use_dpad(status[22:21]==2'b10),

  .joy1(coco_joy1),
  .joy2(coco_joy2),

  .joya1(coco_ajoy1),
  .joya2(coco_ajoy2),


  .cass_snd(),
  .sound(sound),
  .sndout(sndout),

  .v_count(VCount),
  .h_count(HCount),
  .DLine1(DLine1),
  .DLine2(DLine2),

  .clk_Q_out(clk_Q_out)

);

reg [8:0] HCount,VCount;
wire [159:0]DLine1;
wire [159:0]DLine2;

wire clk_Q_out;

wire [24:0] sdram_addr;
wire [7:0] sdram_data;
wire sdram_rd;
wire load_tape = ioctl_index == 2;

sdram sdram
(
  .*,
  .SDRAM_CLK (SDRAM_CLK),
  .init(~locked),
  .clk(clk_sys),
  .addr(ioctl_download ? ioctl_addr : sdram_addr),
  .wtbt(0),
  .dout(sdram_data),
  .din(ioctl_data),
  .rd(sdram_rd),
  .we(ioctl_wr & load_tape),
  .ready()
);

/////////////////  Tape In   /////////////////////////

wire tape_in;
assign tape_in = ear_i;

cassette cassette(
  .clk(clk_sys),
  .Q(clk_Q_out),

  .rewind(status[6] | reset),
  .en(cas_relay),

  .sdram_addr(sdram_addr),
  .sdram_data(sdram_data),
  .sdram_rd(sdram_rd),

  .data(casdout)
//   .status(tape_status)
);

/////////////////  VIDEO  /////////////////////////

wire [7:0]rr = red;
wire [7:0]gg = green;
wire [7:0]bb = blue;
  
/*

video_mixer #(.LINE_LENGTH(380)) video_mixer
(
   .*,
   .ce_pix(vclk),
   .ce_pix_actual(vclk),
   
   .scandoubler_disable(forced_scandoubler),
   .hq2x(scale==1),
   .mono(0),
   .scanlines(0),
   .ypbpr_full(0),
   .line_start(0),

  .R(rr[7:2]),
  .G(gg[7:2]),
  .B(bb[7:2])
  
);
*/

wire        blankn = ~(HBlank | VBlank);
wire direct_video_s = ~status[17] ^ direct_video;

mist_video #(.COLOR_DEPTH(3), .SD_HCNT_WIDTH(10)) mist_video
(
    .clk_sys ( clk_sys ),
    .SPI_SCK ( SPI_SCK ),
    .SPI_SS3 ( SPI_SS2 ),
    .SPI_DI  ( SPI_DI  ),

  //  .R       ( (blankn) ? rr[7:2] : 6'd0 ),
  //  .G       ( (blankn) ? gg[7:2] : 6'd0 ),
  //  .B       ( (blankn) ? bb[7:2] : 6'd0 ),

    .R       ( rr[7:5]  ),
    .G       ( gg[7:5]  ),
    .B       ( bb[7:5]  ),

    .HSync   ( HSync ),
    .VSync   ( VSync ),

    // video output 
    .VGA_R  ( VGA_R  ),
    .VGA_G  ( VGA_G  ),
    .VGA_B  ( VGA_B  ),
    .VGA_HS ( VGA_HS ),
    .VGA_VS ( VGA_VS ),

    .ce_divider ( 1'b0 ),
    .blend      ( status[5] ),
    .rotate     ( osd_rotate ),
    .scanlines  ( status[16:15] ),
    .scandoubler_disable( direct_video_s ),
    .no_csync   ( ~direct_video_s ),
    .osd_enable ( osd_enable )
);


wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player  = ~btn_n_o[1] | m_one_player;
wire btn_two_players = ~btn_n_o[2] | m_two_players;
wire btn_coin        = ~btn_n_o[3] | m_coin1;
wire [7:0] keys_s;
wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable;
wire direct_video;
wire [1:0]osd_rotate;
wire [4:1]btn_n_o;

MC2_HID #(.CLK_SPEED(50000), .use_usb_g ( 1'b1 )) k_hid
(
    .clk_i          ( clock_50_i ),
    .kbd_clk_io     ( ps2_clk_io ),
    .kbd_dat_io     ( ps2_data_io ),
    .usb_rx_i       ( ps2_mouse_clk_io ),

    .keyb_pressed_o    ( ps2_key[9] ), // 1-make (pressed), 0-break (released)
    .keyb_extended_o   ( ps2_key[8] ), // extended code
    .keyb_code_o       ( ps2_key[7:0] ), // key scan code
    .keyb_strobe_o     ( ps2_key[10] ), // toggles every key press/release

    .joystick_0_i   ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1_i   ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
      
    //-- joystick_0 and joystick_1 should be swapped
    .joyswap_i      ( 0 ),

    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer_i    ( 0 ),

    //-- tilt, coin4-1, start4-1
    .controls_o     ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

    //-- fire12-1, up, down, left, right

    .player1_o        ( joy1 ),
    .player1_analog_o ( joya1 ),
    .player2_o        ( joy2 ),
    .player2_analog_o ( joya2 ),


    .direct_video_o ( direct_video ),
    .osd_rotate_o   ( osd_rotate ),

    //-- keys to the OSD
    .osd_o          ( keys_s ),
    .osd_enable_i   ( osd_enable ),
    
    //-- sega joystick
    .sega_strobe_o  ( joy_p7_o ),

     //-- Front buttons
     .front_buttons_i ( 4'b1111 ),
     .front_buttons_o (  )        
);

// Analog emulation 

reg  [7:0] ana1_x = 8'd0;
reg  [7:0] ana1_y = 8'd0;
reg  [7:0] ana2_x = 8'd0;
reg  [7:0] ana2_y = 8'd0;
reg [17:0] cnt = 18'd0;

always @(posedge clk_sys)
begin
            cnt <= cnt + 1;

            if ((cnt == {2'b11, 16'hffff} && status[22:21]==2'b00) || // slow
                (cnt == {2'b01, 16'hffff} && status[22:21]==2'b01))   // fast 
            begin

                cnt <= 18'd0;
                
                if (joy1[1] && ana1_x>0)   ana1_x <= ana1_x - 1;
                if (joy1[0] && ana1_x<255) ana1_x <= ana1_x + 1;
                if (joy1[3] && ana1_y>0)   ana1_y <= ana1_y - 1;
                if (joy1[2] && ana1_y<255) ana1_y <= ana1_y + 1;

                if (joy2[1] && ana2_x>0)   ana2_x <= ana2_x - 1;
                if (joy2[0] && ana2_x<255) ana2_x <= ana2_x + 1;
                if (joy2[3] && ana2_y>0)   ana2_y <= ana2_y - 1;
                if (joy2[2] && ana2_y<255) ana2_y <= ana2_y + 1;


            end
end

endmodule
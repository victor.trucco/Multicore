/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//* qmem_arbiter.v */

module qmem_arbiter #(
  parameter QAW = 32,     // address width
  parameter QDW = 32,     // data width
  parameter QSW = QDW/8,  // byte select width
  parameter MN  = 2       // number of masters
)(
  // system
  input  wire              clk,
  input  wire              rst,
  // slave port for requests from masters
  input  wire [MN    -1:0] qm_cs,
  input  wire [MN    -1:0] qm_we,
  input  wire [MN*QSW-1:0] qm_sel,
  input  wire [MN*QAW-1:0] qm_adr,
  input  wire [MN*QDW-1:0] qm_dat_w,
  output wire [MN*QDW-1:0] qm_dat_r,
  output wire [MN    -1:0] qm_ack,
  output wire [MN    -1:0] qm_err,
  // master port for requests to a slave
  output wire              qs_cs,
  output wire              qs_we,
  output wire    [QSW-1:0] qs_sel,
  output wire    [QAW-1:0] qs_adr,
  output wire    [QDW-1:0] qs_dat_w,
  input  wire    [QDW-1:0] qs_dat_r,
  input  wire              qs_ack,
  input  wire              qs_err,
  // one hot master status (bit MN is always 1'b0)
  output wire     [MN-1:0] ms
);


wire     [3:0] ms_a;
wire  [MN-1:0] ms_tmp;
reg   [MN-1:0] ms_reg;

genvar i;

// masters priority decreases from the LSB to the MSB side
assign ms_tmp[0] = qm_cs[0];
generate for (i=1; i<MN; i=i+1) begin : loop_arbiter
  assign ms_tmp[i] = qm_cs[i] & ~|qm_cs[i-1:0];
end endgenerate

always @(posedge clk, posedge rst) begin
  if (rst)                   ms_reg <= #1 0;
  else if (qs_ack | qs_err)  ms_reg <= #1 0;
  else if (!(|ms_reg))       ms_reg <= #1 ms_tmp;
end

assign ms = |ms_reg ? ms_reg : ms_tmp;

generate if (MN == 1) assign ms_a =                                                         0; endgenerate
generate if (MN == 2) assign ms_a =                                                 ms[1]?1:0; endgenerate
generate if (MN == 3) assign ms_a =                                         ms[2]?2:ms[1]?1:0; endgenerate
generate if (MN == 4) assign ms_a =                                 ms[3]?3:ms[2]?2:ms[1]?1:0; endgenerate
generate if (MN == 5) assign ms_a =                         ms[4]?4:ms[3]?3:ms[2]?2:ms[1]?1:0; endgenerate
generate if (MN == 6) assign ms_a =                 ms[5]?5:ms[4]?4:ms[3]?3:ms[2]?2:ms[1]?1:0; endgenerate
generate if (MN == 7) assign ms_a =         ms[6]?6:ms[5]?5:ms[4]?4:ms[3]?3:ms[2]?2:ms[1]?1:0; endgenerate
generate if (MN == 8) assign ms_a = ms[7]?7:ms[6]?6:ms[5]?5:ms[4]?4:ms[3]?3:ms[2]?2:ms[1]?1:0; endgenerate

// slave port for requests from masters
assign qs_cs    = qm_cs    >>      ms_a ;
assign qs_we    = qm_we    >>      ms_a ;
assign qs_sel   = qm_sel   >> (QSW*ms_a);
assign qs_adr   = qm_adr   >> (QAW*ms_a);
assign qs_dat_w = qm_dat_w >> (QDW*ms_a);

// master ports for requests to a slave
generate for (i=0; i<MN; i=i+1) begin : loop_bus
  assign qm_dat_r [QDW*(i+1)-1:QDW*(i+1)-QDW] = qs_dat_r;
end endgenerate

// one hot is a bit of overkill for register
assign qm_ack = ms & {MN{qs_ack}};
assign qm_err = ms & {MN{qs_err}};


endmodule


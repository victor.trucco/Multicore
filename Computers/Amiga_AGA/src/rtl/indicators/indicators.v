/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//* indicators.v */

module indicators (
  // system
  input  wire           clk,          // clock
  input  wire           rst,          // reset
  // inputs
  input wire  [  8-1:0] track,        // floppy track number
  input wire            f_wr,         // floppy fifo write
  input wire            f_rd,         // floppy fifo read
  input wire            h_wr,         // harddisk fifo write
  input wire            h_rd,         // harddisk fifo read
  input wire  [  4-1:0] status,       // control block slave status
  input wire  [  4-1:0] ctrl_status,  // control block reg-driven status
  input wire  [  4-1:0] sys_status,   // status from system
  input wire            fifo_full,
  output wire [  8-1:0] led      
);


// LEDs
reg [1:0] r0, r1, g0, g1;

always @ (posedge clk or posedge rst) begin
  if (rst) begin
    r0 <= #1 2'b00;
    r1 <= #1 2'b00;
    g0 <= #1 2'b00;
    g1 <= #1 2'b00;
  end else begin
    r0 <= #1 {r0[0], f_wr};
    r1 <= #1 {r1[0], h_wr};
//    g0 <= #1 {g0[0], f_rd};
//    g1 <= #1 {g1[0], h_rd};
    g0 <= #1 {g0[0], f_rd}; // pongo los leds de actividad de FD y HD juntos, tanto escritura como lectura
    g1 <= #1 {g1[0], h_rd}; // quedan asi--->   4=wr-hd  3=rd-hd  2=wr-fd  1=rd-fd
  end
end

wire r0_out, g0_out, r1_out, g1_out;

assign r0_out = |r0;
assign r1_out = |r1;
assign g0_out = |g0;
assign g1_out = |g1;

reg  [  4-1:0] ctrl_leds;
always @ (posedge clk, posedge rst) begin
  if (rst)
    ctrl_leds <= #1 4'b0;
  else
    ctrl_leds <= #1 ctrl_status;
end

// reducido por mi, dado que solo tenemos 3 leds
assign led = {ctrl_leds, 1'b0, fifo_full, g1_out, g0_out};
//assign led_r = {status,    sys_status, r1_out, r0_out};


endmodule


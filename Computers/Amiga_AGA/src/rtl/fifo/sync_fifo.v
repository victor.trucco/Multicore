/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//********************************************/
/* sync_fifo.v                              */
/* simple single-clock fifo                 */
/*                                          */
/* 2011, rok.krajnc@gmail.com               */
/********************************************/


module sync_fifo #(
  parameter FD    = 16,               // fifo depth
  parameter DW    = 32                // data width
)(
  // system
  input  wire           clk,          // clock
  input  wire           clk7_en,      // 7MHz clock enable
  input  wire           rst,          // reset
  // fifo input / output
  input  wire [ DW-1:0] fifo_in,      // write data
  output wire [ DW-1:0] fifo_out,     // read data
  // fifo control
  input  wire           fifo_wr_en,   // fifo write enable
  input  wire           fifo_rd_en,   // fifo read enable
  // fifo status
  output wire           fifo_full,    // fifo full
  output wire           fifo_empty    // fifo empty
);



////////////////////////////////////////
// log function                       //
////////////////////////////////////////

function integer CLogB2;
  input [31:0] d;
  integer i;
begin
  i = d;
  for(CLogB2 = 0; i > 0; CLogB2 = CLogB2 + 1) i = i >> 1;
end
endfunction



////////////////////////////////////////
// local parameters                   //
////////////////////////////////////////

localparam FCW = CLogB2(FD-1) + 1;
localparam FPW = CLogB2(FD-1);



////////////////////////////////////////
// local signals                      //
////////////////////////////////////////

// fifo counter
reg  [FCW-1:0] fifo_cnt;

// fifo write & read pointers
reg  [FPW-1:0] fifo_wp, fifo_rp;

// fifo memory
reg  [ DW-1:0] fifo_mem [0:FD-1];



////////////////////////////////////////
// logic                              //
////////////////////////////////////////

// FIFO write pointer
always @ (posedge clk or posedge rst) begin
  if (rst)
    fifo_wp <= #1 1'b0;
  else if (clk7_en) begin
    if (fifo_wr_en && !fifo_full)
      fifo_wp <= #1 fifo_wp + 1'b1;
  end
end


// FIFO write
always @ (posedge clk) begin
  if (clk7_en) begin
    if (fifo_wr_en && !fifo_full) fifo_mem[fifo_wp] <= #1 fifo_in;
  end
end


// FIFO counter
always @ (posedge clk or posedge rst) begin
  if (rst)
    fifo_cnt <= #1 'd0;
  // read & no write
  else if (clk7_en) begin
    if (fifo_rd_en && !fifo_wr_en && (fifo_cnt != 'd0))
      fifo_cnt <= #1 fifo_cnt - 'd1;
    // write & no read
    else if (fifo_wr_en && !fifo_rd_en && (fifo_cnt != FD))
      fifo_cnt <= #1 fifo_cnt + 'd1;
  end
end


// FIFO full & empty
assign fifo_full  = (fifo_cnt == (FD));
assign fifo_empty = (fifo_cnt == 'd0);


// FIFO read pointer
always @ (posedge clk or posedge rst) begin
  if (rst)
    fifo_rp <= #1 1'b0;
  else if (clk7_en) begin
    if (fifo_rd_en && !fifo_empty)
      fifo_rp <= #1 fifo_rp + 1'b1;
  end
end


// FIFO read
assign fifo_out = fifo_mem[fifo_rp];


endmodule


/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///Blitter minterm function generator
//The minterm function generator takes <ain>,<bin> and <cin> 
//and checks every logic combination against the LF control byte.
//If a combination is marked as 1 in the LF byte,the ouput will
//also be 1,else the output is 0.

module agnus_blitter_minterm
(
	input	[7:0] lf,	//LF control byte
	input	[15:0] ain,	//A channel in
	input	[15:0] bin,	//B channel in
	input	[15:0] cin,	//C channel in
	output	[15:0] out	//function generator output
);

reg		[15:0] mt0;		//minterm 0
reg		[15:0] mt1;		//minterm 1
reg		[15:0] mt2;		//minterm 2
reg		[15:0] mt3;		//minterm 3
reg		[15:0] mt4;		//minterm 4
reg		[15:0] mt5;		//minterm 5
reg		[15:0] mt6;		//minterm 6
reg		[15:0] mt7;		//minterm 7

//Minterm generator for each bit. The code inside the loop 
//describes one bit. The loop is 'unrolled' by the 
//synthesizer to cover all 16 bits in the word.
integer j;
always @(ain or bin or cin or lf)
	for (j=15; j>=0; j=j-1)
	begin
		mt0[j] = ~ain[j] & ~bin[j] & ~cin[j] & lf[0];
		mt1[j] = ~ain[j] & ~bin[j] &  cin[j] & lf[1];
		mt2[j] = ~ain[j] &  bin[j] & ~cin[j] & lf[2];
		mt3[j] = ~ain[j] &  bin[j] &  cin[j] & lf[3];
		mt4[j] =  ain[j] & ~bin[j] & ~cin[j] & lf[4];
		mt5[j] =  ain[j] & ~bin[j] &  cin[j] & lf[5];
		mt6[j] =  ain[j] &  bin[j] & ~cin[j] & lf[6];
		mt7[j] =  ain[j] &  bin[j] &  cin[j] & lf[7];
	end

//Generate function generator output by or-ing all
//minterms together.
assign out = mt0 | mt1 | mt2 | mt3 | mt4 | mt5 | mt6 | mt7;


endmodule		


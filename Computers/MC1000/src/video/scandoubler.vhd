--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-- -----------------------------------------------------------------------
--
--                                 FPGA 64
--
--     A fully functional commodore 64 implementation in a single FPGA
--
-- -----------------------------------------------------------------------
-- Copyright 2005-2008 by Peter Wendrich (pwsoft@syntiac.com)
-- http://www.syntiac.com/fpga64.html
-- -----------------------------------------------------------------------
--
-- fpga64_scandoubler.vhd
--
-- -----------------------------------------------------------------------
--
-- Converts 15.6 Khz PAL/NTSC screen to 31 Khz VGA screen by doubling
-- each scanline.
--
-- -----------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.ALL;
use IEEE.numeric_std.all;

entity scandoubler is
	generic (
		hSyncLength_g	: integer	:= 31;
		vSyncLength_g	: integer	:= 31;
		ramBits_g		: integer	:= 11;
		addr_width_g	: integer	:= 4;
		hsync_pol_g		: std_logic	:= '0';
		vsync_pol_g		: std_logic	:= '0'
	);
	port (
		clock_i			: in    std_logic;
		enable_i			: in    std_logic;
		scanlines_en_i	: in    std_logic := '0';
		video_i			: in    std_logic_vector(addr_width_g-1 downto 0);
		hsync_i			: in    std_logic;
		vsync_i			: in    std_logic;
		video_o			: out   std_logic_vector(addr_width_g-1 downto 0);
		hsync_o			: out   std_logic;
		vsync_o			: out   std_logic
	);
end scandoubler;

architecture rtl of scandoubler is

	constant lineLengthBits : integer := 11;

	signal prescale		: unsigned(0 downto 0);
	signal impar			: std_logic;
	signal startIndex		: unsigned((ramBits_g-1) downto 0) := (others => '0');
	signal endIndex		: unsigned((ramBits_g-1) downto 0) := (others => '0');
	signal readIndex		: unsigned((ramBits_g-1) downto 0) := (others => '0');
	signal writeIndex 	: unsigned((ramBits_g-1) downto 0) := (others => '0');
	signal oldHSync		: std_logic             := '0';
	signal oldVSync		: std_logic             := '0';
	signal hSyncCount		: integer range 0 to hSyncLength_g;
	signal vSyncCount		: integer range 0 to vSyncLength_g;
	signal lineLength		: unsigned(lineLengthBits downto 0);
	signal lineLengthCnt	: unsigned((lineLengthBits+1) downto 0);
	signal nextLengthCnt	: unsigned((lineLengthBits+1) downto 0);
	signal ram_s			: std_logic_vector(addr_width_g-1 downto 0);
	signal ram_q_s			: std_logic_vector(addr_width_g-1 downto 0);

begin

	ram: entity work.dpram
	generic map (
		addr_width_g => ramBits_g,
		data_width_g => addr_width_g
	)
	port map (
		clk_a_i  				=> clock_i,
		we_i     				=> '1',
		addr_a_i 				=> std_logic_vector(writeIndex),
		data_a_i 				=> video_i,
		clk_b_i  				=> clock_i,
		addr_b_i 				=> std_logic_vector(readIndex),
		data_b_o					=> ram_s
	);

	nextLengthCnt <= lineLengthCnt + 1;

	process(clock_i)
	begin
		if rising_edge(clock_i) then
			prescale <= prescale + 1;
			lineLengthCnt <= nextLengthCnt;

			if prescale(0) = '0' and hsync_i = '1' then
				if enable_i = '1' then
					writeIndex <= writeIndex + 1;
				end if;
			end if;

			if hSyncCount /= 0 then
				hSyncCount <= hSyncCount - 1;
			end if;

			if hSyncCount = 0 then
				readIndex <= readIndex + 1;
			end if;

			if lineLengthCnt = lineLength then
				readIndex <= startIndex;
				hSyncCount <= hSyncLength_g;
				prescale <= (others => '0');
				impar <= '1';
			end if;

			oldHSync <= hsync_i;
			if hsync_i = hsync_pol_g and oldHSync = not hsync_pol_g then
				-- Calculate length of the scanline/2
				-- The scandoubler adds a second sync half way to double the lines.
				lineLength <= lineLengthCnt((lineLengthBits+1) downto 1);
				lineLengthCnt <= to_unsigned(0, lineLengthBits+2);

				readIndex   <= endIndex;
				startIndex  <= endIndex;
				endIndex    <= writeIndex;
				hSyncCount  <= hSyncLength_g;
				prescale    <= (others => '0');
				impar			<= '0';

				oldVSync <= vsync_i;
				if vsync_i = vsync_pol_g and oldVSync = not vsync_pol_g then
					vSyncCount <= vSyncLength_g;
				elsif vSyncCount /= 0 then
					vSyncCount <= vSyncCount - 1;
				end if;
			end if;
		end if;
	end process;

	-- Video out
	process(clock_i)
	begin
		if rising_edge(clock_i) then
			ram_q_s	<= ram_s;
			video_o	<= ram_q_s;
			if vSyncCount /= 0 then 
				video_o <= (others => '0');
			elsif (scanlines_en_i = '1' and impar = '1') then
				video_o <= (others => '0');
			end if;
		end if;
	end process;

	-- Horizontal sync
	process(clock_i)
	begin
		if rising_edge(clock_i) then
			hsync_o  <= not hsync_pol_g;
			if hSyncCount /= 0 then
				hsync_o <= hsync_pol_g;
			end if;
		end if;
	end process;

	-- Vertical sync
	process(clock_i)
	begin
		if rising_edge(clock_i) then
			vsync_o <= not vsync_pol_g;
			if (vSyncCount = 9) or (vSyncCount = 10) then
				vsync_o <= vsync_pol_g;
			end if;
		end if;
	end process;

end architecture;

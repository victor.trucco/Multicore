--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
--
--
--

-- altera message_off 10540 10541

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Generic top-level entity for Altera DE2 board
entity de2_top is
	port (
		-- Clocks
		CLOCK_27       : in    std_logic;
		CLOCK_50       : in    std_logic;
		EXT_CLOCK      : in    std_logic;

		-- Switches
		SW             : in    std_logic_vector(17 downto 0);
		-- Buttons
		KEY            : in    std_logic_vector(3 downto 0);

		-- 7 segment displays
		HEX0           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX1           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX2           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX3           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX4           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX5           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX6           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		HEX7           : out   std_logic_vector(6 downto 0)		:= (others => '1');
		
		-- Red LEDs
		LEDR           : out   std_logic_vector(17 downto 0)		:= (others => '0');
		-- Green LEDs
		LEDG           : out   std_logic_vector(8 downto 0)		:= (others => '0');

		-- Serial
		UART_RXD       : in    std_logic;
		UART_TXD       : out   std_logic									:= '1';

		-- IRDA
		IRDA_RXD       : in    std_logic;
		IRDA_TXD       : out   std_logic									:= '0';

		-- SDRAM
		DRAM_ADDR      : out   std_logic_vector(11 downto 0)		:= (others => '0');
		DRAM_DQ        : inout std_logic_vector(15 downto 0)		:= (others => 'Z');
		DRAM_BA_0      : out   std_logic									:= '1';
		DRAM_BA_1      : out   std_logic									:= '1';
		DRAM_CAS_N     : out   std_logic									:= '1';
		DRAM_CKE       : out   std_logic									:= '1';
		DRAM_CLK       : out   std_logic									:= '1';
		DRAM_CS_N      : out   std_logic									:= '1';
		DRAM_LDQM      : out   std_logic									:= '1';
		DRAM_RAS_N     : out   std_logic									:= '1';
		DRAM_UDQM      : out   std_logic									:= '1';
		DRAM_WE_N      : out   std_logic									:= '1';

		-- Flash
		FL_ADDR        : out   std_logic_vector(21 downto 0)		:= (others => '0');
		FL_DQ          : inout std_logic_vector(7 downto 0)		:= (others => 'Z');
		FL_RST_N       : out   std_logic									:= '1';
		FL_OE_N        : out   std_logic									:= '1';
		FL_WE_N        : out   std_logic									:= '1';
		FL_CE_N        : out   std_logic									:= '1';

		-- SRAM
		SRAM_ADDR      : out   std_logic_vector(17 downto 0)		:= (others => '0');
		SRAM_DQ        : inout std_logic_vector(15 downto 0)		:= (others => 'Z');
		SRAM_CE_N      : out   std_logic									:= '1';
		SRAM_OE_N      : out   std_logic									:= '1';
		SRAM_WE_N      : out   std_logic									:= '1';
		SRAM_UB_N      : out   std_logic									:= '1';
		SRAM_LB_N      : out   std_logic									:= '1';

		--	ISP1362 Interface	
		OTG_ADDR       : out   std_logic_vector(1 downto 0)		:= (others => '0');	--	ISP1362 Address 2 Bits
		OTG_DATA       : inout std_logic_vector(15 downto 0)		:= (others => 'Z');	--	ISP1362 Data bus 16 Bits
		OTG_CS_N       : out   std_logic									:= '1';					--	ISP1362 Chip Select
		OTG_RD_N       : out   std_logic									:= '1';					--	ISP1362 Write
		OTG_WR_N       : out   std_logic									:= '1';					--	ISP1362 Read
		OTG_RST_N      : out   std_logic									:= '1';					--	ISP1362 Reset
		OTG_FSPEED     : out   std_logic									:= 'Z';					--	USB Full Speed,	0 = Enable, Z = Disable
		OTG_LSPEED     : out   std_logic									:= 'Z';					--	USB Low Speed, 	0 = Enable, Z = Disable
		OTG_INT0       : in    std_logic;															--	ISP1362 Interrupt 0
		OTG_INT1       : in    std_logic;															--	ISP1362 Interrupt 1
		OTG_DREQ0      : in    std_logic;															--	ISP1362 DMA Request 0
		OTG_DREQ1      : in    std_logic;															--	ISP1362 DMA Request 1
		OTG_DACK0_N    : out   std_logic									:= '1';					--	ISP1362 DMA Acknowledge 0
		OTG_DACK1_N    : out   std_logic									:= '1';					--	ISP1362 DMA Acknowledge 1
		
		--	LCD Module 16X2		
		LCD_ON         : out   std_logic									:= '0';					--	LCD Power ON/OFF, 0 = Off, 1 = On
		LCD_BLON       : out   std_logic									:= '0';					--	LCD Back Light ON/OFF, 0 = Off, 1 = On
		LCD_DATA       : inout std_logic_vector(7 downto 0)		:= (others => '0');	--	LCD Data bus 8 bits
		LCD_RW         : out   std_logic									:= '1';					--	LCD Read/Write Select, 0 = Write, 1 = Read
		LCD_EN         : out   std_logic									:= '1';					--	LCD Enable
		LCD_RS         : out   std_logic									:= '1';					--	LCD Command/Data Select, 0 = Command, 1 = Data
		
		--	SD_Card Interface	
		SD_DAT         : inout std_logic									:= 'Z';					--	SD Card Data (SPI MISO)
		SD_DAT3        : inout std_logic									:= 'Z';					--	SD Card Data 3 (SPI /CS)
		SD_CMD         : inout std_logic									:= 'Z';					--	SD Card Command Signal (SPI MOSI)
		SD_CLK         : out   std_logic									:= '1';					--	SD Card Clock (SPI SCLK)
		
		-- I2C
		I2C_SCLK       : inout std_logic									:= 'Z';
		I2C_SDAT       : inout std_logic									:= 'Z';

		-- PS/2 Keyboard
		PS2_CLK        : inout std_logic									:= 'Z';
		PS2_DAT        : inout std_logic									:= 'Z';

		-- VGA
		VGA_R          : out   std_logic_vector(9 downto 0)		:= (others => '0');
		VGA_G          : out   std_logic_vector(9 downto 0)		:= (others => '0');
		VGA_B          : out   std_logic_vector(9 downto 0)		:= (others => '0');
		VGA_HS         : out   std_logic									:= '0';
		VGA_VS         : out   std_logic									:= '0';
		VGA_BLANK		: out   std_logic									:= '1';				
		VGA_SYNC			: out   std_logic									:= '0';	
		VGA_CLK		   : out   std_logic									:= '0';	
		
		-- Ethernet Interface	
		ENET_CLK       : out   std_logic									:= '0';					--	DM9000A Clock 25 MHz
		ENET_DATA      : inout std_logic_vector(15 downto 0)		:= (others => 'Z');	--	DM9000A DATA bus 16Bits
		ENET_CMD       : out   std_logic									:= '0';					--	DM9000A Command/Data Select, 0 = Command, 1 = Data
		ENET_CS_N      : out   std_logic									:= '1';					--	DM9000A Chip Select
		ENET_WR_N      : out   std_logic									:= '1';					--	DM9000A Write
		ENET_RD_N      : out   std_logic									:= '1';					--	DM9000A Read
		ENET_RST_N     : out   std_logic									:= '1';					--	DM9000A Reset
		ENET_INT       : in    std_logic;															--	DM9000A Interrupt
	               
		-- Audio
		AUD_XCK        : out   std_logic									:= '0';
		AUD_BCLK       : out   std_logic									:= '0';
		AUD_ADCLRCK    : out   std_logic									:= '0';
		AUD_ADCDAT     : in    std_logic;
		AUD_DACLRCK    : out   std_logic									:= '0';
		AUD_DACDAT     : out   std_logic									:= '0';

		-- TV Decoder		
		TD_DATA        : in    std_logic_vector(7 downto 0);									--	TV Decoder Data bus 8 bits
		TD_HS          : in    std_logic;															--	TV Decoder H_SYNC
		TD_VS          : in    std_logic;															--	TV Decoder V_SYNC
		TD_RESET       : out   std_logic									:= '1';					--	TV Decoder Reset
	
		-- GPIO
		GPIO_0         : inout std_logic_vector(35 downto 0)		:= (others => 'Z');
		GPIO_1         : inout std_logic_vector(35 downto 0)		:= (others => 'Z')
	);
end entity;

architecture behavior of de2_top is

	-- PLL
	signal clock_master_s	: std_logic;
	signal clock_audio_s		: std_logic;
	signal pll_locked_s		: std_logic;

	-- Resets
	signal reset_por_s		: std_logic;
	signal reset_s				: std_logic;

	-- RAM
	signal ram_rd_s			: std_logic;
	signal ram_wr_s			: std_logic;
	signal ram_addr_s			: std_logic_vector(15 downto 0);
	signal ram_data_from_s	: std_logic_vector(7 downto 0);
	signal ram_data_to_s		: std_logic_vector(7 downto 0);

	-- ROM
--	signal rom_rd_s			: std_logic;
	signal rom_addr_s			: std_logic_vector(13 downto 0);
	signal rom_data_from_s	: std_logic_vector(7 downto 0);

	-- Audio
	signal sound_s				: std_logic_vector(1 downto 0);

	-- Video and scandoubler
	signal video_bit_s		: std_logic;
	signal video_hs_n_s		: std_logic;
	signal video_vs_n_s		: std_logic;
	signal video_hs_out_s	: std_logic;
	signal video_vs_out_s	: std_logic;
	signal scandoubler_en_s	: std_logic;
	signal video_bit_2x_s	: std_logic;

	-- Cassete
	signal cas500_s			: std_logic;
	signal cas1500_s			: std_logic;

	-- Teclado
	signal kb_rows_s			: std_logic_vector(7 downto 0);
	signal kb_columns_s		: std_logic_vector(7 downto 0);
	signal FKeys_s				: std_logic_vector(12 downto 1);		-- Function keys

	-- Joystick (Minimig standard)
--	alias J0_UP					: std_logic						is GPIO_1(34);
--	alias J0_DOWN				: std_logic						is GPIO_1(32);
--	alias J0_LEFT				: std_logic						is GPIO_1(30);
--	alias J0_RIGHT				: std_logic						is GPIO_1(28);
--	alias J0_BTN				: std_logic						is GPIO_1(35);
--	alias J1_UP					: std_logic						is GPIO_1(24);
--	alias J1_DOWN				: std_logic						is GPIO_1(22);
--	alias J1_LEFT				: std_logic						is GPIO_1(20);
--	alias J1_RIGHT				: std_logic						is GPIO_1(23);
--	alias J1_BTN				: std_logic						is GPIO_1(25);

	-- Debug
	signal D_display1_s			: std_logic_vector(15 downto 0);
	signal D_display2_s			: std_logic_vector(15 downto 0);
	signal D_cpu_a_s				: std_logic_vector(15 downto 0);
	signal D_imgnum_s				: std_logic_vector(15 downto 0);
	signal D_track_num_s			: std_logic_vector( 5 downto 0);
	signal D_track_ram_addr_s	: std_logic_vector(12 downto 0);

begin

	--------------------------------
	-- PLL
	--------------------------------
	pll: entity work.pll1
	port map (
		inclk0		=> CLOCK_50,
		c0				=> clock_master_s,				-- 20.238095 MHz
		locked		=> pll_locked_s
	);

	pllaudio: entity work.pll2
	port map (
		inclk0		=> CLOCK_27,
		c0				=> clock_audio_s					-- 24.000000 MHz
	);

	----------------
	-- TRS-80 Model 3
	----------------
	trs80_inst: entity work.trs80
	generic map (
		use_az80_g			=> true
	)
	port map (
		clock_i				=> clock_master_s,
		por_i					=> reset_por_s,
		reset_i				=> reset_s,
		-- Options
		opt_floppy_i		=> SW(10),
		-- RAM
		ram_addr_o			=> ram_addr_s,
		ram_data_to_o		=> ram_data_to_s,
		ram_data_from_i	=> ram_data_from_s,
		ram_rd_o				=> ram_rd_s,
		ram_wr_o				=> ram_wr_s,
		-- ROM
		rom_addr_o			=> rom_addr_s,
		rom_data_from_i	=> rom_data_from_s,
		rom_rd_o				=> open,
		-- Video
		video_bit_o			=> video_bit_s,
		video_hs_n_o		=> video_hs_n_s,
		video_vs_n_o		=> video_vs_n_s,
		-- Audio
		sound_o				=> sound_s,
		-- Cassete
		cas500_i				=> cas500_s,
		cas1500_i			=> cas1500_s,
		-- Teclado
		kb_rows_o			=> kb_rows_s,
		kb_columns_i		=> kb_columns_s,
		-- Barramento
		bus_a_o				=> open,
		bus_d_io				=> open,
		bus_in_n_o			=> open,
		bus_out_n_o			=> open,
		bus_reset_n_o		=> open,
		bus_int_n_i			=> '1',
		bus_wait_n_i		=> '1',
		bus_extiosel_n_i	=> '1',
		bus_m1_n_o			=> open,
		bus_iorq_n_o		=> open,	
		bus_enextio_n_o	=> open,
		-- Img and SD card
		image_num_i			=> SW(9 downto 0),
		sd_cs_n_o			=> SD_DAT3,
		sd_miso_i			=> SD_DAT,
		sd_mosi_o			=> SD_CMD,
		sd_sclk_o			=> SD_CLK,
		-- Debug
		D_cpu_a_o			=> D_cpu_a_s,
		D_track_num_o		=> D_track_num_s,
		D_track_ram_addr_o => D_track_ram_addr_s,
		D_track_ram_we_o	=> LEDG(1),
		D_error_o			=> LEDG(0)
	);

	----------------
	-- Scandoubler com scanlines
	----------------
	scandbl: entity work.scandoubler
	generic map (
		hsync_polarity_g	=> '0',
		vsync_polarity_g	=> '0'
	)
	port map(
		clock_i				=> clock_master_s,		-- 2x pixel clock
		enable_i				=> scandoubler_en_s,		-- Scandoubler ativado
		scanlines_i			=> '0',						-- Scanlines nao usado
		video_i				=> video_bit_s,
		hsync_i				=> video_hs_n_s,
		vsync_i				=> video_vs_n_s,
		video_o				=> video_bit_2x_s,
		hsync_o				=> video_hs_out_s,
		vsync_o				=> video_vs_out_s
	);

	----------------
	-- Gerenciador de audio com CODEC WM8731
	----------------
	sound_inst: entity work.Audio_WM8731
	port map (
		reset_i			=> reset_s,
		clock_i			=> clock_audio_s,						-- Clock 24 MHz
		spk_i				=> sound_s,
		cas500_o			=> cas500_s,
		cas1500_o		=> cas1500_s,

		i2s_xck_o		=>	AUD_XCK,
		i2s_bclk_o		=> AUD_BCLK,
		i2s_adclrck_o	=> AUD_ADCLRCK,
		i2s_adcdat_i	=> AUD_ADCDAT,
		i2s_daclrck_o	=> AUD_DACLRCK,
		i2s_dacdat_o	=> AUD_DACDAT,
		
		i2c_sda_io		=> I2C_SDAT,
		i2c_scl_io		=> I2C_SCLK
	);

	---------------
	-- ROM
	---------------
	m3_rom: entity work.rom
	port map (
		clk	=> clock_master_s,
		addr	=> rom_addr_s,
		data	=> rom_data_from_s
	);

	---------------
	-- Teclado
	---------------
	teclado: entity work.keyboard
	generic map (
		clkfreq		=> 20238
	)
	port map (
		clock_i		=> clock_master_s,
		por_i			=> reset_por_s,
		reset_i		=> reset_s,
		ps2_clk_io	=> PS2_CLK,
		ps2_data_io	=> PS2_DAT,
		rows_i		=> kb_rows_s,
		cols_o		=> kb_columns_s,
		teclasF_o	=> FKeys_s
	);

	
	-- glue
	reset_por_s	<= not pll_locked_s;
	reset_s		<= not pll_locked_s or not KEY(0) or FKeys_s(4);		-- F4 gera reset

	-- RAM
	SRAM_ADDR			<= "00" & ram_addr_s;
	SRAM_DQ				<= "00000000" & ram_data_to_s		when ram_wr_s = '1' 	else (others => 'Z');
	ram_data_from_s	<= SRAM_DQ(7 downto 0)				when ram_rd_s = '1' 	else (others => '1');
	SRAM_UB_N			<= '1';
	SRAM_LB_N			<= not (ram_rd_s or ram_wr_s);
	SRAM_CE_N			<= not (ram_rd_s or ram_wr_s);
	SRAM_OE_N			<= not ram_rd_s;
	SRAM_WE_N			<= not ram_wr_s;

	-- Video
	scandoubler_en_s	<= SW(11);

	VGA_R			<= (others => video_bit_s) when scandoubler_en_s = '0' else (others => video_bit_2x_s);
	VGA_G			<= (others => video_bit_s) when scandoubler_en_s = '0' else (others => video_bit_2x_s);
	VGA_B			<= (others => video_bit_s) when scandoubler_en_s = '0' else (others => video_bit_2x_s);
	VGA_HS		<= video_hs_n_s				when scandoubler_en_s = '0' else video_hs_out_s;
	VGA_VS		<= video_vs_n_s				when scandoubler_en_s = '0' else video_vs_out_s;
	VGA_BLANK	<= '1';
	VGA_CLK		<= clock_master_s;

	-----------------------------------------------------
	-- Debug
	-----------------------------------------------------

	D_imgnum_s		<= "000000" & SW(9 downto 0);
--	D_display1_s	<= D_imgnum_s;
--	D_display1_s	<= D_cpu_a_s;
	D_display1_s	<= "000" & D_track_ram_addr_s;
	D_display2_s	<= "0000000000" & D_track_num_s;
	
	ld3: entity work.seg7
	port map(
		D		=> D_display1_s(15 downto 12),
		Q		=> HEX3
	);

	ld2: entity work.seg7
	port map(
		D		=> D_display1_s(11 downto 8),
		Q		=> HEX2
	);

	ld1: entity work.seg7
	port map(
		D		=> D_display1_s(7 downto 4),
		Q		=> HEX1
	);

	ld0: entity work.seg7
	port map(
		D		=> D_display1_s(3 downto 0),
		Q		=> HEX0
	);

	ld7: entity work.seg7
	port map(
		D		=> D_display2_s(15 downto 12),
		Q		=> HEX7
	);

	ld6: entity work.seg7
	port map(
		D		=> D_display2_s(11 downto 8),
		Q		=> HEX6
	);

	ld5: entity work.seg7
	port map(
		D		=> D_display2_s(7 downto 4),
		Q		=> HEX5
	);

	ld4: entity work.seg7
	port map(
		D		=> D_display2_s(3 downto 0),
		Q		=> HEX4
	);

end architecture;

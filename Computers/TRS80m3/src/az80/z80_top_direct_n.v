/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///============================================================================
// Z80 Top level using the direct module declaration
//============================================================================
`timescale 1us/ 100 ns

module z80_top_direct_n(
    output wire nM1,
    output wire nMREQ,
    output wire nIORQ,
    output wire nRD,
    output wire nWR,
    output wire nRFSH,
    output wire nHALT,
    output wire nBUSACK,

    input wire nWAIT,
    input wire nINT,
    input wire nNMI,
    input wire nRESET,
    input wire nBUSRQ,

    input wire CLK,
    output wire [15:0] A,
    inout wire [7:0] D
);

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Include core A-Z80 level connecting all internal modules
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
`include "core.vh"

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Address, Data and Control bus drivers connecting to external pins
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
address_pins address_pins_(
    .clk            (clk),
    .bus_ab_pin_we  (bus_ab_pin_we),
    .pin_control_oe (pin_control_oe),
    .address        (address),
    .abus           (A)
);

data_pins data_pins_(
    .bus_db_pin_oe  (bus_db_pin_oe),
    .bus_db_pin_re  (bus_db_pin_re),
    .ctl_bus_db_we  (ctl_bus_db_we),
    .ctl_bus_db_oe  (ctl_bus_db_oe),
    .clk            (clk),
    .db             (db0),
    .D              (D)
);

control_pins_n control_pins_(
    .busack        (busack),
    .CPUCLK        (CLK),
    .pin_control_oe(pin_control_oe),
    .in_halt       (in_halt),
    .pin_nWAIT     (nWAIT),
    .pin_nBUSRQ    (nBUSRQ),
    .pin_nINT      (nINT),
    .pin_nNMI      (nNMI),
    .pin_nRESET    (nRESET),
    .nM1_out       (nM1_out),
    .nRFSH_out     (nRFSH_out),
    .nRD_out       (nRD_out),
    .nWR_out       (nWR_out),
    .nIORQ_out     (nIORQ_out),
    .nMREQ_out     (nMREQ_out),
    .nmi           (nmi),
    .busrq         (busrq),
    .clk           (clk),
    .intr          (intr),
    .mwait         (mwait),
    .reset_in      (reset_in),
    .pin_nM1       (nM1),
    .pin_nMREQ     (nMREQ),
    .pin_nIORQ     (nIORQ),
    .pin_nRD       (nRD),
    .pin_nWR       (nWR),
    .pin_nRFSH     (nRFSH),
    .pin_nHALT     (nHALT),
    .pin_nBUSACK   (nBUSACK)
 );

endmodule

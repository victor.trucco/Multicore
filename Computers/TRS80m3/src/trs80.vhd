--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
--
-- Copyright (c) 2015 Fabio Belavenuto
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity trs80 is
	generic (
		use_az80_g			: boolean					:= false
	);
	port (
		clock_i				: in    std_logic;								-- Clock 20.238095 MHz
		por_i					: in    std_logic;
		reset_i				: in    std_logic;
		-- Options
		opt_floppy_i		: in    std_logic;
		-- RAM
		ram_addr_o			: out   std_logic_vector(15 downto 0);
		ram_data_from_i	: in    std_logic_vector( 7 downto 0);
		ram_data_to_o		: out   std_logic_vector( 7 downto 0);
		ram_rd_o				: out   std_logic;
		ram_wr_o				: out   std_logic;
		-- ROM
		rom_addr_o			: out   std_logic_vector(13 downto 0);
		rom_data_from_i	: in    std_logic_vector( 7 downto 0);
		rom_rd_o				: out   std_logic;
		-- Video
		video_bit_o			: out   std_logic;
		video_hs_n_o		: out   std_logic;
		video_vs_n_o		: out   std_logic;
		pix_hcnt_o			: out   std_logic_vector(9 downto 0);
		pix_vcnt_o			: out   std_logic_vector(8 downto 0);
		-- Audio
		sound_o				: out   std_logic_vector( 1 downto 0);	
		-- Cassete
		cas500_i				: in    std_logic;
		cas1500_i			: in    std_logic;
		-- Keyboard
		kb_rows_o			: out   std_logic_vector( 7 downto 0);
		kb_columns_i		: in    std_logic_vector( 7 downto 0);
		-- Bus
		bus_a_o				: out   std_logic_vector( 7 downto 0);
		bus_d_io				: inout std_logic_vector( 7 downto 0);
		bus_in_n_o			: out   std_logic;
		bus_out_n_o			: out   std_logic;
		bus_reset_n_o		: out   std_logic;
		bus_int_n_i			: in    std_logic;
		bus_wait_n_i		: in    std_logic;
		bus_extiosel_n_i	: in    std_logic;
		bus_m1_n_o			: out   std_logic;
		bus_iorq_n_o		: out   std_logic;
		bus_enextio_n_o	: out   std_logic;
		-- Img and SD card
		image_num_i			: in    std_logic_vector( 9 downto 0);
		sd_cs_n_o			: out   std_logic;
		sd_miso_i			: in    std_logic;
		sd_mosi_o			: out   std_logic;
		sd_sclk_o			: out   std_logic;
		-- Debug
		D_cpu_a_o			: out   std_logic_vector(15 downto 0);
		D_track_num_o		: out   std_logic_vector( 5 downto 0);
		D_track_ram_addr_o: out   std_logic_vector(12 downto 0);
		D_track_ram_we_o	: out std_logic;
		D_error_o			: out   std_logic
	);
end;

architecture Behavior of trs80 is

	component z80_top_direct_n is
	port (
		nM1		: out   std_logic;
		nMREQ		: out   std_logic;
		nIORQ		: out   std_logic;
		nRD		: out   std_logic;
		nWR		: out   std_logic;
		nRFSH		: out   std_logic;
		nHALT		: out   std_logic;
		nBUSACK	: out   std_logic;
		nWAIT		: in    std_logic;
		nINT		: in    std_logic;
		nNMI		: in    std_logic;
		nRESET	: in    std_logic;
		nBUSRQ	: in    std_logic;
		CLK		: in    std_logic;
		A			: out   std_logic_vector(15 downto 0);
		D			: inout std_logic_vector( 7 downto 0)
	);
	end component;

	-- Clocks
	signal clock_video_s		: std_logic;					-- Video: 10.21 MHz
	signal clock_cpu_s		: std_logic;					-- 2 MHz

	-- Reset
	signal reset_n_s			: std_logic;

	-- CPU signals
	signal cpu_addr_s			: std_logic_vector(15 downto 0);
	signal cpu_d_s				: std_logic_vector( 7 downto 0);
	signal cpu_data_to_s		: std_logic_vector( 7 downto 0);
	signal cpu_data_from_s	: std_logic_vector( 7 downto 0);
	signal cpu_wait_n_s		: std_logic								:= '1';
	signal cpu_irq_n_s		: std_logic;
	signal cpu_nmi_n_s		: std_logic;
	signal cpu_busreq_n_s	: std_logic;
	signal cpu_m1_n_s			: std_logic;
	signal cpu_mreq_n_s		: std_logic;
	signal cpu_iorq_n_s		: std_logic;
	signal cpu_rd_n_s			: std_logic;
	signal cpu_wr_n_s			: std_logic;

	-- Memory buses
	signal vram_addr_s		: std_logic_vector( 9 downto 0);
	signal vram_dout_s		: std_logic_vector( 7 downto 0);
	signal vram_cpu_dout_s	: std_logic_vector( 7 downto 0);
	signal vram_we_s			: std_logic;
	signal vram_en_s			: std_logic;
	signal ram_en_s			: std_logic;
	signal rom_en_s			: std_logic;

	-- I/O selects
	signal io_en_s					: std_logic;
	signal RdIntStatus_en_s		: std_logic;
	signal RdNmiStatus_en_s		: std_logic;
	signal Rs232In_en_s			: std_logic;
	signal RtcIn_en_s				: std_logic;
	signal DiskIn_en_s			: std_logic;
	signal ax04_en_s				: std_logic;
	signal LpIn_en_s				: std_logic;
	signal CasIn_en_s				: std_logic;
	signal WrIntMaskReg_en_s	: std_logic;
	signal WrNmiMaskReg_en_s	: std_logic;
	signal Rs232Out_en_s			: std_logic;
	signal ModOut_en_s			: std_logic;
	signal DiskOut_en_s			: std_logic;
	signal DrvSel_en_s			: std_logic;
	signal LpOut_en_s				: std_logic;
	signal CasOut_en_s			: std_logic;

	-- I/O regs
	signal RdIntStatus_r		: std_logic_vector( 7 downto 0);
	signal Rs232In_r			: std_logic_vector( 7 downto 0);
--	signal DiskIn_r			: std_logic_vector( 7 downto 0);
	signal LpIn_r				: std_logic_vector( 7 downto 0);
	signal CasIn_r				: std_logic_vector( 7 downto 0);
	signal WrIntMaskReg_r	: std_logic_vector( 6 downto 0);
	 alias EnRTC_a          : std_logic								is WrIntMaskReg_r(2);
	signal WrNmiMaskReg_r	: std_logic_vector( 7 downto 6);
	signal Rs232Out_r			: std_logic_vector( 7 downto 0);
	signal ModOut_r			: std_logic_vector( 5 downto 1);
	signal LpOut_r				: std_logic_vector( 7 downto 0);
	signal CasOut_r			: std_logic_vector( 1 downto 0);

	-- RTC tick
	signal rtc_vdrv_s			: std_logic;
	signal rtc_int_status_s	: std_logic;
	signal rtc_int_n_s		: std_logic;

	-- Cassete
	signal cas500_bit_s		: std_logic;
	signal casintf_s			: std_logic;
	signal casintr_s			: std_logic;
	signal cas_int_n_s		: std_logic;
	
	-- Barramento
	signal bus_int_n_s		: std_logic;
	signal bus_wait_n_s		: std_logic;

	-- Floppy
	signal disk_in_n_s		: std_logic;
	signal disk_out_n_s		: std_logic;
	signal fdc_data_from_s	: std_logic_vector( 7 downto 0);
	signal fdc_nmi_n_s		: std_logic;
	signal fdc_wait_n_s		: std_logic;

	-- Teclado
	signal kb_en_s				: std_logic;

	-- Video
	signal video_bit_s		: std_logic;
	signal video_hs_n_s		: std_logic;
	signal video_vs_n_s		: std_logic;

	-- AX04
	signal ax04_q				: std_logic;

begin

	---------
	-- CPU
	---------
	ut80: if not use_az80_g generate
		cpu: entity work.T80a
		generic map (
			Mode 		=> 0
		)
		port map (
			RESET_n	=> reset_n_s,
			CLK_n		=> clock_cpu_s,
			A			=> cpu_addr_s,
			DI			=> cpu_data_to_s,
			DO			=> cpu_data_from_s,
			WAIT_n	=> cpu_wait_n_s,
			INT_n		=> cpu_irq_n_s,
			NMI_n		=> cpu_nmi_n_s,
			M1_n		=> cpu_m1_n_s,
			MREQ_n	=> cpu_mreq_n_s,
			IORQ_n	=> cpu_iorq_n_s,
			RD_n		=> cpu_rd_n_s,
			WR_n		=> cpu_wr_n_s,
			RFSH_n	=> open,
			HALT_n	=> open,
			BUSRQ_n	=> '1',
			BUSAK_n	=> open
		);
	end generate;

	uaz80: if use_az80_g generate
		cpu: z80_top_direct_n 
		port map (
			nRESET   => reset_n_s,
			CLK      => clock_cpu_s,
			A        => cpu_addr_s,
			D        => cpu_d_s,
			nWAIT    => cpu_wait_n_s,
			nINT     => cpu_irq_n_s,
			nNMI     => cpu_nmi_n_s,
			nM1      => cpu_m1_n_s,
			nMREQ    => cpu_mreq_n_s,
			nIORQ    => cpu_iorq_n_s,
			nRD      => cpu_rd_n_s,
			nWR      => cpu_wr_n_s,
			nRFSH    => open,
			nHALT    => open,
			nBUSRQ   => '1',
			nBUSACK  => open
		);
		cpu_d_s				<= cpu_data_to_s when cpu_rd_n_s = '0' else (others=>'Z');
		cpu_data_from_s	<= cpu_d_s;
	end generate;

	----------------
	-- VRAM
	----------------
	vram: entity work.dpram
	generic map (
		addr_width_g	=> 10
	)
	port map (
		clk_a_i			=> clock_i,
		we_i				=> vram_we_s,
		addr_a_i			=> cpu_addr_s(9 downto 0),
		data_a_i			=> cpu_data_from_s,
		data_a_o			=> vram_cpu_dout_s,

		clk_b_i			=> clock_video_s,
		addr_b_i			=> vram_addr_s,
		data_b_o			=> vram_dout_s
	);

	----------------
	-- Gerador de video
	----------------
	gvideo: entity work.trs_video
	port map (
		clock_i			=> clock_video_s,
		reset_i			=> reset_i,
		modsel_i			=> ModOut_r(2),
		enaltset_i		=> ModOut_r(3),
--		vram_cs_i		=> vram_en_s,
		vram_d_i			=> vram_dout_s,
		vram_a_o			=> vram_addr_s,
		video_bit_o		=> video_bit_s,
		video_hs_n_o	=> video_hs_n_s,
		video_vs_n_o	=> video_vs_n_s,
		pix_hcnt_o		=> pix_hcnt_o,
		pix_vcnt_o		=> pix_vcnt_o
	);

	-- FDC
	fdc: entity work.intf_floppy
	port map (
		clock_i				=> clock_i,
		clock_ena_i			=> '1',
		clock_cpu_i			=> clock_cpu_s,
		enable_i				=> opt_floppy_i,
		reset_n_i			=> reset_n_s,
		disk_in_n_i			=> disk_in_n_s,
		disk_out_n_i		=> disk_out_n_s,
		data_i				=> cpu_data_from_s,
		data_o				=> fdc_data_from_s,
		address_i			=> cpu_addr_s(1 downto 0),
		WrNmiMaskReg_en_i	=> WrNmiMaskReg_en_s,
		RdNmiStatus_en_i	=> RdNmiStatus_en_s,
		DrvSel_en_i			=> DrvSel_en_s,
		nmi_n_o				=> fdc_nmi_n_s,
		wait_n_o				=> fdc_wait_n_s,
		-- SD card
		image_num_i			=> image_num_i,
		sd_cs_n_o			=> sd_cs_n_o,
		sd_miso_i			=> sd_miso_i,
		sd_mosi_o			=> sd_mosi_o,
		sd_sclk_o			=> sd_sclk_o,
		D_track_num_o		=> D_track_num_o,
		D_track_ram_addr_o => D_track_ram_addr_o,
		D_track_ram_we_o	=> D_track_ram_we_o,
		D_error_o			=> D_error_o
	);

	-- Clock video ( 10 MHz )
	process (reset_i, clock_i)
	begin
		if reset_i = '1' then
			clock_video_s  <= '0';
		elsif rising_edge(clock_i) then
			clock_video_s	<= not clock_video_s;
		end if;
	end process;

	-- Clock CPU ( 2 MHz )
	process(clock_i)
		variable cnt : integer range 0 to 4 := 0;
	begin
		if rising_edge(clock_i) then

			if cnt = 4 then
				clock_cpu_s <= not clock_cpu_s;
				cnt := 0;
			else
				cnt := cnt + 1;
			end if;

		end if;
	end process;

	-- RTC int
	process(reset_i, video_vs_n_s)
	begin
		if reset_i = '1' then
			rtc_vdrv_s	<= '0';
		elsif falling_edge(video_vs_n_s) then
			rtc_vdrv_s <= not rtc_vdrv_s;
		end if;
	end process;

	process(RtcIn_en_s, rtc_vdrv_s, EnRTC_a)
	begin
		if RtcIn_en_s = '1' or EnRTC_a = '0' then
			rtc_int_status_s	<= '1';
		elsif falling_edge(rtc_vdrv_s) then
			rtc_int_status_s	<= '0';
		end if;
	end process;
	
	-- Cassete 1500 baud
	process(CasIn_en_s, cas1500_i)
	begin
		if CasIn_en_s = '1' then
			casintf_s	<= '1';
		elsif falling_edge(cas1500_i) then
			casintf_s	<= not WrIntMaskReg_r(1);
		end if;
	end process;

	process(CasIn_en_s, cas1500_i)
	begin
		if CasIn_en_s = '1' then
			casintr_s	<= '1';
		elsif rising_edge(cas1500_i) then
			casintr_s	<= not WrIntMaskReg_r(0);
		end if;
	end process;

	-- Cassete 500 baud
	process(CasOut_en_s, cas500_i)
	begin
		if CasOut_en_s = '1' then
			cas500_bit_s	<= '0'; 
		elsif falling_edge(cas500_i) then
			cas500_bit_s	<= '1';
		end if;
	end process;

	-- Reset e CPU
	reset_n_s		<= not reset_i;
	cpu_irq_n_s		<= rtc_int_n_s and cas_int_n_s and bus_int_n_s;
--	cpu_irq_n_s		<= '1';
	cpu_nmi_n_s		<= fdc_nmi_n_s;
	cpu_wait_n_s	<= bus_wait_n_s and fdc_wait_n_s;

	-- Selecao das memoria
	rom_en_s		<= '1' when cpu_mreq_n_s = '0' and cpu_rd_n_s = '0' and cpu_addr_s < X"3800"								else '0';		-- Read de 0000 a 37FF
	kb_en_s		<= '1' when cpu_mreq_n_s = '0' and cpu_rd_n_s = '0' and cpu_addr_s(15 downto 10) = "001110"			else '0';		-- Read de 3800 a 3BFF
	vram_en_s	<= '1' when cpu_mreq_n_s = '0' and                      cpu_addr_s(15 downto 10) = "001111"			else '0';		-- R/W  de 3C00 a 3FFF
	ram_en_s		<= '1' when cpu_mreq_n_s = '0' and                      cpu_addr_s(15 downto 14) /= "00"				else '0';		-- R/W  de 4000 a FFFF

	-- Selecao de I/O
	io_en_s				<= '1' when cpu_m1_n_s = '1' and cpu_iorq_n_s = '0'												else '0';

	RdIntStatus_en_s	<= '1' when io_en_s = '1' and cpu_rd_n_s = '0' and cpu_addr_s(7 downto 2) = X"E" & "00"		else '0';	-- E0..E3
	RdNmiStatus_en_s	<= '1' when io_en_s = '1' and cpu_rd_n_s = '0' and cpu_addr_s(7 downto 2) = X"E" & "01"		else '0';	-- E4..E7
	Rs232In_en_s		<= '1' when io_en_s = '1' and cpu_rd_n_s = '0' and cpu_addr_s(7 downto 2) = X"E" & "10"		else '0';	-- E8..EB
	RtcIn_en_s			<= '1' when io_en_s = '1' and cpu_rd_n_s = '0' and cpu_addr_s(7 downto 2) = X"E" & "11"		else '0';	-- EC..EF
	DiskIn_en_s			<= '1' when io_en_s = '1' and cpu_rd_n_s = '0' and cpu_addr_s(7 downto 2) = X"F" & "00"		else '0';	-- F0..F3
	ax04_en_s			<= '1' when io_en_s = '1' and cpu_rd_n_s = '0' and cpu_addr_s(7 downto 2) = X"F" & "01"		else '0';	-- F4..F7
	LpIn_en_s			<= '1' when io_en_s = '1' and cpu_rd_n_s = '0' and cpu_addr_s(7 downto 2) = X"F" & "10"		else '0';	-- F8..FB
	CasIn_en_s			<= '1' when io_en_s = '1' and cpu_rd_n_s = '0' and cpu_addr_s(7 downto 2) = X"F" & "11"		else '0';	-- FC..FF

	WrIntMaskReg_en_s	<= '1' when io_en_s = '1' and cpu_wr_n_s = '0' and cpu_addr_s(7 downto 2) = X"E" & "00"		else '0';	-- E0..E3
	WrNmiMaskReg_en_s	<= '1' when io_en_s = '1' and cpu_wr_n_s = '0' and cpu_addr_s(7 downto 2) = X"E" & "01"		else '0';	-- E4..E7
	Rs232Out_en_s		<= '1' when io_en_s = '1' and cpu_wr_n_s = '0' and cpu_addr_s(7 downto 2) = X"E" & "10"		else '0';	-- E8..EB
	ModOut_en_s			<= '1' when io_en_s = '1' and cpu_wr_n_s = '0' and cpu_addr_s(7 downto 2) = X"E" & "11"		else '0';	-- EC..EF
	DiskOut_en_s		<= '1' when io_en_s = '1' and cpu_wr_n_s = '0' and cpu_addr_s(7 downto 2) = X"F" & "00"		else '0';	-- F0..F3
	DrvSel_en_s			<= '1' when io_en_s = '1' and cpu_wr_n_s = '0' and cpu_addr_s(7 downto 2) = X"F" & "01"		else '0';	-- F4..F7
	LpOut_en_s			<= '1' when io_en_s = '1' and cpu_wr_n_s = '0' and cpu_addr_s(7 downto 2) = X"F" & "10"		else '0';	-- F8..FB
	CasOut_en_s			<= '1' when io_en_s = '1' and cpu_wr_n_s = '0' and cpu_addr_s(7 downto 2) = X"F" & "11"		else '0';	-- FC..FF

	-- Registros I/O
	RdIntStatus_r		<= "1111" & bus_int_n_i & rtc_int_status_s & casintf_s & casintr_s;		-- 0 = acontecendo IRQ
	Rs232In_r			<= (others => '1');
	LpIn_r				<= (others => '1');
	CasIn_r				<= cas500_bit_s & '1' & ModOut_r & cas1500_i;

	-- Escritas I/O
	process (reset_i, cpu_data_from_s, WrIntMaskReg_en_s, Rs232Out_en_s,
				ModOut_en_s, LpOut_en_s, CasOut_en_s)
	begin
		if reset_i = '1' then
			WrIntMaskReg_r	<= (others => '0');
			WrNmiMaskReg_r	<= (others => '0');
			Rs232Out_r		<= (others => '0');
			ModOut_r			<= (others => '0');
			LpOut_r			<= (others => '0');
			CasOut_r			<= (others => '0');
		elsif WrIntMaskReg_en_s = '1' then
			WrIntMaskReg_r	<= cpu_data_from_s(6 downto 0);
		elsif Rs232Out_en_s = '1' then
			Rs232Out_r		<= cpu_data_from_s;
		elsif ModOut_en_s = '1' then
			ModOut_r			<= cpu_data_from_s(5 downto 1);
		elsif LpOut_en_s = '1' then
			LpOut_r			<= cpu_data_from_s;
		elsif CasOut_en_s = '1' then
			CasOut_r			<= cpu_data_from_s(1 downto 0);
		end if;
	end process;

	-- AX04
	process (reset_i, ax04_en_s)
	begin
		if reset_i = '1' then
			ax04_q	<= '0';
		elsif falling_edge(ax04_en_s) then
			ax04_q	<= not ax04_q;
		end if;
	end process;

	-- Conexoes dos barramentos
	ram_data_to_o	<= cpu_data_from_s;
	cpu_data_to_s <=
		-- Memory
		rom_data_from_i			when rom_en_s           = '1'			else		-- Leitura da ROM
		kb_columns_i				when kb_en_s            = '1'			else		-- Leitura do teclado
		vram_cpu_dout_s			when vram_en_s          = '1'			else		-- R/W da VRAM
		ram_data_from_i			when ram_en_s           = '1'			else		-- R/W da RAM
		-- I/O
		RdIntStatus_r 				when RdIntStatus_en_s   = '1'			else		-- Leitura das flags de interrupcoes
		Rs232In_r					when Rs232In_en_s       = '1'			else		-- Leitura da interface RS-232 (nao implementado)
		fdc_data_from_s			when RdNmiStatus_en_s   = '1'			else		-- Leitura das flags de NMI
		fdc_data_from_s			when DiskIn_en_s        = '1'			else		-- Interface Floppy
		LpIn_r						when LpIn_en_s          = '1'			else		-- Leitura Porta Paralela (nao implementado)
		CasIn_r						when CasIn_en_s         = '1'			else		-- Leitura Cassete
		bus_d_io						when bus_extiosel_n_i   = '0'			else		-- Leitura barramento
		(others => '1');

	-- ROM
	rom_addr_o	<= cpu_addr_s(13 downto 0)													when cpu_addr_s < X"3000"	else
						cpu_addr_s(13 downto 12) & ax04_q & cpu_addr_s(10 downto 0);

	rom_rd_o		<= '1'	when rom_en_s = '1' and cpu_rd_n_s = '0'		else '0';

	-- VRAM
	vram_we_s	<= '1'	when vram_en_s = '1' and cpu_wr_n_s = '0'		else '0';

	-- RAM
	ram_addr_o	<= cpu_addr_s(15 downto 0);
	ram_rd_o		<= '1'	when ram_en_s = '1' and cpu_rd_n_s = '0'		else '0';
	ram_wr_o		<= '1'	when ram_en_s = '1' and cpu_wr_n_s = '0'		else '0';

	-- Audio
	sound_o		<= CasOut_r;

	-- Teclado
	kb_rows_o	<= cpu_addr_s(7 downto 0);

	-- RTC int
	rtc_int_n_s <= '0' when rtc_int_status_s = '0' and EnRTC_a = '1'	else '1';
--	rtc_int_n_s <= '1';

	-- Cassete
	cas_int_n_s	<= casintr_s and casintf_s;
	
	-- Floppy
--	clock_fdc_o		<= clock_20m_s;
--	WrNmiMaskReg_o	<= WrNmiMaskReg_r;
--	diskout_cs_o	<= diskOut_en_s;
--	drvsel_cs_o		<= drvSel_en_s;
	
	-- Barramento
	-- WrIntMaskReg_r(3) = ENIOBUSINT
	-- ModOut_r(4)       = ENEXTIO
	bus_a_o				<= cpu_addr_s(7 downto 0);
	bus_d_io				<= cpu_data_from_s	when bus_extiosel_n_i = '1'	else (others => 'Z');
	bus_reset_n_o		<= reset_n_s;
	bus_m1_n_o			<= cpu_m1_n_s;
	bus_iorq_n_o		<= cpu_iorq_n_s;
	bus_enextio_n_o	<= not ModOut_r(4);
	bus_in_n_o			<= cpu_iorq_n_s or cpu_rd_n_s;
	bus_out_n_o			<= cpu_iorq_n_s or cpu_wr_n_s;
	bus_int_n_s			<= '0' 	when WrIntMaskReg_r(3) = '1' and bus_int_n_i = '0'		else '1';
	bus_wait_n_s		<= '0' 	when ModOut_r(4) = '1' and bus_wait_n_i = '0'			else '1';

	-- Video
	video_bit_o		<= video_bit_s;
	video_hs_n_o	<= video_hs_n_s;
	video_vs_n_o	<= video_vs_n_s;

	-- FDC
	disk_in_n_s		<= not DiskIn_en_s;
	disk_out_n_s	<= not DiskOut_en_s;

	-- Debug
	D_cpu_a_o		<= cpu_addr_s;

end architecture;
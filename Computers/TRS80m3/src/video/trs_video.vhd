--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
--
-- Copyright (c) 2015 Fabio Belavenuto
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

library ieee;
use ieee.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity trs_video is
	port (
		clock_i			: in  std_logic;								-- 10.21 MHz
		reset_i			: in  std_logic;
		modsel_i			: in  std_logic;								-- 64 cols if '0' or 32 cols if '1'
		enaltset_i		: in  std_logic;								-- enable alternative charset if '1'
		vram_d_i			: in  std_logic_vector(7 downto 0);
		vram_a_o			: out std_logic_vector(9 downto 0);
		video_bit_o		: out std_logic;
		video_hs_n_o	: out std_logic;
		video_vs_n_o	: out std_logic;
		pix_hcnt_o		: out std_logic_vector(9 downto 0);
		pix_vcnt_o		: out std_logic_vector(8 downto 0)
	);
end entity;

architecture Behavior of trs_video is

	signal clock_n_s				: std_logic;
	signal tilerom_addr_s		: std_logic_vector(10 downto 0)	:= (others => '0');
	signal tilerom_data_from_s	: std_logic_vector(7 downto 0)	:= (others => '0');
	signal tile_a6_s				: std_logic								:= '0';
	signal divider1_q				: unsigned(0 downto 0)				:= (others => '0');
	signal divider2_q				: unsigned(3 downto 0)				:= (others => '0');
	signal hcount_q				: unsigned(5 downto 0)				:= (others => '0');
	signal char_row_q				: unsigned(3 downto 0)				:= (others => '0');
	signal vcount_q				: unsigned(4 downto 0)				:= (others => '0');
	signal latch_s					: std_logic								:= '0';
	signal latch_dly_s			: std_logic								:= '0';
	signal shift_clk_s			: std_logic								:= '0';
	signal chain_clk_s			: std_logic								:= '0';
	signal c1_clk_s				: std_logic								:= '0';
	signal hsync_s					: std_logic								:= '0';
	signal vsync_s					: std_logic								:= '0';
	signal video_shift_q			: std_logic_vector(7 downto 0)	:= (others => '0');
	signal pix_hcnt_q				: std_logic_vector(9 downto 0)	:= (others => '0');
	signal pix_vcnt_q				: std_logic_vector(8 downto 0)	:= (others => '0');

begin

	-- Tile ROM
	tilerom: entity work.tile_rom
	port map (
		clk	=> clock_n_s,
		addr	=> tilerom_addr_s,
		data	=> tilerom_data_from_s
	);

	clock_n_s <= not clock_i;

	vram_a_o	<= std_logic_vector(vcount_q(3 downto 0)) & std_logic_vector(hcount_q(4 downto 0)) & c1_clk_s;

	shift_clk_s	<= clock_i				when modsel_i = '0'	else divider1_q(0);		-- 64 colunas: SHIFT = 10.1MHz		32 colunas: SHIFT = 5MHz
	chain_clk_s	<= divider2_q(3)		when modsel_i = '0'	else divider2_q(2);		-- 64 colunas: CHAIN = SHIFT/16		32 colunas: CHAIN = SHIFT/8
	c1_clk_s		<= divider2_q(3)		when modsel_i = '0'	else '0';					-- 64 colunas: C1 = SHIFT/16			32 colunas: C1 = 0

	-- Divisores/contadores
	process (reset_i, clock_i)
	begin
		if reset_i = '1' then
			divider1_q	<= (others => '0');
		elsif rising_edge(clock_i) then
			divider1_q	<= divider1_q + 1;
		end if;
	end process;

	process (reset_i, shift_clk_s)
	begin
		if reset_i = '1' then
			divider2_q	<= (others => '0');
		elsif falling_edge(shift_clk_s) then				-- era rising
			divider2_q	<= divider2_q + 1;
		end if;
	end process;

	-- Contadores
	process (reset_i, chain_clk_s)
	begin
		if reset_i = '1' then
			hcount_q		<= (others => '0');
			char_row_q	<= (others => '0');
			vcount_q		<= (others => '0');
		elsif falling_edge(chain_clk_s) then				-- era rising
			if hcount_q = 40 then								-- era 39
				hcount_q <= (others => '0');
				if char_row_q = 11 then
					char_row_q	<= (others => '0');
					if vcount_q = 21 then						-- 25 = 50Hz
						vcount_q <= (others => '0');
					else
						vcount_q <= vcount_q + 1;
					end if;
				else
					char_row_q	<= char_row_q + 1;
				end if;
			else
				hcount_q <= hcount_q + 1;
			end if;
		end if;
	end process;

	latch_s			<= '1'			when divider2_q(2 downto 0) = "000"	else '0';										-- 1 pulso a cada 8 shifts
	tile_a6_s		<= vram_d_i(6)	when enaltset_i = '0'					else (not vram_d_i(7)) and vram_d_i(6);

	tilerom_addr_s	<= vram_d_i(7) & tile_a6_s & vram_d_i(5 downto 0) & std_logic_vector(char_row_q(2 downto 0));

	-- Atrasar latch
	process (shift_clk_s)
	begin
		if falling_edge(shift_clk_s) then
			latch_dly_s <= latch_s;
		end if;
	end process;

	-- Video shift register
	process (reset_i, shift_clk_s)
		variable blank_v : std_logic := '0';
	begin
		if reset_i = '1' then
			video_shift_q	<= (others => '0');
		elsif rising_edge(shift_clk_s) then

			if latch_dly_s = '1' then

				if vram_d_i(7 downto 6) /= "10" and char_row_q(3) = '1' then
					blank_v := '1';
				else
					blank_v := '0';
				end if;

				if hcount_q(5) = '0' and vcount_q(4) = '0' and blank_v = '0' then

					if vram_d_i(7 downto 6) = "10" then

						if char_row_q < 4 then
							video_shift_q(7 downto 4) <= (others => vram_d_i(0));
							video_shift_q(3 downto 0) <= (others => vram_d_i(1));
						elsif char_row_q < 8 then
							video_shift_q(7 downto 4) <= (others => vram_d_i(2));
							video_shift_q(3 downto 0) <= (others => vram_d_i(3));
						else
							video_shift_q(7 downto 4) <= (others => vram_d_i(4));
							video_shift_q(3 downto 0) <= (others => vram_d_i(5));
						end if;

					else
						video_shift_q <= tilerom_data_from_s;
					end if;

				else
					video_shift_q	<= (others => '0');
				end if;
			else
				video_shift_q(7 downto 1) <= video_shift_q(6 downto 0);
				video_shift_q(0) <= '0';
			end if;

		end if;
	end process;

	video_bit_o <= video_shift_q(7);

	-- Sync
	process (reset_i, clock_i)
	begin
		if reset_i = '1' then
			hsync_s <= '0';
			vsync_s <= '0';
		elsif rising_edge(clock_i) then
			-- H-Sync
			if hcount_q = 35 then
				hsync_s <= '1';
			end if;
			if hcount_q = 37 then
				hsync_s <= '0';
			end if;
			-- V-Sync
			if vcount_q = 18 then					-- 20 = 50Hz
				if char_row_q = 0 then
					vsync_s <= '1';
				end if;
				if char_row_q = 7 then
					vsync_s <= '0';
				end if;
			end if;
		end if;
	end process;

	video_hs_n_o	<= not hsync_s;
	video_vs_n_o	<= not vsync_s;

	-- for HDMI:
	process (clock_i)
		variable fall_v : std_logic_vector(1 downto 0);
	begin
		if falling_edge(clock_i) then
			fall_v := fall_v(0) & chain_clk_s;
			if fall_v = "10" and hcount_q = 0 then
				pix_hcnt_q <= (others => '0');
				if vcount_q = 0 and char_row_q = 0 then
					pix_vcnt_q <= (others => '0');
				else
					pix_vcnt_q <= pix_vcnt_q + 1;
				end if;
			else
				pix_hcnt_q <= pix_hcnt_q + 1;
			end if;
		end if;
	end process;

	pix_hcnt_o <= pix_hcnt_q;
	pix_vcnt_o <= pix_vcnt_q;

end Behavior;
--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

entity Kbd_edsac is
port 
(
        Clk              : in std_logic;
        KbdInt           : in std_logic;
        KbdScanCode      : in std_logic_vector(7 downto 0);
          
        rotary_dial      : out std_logic_vector(4 downto 0);
        active_mode      : out std_logic_vector(1 downto 0);
        write_bootloader : out std_logic;
        tty_erase        : out std_logic;
        cpu_reset        : out std_logic;
        cpu_halt         : out std_logic;
        cpu_resume       : out std_logic;

        osd_o            : out std_logic_vector(7 downto 0)      
);
end Kbd_edsac;

architecture Behavioral of Kbd_edsac is

signal IsReleased : std_logic;
signal osd_s        : std_logic_vector(7 downto 0) := (others=>'1');

signal cpu_halt_s : std_logic := '0';
signal cpu_resume_s : std_logic := '0';
signal key_halt_s : std_logic := '0';
signal key_resume_s : std_logic := '0';

signal edge_halt_s : std_logic_vector (1 downto 0);
signal edge_resu_s : std_logic_vector (1 downto 0);


begin

osd_o <= osd_s;
cpu_halt <= cpu_halt_s;
cpu_resume <= cpu_resume_s;

process(Clk)
begin
                                
  if rising_edge(Clk) then
  
    edge_halt_s <= edge_halt_s(0) & key_halt_s;
    edge_resu_s <= edge_resu_s(0) & key_resume_s;

    if edge_resu_s = "01" then
        cpu_resume_s <= not cpu_resume_s;
    end if;

    if edge_halt_s = "01" then
        cpu_halt_s <= not cpu_halt_s;
    end if;

            
    if KbdInt = '1' then
      
            if KbdScanCode = "11110000" then IsReleased <= '1'; else IsReleased <= '0'; end if; 

            if KbdScanCode = x"2D" then cpu_reset   <= not(IsReleased); rotary_dial <= "00000"; end if; -- R Reset
            if KbdScanCode = x"33" then key_halt_s  <= not(IsReleased); end if; -- H halt
            if KbdScanCode = x"21" then key_resume_s<= not(IsReleased); end if; -- C Continue Execution (resume)        

            if KbdScanCode = x"45" then rotary_dial <= "10100"; end if; -- 0  <= 5'd20;        // 0 Adds 2 * 10 instead of 0, to differentiate between 0 dialed and nothing dialed
            if KbdScanCode = x"16" then rotary_dial <= "00010"; end if; -- 1  <= 5'd2;         // 1 adds 2
            if KbdScanCode = x"1E" then rotary_dial <= "00100"; end if; -- 2  <= 5'd4;         // 2 adds 4
            if KbdScanCode = x"26" then rotary_dial <= "00110"; end if; -- 3  <= 5'd6;         // ... etc, n adds n * 2
            if KbdScanCode = x"25" then rotary_dial <= "01000"; end if; -- 4  <= 5'd8;
            if KbdScanCode = x"2E" then rotary_dial <= "01010"; end if; -- 5  <= 5'd10;
            if KbdScanCode = x"36" then rotary_dial <= "01100"; end if; -- 6  <= 5'd12;
            if KbdScanCode = x"3D" then rotary_dial <= "01110"; end if; -- 7  <= 5'd14;
            if KbdScanCode = x"3E" then rotary_dial <= "10000"; end if; -- 8  <= 5'd16;
            if KbdScanCode = x"46" then rotary_dial <= "10010"; end if; -- 8  <= 5'd18;

            if KbdScanCode = x"05" then active_mode <= "00"; end if; -- F1 = CRT 
            if KbdScanCode = x"06" then active_mode <= "01"; end if; -- F2 = Teletype
            if KbdScanCode = x"04" then active_mode <= "10"; end if; -- F3 = Panel

            if KbdScanCode = x"43" then write_bootloader <= not(IsReleased); end if; -- I = Initial Orders Write
            if KbdScanCode = x"24" then tty_erase        <= not(IsReleased); end if; -- E = Erase Teleprinter Screen        

            -- OSD
            osd_s (4 downto 0) <= "11111";
            if (IsReleased = '0') then  
                    if KbdScanCode = x"75" then osd_s(4 downto 0) <= "11110"; end if; -- up    arrow : 0x75
                    if KbdScanCode = x"72" then osd_s(4 downto 0) <= "11101"; end if; -- down  arrow : 0x72
                    if KbdScanCode = x"6b" then osd_s(4 downto 0) <= "11011"; end if; -- left  arrow : 0x6B
                    if KbdScanCode = x"74" then osd_s(4 downto 0) <= "10111"; end if; -- right arrow : 0x74
                    if KbdScanCode = x"5A" then osd_s(4 downto 0) <= "01111"; end if; -- ENTER
                        
                    if KbdScanCode = x"1c" then osd_s(4 downto 0) <= "00000"; end if;   -- A
                    if KbdScanCode = x"32" then osd_s(4 downto 0) <= "00001"; end if;   -- B
                    if KbdScanCode = x"21" then osd_s(4 downto 0) <= "00010"; end if;   -- C
                    if KbdScanCode = x"23" then osd_s(4 downto 0) <= "00011"; end if;   -- D
                    if KbdScanCode = x"24" then osd_s(4 downto 0) <= "00100"; end if;   -- E
                    if KbdScanCode = x"2b" then osd_s(4 downto 0) <= "00101"; end if;   -- F
                    if KbdScanCode = x"34" then osd_s(4 downto 0) <= "00110"; end if;   -- G
                    if KbdScanCode = x"33" then osd_s(4 downto 0) <= "00111"; end if;   -- H
                    if KbdScanCode = x"43" then osd_s(4 downto 0) <= "01000"; end if;   -- I
                    if KbdScanCode = x"3b" then osd_s(4 downto 0) <= "01001"; end if;   -- J
                    if KbdScanCode = x"42" then osd_s(4 downto 0) <= "01010"; end if;   -- K
                    if KbdScanCode = x"4b" then osd_s(4 downto 0) <= "01011"; end if;   -- L
                    if KbdScanCode = x"3a" then osd_s(4 downto 0) <= "01100"; end if;   -- M
                    if KbdScanCode = x"31" then osd_s(4 downto 0) <= "01101"; end if;   -- N
                    if KbdScanCode = x"44" then osd_s(4 downto 0) <= "01110"; end if;   -- O
                    if KbdScanCode = x"4d" then osd_s(4 downto 0) <= "10000"; end if;   -- P
                    if KbdScanCode = x"15" then osd_s(4 downto 0) <= "10001"; end if;   -- Q
                    if KbdScanCode = x"2d" then osd_s(4 downto 0) <= "10010"; end if;   -- R
                    if KbdScanCode = x"1b" then osd_s(4 downto 0) <= "10011"; end if;   -- S
                    if KbdScanCode = x"2c" then osd_s(4 downto 0) <= "10100"; end if;   -- T
                    if KbdScanCode = x"3c" then osd_s(4 downto 0) <= "10101"; end if;   -- U
                    if KbdScanCode = x"2a" then osd_s(4 downto 0) <= "10110"; end if;   -- V
                    if KbdScanCode = x"1d" then osd_s(4 downto 0) <= "11000"; end if;   -- W
                    if KbdScanCode = x"22" then osd_s(4 downto 0) <= "11001"; end if;   -- X
                    if KbdScanCode = x"35" then osd_s(4 downto 0) <= "11010"; end if;   -- Y
                    if KbdScanCode = x"1a" then osd_s(4 downto 0) <= "11100"; end if;   -- Z
                    
            end if;
            
            if (KbdScanCode = x"07" and IsReleased = '0') then  -- key F12
       --        (KbdScanCode = x"76" and IsReleased = '0' and osd_enable = '1') then -- ESC to abort an opened menu
                osd_s(7 downto 5) <= "011"; -- OSD Menu command
        --  elsif (KbdScanCode = x"78" and IsReleased = '0') then  -- key F11
        --      osd_s(7 downto 5) <= "111"; -- OSD Menu command
            else
                osd_s(7 downto 5) <= "111"; -- release
            end if;
            
     end if;
     

 
  end if;
end process;



end Behavioral;



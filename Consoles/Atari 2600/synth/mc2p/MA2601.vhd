--
-- MA2601.vhd
--
-- Atari VCS 2600 toplevel for the MiST board
-- https://github.com/wsoltys/tca2601
--
-- Copyright (c) 2014 W. Soltys <wsoltys@gmail.com>
--
-- This source file is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published
-- by the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This source file is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--//============================================================================
--//
--//  Multicore 2+ Top by Victor Trucco
--//
--//============================================================================


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
-- -----------------------------------------------------------------------

entity MA2601 is
    port (
    
        -- Clocks
        clock_50_i         : in    std_logic;

        -- Buttons
        btn_n_i            : in    std_logic_vector(4 downto 1);

        -- SRAM
        sram_addr_o        : out   std_logic_vector(20 downto 0)   := (others => '0');
        sram_data_io       : inout std_logic_vector(7 downto 0)    := (others => 'Z');
        sram_we_n_o        : out   std_logic                               := '1';
        sram_oe_n_o        : out   std_logic                               := '1';

        -- SDRAM
        SDRAM_A            : out std_logic_vector(12 downto 0);
        SDRAM_DQ           : inout std_logic_vector(15 downto 0);

        SDRAM_BA           : out std_logic_vector(1 downto 0);
        SDRAM_DQMH         : out std_logic;
        SDRAM_DQML         : out std_logic;    

        SDRAM_nRAS         : out std_logic;
        SDRAM_nCAS         : out std_logic;
        SDRAM_CKE          : out std_logic;
        SDRAM_CLK          : out std_logic;
        SDRAM_nCS          : out std_logic;
        SDRAM_nWE          : out std_logic;
    
        -- PS2
        ps2_clk_io         : inout std_logic                        := 'Z';
        ps2_data_io        : inout std_logic                        := 'Z';
        ps2_mouse_clk_io   : inout std_logic                        := 'Z';
        ps2_mouse_data_io  : inout std_logic                        := 'Z';

        -- SD Card
        sd_cs_n_o          : out   std_logic                        := 'Z';
        sd_sclk_o          : out   std_logic                        := 'Z';
        sd_mosi_o          : out   std_logic                        := 'Z';
        sd_miso_i          : in    std_logic;

        -- Joysticks
        joy_clock_o        : out   std_logic;
        joy_load_o         : out   std_logic;
        joy_data_i         : in    std_logic;
        joy_p7_o           : out   std_logic                        := '1';

        -- Audio
        AUDIO_L             : out   std_logic                       := '0';
        AUDIO_R             : out   std_logic                       := '0';
        ear_i               : in    std_logic;
        mic_o               : out   std_logic                       := '0';

        -- VGA
        VGA_R               : out   std_logic_vector(4 downto 0)    := (others => '0');
        VGA_G               : out   std_logic_vector(4 downto 0)    := (others => '0');
        VGA_B               : out   std_logic_vector(4 downto 0)    := (others => '0');
        VGA_HS              : out   std_logic                       := '1';
        VGA_VS              : out   std_logic                       := '1';

        LED                 : out   std_logic                       := '1';-- 0 is led on

        --STM32
        stm_rx_o            : out std_logic     := 'Z'; -- stm RX pin, so, is OUT on the slave
        stm_tx_i            : in  std_logic     := 'Z'; -- stm TX pin, so, is IN on the slave
        stm_rst_o           : out std_logic     := 'Z'; -- '0' to hold the microcontroller reset line, to free the SD card
        
        SPI_SCK             : in  std_logic;
        SPI_DO              : out std_logic   := 'Z';
        SPI_DI              : in  std_logic;
        SPI_SS2             : in  std_logic;
        SPI_nWAIT           : out std_logic   := '1';

        GPIO                : inout std_logic_vector(31 downto 0)   := (others => 'Z')
    );
end entity;

-- -----------------------------------------------------------------------

architecture rtl of MA2601 is

-- System clocks
  signal vid_clk: std_logic := '0'; -- 28 MHz
  signal clk : std_logic; -- 3.5 MHz

-- A2601
  signal audio: std_logic := '0';
  signal O_VSYNC: std_logic := '0';
  signal O_HSYNC: std_logic := '0';
  signal O_VIDEO_R: std_logic_vector(5 downto 0) := (others => '0');
  signal O_VIDEO_G: std_logic_vector(5 downto 0) := (others => '0');
  signal O_VIDEO_B: std_logic_vector(5 downto 0) := (others => '0');
  signal res: std_logic := '0';
  signal p_l: std_logic := '0';
  signal p_r: std_logic := '0';
  signal p_a: std_logic := '0';
  signal p_b: std_logic := '0';
  signal p_u: std_logic := '0';
  signal p_d: std_logic := '0';
  signal p2_l: std_logic := '0';
  signal p2_r: std_logic := '0';
  signal p2_a: std_logic := '0';
  signal p2_b: std_logic := '0';
  signal p2_u: std_logic := '0';
  signal p2_d: std_logic := '0';
  signal b_start: std_logic := '1';
  signal b_select: std_logic := '1';
  signal p_start: std_logic := '1';
  signal p_select: std_logic := '1';
  signal p_color: std_logic := '1';
  signal sc: std_logic := '0';
  signal force_bs: std_logic_vector(3 downto 0) := "0000";
  signal pal: std_logic := '0';
  signal p_dif: std_logic_vector(1 downto 0) := (others => '0');

    -- joystick
    signal joyP7_s          : std_logic;
  signal joy_a_0    : std_logic_vector(15 downto 0);
  signal joy_a_1    : std_logic_vector(15 downto 0);
  signal joy_ana_0  : std_logic_vector(15 downto 0);
  signal joy_ana_1  : std_logic_vector(15 downto 0);

-- User IO  
  signal switches   : std_logic_vector(1 downto 0);
  signal buttons    : std_logic_vector(1 downto 0);
  signal status     : std_logic_vector(31 downto 0);
  signal ascii_new  : std_logic;
  signal ascii_code : STD_LOGIC_VECTOR(6 DOWNTO 0);
  signal ps2Clk     : std_logic;
  signal ps2Data    : std_logic;
  signal ps2_scancode : std_logic_vector(7 downto 0);
  signal scandoubler_disable : std_logic;
  signal ypbpr      : std_logic;
  signal no_csync   : std_logic;

-- Data IO
  signal downl      : std_logic;
  signal index      : std_logic_vector(7 downto 0);
  --signal file_ext   : std_logic_vector(23 downto 0);
  signal rom_a      : std_logic_vector(16 downto 0);
  signal rom_do     : std_logic_vector(7 downto 0);
  signal rom_size   : std_logic_vector(17 downto 0);
  signal rom_wr_a   : std_logic_vector(24 downto 0);
  signal rom_wr     : std_logic;
  signal rom_di     : std_logic_vector(7 downto 0);

  type config_array is array(natural range 15 downto 0) of std_logic_vector(7 downto 0);
  signal config_buffer_s : config_array;

  -- config string used by the io controller to fill the OSD
  constant CONF_STR : string :=
    "P,Atari 2600.ini;"&
    "S1,*,Load Game ...;"&
    "O78,Scanlines,Off,25%,50%,75%;"&
    "O2,Video mode,Color,B&W;"&
    "O3,Difficulty P1,A,B;"&
    "O4,Difficulty P2,A,B;"&
    "OAB,Controller,Joy,Pad.Slo,Pad.Fst,Pad.Fastest;"& 
    "O6,Joystick Swap,Off,On;"&
    "O1,Video standard,NTSC,PAL;"&
    "OC,Scandoubler,On,Off;"&
    "T0,Reset";

  function to_slv(s: string) return std_logic_vector is
    constant ss: string(1 to s'length) := s;
    variable rval: std_logic_vector(1 to 8 * s'length);
    variable p: integer;
    variable c: integer;
  
  begin  
    for i in ss'range loop
      p := 8 * i;
      c := character'pos(ss(i));
      rval(p - 7 to p) := std_logic_vector(to_unsigned(c,8));
    end loop;
    return rval;

  end function;

    component joystick_serial is
    port
    (
        clk_i           : in  std_logic;
        joy_data_i      : in  std_logic;
        joy_clk_o       : out  std_logic;
        joy_load_o      : out  std_logic;

        joy1_up_o       : out std_logic;
        joy1_down_o     : out std_logic;
        joy1_left_o     : out std_logic;
        joy1_right_o    : out std_logic;
        joy1_fire1_o    : out std_logic;
        joy1_fire2_o    : out std_logic;
        joy2_up_o       : out std_logic;
        joy2_down_o     : out std_logic;
        joy2_left_o     : out std_logic;
        joy2_right_o    : out std_logic;
        joy2_fire1_o    : out std_logic;
        joy2_fire2_o    : out std_logic
    );
    end component;


  component data_io 
      generic ( STRLEN : integer := 0 );
    port (
        clk_sys         : in std_logic;

        SPI_SCK         : in std_logic;
        SPI_SS2         : in std_logic;
        SPI_DI          : in std_logic;
        SPI_DO          : out std_logic;

        data_in         : in std_logic_vector(7 downto 0);   
        conf_str        : in std_logic_vector((8*STRLEN)-1 downto 0);   
        status          : out std_logic_vector(31 downto 0);   
        config_buffer_o : out config_array;

        ioctl_download  : out std_logic;
        ioctl_index     : out std_logic_vector(7 downto 0);

        ioctl_wr        : out std_logic;
        ioctl_addr      : out std_logic_vector(24 downto 0);
        ioctl_dout      : out std_logic_vector(7 downto 0)
    );
  end component data_io;

  
component mist_video
generic (
    OSD_COLOR    : std_logic_vector(2 downto 0) := "110";
    OSD_X_OFFSET : std_logic_vector(9 downto 0) := (others => '0');
    OSD_Y_OFFSET : std_logic_vector(9 downto 0) := (others => '0');
    SD_HCNT_WIDTH: integer := 9;
    COLOR_DEPTH  : integer := 6;
    OSD_AUTO_CE  : boolean := true
);
port (
    clk_sys     : in std_logic;

    SPI_SCK     : in std_logic;
    SPI_SS3     : in std_logic;
    SPI_DI      : in std_logic;

    scanlines   : in std_logic_vector(1 downto 0);
    ce_divider  : in std_logic := '0';
    scandoubler_disable : in std_logic;

    rotate      : in std_logic_vector(1 downto 0);
    no_csync    : in std_logic := '0';
    blend       : in std_logic := '0';

    HSync       : in std_logic;
    VSync       : in std_logic;
    R           : in std_logic_vector(COLOR_DEPTH-1 downto 0);
    G           : in std_logic_vector(COLOR_DEPTH-1 downto 0);
    B           : in std_logic_vector(COLOR_DEPTH-1 downto 0);

    VGA_HS      : out std_logic;
    VGA_VS      : out std_logic;
    VGA_R       : out std_logic_vector(4 downto 0);
    VGA_G       : out std_logic_vector(4 downto 0);
    VGA_B       : out std_logic_vector(4 downto 0);

    osd_enable  : out std_logic
);
end component mist_video;

    component PumpSignal
    port(
        clk_i       : in  std_logic;
        reset_i     : in  std_logic;
        download_i  : in  std_logic;   
        pump_o      : out std_logic_vector( 7 downto 0)
    );
    end component;

  signal joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i : std_logic;
  signal joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i : std_logic;

    signal joy1_s       : std_logic_vector(15 downto 0) := (others => '1'); 
    signal joy2_s       : std_logic_vector(15 downto 0) := (others => '1'); 

    signal osd_s        : std_logic_vector(7 downto 0) := (others => '1');
    signal osd_b        : std_logic_vector(7 downto 0) := (others => '1'); 
    signal keys_s       : std_logic_vector(7 downto 0) := (others => '1'); 

    signal osd_enable   : std_logic;

    signal paddle_ena_s : std_logic;
    signal pll_locked   : std_logic;
    signal direct_video_s : std_logic;
    signal rom_wr_s     : std_logic;

    signal btn_n_o      : std_logic_vector(4 downto 1); 


  ------------------------------------------------------------------------------
  -- external cartridge expansion #01
  ------------------------------------------------------------------------------

  alias ext_data is sram_data_io; 
  alias ext_a0   is sram_addr_o(7);
  alias ext_a1   is sram_addr_o(9);
  alias ext_a2   is sram_addr_o(11);
  alias ext_a3   is GPIO(15);
  alias ext_a4   is GPIO(17);
  alias ext_a5   is sram_addr_o(15);
  alias ext_a6   is sram_addr_o(16);
  alias ext_a7   is GPIO(20);
  alias ext_a8   is GPIO(18);
  alias ext_a9   is GPIO(16);
  alias ext_a10  is sram_addr_o(8);
  alias ext_a11  is sram_addr_o(10);

  alias ext_cs      is GPIO(28);

  alias ext_lvc_oe  is GPIO(12);
  alias ext_lvc_dir is GPIO(14);

  alias ext_cart_detect is GPIO(25);

  signal atari_cart_d : std_logic_vector(7 downto 0);
  signal cpu_d : std_logic_vector(7 downto 0);
  signal ext_cart_detect_r : std_logic;
  signal ext_cart_reset_cnt : unsigned(20 downto 0) := (others=>'1');
  signal ext_cart_reset_s : std_logic;
  signal ext_cart_detect_db : std_logic;

  signal rom_cs : std_logic;
  signal cpu_r_s : std_logic;
begin


-- -----------------------------------------------------------------------
-- Defaults
-- -----------------------------------------------------------------------

 joystick_serial1 : joystick_serial 
    port map
    (
        clk_i           => clock_50_i,
        joy_data_i      => joy_data_i,
        joy_clk_o       => joy_clock_o,
        joy_load_o      => joy_load_o,

        joy1_up_o       => joy1_up_i,
        joy1_down_o     => joy1_down_i,
        joy1_left_o     => joy1_left_i,
        joy1_right_o    => joy1_right_i,
        joy1_fire1_o    => joy1_p6_i,
        joy1_fire2_o    => joy1_p9_i,

        joy2_up_o       => joy2_up_i,
        joy2_down_o     => joy2_down_i,
        joy2_left_o     => joy2_left_i,
        joy2_right_o    => joy2_right_i,
        joy2_fire1_o    => joy2_p6_i,
        joy2_fire2_o    => joy2_p9_i
    );

  SDRAM_nCS <= '1'; -- disable ram
  sram_we_n_o <= '1';
  sram_oe_n_o <= '1';
  stm_rst_o <= 'Z';

PumpSignal_1 : PumpSignal port map(vid_clk, (not pll_locked), downl, osd_s);


  joy_ana_0 <= joy_a_0 when status(6) = '0' else joy_a_1;
  joy_ana_1 <= joy_a_1 when status(6) = '0' else joy_a_0;

  paddle_ena_s <= '0' when status(11 downto 10) = "00" else '1';

  res <= not btn_n_o(4) or downl or ext_cart_reset_s;-- or status(0);-- status(0) or buttons(1) or downl;
 
  p_start <= (not btn_n_o(1)) or downl;
  p_select <= (not btn_n_o(2)) or downl;
  p_color <= not status(2);
  pal <= status(1);
  p_dif(0) <= not status(3);
  p_dif(1) <= not status(4);
  
  joy_ana_0 <= joy_a_0 when status(6) = '0' else joy_a_1;
  joy_ana_1 <= joy_a_1 when status(6) = '0' else joy_a_0;
-- -----------------------------------------------------------------------
-- A2601 core
-- -----------------------------------------------------------------------
  a2601Instance : entity work.A2601NoFlash
    port map (
      vid_clk => vid_clk,
      clk => clk,
      audio => audio,
      O_VSYNC => O_VSYNC,
      O_HSYNC => O_HSYNC,
      O_VIDEO_R => O_VIDEO_R,
      O_VIDEO_G => O_VIDEO_G,
      O_VIDEO_B => O_VIDEO_B,
      res => res,
      p_l => p_l,
      p_r => p_r,
      p_a => p_a,
      p_b => p_b,
      p_u => p_u,
      p_d => p_d,
      p2_l => p2_l,
      p2_r => p2_r,
      p2_a => p2_a,
      p2_b => p2_b,
      p2_u => p2_u,
      p2_d => p2_d,
      paddle_0 => joy_ana_0(15 downto 8),
      paddle_1 => joy_ana_0(7 downto 0),
      paddle_2 => joy_ana_1(15 downto 8),
      paddle_3 => joy_ana_1(7 downto 0),
      paddle_ena => status(5),
      p_start => (not p_start) and (not b_start),
      p_select => (not p_select) and (not b_select),
      p_color => p_color,
      sc => sc,
      force_bs => force_bs,
      
      rom_a => rom_a,
      rom_do => atari_cart_d, --rom_do,
      rom_size => rom_size,
      rom_cs => rom_cs,
      cpu_d_o => cpu_d,
      cpu_r_o => cpu_r_s,

      pal => pal,
      p_dif => p_dif,
      tv15khz => '1'
    );

  mist_video_inst : mist_video
    generic map (
        OSD_COLOR => "001"
    )
    port map (
        clk_sys     => vid_clk,
        scanlines   => status(8 downto 7),
        rotate      => "00",
        scandoubler_disable => (not status(12)) xor direct_video_s,

        no_csync => no_csync,

        SPI_SCK     => SPI_SCK,
        SPI_SS3     => SPI_SS2,
        SPI_DI      => SPI_DI,

        HSync       => not O_HSYNC,
        VSync       => not O_VSYNC,
        R           => O_VIDEO_R,
        G           => O_VIDEO_G,
        B           => O_VIDEO_B,

        VGA_HS      => VGA_HS,
        VGA_VS      => VGA_VS,
        VGA_R       => VGA_R,
        VGA_G       => VGA_G,
        VGA_B       => VGA_B,

        osd_enable  => osd_enable
    );

  AUDIO_L <= audio;
  AUDIO_R <= audio;

  -- 9 pin d-sub joystick pinout:
  -- pin 1: up
  -- pin 2: down
  -- pin 3: left
  -- pin 4: right
  -- pin 6: fire

  -- Atari 2600, 6532 ports:
  -- PA0: right joystick, up
  -- PA1: right joystick, down
  -- PA2: right joystick, left
  -- PA3: right joystick, right
  -- PA4: left joystick, up
  -- PA5: left joystick, down
  -- PA6: left joystick, left
  -- PA7: left joystick, right
  -- PB0: start
  -- PB1: select
  -- PB3: B/W, color
  -- PB6: left difficulty
  -- PB7: right difficulty

  -- Atari 2600, TIA input:
  -- I5: right joystick, fire
  -- I6: left joystick, fire

  -- pinout docking station joystick 1/2:
  -- bit 0: up
  -- bit 1: down
  -- bit 2: left
  -- bit 3: right
  -- bit 4: fire
  -- bit 5: 2nd fire (required for paddle emulation)
  p_l <= not joy1_s(1) when status(6) = '0' else not joy2_s(1);
  p_r <= not joy1_s(0) when status(6) = '0' else not joy2_s(0);
  p_a <= not (joy1_s(4) or joy1_s(6) or joy1_s(7)) when status(6) = '0' else not (joy2_s(4) or joy2_s(6) or joy2_s(7));
  p_b <= not joy1_s(5) when status(6) = '0' else not joy2_s(5);
  p_u <= not joy1_s(3) when status(6) = '0' else not joy2_s(3);
  p_d <= not joy1_s(2) when status(6) = '0' else not joy2_s(2);

  p2_l <= not joy2_s(1) when status(6) = '0' else not joy1_s(1);
  p2_r <= not joy2_s(0) when status(6) = '0' else not joy1_s(0);
  p2_a <= not (joy2_s(4) or joy2_s(6) or joy2_s(7)) when status(6) = '0' else not (joy1_s(4) or joy1_s(6) or joy1_s(7));
  p2_b <= not joy2_s(5) when status(6) = '0' else not joy1_s(5);
  p2_u <= not joy2_s(3) when status(6) = '0' else not joy1_s(3);
  p2_d <= not joy2_s(2) when status(6) = '0' else not joy1_s(2);


-- -----------------------------------------------------------------------
-- Clocks and PLL
-- -----------------------------------------------------------------------
  pllInstance : entity work.pll
    port map (
      inclk0 => clock_50_i,
      c0 => vid_clk,
      c1 => clk,
      locked => open
    );

-- ------------------------------------------------------------------------
-- User IO
-- ------------------------------------------------------------------------

--  user_io_inst : user_io
--    generic map (STRLEN => CONF_STR'length)
--   port map (
--      clk_sys => vid_clk,
--      SPI_CLK => SPI_SCK,
--      SPI_SS_IO => CONF_DATA0,
--      SPI_MOSI => SPI_DI,
--      SPI_MISO => SPI_DO,
--      conf_str => to_slv(CONF_STR),
--      switches => switches,
--      buttons  => buttons,
--      scandoubler_disable => scandoubler_disable,
--      ypbpr => ypbpr,
--      no_csync => no_csync,
--      joystick_1 => joy0,
--      joystick_0 => joy1,
--      joystick_analog_1 => joy_a_0,
--      joystick_analog_0 => joy_a_1,
--      status => status,
--      sd_sdhc => '1',
--      ps2_kbd_clk => ps2Clk,
--      ps2_kbd_data => ps2Data
--    );

  data_io_inst: data_io
    generic map (STRLEN => CONF_STR'length)
    port map (
      clk_sys => vid_clk,
      SPI_SCK => SPI_SCK,
      SPI_SS2 => SPI_SS2,
      SPI_DI => SPI_DI,
      SPI_DO => SPI_DO,
      
      data_in   => osd_s and osd_b and keys_s,
      conf_str  => to_slv(CONF_STR),
      status    => status,
      config_buffer_o=> config_buffer_s,

      ioctl_download => downl,
      ioctl_index    => index,
--      ioctl_fileext  => file_ext,
      ioctl_wr       => rom_wr,
      ioctl_addr     => rom_wr_a,
      ioctl_dout     => rom_di
    );

  rom_inst: entity work.data_io_ram
    port map (
      -- wire up cpu port
      rdaddress => rom_a,
      rdclock => clk,
      q => rom_do,

      -- io controller port
      wraddress => rom_wr_a(16 downto 0),
      wrclock => vid_clk,
      data => rom_di,
      wren => rom_wr
    );

  rom_size <= std_logic_vector(unsigned('0'&rom_wr_a(16 downto 0)) + 1) when ext_cart_detect_db = '1' else  "00" & x"1000"; --always no Banking, when external is selected

  -- 2nd menu index - load with SuperChip support OR 3rd character in extension is 's'
  sc <= '1' when index(1) = '1' or config_buffer_s(9) = x"53" or config_buffer_s(9) = x"73" else '0';

  -- force bank switch type by file extension
  process (config_buffer_s) begin 
    force_bs <= "0000";

       if  ext_cart_detect_db = '0' then  force_bs <= "0000"; --always no Banking, when external is selected
    elsif (config_buffer_s(10) = x"45" and config_buffer_s(9) = x"30") or (config_buffer_s(10) = x"65" and config_buffer_s(9) = x"30") then force_bs <= "0100"; -- E0
    elsif (config_buffer_s(10) = x"46" and config_buffer_s(9) = x"45") or (config_buffer_s(10) = x"66" and config_buffer_s(9) = x"65") then force_bs <= "0011"; -- FE
    elsif (config_buffer_s(10) = x"33" and config_buffer_s(9) = x"46") or (config_buffer_s(10) = x"33" and config_buffer_s(9) = x"66") then force_bs <= "0101"; -- 3F
    elsif (config_buffer_s(10) = x"50" and config_buffer_s(9) = x"32") or (config_buffer_s(10) = x"70" and config_buffer_s(9) = x"32") then force_bs <= "0111"; -- P2 (Pitfall II)
    elsif (config_buffer_s(10) = x"46" and config_buffer_s(9) = x"41") or (config_buffer_s(10) = x"66" and config_buffer_s(9) = x"61") then force_bs <= "1000"; -- FA
    elsif (config_buffer_s(10) = x"43" and config_buffer_s(9) = x"56") or (config_buffer_s(10) = x"63" and config_buffer_s(9) = x"76") then force_bs <= "1001"; -- CV
    elsif (config_buffer_s(10) = x"45" and config_buffer_s(9) = x"37") or (config_buffer_s(10) = x"65" and config_buffer_s(9) = x"37") then force_bs <= "1010"; -- E7
    elsif (config_buffer_s(10) = x"55" and config_buffer_s(9) = x"41") or (config_buffer_s(10) = x"75" and config_buffer_s(9) = x"61") then force_bs <= "1011"; -- UA
    elsif (config_buffer_s(10) = x"53" and config_buffer_s(9) = x"42") or (config_buffer_s(10) = x"73" and config_buffer_s(9) = x"62") then force_bs <= "1100"; -- SB
        end if;
  end process;

  -----------------------------------------------------------------------------------------


    -- translate scancode to joystick
    hid : entity work.MC2_HID
    generic map 
    (
        use_usb_g   => '1',
        CLK_SPEED   => 50000
    )
    port map 
    (
        clk_i          => clock_50_i,
        kbd_clk_io     => ps2_clk_io,
        kbd_dat_io     => ps2_data_io,
		  usb_rx_i       => ps2_mouse_clk_io,
		  
        osd_o          => keys_s,
        osd_enable_i   => osd_enable,
        direct_video_o => direct_video_s,
           
        -- 1,2,u,d,l,r   
        joystick_0_i  => joy1_p9_i & joy1_p6_i & joy1_up_i & joy1_down_i & joy1_left_i & joy1_right_i,
        joystick_1_i  => joy2_p9_i & joy2_p6_i & joy2_up_i & joy2_down_i & joy2_left_i & joy2_right_i,

        -- joystick_0 and joystick_1 should be swapped
        joyswap_i     => status(6),

        -- player1 and player2 should get both joystick_0 and joystick_1
        oneplayer_i   => '0',

        -- tilt, coin4-1, start4-1
        controls_o(8 downto 2) => open,
        controls_o(1) => b_select,
        controls_o(0) => b_start,

        -- fire12-1, up, down, left, right

        player1_o    => joy1_s,
        player2_o    => joy2_s,
       
        -- sega joystick
        sega_strobe_o => joy_p7_o ,
      
      front_buttons_i => btn_n_i,
      front_buttons_o => btn_n_o
    );




------------------------------------------------------------------------------
-- external cartridge expansion #01
------------------------------------------------------------------------------

GPIO(31 downto 0) <= (others=>'Z');

sram_oe_n_o   <= '1';
sram_we_n_o   <= '1';

GPIO(13) <= '1'; -- SMS cart disabled
GPIO(10) <= '1'; -- Odyssey cart disabled
GPIO(0) <= '1';  -- Coleco cart disabled #1
GPIO(24 downto 23) <= "11"; -- Coleco cart disabled #2 and #3
GPIO(31) <= '1'; -- Coleco cart disabled #4

ext_lvc_oe  <= '0'; -- always selected
 

process (clk)
  begin
    if rising_edge(clk) then

      -- the data lines need to be synced with the clock
      ext_a0   <= rom_a(0);
      ext_a1   <= rom_a(1);
      ext_a2   <= rom_a(2);
      ext_a3   <= rom_a(3);
      ext_a4   <= rom_a(4);
      ext_a5   <= rom_a(5);
      ext_a6   <= rom_a(6);
      ext_a7   <= rom_a(7);
      ext_a8   <= rom_a(8);
      ext_a9   <= rom_a(9);
      ext_a10  <= rom_a(10);
      ext_a11  <= rom_a(11);
      ext_cs   <= rom_cs; -- the cs is A(12)

      --ext_lvc_dir <= cpu_r_s;-- ; rom_cs-- 0 = data from FPGA to cart   1 = data from cart to the FPGA (nao funciona decathlon)
      ext_lvc_dir <= rom_cs;-- 0 = data from FPGA to cart   1 = data from cart to the FPGA (funciona com decathlon)

  end if;
end process;

ext_data <= cpu_d when rom_cs = '0' else (others=>'Z');
atari_cart_d <= rom_do when ext_cart_detect_db = '1' else ext_data;

debounce : entity work.debounce
port map 
(
    clk_i     => clk,
    button_i  => ext_cart_detect,
    result_o  => ext_cart_detect_db
);

  -- Reset when the external module is turned on or off
  process (clk)
  begin
    if rising_edge(clk) then

      ext_cart_detect_r <= ext_cart_detect_db;

      if (ext_cart_detect_db = '0' and ext_cart_detect_r = '1') or
         (ext_cart_detect_db = '1' and ext_cart_detect_r = '0') then --reset signal on edges
        ext_cart_reset_cnt <= (others=>'1');
      end if;

      if ext_cart_reset_cnt > 0 then
        ext_cart_reset_s <= '1';
        ext_cart_reset_cnt <= ext_cart_reset_cnt - 1;
      else
        ext_cart_reset_s <= '0';
      end if;

    end if;
  end process;


end architecture;

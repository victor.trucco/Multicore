--
-- MA2601.vhd
--
-- Atari VCS 2600 toplevel for the MiST board
-- https://github.com/wsoltys/tca2601
--
-- Copyright (c) 2014 W. Soltys <wsoltys@gmail.com>
--
-- This source file is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published
-- by the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This source file is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
-- -----------------------------------------------------------------------

entity MA2601 is
    port (
    -- Clocks
        clock_50_i          : in    std_logic;

        -- Buttons
        btn_n_i             : in    std_logic_vector(4 downto 1);

        -- SRAMs (AS7C34096)
        sram_addr_o         : out   std_logic_vector(18 downto 0)   := (others => '0');
        sram_data_io        : inout std_logic_vector(7 downto 0)    := (others => 'Z');
        sram_we_n_o         : out   std_logic                               := '1';
        sram_oe_n_o         : out   std_logic                               := '1';
        
        -- SDRAM    (H57V256)
        SDRAM_A             : out std_logic_vector(12 downto 0);
        SDRAM_DQ                : inout std_logic_vector(15 downto 0);

        SDRAM_BA                : out std_logic_vector(1 downto 0);
        SDRAM_DQMH          : out std_logic;
        SDRAM_DQML          : out std_logic;    

        SDRAM_nRAS          : out std_logic;
        SDRAM_nCAS          : out std_logic;
        SDRAM_CKE           : out std_logic;
        SDRAM_CLK           : out std_logic;
        SDRAM_nCS           : out std_logic;
        SDRAM_nWE           : out std_logic;
    
        -- PS2
        ps2_clk_io          : inout std_logic                               := 'Z';
        ps2_data_io         : inout std_logic                               := 'Z';
        ps2_mouse_clk_io  : inout std_logic                             := 'Z';
        ps2_mouse_data_io : inout std_logic                             := 'Z';

        -- SD Card
        sd_cs_n_o           : out   std_logic                               := 'Z';
        sd_sclk_o           : out   std_logic                               := 'Z';
        sd_mosi_o           : out   std_logic                               := 'Z';
        sd_miso_i           : in    std_logic;

        -- Joysticks
        joy1_up_i           : inout    std_logic;
        joy1_down_i         : inout    std_logic;
        joy1_left_i         : in    std_logic;
        joy1_right_i        : in    std_logic;
        joy1_p6_i           : in    std_logic;
        joy1_p9_i           : in    std_logic;
        joy2_up_i           : inout    std_logic;
        joy2_down_i         : inout    std_logic;
        joy2_left_i         : in    std_logic;
        joy2_right_i        : in    std_logic;
        joy2_p6_i           : in    std_logic;
        joy2_p9_i           : in    std_logic;
        joyX_p7_o           : out   std_logic                               := '1';

        -- Audio
        AUDIO_L             : out   std_logic                               := '0';
        AUDIO_R             : out   std_logic                               := '0';
        ear_i                   : in    std_logic;
        mic_o                   : out   std_logic                               := '0';

        -- VGA
        VGA_R                   : out   std_logic_vector(4 downto 0)    := (others => '0');
        VGA_G                   : out   std_logic_vector(4 downto 0)    := (others => '0');
        VGA_B                   : out   std_logic_vector(4 downto 0)    := (others => '0');
        VGA_HS              : out   std_logic                               := '1';
        VGA_VS              : out   std_logic                               := '1';

        -- HDMI
        tmds_o              : out   std_logic_vector(7 downto 0)    := (others => '0');

        --STM32
        stm_rx_o                : out std_logic     := 'Z'; -- stm RX pin, so, is OUT on the slave
        stm_tx_i                : in  std_logic     := 'Z'; -- stm TX pin, so, is IN on the slave
        stm_rst_o           : out std_logic     := 'Z'; -- '0' to hold the microcontroller reset line, to free the SD card
        
        stm_a15_io          : inout std_logic;
        stm_b8_io           : inout std_logic       := 'Z';
        stm_b9_io           : inout std_logic       := 'Z';
        
        SPI_SCK             : inout std_logic       := 'Z';
        SPI_DO              : inout std_logic       := 'Z';
        SPI_DI              : inout std_logic       := 'Z';
        SPI_SS2             : inout std_logic       := 'Z'
    );
end entity;

-- -----------------------------------------------------------------------

architecture rtl of MA2601 is

-- System clocks
  signal vid_clk: std_logic := '0'; -- 28 MHz
  signal clk : std_logic; -- 3.5 MHz

-- A2601
  signal audio: std_logic := '0';
  signal O_VSYNC: std_logic := '0';
  signal O_HSYNC: std_logic := '0';
  signal O_VIDEO_R: std_logic_vector(5 downto 0) := (others => '0');
  signal O_VIDEO_G: std_logic_vector(5 downto 0) := (others => '0');
  signal O_VIDEO_B: std_logic_vector(5 downto 0) := (others => '0');
  signal res: std_logic := '0';
  signal p_l: std_logic := '0';
  signal p_r: std_logic := '0';
  signal p_a: std_logic := '0';
  signal p_b: std_logic := '0';
  signal p_u: std_logic := '0';
  signal p_d: std_logic := '0';
  signal p2_l: std_logic := '0';
  signal p2_r: std_logic := '0';
  signal p2_a: std_logic := '0';
  signal p2_b: std_logic := '0';
  signal p2_u: std_logic := '0';
  signal p2_d: std_logic := '0';
  signal p_start: std_logic := '1';
  signal p_select: std_logic := '1';
  signal p_color: std_logic := '1';
  signal sc: std_logic := '0';
  signal force_bs: std_logic_vector(3 downto 0) := "0000";
  signal pal: std_logic := '0';
  signal p_dif: std_logic_vector(1 downto 0) := (others => '0');

    -- joystick
    signal joy1_s           : std_logic_vector(11 downto 0) := (others => '1'); --  MXYZ SACB RLDU
    signal joy2_s           : std_logic_vector(11 downto 0) := (others => '1'); --  MXYZ SACB RLDU
    signal joyP7_s          : std_logic;
  signal joy_a_0    : std_logic_vector(15 downto 0);
  signal joy_a_1    : std_logic_vector(15 downto 0);
  signal joy_ana_0  : std_logic_vector(15 downto 0);
  signal joy_ana_1  : std_logic_vector(15 downto 0);

-- User IO  
  signal switches   : std_logic_vector(1 downto 0);
  signal buttons    : std_logic_vector(1 downto 0);
  signal status     : std_logic_vector(31 downto 0);
  signal ascii_new  : std_logic;
  signal ascii_code : STD_LOGIC_VECTOR(6 DOWNTO 0);
  signal ps2Clk     : std_logic;
  signal ps2Data    : std_logic;
  signal ps2_scancode : std_logic_vector(7 downto 0);
  signal scandoubler_disable : std_logic;
  signal ypbpr      : std_logic;
  signal no_csync   : std_logic;

-- Data IO
  signal downl      : std_logic;
  signal index      : std_logic_vector(7 downto 0);
  --signal file_ext   : std_logic_vector(23 downto 0);
  signal rom_a      : std_logic_vector(16 downto 0);
  signal rom_do     : std_logic_vector(7 downto 0);
  signal rom_size   : std_logic_vector(17 downto 0);
  signal rom_wr_a   : std_logic_vector(24 downto 0);
  signal rom_wr     : std_logic;
  signal rom_di     : std_logic_vector(7 downto 0);

  signal keys_s     : std_logic_vector(7 downto 0);
  type config_array is array(natural range 15 downto 0) of std_logic_vector(7 downto 0);
  signal config_buffer_s : config_array;

  -- config string used by the io controller to fill the OSD
  constant CONF_STR : string :=
    "S,*,Load Game...;"&
    "T9,Select Switch;"&
    "TA,Start Switch;"&
    "O3,Difficulty P1,A,B;"&
    "O4,Difficulty P2,A,B;"&
    "O2,Video mode,Color,B&W;"&  
    "O78,Scanlines,Off,25%,50%,75%;" &
    "O5,Controller,Joystick,Paddle;"&
--    "O6,Joystick Swap,Off,On;"&
    "O1,Video standard,NTSC,PAL;"
    ;

  function to_slv(s: string) return std_logic_vector is
    constant ss: string(1 to s'length) := s;
    variable rval: std_logic_vector(1 to 8 * s'length);
    variable p: integer;
    variable c: integer;
  
  begin  
    for i in ss'range loop
      p := 8 * i;
      c := character'pos(ss(i));
      rval(p - 7 to p) := std_logic_vector(to_unsigned(c,8));
    end loop;
    return rval;

  end function;


  component data_io 
      generic ( STRLEN : integer := 0 );
    port (
        clk_sys         : in std_logic;

        SPI_SCK         : in std_logic;
        SPI_SS2         : in std_logic;
        SPI_DI          : in std_logic;
        SPI_DO          : out std_logic;

        data_in         : in std_logic_vector(7 downto 0);   
        conf_str        : in std_logic_vector((8*STRLEN)-1 downto 0);   
        status          : out std_logic_vector(31 downto 0);   
        config_buffer_o : out config_array;

        ioctl_download  : out std_logic;
        ioctl_index     : out std_logic_vector(7 downto 0);

        ioctl_wr        : out std_logic;
        ioctl_addr      : out std_logic_vector(24 downto 0);
        ioctl_dout      : out std_logic_vector(7 downto 0)
    );
  end component data_io;

  
component mist_video
generic (
    OSD_COLOR    : std_logic_vector(2 downto 0) := "110";
    OSD_X_OFFSET : std_logic_vector(9 downto 0) := (others => '0');
    OSD_Y_OFFSET : std_logic_vector(9 downto 0) := (others => '0');
    SD_HCNT_WIDTH: integer := 9;
    COLOR_DEPTH  : integer := 6;
    OSD_AUTO_CE  : boolean := true
);
port (
    clk_sys     : in std_logic;

    SPI_SCK     : in std_logic;
    SPI_SS3     : in std_logic;
    SPI_DI      : in std_logic;

    scanlines   : in std_logic_vector(1 downto 0);
    ce_divider  : in std_logic := '0';
    scandoubler_disable : in std_logic;

    rotate      : in std_logic_vector(1 downto 0);
    no_csync    : in std_logic := '0';
    blend       : in std_logic := '0';

    HSync       : in std_logic;
    VSync       : in std_logic;
    R           : in std_logic_vector(COLOR_DEPTH-1 downto 0);
    G           : in std_logic_vector(COLOR_DEPTH-1 downto 0);
    B           : in std_logic_vector(COLOR_DEPTH-1 downto 0);

    VGA_HS      : out std_logic;
    VGA_VS      : out std_logic;
    VGA_R       : out std_logic_vector(5 downto 0);
    VGA_G       : out std_logic_vector(5 downto 0);
    VGA_B       : out std_logic_vector(5 downto 0)
);
end component mist_video;


begin

  stm_rst_o  <= 'Z'; 
  sram_oe_n_o <= '1';
  sram_we_n_o <= '1';


  SDRAM_nCS <= '1'; -- disable ram
  res <= not btn_n_i(4) or downl;-- status(0) or buttons(1) or downl;
  p_color <= not status(2);
  pal <= status(1);
  p_dif(0) <= not status(3);
  p_dif(1) <= not status(4);
  joy_ana_0 <= joy_a_0 when status(6) = '0' else joy_a_1;
  joy_ana_1 <= joy_a_1 when status(6) = '0' else joy_a_0;
-- -----------------------------------------------------------------------
-- A2601 core
-- -----------------------------------------------------------------------
  a2601Instance : entity work.A2601NoFlash
    port map (
      vid_clk => vid_clk,
      clk => clk,
      audio => audio,
      O_VSYNC => O_VSYNC,
      O_HSYNC => O_HSYNC,
      O_VIDEO_R => O_VIDEO_R,
      O_VIDEO_G => O_VIDEO_G,
      O_VIDEO_B => O_VIDEO_B,
      res => res,
      p_l => p_l,
      p_r => p_r,
      p_a => p_a,
      p_b => p_b,
      p_u => p_u,
      p_d => p_d,
      p2_l => p2_l,
      p2_r => p2_r,
      p2_a => p2_a,
      p2_b => p2_b,
      p2_u => p2_u,
      p2_d => p2_d,
      paddle_0 => joy_ana_0(15 downto 8),
      paddle_1 => joy_ana_0(7 downto 0),
      paddle_2 => joy_ana_1(15 downto 8),
      paddle_3 => joy_ana_1(7 downto 0),
      paddle_ena => status(5),
      p_start => p_start,
      p_select => p_select,
      p_color => p_color,
      sc => sc,
      force_bs => force_bs,
      rom_a => rom_a,
      rom_do => rom_do,
      rom_size => rom_size,
      pal => pal,
      p_dif => p_dif,
      tv15khz => '1'
    );

  mist_video_inst : mist_video
    generic map (
        OSD_COLOR => "001"
    )
    port map (
        clk_sys     => vid_clk,
        scanlines   => status(8 downto 7),
        rotate      => "00",
        scandoubler_disable => scandoubler_disable,

        no_csync => no_csync,

        SPI_SCK     => SPI_SCK,
        SPI_SS3     => SPI_SS2,
        SPI_DI      => SPI_DI,

        HSync       => not O_HSYNC,
        VSync       => not O_VSYNC,
        R           => O_VIDEO_R,
        G           => O_VIDEO_G,
        B           => O_VIDEO_B,

        VGA_HS      => VGA_HS,
        VGA_VS      => VGA_VS,
        VGA_R(5 downto 1) => VGA_R,
        VGA_G(5 downto 1) => VGA_G,
        VGA_B(5 downto 1) => VGA_B
    );

  AUDIO_L <= audio;
  AUDIO_R <= audio;

  -- 9 pin d-sub joystick pinout:
  -- pin 1: up
  -- pin 2: down
  -- pin 3: left
  -- pin 4: right
  -- pin 6: fire

  -- Atari 2600, 6532 ports:
  -- PA0: right joystick, up
  -- PA1: right joystick, down
  -- PA2: right joystick, left
  -- PA3: right joystick, right
  -- PA4: left joystick, up
  -- PA5: left joystick, down
  -- PA6: left joystick, left
  -- PA7: left joystick, right
  -- PB0: start
  -- PB1: select
  -- PB3: B/W, color
  -- PB6: left difficulty
  -- PB7: right difficulty

  -- Atari 2600, TIA input:
  -- I5: right joystick, fire
  -- I6: left joystick, fire

  -- pinout docking station joystick 1/2:
  -- bit 0: up
  -- bit 1: down
  -- bit 2: left
  -- bit 3: right
  -- bit 4: fire
  -- bit 5: 2nd fire (required for paddle emulation)
  p_l <= joy1_s(2) when status(6) = '0' else joy2_s(2);
  p_r <= joy1_s(3) when status(6) = '0' else joy2_s(3);
  p_a <= joy1_s(4) when status(6) = '0' else joy2_s(4);
  p_b <= joy1_s(5) when status(6) = '0' else joy2_s(5);
  p_u <= joy1_s(0) when status(6) = '0' else joy2_s(0);
  p_d <= joy1_s(1) when status(6) = '0' else joy2_s(1);

  p2_l <= joy2_s(2) when status(6) = '0' else joy1_s(2);
  p2_r <= joy2_s(3) when status(6) = '0' else joy1_s(3);
  p2_a <= joy2_s(4) when status(6) = '0' else joy1_s(4);
  p2_b <= joy2_s(5) when status(6) = '0' else joy1_s(5);
  p2_u <= joy2_s(0) when status(6) = '0' else joy1_s(0);
  p2_d <= joy2_s(1) when status(6) = '0' else joy1_s(1);


-- -----------------------------------------------------------------------
-- Clocks and PLL
-- -----------------------------------------------------------------------
  pllInstance : entity work.pll
    port map (
      inclk0 => clock_50_i,
      c0 => vid_clk,
      c1 => clk,
      locked => open
    );

-- ------------------------------------------------------------------------
-- User IO
-- ------------------------------------------------------------------------

--  user_io_inst : user_io
--    generic map (STRLEN => CONF_STR'length)
--   port map (
--      clk_sys => vid_clk,
--      SPI_CLK => SPI_SCK,
--      SPI_SS_IO => CONF_DATA0,
--      SPI_MOSI => SPI_DI,
--      SPI_MISO => SPI_DO,
--      conf_str => to_slv(CONF_STR),
--      switches => switches,
--      buttons  => buttons,
--      scandoubler_disable => scandoubler_disable,
--      ypbpr => ypbpr,
--      no_csync => no_csync,
--      joystick_1 => joy0,
--      joystick_0 => joy1,
--      joystick_analog_1 => joy_a_0,
--      joystick_analog_0 => joy_a_1,
--      status => status,
--      sd_sdhc => '1',
--      ps2_kbd_clk => ps2Clk,
--      ps2_kbd_data => ps2Data
--    );

  data_io_inst: data_io
    generic map (STRLEN => CONF_STR'length)
    port map (
      clk_sys => vid_clk,
      SPI_SCK => SPI_SCK,
      SPI_SS2 => SPI_SS2,
      SPI_DI => SPI_DI,
      SPI_DO => SPI_DO,
      
      data_in   => keys_s,
      conf_str  => to_slv(CONF_STR),
      status    => status,
      config_buffer_o=> config_buffer_s,

      ioctl_download => downl,
      ioctl_index    => index,
--      ioctl_fileext  => file_ext,
      ioctl_wr       => rom_wr,
      ioctl_addr     => rom_wr_a,
      ioctl_dout     => rom_di
    );

  rom_inst: entity work.data_io_ram
    port map (
      -- wire up cpu port
      rdaddress => rom_a(14 downto 0),
      rdclock => clk,
      q => rom_do,

      -- io controller port
      wraddress => rom_wr_a(14 downto 0),
      wrclock => vid_clk,
      data => rom_di,
      wren => rom_wr
    );

  rom_size <= "00" & std_logic_vector(unsigned('0'&rom_wr_a(14 downto 0)) + 1);

  -- 2nd menu index - load with SuperChip support OR 3rd character in extension is 's'
  sc <= '1' when index(1) = '1' or config_buffer_s(9) = x"53" or config_buffer_s(9) = x"73" else '0';

  -- force bank switch type by file extension
  process (config_buffer_s) begin 
    force_bs <= "0000";
       if (config_buffer_s(10) = x"45" and config_buffer_s(9) = x"30") or (config_buffer_s(10) = x"65" and config_buffer_s(9) = x"30") then force_bs <= "0100"; -- E0
    elsif (config_buffer_s(10) = x"46" and config_buffer_s(9) = x"45") or (config_buffer_s(10) = x"66" and config_buffer_s(9) = x"65") then force_bs <= "0011"; -- FE
    elsif (config_buffer_s(10) = x"33" and config_buffer_s(9) = x"46") or (config_buffer_s(10) = x"33" and config_buffer_s(9) = x"66") then force_bs <= "0101"; -- 3F
    elsif (config_buffer_s(10) = x"50" and config_buffer_s(9) = x"32") or (config_buffer_s(10) = x"70" and config_buffer_s(9) = x"32") then force_bs <= "0111"; -- P2 (Pitfall II)
    elsif (config_buffer_s(10) = x"46" and config_buffer_s(9) = x"41") or (config_buffer_s(10) = x"66" and config_buffer_s(9) = x"61") then force_bs <= "1000"; -- FA
    elsif (config_buffer_s(10) = x"43" and config_buffer_s(9) = x"56") or (config_buffer_s(10) = x"63" and config_buffer_s(9) = x"76") then force_bs <= "1001"; -- CV
    elsif (config_buffer_s(10) = x"45" and config_buffer_s(9) = x"37") or (config_buffer_s(10) = x"65" and config_buffer_s(9) = x"37") then force_bs <= "1010"; -- E7
    elsif (config_buffer_s(10) = x"55" and config_buffer_s(9) = x"41") or (config_buffer_s(10) = x"75" and config_buffer_s(9) = x"61") then force_bs <= "1011"; -- UA
    end if;
  end process;

  keyboard : entity work.ps2Keyboard
    port map (vid_clk, '0', ps2_clk_io, ps2_data_io, ps2_scancode);

  p_start  <= '0' when (ps2_scancode = X"01" ) else '1'; -- F9 
  p_select <= '0' when (ps2_scancode = X"09" ) else '1'; -- F10

  -----------------------------------------------------------------------------------------

  -- Keys for OSD menu
  keys_s(7 downto 5) <= "001" when ps2_scancode = X"07" else "111";
  keys_s(4) <= '0' when ps2_scancode = x"5a" else '1';
  keys_s(3) <= '0' when ps2_scancode = x"74" else '1';
  keys_s(2) <= '0' when ps2_scancode = x"6b" else '1';
  keys_s(1) <= '0' when ps2_scancode = x"72" else '1';
  keys_s(0) <= '0' when ps2_scancode = x"75" else '1';

  --- Joystick read with sega 6 button support----------------------

    process(O_HSYNC)
        variable state_v : unsigned(7 downto 0) := (others=>'0');
        variable j1_sixbutton_v : std_logic := '0';
        variable j2_sixbutton_v : std_logic := '0';
    begin
        if falling_edge(O_HSYNC) then
        
            state_v := state_v + 1;
            
            case state_v is
                -- joy_s format MXYZ SACB RLDU
            
                when X"00" =>  
                    joyP7_s <= '0';
                    
                when X"01" =>
                    joyP7_s <= '1';

                when X"02" => 
                    joy1_s(3 downto 0) <= joy1_right_i & joy1_left_i & joy1_down_i & joy1_up_i; -- R, L, D, U
                    joy2_s(3 downto 0) <= joy2_right_i & joy2_left_i & joy2_down_i & joy2_up_i; -- R, L, D, U
                    joy1_s(5 downto 4) <= joy1_p9_i & joy1_p6_i; -- C, B
                    joy2_s(5 downto 4) <= joy2_p9_i & joy2_p6_i; -- C, B                    
                    joyP7_s <= '0';
                    j1_sixbutton_v := '0'; -- Assume it's not a six-button controller
                    j2_sixbutton_v := '0'; -- Assume it's not a six-button controller

                when X"03" =>
                
                    if (joy1_right_i = '0' and joy1_left_i = '0') then --// it's a megadrive controller
                        joy1_s(7 downto 6) <= joy1_p9_i & joy1_p6_i; -- Start, A
                    else-- read A/B as master System
                        joy1_s(4) <= joy1_p6_i; 
                        joy1_s(6) <= joy1_p9_i; 
                    end if;
                    
                    if (joy2_right_i = '0' and joy2_left_i = '0') then --// it's a megadrive controller
                        joy2_s(7 downto 6) <= joy2_p9_i & joy2_p6_i; -- Start, A
                    else -- read A/B as master System
                        joy2_s(4) <= joy2_p6_i; 
                        joy2_s(6) <= joy2_p9_i; 
                    end if;
                    
                    joyP7_s <= '1';
                    

                when X"04" =>  
                    joyP7_s <= '0';

                when X"05" =>
                    if joy1_right_i = '0' and joy1_left_i = '0' and joy1_down_i = '0' and joy1_up_i = '0' then 
                        j1_sixbutton_v := '1'; --it's a six button
                    end if;
                    
                    if joy2_right_i = '0' and joy2_left_i = '0' and joy2_down_i = '0' and joy2_up_i = '0' then 
                        j2_sixbutton_v := '1'; --it's a six button
                    end if;
                    
                    joyP7_s <= '1';
                    
                when X"06" =>
                    if j1_sixbutton_v = '1' then
                        joy1_s(11 downto 8) <= joy1_right_i & joy1_left_i & joy1_down_i & joy1_up_i; -- Mode, X, Y e Z
                    end if;
                    
                    if j2_sixbutton_v = '1' then
                        joy2_s(11 downto 8) <= joy2_right_i & joy2_left_i & joy2_down_i & joy2_up_i; -- Mode, X, Y e Z
                    end if;
                    
                    joyP7_s <= '0';

                when others =>
                    joyP7_s <= '1';
                    
            end case;

        end if;
    end process;
    
    joyX_p7_o <= joyP7_s;
    ---------------------------

end architecture;

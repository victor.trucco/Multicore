--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
--
-- Rev. 2020/12/14 - Oduvaldo (ducasp@gmail.com)
--      * Pushed Controler Handling to this VHDL so it can control a 6 button 
--        controller
--      * When using a Mega Drive 6 button controller you can use the MODE
---       button to invoke OSD menu, on 8bitdo M30 the - button is the MODE 
---       button.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.ALL;
use work.kbd_pkg.all;

entity colecoKeyboard is
generic
(
        OSD_CMD         : in   std_logic_vector(2 downto 0) := "011";
        CLK_SPEED       : integer := 21300

);
port
(
    clk         : in     std_logic;
    reset       : in     std_logic;

    -- inputs from PS/2 port
    ps2_clk     : inout  std_logic;
    ps2_data    : inout  std_logic;

    -- inputs from Joystick
    joystick_0  : in std_logic_vector(5 downto 0);
    joystick_1  : in std_logic_vector(5 downto 0);

    -- user outputs
    keys        : out std_logic_vector(15 downto 0);
    joy         : out std_logic_vector(15 downto 0);

    -- sg1000/sc3000 matrix
    sg1000_row  : in  std_logic_vector( 2 downto 0);
    sg1000_col  : out std_logic_vector(11 downto 0);

    -- fire12-1, up, down, left, right
    player1     : out std_logic_vector(7 downto 0);
    player2     : out std_logic_vector(7 downto 0);

    osd_enable  : in  std_logic;
    osd_o       : out std_logic_vector(7 downto 0);

    -- sega joystick
    sega_strobe : out std_logic
);
end colecoKeyboard;

architecture SYN of colecoKeyboard is

  component ps2kbd
    port
    (
      clk       : in  std_logic;
      rst_n     : in  std_logic;
      tick1us   : in  std_logic;
      ps2_clk   : in  std_logic;
      ps2_data  : in  std_logic;

      reset     : out std_logic;
      keydown   : out std_logic;
      keyup     : out std_logic;
      scancode  : out std_logic_vector(7 downto 0)
    );
  end component;

  signal rst_n      : std_logic;

  -- 1us tick for PS/2 interface
  signal tick1us    : std_logic;

  signal ps2_reset      : std_logic;
  signal ps2_press      : std_logic;
  signal ps2_release    : std_logic;
  signal ps2_scancode   : std_logic_vector(7 downto 0);

  type key_matrix is array (0 to 7) of std_logic_vector(11 downto 0);
  signal sg1000_matrix  : key_matrix;

  signal osd_s    : std_logic_vector(7 downto 0) := (others=>'1');
  -- sega
  signal p1 : std_logic_vector(7 downto 0);
  signal p2 : std_logic_vector(7 downto 0);
  signal clk_sega_s : std_logic := '0';
  signal clk_delay : unsigned(9 downto 0) := (others=>'1');
  signal TIMECLK   : integer;

  signal joyP7_s  : std_logic := '0';
  signal sega1_s  : std_logic_vector(11 downto 0) := (others=>'1');
  signal segaf1_s : std_logic_vector(11 downto 0) := (others=>'1');
  signal sega2_s  : std_logic_vector(11 downto 0) := (others=>'1');
  signal segaOSD_on : std_logic := '0';
  signal segaOSD_off : std_logic := '0';
  signal segaEmulate_Key1 : std_logic := '0';
  signal segaEmulate_Key2 : std_logic := '0';
  signal segaEmulate_Key3 : std_logic := '0';
  signal segaEmulate_Key4 : std_logic := '0';
  signal segaEmulate_Key5 : std_logic := '0';
  signal segaEmulate_Key6 : std_logic := '0';
  signal segaEmulate_Key7 : std_logic := '0';
  signal segaEmulate_Key8 : std_logic := '0';
  signal segaEmulate_Key9 : std_logic := '0';
  signal segaEmulate_KeyS : std_logic := '0';
  signal segaEmulate_Key0 : std_logic := '0';
  signal segaEmulate_KeyH : std_logic := '0';
  signal Keyb_Key1 : std_logic := '0';
  signal Keyb_Key2 : std_logic := '0';
  signal Keyb_Key3 : std_logic := '0';
  signal Keyb_Key4 : std_logic := '0';
  signal Keyb_Key5 : std_logic := '0';
  signal Keyb_Key6 : std_logic := '0';
  signal Keyb_Key7 : std_logic := '0';
  signal Keyb_Key8 : std_logic := '0';
  signal Keyb_Key9 : std_logic := '0';
  signal Keyb_KeyS : std_logic := '0';
  signal Keyb_Key0 : std_logic := '0';
  signal Keyb_KeyH : std_logic := '0';

begin

  osd_o <= osd_s and ("111" & (sega1_s(4) and sega1_s(5) and '1' and sega1_s(7)) & sega1_s(0) & sega1_s(1) & sega1_s(2) & sega1_s(3)) when osd_enable = '1' else osd_s(7 downto 5) & "11111";

  rst_n <= not reset;

  p1 <= ("00" & not segaf1_s(4) & not segaf1_s(5) & not segaf1_s(3) & not segaf1_s(2) & not segaf1_s(1) & not segaf1_s(0));
  p2 <= ("00" & not sega2_s(4) & not sega2_s(5) & not sega2_s(3) & not sega2_s(2) & not sega2_s(1) & not sega2_s(0));

  player1 <= p1;
  player2 <= p2;

  -- produce a 1us tick from the 20MHz ref clock
  process (clk, reset)
    variable count : integer range 0 to 19;
  begin
    if reset = '1' then
      tick1us <= '0';
      count := 0;
    elsif rising_edge (clk) then
      if count = 19 then
        tick1us <= '1';
        count := 0;
      else
        tick1us <= '0';
        count := count + 1;
      end if;
    end if;
  end process;
  
    sg1000_col <= not sg1000_matrix(CONV_INTEGER(sg1000_row));

    latchInputs: process (clk, rst_n)

    begin

         -- note: all inputs are active HIGH

        if rst_n = '0' then
           keys <= (others => '0');
           joy <= (others => '0');
        elsif rising_edge (clk) then

            if (ps2_press or ps2_release) = '1' then

               osd_s (7 downto 0) <= "11111111";

               case ps2_scancode is

                    -- this is not a valid scancode
                    -- but stuff the right button in here
                    when SCANCODE_X =>
                         keys(0) <= ps2_press;
                    when SCANCODE_8 =>
                         Keyb_Key8 <= ps2_press;
                    when SCANCODE_4 =>
                         Keyb_Key4 <= ps2_press;
                    when SCANCODE_5 =>
                         Keyb_Key5 <= ps2_press;
                    when SCANCODE_7 =>
                         Keyb_Key7 <= ps2_press;
                    when SCANCODE_Q =>            -- '#'
                         Keyb_KeyH <= ps2_press;
                    when SCANCODE_2 =>
                         Keyb_Key2 <= ps2_press;
                    when SCANCODE_W =>            -- '*'
                         Keyb_KeyS <= ps2_press;
                    when SCANCODE_0 =>
                         Keyb_Key0 <= ps2_press;
                    when SCANCODE_9 =>
                         Keyb_Key9 <= ps2_press;
                    when SCANCODE_3 =>
                         Keyb_Key3 <= ps2_press;
                    when SCANCODE_1 =>
                         Keyb_Key1 <= ps2_press;
                    when SCANCODE_6 =>
                         Keyb_Key6 <= ps2_press;

                    when SCANCODE_UP =>
                         joy(0) <= ps2_press; osd_s(0) <= not ps2_press;
                    when SCANCODE_DOWN =>
                         joy(1) <= ps2_press; osd_s(1) <= not ps2_press;
                    when SCANCODE_LEFT =>
                         joy(2) <= ps2_press; osd_s(2) <= not ps2_press;
                    when SCANCODE_RIGHT =>
                         joy(3) <= ps2_press; osd_s(3) <= not ps2_press;
                    when SCANCODE_Z =>
                         joy(4) <= ps2_press;

                    when SCANCODE_ENTER =>  osd_s(4) <= not ps2_press;

                     when X"07" => --F12
                         if (ps2_press = '1') then osd_s(7 downto 5) <= "011"; else osd_s(7 downto 5) <= "111"; end if;

                    when others =>
                end case;
            elsif (segaOSD_on = '1') then 
                osd_s(7 downto 5) <= OSD_CMD; -- OSD Menu command
            elsif (segaOSD_off = '1') then
                osd_s(7 downto 5) <= "111"; -- release
            end if; -- ps2_press or release

           

            -- sg1000 key matrix
            if (ps2_press or ps2_release) = '1' then
                case ps2_scancode is
                    when SCANCODE_8         => sg1000_matrix(0)(8) <= ps2_press;
                    when SCANCODE_I         => sg1000_matrix(0)(7) <= ps2_press;
                    when SCANCODE_K         => sg1000_matrix(0)(6) <= ps2_press;
                    when SCANCODE_COMMA     => sg1000_matrix(0)(5) <= ps2_press;
                    when SCANCODE_EQUALS    => sg1000_matrix(0)(4) <= ps2_press;
                    when SCANCODE_Z         => sg1000_matrix(0)(3) <= ps2_press;
                    when SCANCODE_A         => sg1000_matrix(0)(2) <= ps2_press;
                    when SCANCODE_Q         => sg1000_matrix(0)(1) <= ps2_press;
                    when SCANCODE_1         => sg1000_matrix(0)(0) <= ps2_press;

                    when SCANCODE_9         => sg1000_matrix(1)(8) <= ps2_press;
                    when SCANCODE_O         => sg1000_matrix(1)(7) <= ps2_press;
                    when SCANCODE_L         => sg1000_matrix(1)(6) <= ps2_press;
                    when SCANCODE_PERIOD    => sg1000_matrix(1)(5) <= ps2_press;
                    when SCANCODE_SPACE     => sg1000_matrix(1)(4) <= ps2_press;
                    when SCANCODE_X         => sg1000_matrix(1)(3) <= ps2_press;
                    when SCANCODE_S         => sg1000_matrix(1)(2) <= ps2_press;
                    when SCANCODE_W         => sg1000_matrix(1)(1) <= ps2_press;
                    when SCANCODE_2         => sg1000_matrix(1)(0) <= ps2_press;

                    when SCANCODE_0         => sg1000_matrix(2)(8) <= ps2_press;
                    when SCANCODE_P         => sg1000_matrix(2)(7) <= ps2_press;
                    when SCANCODE_SEMICOLON => sg1000_matrix(2)(6) <= ps2_press;
                    when SCANCODE_SLASH     => sg1000_matrix(2)(5) <= ps2_press;
                    when SCANCODE_HOME      => sg1000_matrix(2)(4) <= ps2_press;
                    when SCANCODE_C         => sg1000_matrix(2)(3) <= ps2_press;
                    when SCANCODE_D         => sg1000_matrix(2)(2) <= ps2_press;
                    when SCANCODE_E         => sg1000_matrix(2)(1) <= ps2_press;
                    when SCANCODE_3         => sg1000_matrix(2)(0) <= ps2_press;

                    when SCANCODE_MINUS     => sg1000_matrix(3)(8) <= ps2_press;
                    when SCANCODE_BACKQUOTE => sg1000_matrix(3)(7) <= ps2_press;
                    when SCANCODE_QUOTE     => sg1000_matrix(3)(6) <= ps2_press;
                    when SCANCODE_BACKSPACE => sg1000_matrix(3)(4) <= ps2_press;
                    when SCANCODE_DELETE    => sg1000_matrix(3)(4) <= ps2_press;
                    when SCANCODE_INS       => sg1000_matrix(3)(4) <= ps2_press;
                    when SCANCODE_V         => sg1000_matrix(3)(3) <= ps2_press;
                    when SCANCODE_F         => sg1000_matrix(3)(2) <= ps2_press;
                    when SCANCODE_R         => sg1000_matrix(3)(1) <= ps2_press;
                    when SCANCODE_4         => sg1000_matrix(3)(0) <= ps2_press;

                    when SCANCODE_F1        => sg1000_matrix(4)(8) <= ps2_press; -- ^
                    when SCANCODE_OPENBRKT  => sg1000_matrix(4)(7) <= ps2_press;
                    when SCANCODE_CLOSEBRKT => sg1000_matrix(4)(6) <= ps2_press;
                    when SCANCODE_DOWN      => sg1000_matrix(4)(5) <= ps2_press;
                    when SCANCODE_B         => sg1000_matrix(4)(3) <= ps2_press;
                    when SCANCODE_G         => sg1000_matrix(4)(2) <= ps2_press;
                    when SCANCODE_T         => sg1000_matrix(4)(1) <= ps2_press;
                    when SCANCODE_5         => sg1000_matrix(4)(0) <= ps2_press;

                    when SCANCODE_LALT      => sg1000_matrix(5)(11) <= ps2_press; -- FUNC
                    when SCANCODE_BACKSLASH => sg1000_matrix(5)(8) <= ps2_press;
                    when SCANCODE_ENTER     => sg1000_matrix(5)(6) <= ps2_press;  
                    when SCANCODE_LEFT      => sg1000_matrix(5)(5) <= ps2_press;
                    when SCANCODE_N         => sg1000_matrix(5)(3) <= ps2_press;
                    when SCANCODE_H         => sg1000_matrix(5)(2) <= ps2_press;
                    when SCANCODE_Y         => sg1000_matrix(5)(1) <= ps2_press;
                    when SCANCODE_6         => sg1000_matrix(5)(0) <= ps2_press;

                    when SCANCODE_RSHIFT    => sg1000_matrix(6)(11) <= ps2_press;
                    when SCANCODE_LSHIFT    => sg1000_matrix(6)(11) <= ps2_press;
                    when SCANCODE_LCTRL     => sg1000_matrix(6)(10) <= ps2_press;
                    when SCANCODE_TAB       => sg1000_matrix(6)(9) <= ps2_press; -- GRAPH
                    when SCANCODE_ESC       => sg1000_matrix(6)(8) <= ps2_press; -- BREAK
                    when SCANCODE_UP        => sg1000_matrix(6)(6) <= ps2_press; 
                    when SCANCODE_RIGHT     => sg1000_matrix(6)(5) <= ps2_press;
                    when SCANCODE_M         => sg1000_matrix(6)(3) <= ps2_press;
                    when SCANCODE_J         => sg1000_matrix(6)(2) <= ps2_press;
                    when SCANCODE_U         => sg1000_matrix(6)(1) <= ps2_press;
                    when SCANCODE_7         => sg1000_matrix(6)(0) <= ps2_press;

                    when others          => null;
                end case;
            end if;

            if (ps2_reset = '1') then
               keys <= (others => '0');
               joy <= (others => '0');
               sg1000_matrix(0) <= (others => '0');
               sg1000_matrix(1) <= (others => '0');
               sg1000_matrix(2) <= (others => '0');
               sg1000_matrix(3) <= (others => '0');
               sg1000_matrix(4) <= (others => '0');
               sg1000_matrix(5) <= (others => '0');
               sg1000_matrix(6) <= (others => '0');
               sg1000_matrix(7) <= (others => '0');
            end if;
        end if; -- rising_edge (clk)

    keys(13) <= segaEmulate_Key1 or Keyb_Key1;
    keys(7)  <= segaEmulate_Key2 or Keyb_Key2;
    keys(12) <= segaEmulate_Key3 or Keyb_Key3;
    keys(2)  <= segaEmulate_Key4 or Keyb_Key4;
    keys(3)  <= segaEmulate_Key5 or Keyb_Key5;
    keys(14) <= segaEmulate_Key6 or Keyb_Key6;
    keys(5)  <= segaEmulate_Key7 or Keyb_Key7;
    keys(1)  <= segaEmulate_Key8 or Keyb_Key8;
    keys(11) <= segaEmulate_Key9 or Keyb_Key9;
    keys(9)  <= segaEmulate_KeyS or Keyb_KeyS;
    keys(10) <= segaEmulate_Key0 or Keyb_Key0;
    keys(6)  <= segaEmulate_KeyH or Keyb_KeyH;
    end process latchInputs;

--- Joystick read with sega 6 button support----------------------

  process(clk)
  begin
    if rising_edge(clk) then

        
        TIMECLK <= (9 * (CLK_SPEED/1000)); -- calculate ~9us from the master clock

        clk_delay <= clk_delay - 1;
        
        if (clk_delay = 0) then
            clk_sega_s <= not clk_sega_s;
            clk_delay <= to_unsigned(TIMECLK,10); 
        end if;

    end if;
  end process;


  process(clk)
    variable state_v : unsigned(8 downto 0) := (others=>'0');
    variable j1_sixbutton_v : std_logic := '0';
    variable j2_sixbutton_v : std_logic := '0';
    variable sega_edge : std_logic_vector(1 downto 0);
    variable segaOSD_edge : std_logic_vector(1 downto 0) := (others=>'0');
  begin
    if rising_edge(clk) then

        sega_edge := sega_edge(0) & clk_sega_s;

        if sega_edge = "01" then
            state_v := state_v + 1;
            
            case state_v is
                -- joy_s format MXYZ ASCB UDLR
                
                when '0'&X"01" =>  
                    joyP7_s <= '0';
                    
                when '0'&X"02" =>  
                    joyP7_s <= '1';
                    
                when '0'&X"03" => 
                    sega1_s(5 downto 0) <= joystick_0(5 downto 0); -- C, B, up, down, left, right 
                    sega2_s(5 downto 0) <= joystick_1(5 downto 0);
                    
                    j1_sixbutton_v := '0'; -- Assume it's not a six-button controller
                    j2_sixbutton_v := '0'; -- Assume it's not a six-button controller

                    joyP7_s <= '0';

                when '0'&X"04" =>
                    if joystick_0(0) = '0' and joystick_0(1) = '0' then -- it's a megadrive controller
                                sega1_s(7 downto 6) <= joystick_0(5 downto 4); -- A, Start
                    else
                                sega1_s(7 downto 4) <= joystick_0(4) & '1' & joystick_0(5) & '1'; -- read A/B as master System
                    end if;
                            
                    if joystick_1(0) = '0' and joystick_1(1) = '0' then -- it's a megadrive controller
                                sega2_s(7 downto 6) <= joystick_1(5 downto 4); -- A, Start
                    else
                                sega2_s(7 downto 4) <= joystick_1(4) & '1' & joystick_1(5) & '1'; -- read A/B as master System
                    end if;
                    
                                        
                    joyP7_s <= '1';
            
                when '0'&X"05" =>  
                    joyP7_s <= '0';
                    
                when '0'&X"06" =>
                    if joystick_0(2) = '0' and joystick_0(3) = '0' then 
                        j1_sixbutton_v := '1'; --it's a six button
                    end if;
                    
                    if joystick_1(2) = '0' and joystick_1(3) = '0' then 
                        j2_sixbutton_v := '1'; --it's a six button
                    end if;
                    
                    joyP7_s <= '1';
                    
                when '0'&X"07" =>
                    if j1_sixbutton_v = '1' then
                        sega1_s(11 downto 8) <= joystick_0(0) & joystick_0(1) & joystick_0(2) & joystick_0(3); -- Mode, X, Y e Z                        
                    end if;

                    if j2_sixbutton_v = '1' then
                        sega2_s(11 downto 8) <= joystick_1(0) & joystick_1(1) & joystick_1(2) & joystick_1(3); -- Mode, X, Y e Z
                    end if;

                    if (sega1_s(11) = '0' and sega1_s(6) = '0') or (sega2_s(11) = '0' and sega2_s(6) = '0') then
                        segaOSD_edge := segaOSD_edge(0) & '1';
                    else
                        segaOSD_edge := segaOSD_edge(0) & '0';
                    end if;

                    if (segaOSD_edge = "10") then segaOSD_off <= '1'; else segaOSD_off <= '0'; end if;
                    if (segaOSD_edge = "01") then segaOSD_on <= '1'; else segaOSD_on <= '0'; end if;
                    joyP7_s <= '0';

                when others =>
                    joyP7_s <= '1';
                    
            end case;

            segaEmulate_Key1 <= (not sega1_s(10)) and sega1_s(6)       and (not sega1_s(11));
            segaEmulate_Key2 <= (not sega1_s(9))  and sega1_s(6)       and (not sega1_s(11));
            segaEmulate_Key3 <= (not sega1_s(8))  and sega1_s(6)       and (not sega1_s(11));
            segaEmulate_Key4 <= (not sega1_s(7))  and sega1_s(6)       and (not sega1_s(11));
            segaEmulate_Key5 <= (not sega1_s(5))  and sega1_s(6)       and (not sega1_s(11));
            segaEmulate_Key6 <= (not sega1_s(4))  and sega1_s(6)       and (not sega1_s(11));
            segaEmulate_Key7 <= (not sega1_s(10)) and (not sega1_s(6)) and sega1_s(11);
            segaEmulate_Key8 <= (not sega1_s(9))  and (not sega1_s(6)) and sega1_s(11);
            segaEmulate_Key9 <= (not sega1_s(8))  and (not sega1_s(6)) and sega1_s(11);
            segaEmulate_KeyS <= (not sega1_s(7))  and (not sega1_s(6)) and sega1_s(11);
            segaEmulate_Key0 <= (not sega1_s(5))  and (not sega1_s(6)) and sega1_s(11);
            segaEmulate_KeyH <= (not sega1_s(4))  and (not sega1_s(6)) and sega1_s(11);
            segaf1_s <= sega1_s;
            --- In case virtual keypad key is pressed, do not press the button related to it
            if ( segaEmulate_Key5 = '1' or segaEmulate_Key0 = '1' ) then segaf1_s(5) <= '1'; end if;
            if ( segaEmulate_Key6 = '1' or segaEmulate_KeyH = '1' ) then segaf1_s(4) <= '1'; end if;
            
        end if;
    end if;
  end process;

  sega_strobe <= joyP7_s;
---------------------------

  ps2kbd_inst : ps2kbd                                        
    port map
    (
      clk       => clk,                                     
      rst_n     => rst_n,
      tick1us   => tick1us,
      ps2_clk   => ps2_clk,
      ps2_data  => ps2_data,

      reset     => ps2_reset,
      keydown   => ps2_press,
      keyup     => ps2_release,
      scancode  => ps2_scancode
    );

end SYN;

--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-------------------------------------------------------------------------------
--
-- SNESpad controller core
--
-- Copyright (c) 2004, Arnim Laeuger (arniml@opencores.org)
--
-- $Id: snespad_pack-p.vhd,v 1.1 2004/10/05 17:01:27 arniml Exp $
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

package snespad_pack is

  constant num_buttons_c : natural := 12;
  subtype  buttons_t is std_logic_vector(num_buttons_c-1 downto 0);
  subtype  num_buttons_read_t is natural range 0 to num_buttons_c-1;

  function button_active_f(state : in std_logic; ref : in natural) return std_logic;
  function button_reset_f(ref : in natural) return std_logic;
  function "=" (a : std_logic; b : integer) return boolean;

  -----------------------------------------------------------------------------
  -- The button positions inside the SNES packet
  -----------------------------------------------------------------------------
  constant but_pos_b_c     : natural := 11;
  constant but_pos_y_c     : natural := 10;
  constant but_pos_sel_c   : natural := 9;
  constant but_pos_start_c : natural := 8;
  constant but_pos_up_c    : natural := 7;
  constant but_pos_down_c  : natural := 6;
  constant but_pos_left_c  : natural := 5;
  constant but_pos_right_c : natural := 4;
  constant but_pos_a_c     : natural := 3;
  constant but_pos_x_c     : natural := 2;
  constant but_pos_tl_c    : natural := 1;
  constant but_pos_tr_c    : natural := 0;

end snespad_pack;


package body snespad_pack is

  function button_active_f(state : in std_logic; ref : in natural) return std_logic is
    variable result_v : std_logic;
  begin
    if ref = 0 then
      result_v := state;
    else
      result_v := not state;
    end if;

    return result_v;
  end button_active_f;

  function button_reset_f(ref : in natural) return std_logic is
    variable result_v : std_logic;
  begin
    if ref = 0 then
      result_v := '1';
    else
      result_v := '0';
    end if;

    return result_v;
  end button_reset_f;

  function "=" (a : std_logic; b : integer) return boolean is
    variable result_v : boolean;
  begin
    result_v := false;

    case a is
      when '0' =>
        if b = 0 then
          result_v := true;
        end if;

      when '1' =>
        if b = 1 then
          result_v := true;
        end if;

      when others =>
        null;

    end case;

    return result_v;
  end;


end snespad_pack;

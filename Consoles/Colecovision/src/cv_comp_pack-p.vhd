--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-------------------------------------------------------------------------------
--
-- $Id: cv_comp_pack-p.vhd,v 1.5 2006/01/05 22:22:29 arnim Exp $
--
-- Copyright (c) 2006, Arnim Laeuger (arnim.laeuger@gmx.net)
--
-- All rights reserved
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

package cv_comp_pack is

  component cv_clock
    port (
      clk_i         : in  std_logic;
      clk_en_10m7_i : in  std_logic;
      reset_n_i     : in  std_logic;
      clk_en_3m58_p_o : out std_logic;
      clk_en_3m58_n_o : out std_logic
    );
  end component;

  component cv_ctrl
    port (
      clk_i           : in  std_logic;
      clk_en_3m58_i   : in  std_logic;
      reset_n_i       : in  std_logic;
      ctrl_en_key_n_i : in  std_logic;
      ctrl_en_joy_n_i : in  std_logic;
      a1_i            : in  std_logic;
      ctrl_p1_i       : in  std_logic_vector(2 downto 1);
      ctrl_p2_i       : in  std_logic_vector(2 downto 1);
      ctrl_p3_i       : in  std_logic_vector(2 downto 1);
      ctrl_p4_i       : in  std_logic_vector(2 downto 1);
      ctrl_p5_o       : out std_logic_vector(2 downto 1);
      ctrl_p6_i       : in  std_logic_vector(2 downto 1);
      ctrl_p7_i       : in  std_logic_vector(2 downto 1);
      ctrl_p8_o       : out std_logic_vector(2 downto 1);
      ctrl_p9_i       : in  std_logic_vector(2 downto 1);
      d_o             : out std_logic_vector(7 downto 0);
      int_n_o         : out std_logic
    );
  end component;

  component cv_addr_dec
    port (
      clk_i           : in  std_logic;
      reset_n_i       : in  std_logic;
      sg1000          : in  std_logic;
      dahjeeA_i       : in  std_logic;
      a_i             : in  std_logic_vector(15 downto 0);
      d_i             : in  std_logic_vector(7 downto 0);
      cart_pages_i    : in  std_logic_vector(5 downto 0);
      cart_page_o     : out std_logic_vector(5 downto 0);
      iorq_n_i        : in  std_logic;
      rd_n_i          : in  std_logic;
      wr_n_i          : in  std_logic;
      mreq_n_i        : in  std_logic;
      rfsh_n_i        : in  std_logic;
      bios_rom_ce_n_o : out std_logic;
      ram_ce_n_o      : out std_logic;
      vdp_r_n_o       : out std_logic;
      vdp_w_n_o       : out std_logic;
      psg_we_n_o      : out std_logic;
      ay_addr_we_n_o  : out std_logic;
      ay_data_we_n_o  : out std_logic;
      ay_data_rd_n_o  : out std_logic;
      ctrl_r_n_o      : out std_logic;
      ctrl_en_key_n_o : out std_logic;
      ctrl_en_joy_n_o : out std_logic;
      cart_en_80_n_o  : out std_logic;
      cart_en_a0_n_o  : out std_logic;
      cart_en_c0_n_o  : out std_logic;
      cart_en_e0_n_o  : out std_logic;
      cart_en_sg1000_n_o: out std_logic;
      pa_r_sg1000_n_o : out std_logic;
      pb_r_sg1000_n_o : out std_logic;
      pc_w_sg1000_n_o : out std_logic
    );
  end component;

  component cv_bus_mux
    port (
      bios_rom_ce_n_i : in  std_logic;
      ram_ce_n_i      : in  std_logic;
      vdp_r_n_i       : in  std_logic;
      ctrl_r_n_i      : in  std_logic;
      cart_en_80_n_i  : in  std_logic;
      cart_en_a0_n_i  : in  std_logic;
      cart_en_c0_n_i  : in  std_logic;
      cart_en_e0_n_i  : in  std_logic;
      cart_en_sg1000_n_i : in std_logic;
      pa_r_sg1000_n_i : in  std_logic;
      pb_r_sg1000_n_i : in  std_logic;
      ay_data_rd_n_i  : in  std_logic;
      bios_rom_d_i    : in  std_logic_vector(7 downto 0);
      cpu_ram_d_i     : in  std_logic_vector(7 downto 0);
      vdp_d_i         : in  std_logic_vector(7 downto 0);
      ctrl_d_i        : in  std_logic_vector(7 downto 0);
      cart_d_i        : in  std_logic_vector(7 downto 0);
      ay_d_i          : in  std_logic_vector(7 downto 0);
      col_sg1000_i    : in  std_logic_vector(11 downto 0);
      tap_sg1000_i    : in  std_logic;
      d_o             : out std_logic_vector(7 downto 0)
    );
  end component;

end cv_comp_pack;

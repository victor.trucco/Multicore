--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-------------------------------------------------------------------------------
--
-- Synthesizable model of TI's TMS9918A, TMS9928A, TMS9929A.
--
-- $Id: vdp18_core-c.vhd,v 1.10 2006/06/18 10:47:01 arnim Exp $
--
-------------------------------------------------------------------------------

configuration vdp18_core_struct_c0 of vdp18_core is

  for struct

    for clk_gen_b: vdp18_clk_gen
      use configuration work.vdp18_clk_gen_rtl_c0;
    end for;

    for hor_vert_b: vdp18_hor_vert
      use configuration work.vdp18_hor_vert_rtl_c0;
    end for;

    for ctrl_b: vdp18_ctrl
      use configuration work.vdp18_ctrl_rtl_c0;
    end for;

    for cpu_io_b: vdp18_cpuio
      use configuration work.vdp18_cpuio_rtl_c0;
    end for;

    for addr_mux_b: vdp18_addr_mux
      use configuration work.vdp18_addr_mux_rtl_c0;
    end for;

    for pattern_b: vdp18_pattern
      use configuration work.vdp18_pattern_rtl_c0;
    end for;

    for sprite_b: vdp18_sprite
      use configuration work.vdp18_sprite_rtl_c0;
    end for;

    for col_mux_b: vdp18_col_mux
      use configuration work.vdp18_col_mux_rtl_c0;
    end for;

  end for;

end vdp18_core_struct_c0;

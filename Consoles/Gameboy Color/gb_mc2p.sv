/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///
// gb_mist.v
//
// Gameboy for the MIST board https://github.com/mist-devel
// 
// Copyright (c) 2015 Till Harbaum <till@harbaum.org> 
// 
// This source file is free software: you can redistribute it and/or modify 
// it under the terms of the GNU General Public License as published 
// by the Free Software Foundation, either version 3 of the License, or 
// (at your option) any later version. 
// 
// This source file is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License 
// along with this program.  If not, see <http://www.gnu.org/licenses/>. 
//

//============================================================================
//
//  Multicore 2+ Top by Victor Trucco
//
//============================================================================

`default_nettype none


module gb_mc2p (
   // Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAM (IS61WV20488FBLL-10)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM (W9825G6KH-6)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io        = 1'bz,
    inout wire  ps2_data_io       = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o         = 1'bZ,
    output wire sd_sclk_o         = 1'bZ,
    output wire sd_mosi_o         = 1'bZ,
    input wire  sd_miso_i,

    // Joysticks
    output wire joy_clock_o       = 1'b1,
    output wire joy_load_o        = 1'b1,
    input  wire joy_data_i,
    output wire joy_p7_o          = 1'b1,

    // Audio
    output      AUDIO_L,
    output      AUDIO_R,
    input wire  ear_i,
    output wire mic_o             = 1'b0,

    // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

    //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
   
    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output wire   SPI_nWAIT        = 1'b1, // '0' to hold the microcontroller data streaming

    inout [31:0] GPIO,

    output LED                    = 1'b1 // '0' is LED on
);


//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------
assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign stm_rst_o    = 1'bZ;
assign stm_rx_o = 1'bZ;
assign LED  = ~dio_download;

//no SRAM for this core
assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;

//all the SD reading goes thru the microcontroller for this core
assign sd_cs_n_o = 1'bZ;
assign sd_sclk_o = 1'bZ;
assign sd_mosi_o = 1'bZ;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

joystick_serial  joystick_serial 
(
    .clk_i           ( cnt_clk[1] ),
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

//-----------------------------------------------------------------

// mix both joysticks to allow the user to use any
wire [7:0] joystick = joystick_0 | joystick_1;
wire [7:0] joystick_0;
wire [7:0] joystick_1;

// the configuration string is returned to the io controller to allow
// it to control the menu on the OSD 
parameter CONF_STR = {
        "S,GBC/GB,Load Game;",
        "O4,Mode,Auto,Color;",
        "O1,LCD color,white,yellow;",
        "O2,Boot,Normal,Fast;",
        "T3,Reset"
};


// the status register is controlled by the on screen display (OSD)
wire [7:0] status;
wire [1:0] buttons;
wire       isGBC = (dio_index[7:6] == 0) || status[4];

/*
// include user_io module for arm controller communication
user_io #(.STRLEN(CONF_STR_LEN)) user_io ( 
      .conf_str   ( CONF_STR   ),
      .clk_sys    ( clk64      ),
      .SPI_CLK    ( SPI_SCK    ),
      .SPI_SS_IO  ( CONF_DATA0 ),
      .SPI_MISO   ( SPI_DO     ),
      .SPI_MOSI   ( SPI_DI     ),

      .status     ( status     ),
      .buttons    ( buttons    ),

      .joystick_0 ( joystick_0 ),
      .joystick_1 ( joystick_1 )
);
*/
wire reset = (reset_cnt != 0);
reg [9:0] reset_cnt;
always @(posedge clk64) begin
    if( status[3] || ~btn_n_i[4] || !pll_locked || dio_download)
        reset_cnt <= 10'd1023;
    else
        if(reset_cnt != 0)
            reset_cnt <= reset_cnt - 10'd1;
end

assign SDRAM_CKE = 1'b1;

sdram sdram (
   // interface to the MT48LC16M16 chip
   .sd_data        ( SDRAM_DQ                  ),
   .sd_addr        ( SDRAM_A                   ),
   .sd_dqm         ( {SDRAM_DQMH, SDRAM_DQML}  ),
   .sd_cs          ( SDRAM_nCS                 ),
   .sd_ba          ( SDRAM_BA                  ),
   .sd_we          ( SDRAM_nWE                 ),
   .sd_ras         ( SDRAM_nRAS                ),
   .sd_cas         ( SDRAM_nCAS                ),

    // system interface
   .clk            ( clk64                     ),
   .sync           ( clk8                      ),
   .init           ( !pll_locked               ),

   // cpu interface
   .din            ( sdram_di                  ),
   .addr           ( sdram_addr                ),
   .ds             ( sdram_ds                  ),
   .we             ( sdram_we                  ),
   .oe             ( sdram_oe                  ),
   .dout           ( sdram_do                  )
);

// TODO: ds for cart ram write
wire [1:0] sdram_ds    = dio_download ? 2'b11    : {!cart_addr[0], cart_addr[0]};
wire [15:0] sdram_do;
wire [15:0] sdram_di   = dio_download ? dio_data : {cart_di, cart_di};
wire [23:0] sdram_addr = dio_download ? dio_addr : {1'b0, mbc_bank, cart_addr[12:1]};
wire sdram_oe = !dio_download && cart_rd;
wire sdram_we = (dio_download && dio_write) || (!dio_download && cart_ram_wr);

wire dio_download;
wire [23:0] dio_addr;
wire [15:0] dio_data;
wire dio_write;
wire [7:0] dio_index;

// TODO: RAM bank
// http://fms.komkon.org/GameBoy/Tech/Carts.html

// 32MB SDRAM memory map using word addresses
// 2 2 2 2 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 D
// 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 S
// -------------------------------------------------
// 0 0 X X X X X X X X X X X X X X X X X X X X X X X up to 8MB used as ROM
// 0 1 X X X X X X X X X X X X X X X X X X X X X X X up to 8MB used as RAM
// 0 0 0 0 R R B B B B B C C C C C C C C C C C C C C MBC1 ROM (R=RAM bank in mode 0)
// 0 1 0 0 0 0 0 0 0 0 R R C C C C C C C C C C C C C MBC1 RAM (R=RAM bank in mode 1)
// 0 0 0 0 B B B B B B B C C C C C C C C C C C C C C MBC2 ROM
// 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 C C C C C C C C C MBC2 RAM
// 0 0 0 0 B B B B B B B C C C C C C C C C C C C C C MBC3 ROM
// 0 1 0 0 0 0 0 0 0 R R C C C C C C C C C C C C C C MBC3 RAM
// 0 0 B B B B B B B B B C C C C C C C C C C C C C C MBC5 ROM
// 0 1 0 0 0 0 0 0 R R R R C C C C C C C C C C C C C MBC5 RAM

// ---------------------------------------------------------------

wire [10:0] mbc1_addr =
    (cart_addr[15:14] == 2'b00)?{10'd0, cart_addr[13]}:                 // 16k ROM Bank 0
    (cart_addr[15:14] == 2'b01)?{3'b000, mbc1_rom_bank, cart_addr[13]}: // 16k ROM Bank 1-127
    (cart_addr[15:13] == 3'b101)?{9'b100000000, mbc1_ram_bank}:         // 8k RAM Bank 0-3
    11'd0;

wire [10:0] mbc2_addr =
    (cart_addr[15:14] == 2'b00)?{10'd0, cart_addr[13]}:                 // 16k ROM Bank 0
    (cart_addr[15:14] == 2'b01)?{6'd0, mbc2_rom_bank, cart_addr[13]}:   // 16k ROM Bank 1-15
    (cart_addr[15:9] == 7'b1010000)?(11'b10000000000):                  // 512x4 bit RAM
    11'd0;

wire [10:0] mbc3_addr =
    (cart_addr[15:14] == 2'b00)?{10'd0, cart_addr[13]}:                 // 16k ROM Bank 0
    (cart_addr[15:14] == 2'b01)?{3'b000, mbc3_rom_bank, cart_addr[13]}: // 16k ROM Bank 1-127
    (cart_addr[15:13] == 3'b101)?{9'b100000000, mbc3_ram_bank}:         // 8k RAM Bank 0-3
    11'd0;

wire [10:0] mbc5_addr =
    (cart_addr[15:14] == 2'b00)?{10'd0, cart_addr[13]}:                 // 16k ROM Bank 0
    (cart_addr[15:14] == 2'b01)?{1'b0, mbc5_rom_bank, cart_addr[13]}:   // 16k ROM Bank 0-480 (0h-1E0h)
    (cart_addr[15:13] == 3'b101)?{7'b1000000, mbc5_ram_bank}:           // 8k RAM Bank 0-15
    11'd0;

// -------------------------- RAM banking ------------------------

// in mode 0 (16/8 mode) the ram is not banked 
// in mode 1 (4/32 mode) four ram banks are used
wire [1:0] mbc1_ram_bank = (mbc1_mode?mbc_ram_bank_reg[1:0]:2'b00) & ram_mask[1:0];
wire [1:0] mbc3_ram_bank = mbc_ram_bank_reg[1:0] & ram_mask[1:0];
wire [3:0] mbc5_ram_bank = mbc_ram_bank_reg & ram_mask;

// -------------------------- ROM banking ------------------------

// in mode 0 (16/8 mode) the ram bank select signals are the upper rom address lines 
// in mode 1 (4/32 mode) the upper two rom address lines are 2'b00
wire [6:0] mbc1_rom_bank_mode = { mbc1_mode?2'b00:mbc_ram_bank_reg[1:0], mbc_rom_bank_reg[4:0]};
// mask address lines to enable proper mirroring
wire [6:0] mbc1_rom_bank = mbc1_rom_bank_mode & rom_mask[6:0];
wire [3:0] mbc2_rom_bank = mbc_rom_bank_reg[3:0] & rom_mask[3:0];  //16
wire [6:0] mbc3_rom_bank = mbc_rom_bank_reg[6:0] & rom_mask[6:0];  //128
wire [8:0] mbc5_rom_bank = mbc_rom_bank_reg & rom_mask;  //480

// --------------------- CPU register interface ------------------
reg mbc_ram_enable;
reg mbc1_mode;
reg mbc3_mode;
reg [8:0] mbc_rom_bank_reg;
reg [3:0] mbc_ram_bank_reg;

always @(posedge clk64) begin
    if(reset) begin
        mbc_rom_bank_reg <= 5'd1;
        mbc_ram_bank_reg <= 2'd0;
        mbc_ram_enable <= 1'b0;
        mbc1_mode <= 1'b0;
    end else begin
        //write to ROM bank register
        if(cart_wr && (cart_addr[15:13] == 3'b001)) begin
            if(~mbc5 && cart_di[6:0]==0) //special case mbc1-3 rombank 0=1
                mbc_rom_bank_reg <= 5'd1;
            else if (mbc5) begin
                if (cart_addr[13:12] == 2'b11) //3000-3FFF High bit
                    mbc_rom_bank_reg[8] <= cart_di[0];
                else //2000-2FFF low 8 bits
                    mbc_rom_bank_reg[7:0] <= cart_di[7:0];
            end else
                mbc_rom_bank_reg <= {2'b00,cart_di[6:0]}; //mbc1-3
        end

        //write to RAM bank register
        if(cart_wr && (cart_addr[15:13] == 3'b010)) begin
            if (mbc3) begin
                if (cart_di[3]==1)
                    mbc3_mode <= 1'b1; //enable RTC
                else begin
                    mbc3_mode <= 1'b0; //enable RAM
                    mbc_ram_bank_reg <= {2'b00,cart_di[1:0]};
                end
            end else
                if (mbc5)//can probably be simplified
                    mbc_ram_bank_reg <= cart_di[3:0];
                else
                    mbc_ram_bank_reg <= {2'b00,cart_di[1:0]};
        end 

        if(cart_wr && (cart_addr[15:13] == 3'b000))
            mbc_ram_enable <= (cart_di[3:0] == 4'ha);

        if(cart_wr && (cart_addr[15:13] == 3'b011))
            mbc1_mode <= cart_di[0];
    end
end

// extract header fields extracted from cartridge
// during download
reg [7:0] cart_mbc_type;
reg [7:0] cart_rom_size;
reg [7:0] cart_ram_size;
reg [7:0] cart_cgb_flag;
wire      isGBC_game = (cart_cgb_flag == 8'h80 || cart_cgb_flag == 8'hC0);

// only write sdram if the write attept comes from the cart ram area
wire cart_ram_wr = cart_wr && mbc_ram_enable && ((cart_addr[15:13] == 3'b101 && ~mbc2) || (cart_addr[15:9] == 7'b1010000 && mbc2));

// RAM size
wire [3:0] ram_mask =                   // 0 - no ram
    (cart_ram_size == 1)?4'b0000:       // 1 - 2k, 1 bank
    (cart_ram_size == 2)?4'b0000:       // 2 - 8k, 1 bank
    (cart_ram_size == 3)?4'b0011:       // 3 - 32k, 4 banks
    4'b1111;                            // 4 - 128k 16 banks

// ROM size
wire [8:0] rom_mask =                            // 0 - 2 banks, 32k direct mapped
            (cart_rom_size == 1)? 9'b000000011:  // 1 - 4 banks = 64k
            (cart_rom_size == 2)? 9'b000000111:  // 2 - 8 banks = 128k
            (cart_rom_size == 3)? 9'b000001111:  // 3 - 16 banks = 256k
            (cart_rom_size == 4)? 9'b000011111:  // 4 - 32 banks = 512k
            (cart_rom_size == 5)? 9'b000111111:  // 5 - 64 banks = 1M
            (cart_rom_size == 6)? 9'b001111111:  // 6 - 128 banks = 2M
            (cart_rom_size == 7)? 9'b011111111:  // 7 - 256 banks = 4M
            (cart_rom_size == 8)? 9'b111111111:  // 8 - 512 banks = 8M
            (cart_rom_size == 82)?9'b001111111:  //$52 - 72 banks = 1.1M
            (cart_rom_size == 83)?9'b001111111:  //$53 - 80 banks = 1.2M
            (cart_rom_size == 84)?9'b001111111:
                                  9'b001111111;  //$54 - 96 banks = 1.5M// RAM size

// MBC types
// 0 - none
// 1 - mbc1
// 2 - mbc1 + ram
// 3 - mbc1 + ram + bat

// MBC1, MBC1+RAM, MBC1+RAM+BAT
wire mbc1 = (cart_mbc_type == 1) || (cart_mbc_type == 2) || (cart_mbc_type == 3);
wire mbc2 = (cart_mbc_type == 5) || (cart_mbc_type == 6);
//wire mmm01 = (cart_mbc_type == 11) || (cart_mbc_type == 12) || (cart_mbc_type == 13) || (cart_mbc_type == 14);
wire mbc3 = (cart_mbc_type == 15) || (cart_mbc_type == 16) || (cart_mbc_type == 17) || (cart_mbc_type == 18) || (cart_mbc_type == 19);
//wire mbc4 = (cart_mbc_type == 21) || (cart_mbc_type == 22) || (cart_mbc_type == 23);
wire mbc5 = (cart_mbc_type == 25) || (cart_mbc_type == 26) || (cart_mbc_type == 27) || (cart_mbc_type == 28) || (cart_mbc_type == 29) || (cart_mbc_type == 30);

wire [10:0] mbc_bank =
    mbc1?mbc1_addr:                  // MBC1, 16k bank 0, 16k bank 1-127 + ram
    mbc2?mbc2_addr:                  // MBC2, 16k bank 0, 16k bank 1-15 + ram
    mbc3?mbc3_addr:
    mbc5?mbc5_addr:
    {9'd0, cart_addr[14:13]};  // no MBC, 32k linear address

always @(posedge clk64) begin
    if(!pll_locked) begin
        cart_mbc_type <= 8'h00;
        cart_rom_size <= 8'h00;
        cart_ram_size <= 8'h00;
        cart_cgb_flag <= 8'h00;
    end else begin
        if(dio_download && dio_write) begin
            // cart is stored in 16 bit wide sdram, so addresses are shifted right
            case(dio_addr)
                24'ha1: cart_cgb_flag <= dio_data[7:0];               // $143
                24'ha3: cart_mbc_type <= dio_data[7:0];               // $147
                24'ha4: { cart_rom_size, cart_ram_size } <= dio_data; // $148/$149
            endcase
        end
    end
end

// include ROM download helper
data_io #(
    .STRLEN(($size(CONF_STR)>>3)))
data_io(
   .clk_sys ( clk64     ),
   // io controller spi interface
   .SPI_SCK ( SPI_SCK ),
   .SPI_SS2 ( SPI_SS2 ),
   .SPI_DI  ( SPI_DI  ),
    .SPI_DO        ( SPI_DO       ),
    
    .data_in       ( keys_s ),
    .conf_str      ( CONF_STR     ),
    .status        ( status       ),

   .ioctl_download ( dio_download ),  // signal indicating an active rom download

   // external ram interface
   .ioctl_clkref ( clk8      ),
   .ioctl_index  ( dio_index ),
   .ioctl_wr     ( dio_write ),
   .ioctl_addr   ( dio_addr  ),
   .ioctl_dout   ( dio_data  )
);

// select appropriate byte from 16 bit word returned by cart
wire [7:0] cart_di;    // data from cpu to cart
wire [7:0] cart_do = cart_addr[0]?sdram_do[7:0]:sdram_do[15:8];
wire [15:0] cart_addr;
wire cart_rd;
wire cart_wr;

wire lcd_clkena;
wire [14:0] lcd_data;
wire [1:0] lcd_mode;
wire lcd_on;

wire [15:0] audio_left;
wire [15:0] audio_right;

wire [11:0] bios_addr;
wire  [7:0] bios_do;

gbc_bios gbc_bios (
    .clock      ( clk64        ),
    .address    ( bios_addr    ),
    .q          ( bios_do      )
);

wire [1:0] joy_p54;
wire [3:0] joy_p4 = ~{ m_down, m_up, m_left, m_right } | {4{joy_p54[0]}};
wire [3:0] joy_p5 = ~{ btn_two_players | m_fireD, btn_one_player | m_fireC, m_fireA, m_fireB } | {4{joy_p54[1]}};

wire [7:0] joy_do = { 2'b11, joy_p54, joy_p4 & joy_p5 };

// the gameboy itself
gb gb (
    .reset      ( reset        ),
    .clk_sys    ( clk64        ),
    .ce         ( clk4         ),
    .ce_2x      ( clk8         ),

    .fast_boot   ( status[2]   ),
    .isGBC       ( isGBC       ),
    .isGBC_game  ( isGBC_game  ),

    .joy_p54     ( joy_p54     ),
    .joy_din     ( joy_do      ),

    // interface to the "external" game cartridge
    .cart_addr   ( cart_addr   ),
    .cart_rd     ( cart_rd     ),
    .cart_wr     ( cart_wr     ),
    .cart_do     ( cart_do     ),
    .cart_di     ( cart_di     ),

    //gbc bios interface
    .gbc_bios_addr ( bios_addr  ),
    .gbc_bios_do   ( bios_do    ),

    // audio
    .audio_l    ( audio_left    ),
    .audio_r    ( audio_right   ),

    // interface to the lcd
    .lcd_clkena   ( lcd_clkena ),
    .lcd_data     ( lcd_data   ),
    .lcd_mode     ( lcd_mode   ),
    .lcd_on       ( lcd_on     )
);

sigma_delta_dac dac (
    .clk        ( clk64             ),
    .ldatasum   ( {~audio_left[15], audio_left[14:1]} ),
    .rdatasum   ( {~audio_right[15], audio_right[14:1]} ),
    .left       ( AUDIO_L           ),
    .right      ( AUDIO_R           )
);

// the lcd to vga converter
wire [5:0] video_r, video_g, video_b;
wire video_hs, video_vs;

lcd lcd (
     .clk    ( clk64      ),
     .pclk_en( ce_pix     ),
     .clk4_en( clk4       ),

     .tint   ( status[1]  ),
     .isGBC  ( isGBC      ),

     // serial interface
     .clkena ( lcd_clkena ),
     .data   ( lcd_data   ),
     .mode   ( lcd_mode   ),  // used to detect begin of new lines and frames
     .on     ( lcd_on     ),
     
     .hs    ( video_hs    ),
     .vs    ( video_vs    ),
     .r     ( video_r     ),
     .g     ( video_g     ),
     .b     ( video_b     )
);

wire [5:0] vga_r_s;
wire [5:0] vga_g_s;
wire [5:0] vga_b_s;

// include the on screen display
osd #(10'd16,10'd0,1) osd (
   .clk_sys    ( clk64       ),

   // spi for OSD
   .SPI_DI     ( SPI_DI       ),
   .SPI_SCK    ( SPI_SCK      ),
   .SPI_SS3    ( SPI_SS2      ),

   .R_in       ( video_r      ),
   .G_in       ( video_g      ),
   .B_in       ( video_b      ),
   .HSync      ( video_hs     ),
   .VSync      ( video_vs     ),

   .R_out      ( vga_r_s      ),
   .G_out      ( vga_g_s      ),
   .B_out      ( vga_b_s      ),

   .osd_enable ( osd_enable   )
);

assign VGA_R = vga_r_s[5:1];
assign VGA_G = vga_g_s[5:1];
assign VGA_B = vga_b_s[5:1];

assign VGA_HS = video_hs;
assign VGA_VS = video_vs;

wire clk4 = ce_cpu;
wire clk8 = ce_pix;

reg ce_pix, ce_cpu;
always @(posedge clk64) begin
    reg [3:0] div = 0;
    div <= div + 1'd1;
    ce_pix   <= !div[2:0];
    ce_cpu   <= !div[3:0];
end

wire pll_locked;
wire clk64;
pll pll (
     .inclk0(clock_50_i),
     .c0(clk64),        // 4*16.777216 MHz
     .c1(SDRAM_CLK),
     .locked(pll_locked)
);

//assign SDRAM_CLK = clk64;


//------------------------------------------------------------
    
reg [1:0] cnt_clk;

always @(posedge clk64) 
begin 
        cnt_clk <= cnt_clk + 1'b1;
end


wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player  = ~btn_n_i[1] | m_one_player;
wire btn_two_players = ~btn_n_i[2] | m_two_players;
wire btn_coin        = ~btn_n_i[3] | m_coin1;

wire kbd_intr;
wire [7:0] kbd_scancode;
wire [7:0] keys_s;

//get scancode from keyboard
io_ps2_keyboard keyboard 
 (
  .clk       ( cnt_clk[1] ),
  .kbd_clk   ( ps2_clk_io ),
  .kbd_dat   ( ps2_data_io ),
  .interrupt ( kbd_intr ),
  .scancode  ( kbd_scancode )
);

wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable;
wire direct_video;
wire [1:0]osd_rotate;

//translate scancode to joystick
kbd_joystick #( .OSD_CMD    ( 3'b011 ), .CLK_SPEED(16000)) k_joystick
(
    .clk          ( cnt_clk[1] ), //16mhz
    .kbdint       ( kbd_intr ),
    .kbdscancode  ( kbd_scancode ), 

    .joystick_0   ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1   ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
      
    //-- joystick_0 and joystick_1 should be swapped
    .joyswap      ( 0 ),

    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer    ( 1 ),

    //-- tilt, coin4-1, start4-1
    .controls     ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

    //-- fire12-1, up, down, left, right

    .player1      ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2      ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),

    .direct_video ( direct_video ),
    .osd_rotate   ( osd_rotate ),

    //-- keys to the OSD
    .osd_o        ( keys_s ),
    .osd_enable   ( osd_enable ),

    //-- sega joystick
    .sega_strobe  ( joy_p7_o )

        
);

endmodule

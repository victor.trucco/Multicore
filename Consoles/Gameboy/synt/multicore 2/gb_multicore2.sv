/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///
//
// Gameboy for the Multicore 2
// 
// Copyright (c) 2015 Till Harbaum <till@harbaum.org> 
// Copyright (c) 2018 Victor Trucco (changes for Multicore 2) 
// 
// This source file is free software: you can redistribute it and/or modify 
// it under the terms of the GNU General Public License as published 
// by the Free Software Foundation, either version 3 of the License, or 
// (at your option) any later version. 
// 
// This source file is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License 
// along with this program.  If not, see <http://www.gnu.org/licenses/>. 
//

//============================================================================
//
//  Multicore 2 Top by Victor Trucco
//
//============================================================================

`default_nettype none

module gb_multicore2 
(
    // Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAMs (AS7C34096)
    output wire [18:0]sram_addr_o  = 18'b0000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM    (H57V256)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io          = 1'bz,
    inout wire  ps2_data_io         = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o           = 1'b1,
    output wire sd_sclk_o           = 1'b0,
    output wire sd_mosi_o           = 1'b0,
    input wire  sd_miso_i,

    // Joysticks
    input wire  joy1_up_i,
    input wire  joy1_down_i,
    input wire  joy1_left_i,
    input wire  joy1_right_i,
    input wire  joy1_p6_i,
    input wire  joy1_p9_i,
    input wire  joy2_up_i,
    input wire  joy2_down_i,
    input wire  joy2_left_i,
    input wire  joy2_right_i,
    input wire  joy2_p6_i,
    input wire  joy2_p9_i,
    output wire joyX_p7_o           = 1'b1,

    // Audio
    output        AUDIO_L,
    output        AUDIO_R,
    input wire  ear_i,
    output wire mic_o                   = 1'b0,

        // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

        // HDMI
    output wire [7:0]tmds_o         = 8'b00000000,

        //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
        
    inout wire  stm_b8_io, 
    inout wire  stm_b9_io,

    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2
    
);

// mix both joysticks to allow the user to use any
wire [7:0] joystick = joystick_0;// | joystick_1;
wire [7:0] joystick_0;
wire [7:0] joystick_1;
wire [31:0] status;



// the configuration string is returned to the io controller to allow
// it to control the menu on the OSD 
parameter CONF_STR = {
        "S,GB,Load Game...;",
        "OG,LCD color,white,yellow;",
        "OH,Boot,Normal,Fast;",
        "T6,Reset"
};

assign stm_rst_o = 1'bz;


wire reset = (reset_cnt != 0);
reg [9:0] reset_cnt;
always @(posedge clk4) begin
//  if(status[0] || status[3] || buttons[1] || !pll_locked || dio_download)
    if( !btn_n_i[4] || status[6] || !pll_locked || dio_download )
    //if(btn_n_i[1] )
        reset_cnt <= 10'd1023;
    else
        if(reset_cnt != 0)
            reset_cnt <= reset_cnt - 10'd1;
end

assign SDRAM_CKE = 1'b1;
    
sdram sdram (
   // interface to the MT48LC16M16 chip
   .sd_data        ( SDRAM_DQ               ),
   .sd_addr        ( SDRAM_A                ),
   .sd_dqm         ( {SDRAM_DQMH, SDRAM_DQML}  ),
   .sd_cs          ( SDRAM_nCS                ),
   .sd_ba          ( SDRAM_BA                ),
   .sd_we          ( SDRAM_nWE                ),
   .sd_ras         ( SDRAM_nRAS               ),
   .sd_cas         ( SDRAM_nCAS               ),

    // system interface
   .clk            ( clk64                    ), // sdram is accessed at up to 128MHz
   .clkref         ( clk8                      ), // reference clock to sync to
   .init           ( !pll_locked               ), // init signal after FPGA config to initialize RAM

   // cpu interface
   .din            ( sdram_di                  ), // data input from chipset/cpu
   .addr           ( sdram_addr                ), // 24 bit word address
   .ds             ( sdram_ds                  ), // data strobe for hi/low byte (sdram DQM)
   .we             ( sdram_we                  ), // cpu/chipset requests write
   .oe             ( sdram_oe                  ), // cpu/chipset requests read
   .dout           ( sdram_do                  )  // data output to chipset/cpu
);


// TODO: ds for cart ram write
wire [1:0]  sdram_ds = dio_download?2'b11:{!cart_addr[0], cart_addr[0]};
wire [15:0] sdram_do;
wire [15:0] sdram_di = dio_download?dio_data:{cart_di, cart_di};
wire [23:0] sdram_addr = dio_download?dio_addr:{3'b000, mbc_bank, cart_addr[12:1]};
wire sdram_oe = !dio_download && cart_rd;
wire sdram_we = (dio_download && dio_write) || (!dio_download && cart_ram_wr);

wire dio_download;
wire [23:0] dio_addr;
wire [15:0] dio_data;
wire dio_write;

// TODO: RAM bank
// http://fms.komkon.org/GameBoy/Tech/Carts.html

// 32MB SDRAM memory map using word addresses
// 2 2 2 2 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 D
// 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 S
// -------------------------------------------------
// 0 0 0 0 X X X X X X X X X X X X X X X X X X X X X up to 2MB used as ROM
// 0 0 0 1 X X X X X X X X X X X X X X X X X X X X X up to 2MB used as RAM
// 0 0 0 0 R R B B B B B C C C C C C C C C C C C C C MBC1 ROM (R=RAM bank in mode 0)
// 0 0 0 1 0 0 0 0 0 0 R R C C C C C C C C C C C C C MBC1 RAM (R=RAM bank in mode 1)

// ---------------------------------------------------------------
// ----------------------------- MBC1 ----------------------------
// ---------------------------------------------------------------

wire [8:0] mbc1_addr = 
    (cart_addr[15:14] == 2'b00)?{8'b000000000, cart_addr[13]}:        // 16k ROM Bank 0
    (cart_addr[15:14] == 2'b01)?{1'b0, mbc1_rom_bank, cart_addr[13]}: // 16k ROM Bank 1-127
    (cart_addr[15:13] == 3'b101)?{7'b1000000, mbc1_ram_bank}:         // 8k RAM Bank 0-3
    9'd0;

// -------------------------- RAM banking ------------------------

// in mode 0 (16/8 mode) the ram is not banked 
// in mode 1 (4/32 mode) four ram banks are used
wire [1:0] mbc1_ram_bank = (mbc1_mode?mbc1_ram_bank_reg:2'b00) & ram_mask;

// -------------------------- ROM banking ------------------------
   
// in mode 0 (16/8 mode) the ram bank select signals are the upper rom address lines 
// in mode 1 (4/32 mode) the upper two rom address lines are 2'b00
wire [6:0] mbc1_rom_bank_mode = { mbc1_mode?2'b00:mbc1_ram_bank_reg, mbc1_rom_bank_reg};
// mask address lines to enable proper mirroring
wire [6:0] mbc1_rom_bank = mbc1_rom_bank_mode & rom_mask;

// --------------------- CPU register interface ------------------
reg mbc1_ram_enable;
reg mbc1_mode;
reg [4:0] mbc1_rom_bank_reg;
reg [1:0] mbc1_ram_bank_reg;
always @(posedge clk4) begin
    if(reset) begin
        mbc1_rom_bank_reg <= 5'd1;
        mbc1_ram_bank_reg <= 2'd0;
      mbc1_ram_enable <= 1'b0;
      mbc1_mode <= 1'b0;
    end else begin
        if(cart_wr && (cart_addr[15:13] == 3'b000))
            mbc1_ram_enable <= (cart_di[3:0] == 4'ha);
        if(cart_wr && (cart_addr[15:13] == 3'b001)) begin
            if(cart_di[4:0]==0) mbc1_rom_bank_reg <= 5'd1;
            else                  mbc1_rom_bank_reg <= cart_di[4:0];
        end 
        if(cart_wr && (cart_addr[15:13] == 3'b010))
            mbc1_ram_bank_reg <= cart_di[1:0];
        if(cart_wr && (cart_addr[15:13] == 3'b011))
            mbc1_mode <= cart_di[0];
    end
end

// extract header fields extracted from cartridge
// during download
reg [7:0] cart_mbc_type;
reg [7:0] cart_rom_size;
reg [7:0] cart_ram_size;

// only write sdram if the write attept comes from the cart ram area
wire cart_ram_wr = cart_wr && mbc1_ram_enable && (cart_addr[15:13] == 3'b101);
   
// RAM size
wire [1:0] ram_mask =                       // 0 - no ram
       (cart_ram_size == 1)?2'b00:              // 1 - 2k, 1 bank
       (cart_ram_size == 2)?2'b00:              // 2 - 8k, 1 bank
       2'b11;                                   // 3 - 32k, 4 banks

// ROM size
wire [6:0] rom_mask =                       // 0 - 2 banks, 32k direct mapped
       (cart_rom_size == 1)?7'b0000011:     // 1 - 4 banks = 64k
       (cart_rom_size == 2)?7'b0000111:     // 2 - 8 banks = 128k
       (cart_rom_size == 3)?7'b0001111:     // 3 - 16 banks = 256k
       (cart_rom_size == 4)?7'b0011111:     // 4 - 32 banks = 512k
       (cart_rom_size == 5)?7'b0111111:     // 5 - 64 banks = 1M
       7'b1111111;                          // 6 - 128 banks = 2M

// MBC types
// 0 - none
// 1 - mbc1
// 2 - mbc1 + ram
// 3 - mbc1 + ram + bat

// MBC1, MBC1+RAM, MBC1+RAM+BAT
wire mbc1 = (cart_mbc_type == 1) || (cart_mbc_type == 2) || (cart_mbc_type == 3);

wire [8:0] mbc_bank =
    mbc1?mbc1_addr:                  // MBC1, 16k bank 0, 16k bank 1-127 + ram
    {7'b0000000, cart_addr[14:13]};  // no MBC, 32k linear address

always @(posedge clk4) begin
    if(!pll_locked) begin
        cart_mbc_type <= 8'h00;
        cart_rom_size <= 8'h00;
        cart_ram_size <= 8'h00;
    end else begin
        if(dio_download && dio_write) begin
            // cart is stored in 16 bit wide sdram, so addresses are shifted right
            case(dio_addr)
                24'ha3:  cart_mbc_type <= dio_data[7:0];                 // $147
                24'ha4: { cart_rom_size, cart_ram_size } <= dio_data;    // $148/$149
            endcase
        end
    end
end

// include ROM download helper
data_io_gb 
#(   .STRLEN(($size(CONF_STR)>>3)))
data_io_gb (
   // io controller spi interface
    .sdi        ( SPI_DI   ),
    .sck        ( SPI_SCK   ),
    .ss         ( SPI_SS2   ),
    .sdo        ( SPI_DO ),

    .data_in    ( osd_s ),
    .conf_str   ( CONF_STR      ),
    .status     ( status        ),
    .core_mod   (      ),

    .downloading ( dio_download ),  // signal indicating an active rom download

    // external ram interface
    .clk   ( clk8      ),
    .wr    ( dio_write ),
    .addr  ( dio_addr  ),
    .data  ( dio_data  )
);

/*
// include ROM download helper
data_io_gbc #(
    .STRLEN(($size(CONF_STR)>>3)))
data_io_gbc(
   .clk_sys ( clk64     ),
   // io controller spi interface
   .SPI_SCK ( SPI_SCK ),
   .SPI_SS2 ( SPI_SS2 ),
   .SPI_DI  ( SPI_DI  ),
    .SPI_DO        ( SPI_DO       ),
    
    .data_in       ( osd_s ),
    .conf_str      ( CONF_STR     ),
    .status        ( status       ),

   .ioctl_download ( dio_download ),  // signal indicating an active rom download

   // external ram interface
   .ioctl_clkref ( clk8      ),
   .ioctl_index  (  ),
   .ioctl_wr     ( dio_write ),
   .ioctl_addr   ( dio_addr  ),
   .ioctl_dout   ( dio_data  )
);
*/
// select appropriate byte from 16 bit word returned by cart
wire [7:0] cart_di;    // data from cpu to cart
wire [7:0] cart_do = cart_addr[0]?sdram_do[7:0]:sdram_do[15:8];
wire [15:0] cart_addr;
wire cart_rd;
wire cart_wr;

wire lcd_clkena;
wire [1:0] lcd_data;
wire [1:0] lcd_mode;
wire lcd_on;

wire [15:0] audio_left;
wire [15:0] audio_right;

// the gameboy itself
gb gb (
    .reset      ( reset        ),
    .clk         ( clk4         ),   // the whole gameboy runs on 4mhnz

    .fast_boot   ( 1'b1    ), //status[2]
    .joystick    ( joystick     ),

    // interface to the "external" game cartridge
    .cart_addr   ( cart_addr   ),
    .cart_rd     ( cart_rd     ),
    .cart_wr     ( cart_wr     ),
    .cart_do     ( cart_do     ),
    .cart_di     ( cart_di     ),

    // audio
    .audio_l    ( audio_left    ),
    .audio_r    ( audio_right   ),
    
    // interface to the lcd
    .lcd_clkena   ( lcd_clkena ),
    .lcd_data     ( lcd_data   ),
    .lcd_mode     ( lcd_mode   ),
    .lcd_on       ( lcd_on     )
);

reg tint = 1'b0;
reg db_tint = 1'b1;

always @(negedge db_tint) 
    tint <= !tint;

debounce 
#(  
    .counter_size           ( 6 )
) 
debounce 
(
    .clk_i     (clk4),          //--input clock
    .button_i  (btn_n_i[1]),  //--input signal to be debounced
    .result_o  (db_tint)        //--debounced signal    
); 
    
sigma_delta_dac dac (
    .clk            ( clk32                     ),
    .ldatasum   ( audio_left[15:1]  ),
    .rdatasum   ( audio_right[15:1] ),
    .left           ( AUDIO_L               ),
    .right      ( AUDIO_R               )
);

// the lcd to vga converter
wire [5:0] video_r, video_g, video_b;
wire video_hs, video_vs;
reg [7:0] osd_s = 8'b00111111;
reg [12:1] teclasF;

lcd lcd (
     .pclk   ( clk8       ),
     .clk    ( clk4       ),

     .tint   ( tint ),//status[1] 

     // serial interface
     .clkena ( lcd_clkena ),
     .data   ( lcd_data   ),
     .mode   ( lcd_mode   ),  // used to detect begin of new lines and frames
     .on     ( lcd_on     ),
     
     .hs    ( video_hs    ),
     .vs    ( video_vs    ),
     .r     ( video_r     ),
     .g     ( video_g     ),
     .b     ( video_b     )
);

// include the on screen display
osd_gb #(  
            .OSD_COLOR      ( 3'b001 ), //RGB
            .OSD_X_OFFSET   ( 10'd18 ),
            .OSD_Y_OFFSET   ( 10'd15 )
    ) 
    osd_gb (
   .pclk       ( clk32     ),
            
   // spi for OSD
   .sdi        ( SPI_DI   ),
   .sck        ( SPI_SCK   ),
   .ss         ( SPI_SS2 ),
//    .sdo        ( SPI_DO   ),


   .red_in     ( video_r[5:1] ),
   .green_in   ( video_g[5:1] ),
   .blue_in    ( video_b[5:1] ),
   .hs_in      ( video_hs     ),
   .vs_in      ( video_vs     ),

   .red_out    ( VGA_R  ),
   .green_out  ( VGA_G  ),
   .blue_out   ( VGA_B  ),
   .hs_out     ( VGA_HS ),
   .vs_out     ( VGA_VS ),
    
    .data_in        ( osd_s ), //combine the start CMD and the keyboard CMD
    .conf_str   ( CONF_STR      )
        
            
);
/*
wire osd_enable;

mist_video #(.COLOR_DEPTH(6), .SD_HCNT_WIDTH(11)) mist_video
(
    .clk_sys        ( clk32     ),
    .SPI_SCK        ( SPI_SCK   ),
    .SPI_SS3        ( SPI_SS2   ),
    .SPI_DI         ( SPI_DI    ),
    
    .R              ( video_r   ),
    .G              ( video_g   ),
    .B              ( video_b   ),
    .HSync          ( video_hs  ),
    .VSync          ( video_vs  ),
    
    .VGA_R          ( VGA_R     ),
    .VGA_G          ( VGA_G     ),
    .VGA_B          ( VGA_B     ),
    .VGA_VS         ( VGA_VS    ),
    .VGA_HS         ( VGA_HS    ),
    
    .rotate         ( 2'b00 ),
    .ce_divider     ( 1'b0  ),
    .scandoubler_disable( 1 ),
    .scanlines      ( 2'b00 ),
    .blend          ( 1'b0  ),
    .osd_enable     ( osd_enable )    
);
*/
                
reg clk4;   // 4.194304 MHz CPU clock and GB pixel clock
always @(posedge clk8) 
    clk4 <= !clk4;

reg clk8;   // 8.388608 MHz VGA pixel clock
always @(posedge clk16) 
    clk8 <= !clk8;

reg clk16;   // 16.777216 MHz
always @(posedge clk32) 
    clk16 <= !clk16;
                
reg clk32;   // 32 MHz
always @(posedge clk64) 
    clk32 <= !clk32;


// 32 Mhz SDRAM clk
wire pll_locked;
wire clk64;
pll pll (
     .inclk0(clock_50_i),
     .c0(clk64),        // 4*16.777216 MHz
     .c1(SDRAM_CLK),    // same phase shifted
     .locked(pll_locked)
);

    keyboard keyboard
    (
        .CLK        ( clk4 ),
        .nRESET     ( pll_locked ),
        .PS2_CLK    ( ps2_clk_io ),
        .PS2_DATA   ( ps2_data_io ),
        .teclasF    ( teclasF ),
        .osd_o      ( osd_s )
    );
    
    //--- Joystick read with sega 6 button support----------------------
    
    // up, down, left, right, start, select, b, a
    assign joystick_0 = {!joy1_s[7], !joy1_s[5], !joy1_s[4], !joy1_s[6], !joy1_s[0] , !joy1_s[1], !joy1_s[2],   !joy1_s[3] };

    reg [11:0]joy1_s;   
    reg [11:0]joy2_s; 
    reg joyP7_s;

    reg [7:0]state_v = 8'd0;
    reg j1_sixbutton_v = 1'b0;
    reg j2_sixbutton_v = 1'b0;
    
    always @(negedge VGA_HS) 
    begin
        

            state_v <= state_v + 1;

            
            case (state_v)          //-- joy_s format MXYZ SACB RLDU
                8'd0:  
                    joyP7_s <=  1'b0;
                    
                8'd1:
                    joyP7_s <=  1'b1;

                8'd2:
                    begin
                        joy1_s[3:0] <= {joy1_right_i, joy1_left_i, joy1_down_i, joy1_up_i}; //-- R, L, D, U
                        joy2_s[3:0] <= {joy2_right_i, joy2_left_i, joy2_down_i, joy2_up_i}; //-- R, L, D, U
                        joy1_s[5:4] <= {joy1_p9_i, joy1_p6_i}; //-- C, B
                        joy2_s[5:4] <= {joy2_p9_i, joy2_p6_i}; //-- C, B                    
                        joyP7_s <= 1'b0;
                        j1_sixbutton_v <= 1'b0; //-- Assume it's not a six-button controller
                        j2_sixbutton_v <= 1'b0; //-- Assume it's not a six-button controller
                    end
                    
                8'd3:
                    begin
                        joy1_s[7:6] <= { joy1_p9_i , joy1_p6_i }; //-- Start, A
                        joy2_s[7:6] <= { joy2_p9_i , joy2_p6_i }; //-- Start, A
                        joyP7_s <= 1'b1;
                    end
                    
                8'd4:  
                    joyP7_s <= 1'b0;

                8'd5:
                    begin
                        if (joy1_right_i == 1'b0 && joy1_left_i == 1'b0 && joy1_down_i == 1'b0 && joy1_up_i == 1'b0 )
                            j1_sixbutton_v <= 1'b1; // --it's a six button
                        
                        
                        if (joy2_right_i == 1'b0 && joy2_left_i == 1'b0 && joy2_down_i == 1'b0 && joy2_up_i == 1'b0 )
                            j2_sixbutton_v <= 1'b1; // --it's a six button
                        
                        
                        joyP7_s <= 1'b1;
                    end
                    
                8'd6:
                    begin
                        if (j1_sixbutton_v == 1'b1)
                            joy1_s[11:8] <= { joy1_right_i, joy1_left_i, joy1_down_i, joy1_up_i }; //-- Mode, X, Y e Z
                        
                        
                        if (j2_sixbutton_v == 1'b1)
                            joy2_s[11:8] <= { joy2_right_i, joy2_left_i, joy2_down_i, joy2_up_i }; //-- Mode, X, Y e Z
                        
                        
                        joyP7_s <= 1'b0;
                    end 
                    
                default:
                    joyP7_s <= 1'b1;
                    
            endcase

    end
    
    assign joyX_p7_o = joyP7_s;
    //---------------------------

endmodule

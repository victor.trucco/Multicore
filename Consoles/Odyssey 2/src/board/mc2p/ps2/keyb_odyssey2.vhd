--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- * Redistributions of source code must retain the above copyright notice,
--   this list of conditions and the following disclaimer.
--
-- * Redistributions in synthesized form must reproduce the above copyright
--   notice, this list of conditions and the following disclaimer in the
--   documentation and/or other materials provided with the distribution.
--
-- * Neither the name of the author nor the names of other contributors may
--   be used to endorse or promote products derived from this software without
--   specific prior written agreement from the author.
--
-- * License is granted for non-commercial use only.  A fee may not be charged
--   for redistributions as source code or in synthesized/hardware form without
--   specific prior written agreement from the author.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--

-- PS/2 scancode to Spectrum matrix conversion
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.keyscans.all;

entity keyb_odyssey2 is
    port (
        CLK         :   in  std_logic;
        nRESET      :   in  std_logic;

        -- PS/2 interface
        keyb_data   :   in  std_logic_vector(7 downto 0);
        keyb_valid  :   in  std_logic;

        Key_dec     :   in  std_logic_vector(6 downto 1);
        key_enc     :   out std_logic_vector(14 downto 7);
        
        reset_key_n_o   :   out std_logic;
        osd_o           : out   std_logic_vector(7 downto 0);

        keys_o          : out   std_logic_vector(4 downto 0);
        direct_video_o  : out   std_logic
    );
end keyb_odyssey2;

architecture rtl of keyb_odyssey2 is


    -- Interface to PS/2 block
    signal keyb_data_s    :   std_logic_vector(7 downto 0);
    signal osd_s    :   std_logic_vector(7 downto 0);
    signal keyb_valid_s   :   std_logic;
    signal reset_key_n_s        :   std_logic := '1';

    -- Internal signals
    type key_matrix is array (5 downto 0) of std_logic_vector(7 downto 0);
    signal keys     :   key_matrix;
    signal release  :   std_logic;
    signal extended :   std_logic;
    signal k1, k2, k3, k4, k5, k6, k7, k8 : std_logic_vector(4 downto 0);

    signal direct_video : std_logic := '0';

begin

    direct_video_o <= direct_video;

    keyb_data_s <= keyb_data;
    keyb_valid_s <=keyb_valid;

    reset_key_n_o <= reset_key_n_s;
    
    osd_o   <= osd_s;

    process(Key_dec, keys)
    begin
    
        if Key_dec(1) = '0' then -- 0 1 2 3 4 5 6 7
            key_enc <= keys(0); --0
        end if;
        
        if Key_dec(2) = '0' then --8 9 () () space ? L P
            key_enc <= keys(1); --0
        end if;
        
        if Key_dec(3) = '0' then -- + W E R T U I O
            key_enc <= keys(2);
        end if;

        if Key_dec(4) = '0' then -- Q D S F G H J K
            key_enc <= keys(3);
        end if;
        
        if Key_dec(5) = '0' then -- A Z X C V B M .
            key_enc <= keys(4);
        end if;

        if Key_dec(6) = '0' then -- - * / = Y(YES) N(NO) CLR ENT
            key_enc <= keys(5);
        end if;
        
    end process;

    process(nRESET,CLK)
    begin
        if nRESET = '0' then
            release <= '0';
            extended <= '0';
            reset_key_n_s <= '1';

            osd_s <= (others => '1');
            keys_o <= (others =>'0');

            keys(0) <= (others => '1');
            keys(1) <= (others => '1');
            keys(2) <= (others => '1');
            keys(3) <= (others => '1');
            keys(4) <= (others => '1');
            keys(5) <= (others => '1');

        elsif rising_edge(CLK) then
            if keyb_valid_s = '1' then
                if keyb_data_s = X"e0" then
                    -- Extended key code follows
                    extended <= '1';
                elsif keyb_data_s = X"f0" then
                    -- Release code follows
                    release <= '1';
                else
                    -- Cancel extended/release flags for next time
                    release <= '0';
                    extended <= '0';
                    
                    osd_s <= (others => '1');
                    reset_key_n_s <= '1';

                    if (extended = '0') then
                    
                        if keyb_data_s = KEY_F12 and release = '0' then
                            osd_s(7 downto 5) <= "011"; -- OSD menu command
                        else
                            osd_s(7 downto 5) <= "111"; -- release
                        end if;
                    
                        -- Normal scancodes
                        case keyb_data_s is
                        
                            when KEY_KP0         => keys(0)(0) <= release; -- 0
                            when KEY_KP1            => keys(0)(1) <= release; -- 1
                            when KEY_KP2            => keys(0)(2) <= release; -- 2
                            when KEY_KP3            => keys(0)(3) <= release; -- 3
                            when KEY_KP4            => keys(0)(4) <= release; -- 4
                            when KEY_KP5            => keys(0)(5) <= release; -- 5
                            when KEY_KP6            => keys(0)(6) <= release; -- 6
                            when KEY_KP7            => keys(0)(7) <= release; -- 7
                            when KEY_KP8            => keys(1)(0) <= release; -- 8
                            when KEY_KP9            => keys(1)(1) <= release; -- 9
                            
                            
                            when KEY_0              => keys(0)(0) <= release; -- 0
                            when KEY_1              => keys(0)(1) <= release; -- 1
                            when KEY_2              => keys(0)(2) <= release; -- 2
                            when KEY_3              => keys(0)(3) <= release; -- 3
                            when KEY_4              => keys(0)(4) <= release; -- 4
                            when KEY_5              => keys(0)(5) <= release; -- 5
                            when KEY_6              => keys(0)(6) <= release; -- 6
                            when KEY_7              => keys(0)(7) <= release; -- 7
                                
                            when KEY_8              => keys(1)(0) <= release; -- 8
                            when KEY_9              => keys(1)(1) <= release; -- 9
                            when KEY_SPACE      => keys(1)(4) <= release;-- SPACE  
                            when KEY_COMMA      => keys(1)(5) <= release; -- ?
                            when KEY_L              => keys(1)(6) <= release; -- L
                            when KEY_P              => keys(1)(7) <= release; -- P                      
                            
                            when KEY_KPPLUS     => keys(2)(0) <= release; -- +          
                            when KEY_W              => keys(2)(1) <= release; -- W
                            when KEY_E              => keys(2)(2) <= release; -- E
                            when KEY_R              => keys(2)(3) <= release; -- R
                            when KEY_T              => keys(2)(4) <= release; -- T
                            when KEY_U              => keys(2)(5) <= release; -- U              
                            when KEY_I              => keys(2)(6) <= release; -- I
                            when KEY_O              => keys(2)(7) <= release; -- O
                            
                            when KEY_Q              => keys(3)(0) <= release; -- Q
                            when KEY_S              => keys(3)(1) <= release; -- S
                            when KEY_D              => keys(3)(2) <= release; -- D
                            when KEY_F              => keys(3)(3) <= release; -- F
                            when KEY_G              => keys(3)(4) <= release; -- G
                            when KEY_H              => keys(3)(5) <= release; -- H
                            when KEY_J              => keys(3)(6) <= release; -- J
                            when KEY_K              => keys(3)(7) <= release; -- K
                            
                            when KEY_A              => keys(4)(0) <= release; -- A
                            when KEY_Z              => keys(4)(1) <= release; -- Z
                            when KEY_X              => keys(4)(2) <= release; -- X
                            when KEY_C              => keys(4)(3) <= release; -- C
                            when KEY_V              => keys(4)(4) <= release; -- V
                            when KEY_B              => keys(4)(5) <= release; -- B
                            when KEY_M              => keys(4)(6) <= release; -- M
                            when KEY_POINT          => keys(4)(7) <= release; -- . 
                            
                            when KEY_MINUS          => keys(5)(0) <= release; -- - 
                            when KEY_KPMINUS        => keys(5)(0) <= release; -- -                      
                            when KEY_KPASTER        => keys(5)(1) <= release; -- * 
                            when KEY_SLASH          => keys(5)(2) <= release; 
                            when KEY_EQUAL          => keys(5)(3) <= release; -- = +            
                            when KEY_Y              => keys(5)(4) <= release; -- Y
                            when KEY_N              => keys(5)(5) <= release; -- N
                            when KEY_BACKSPACE      => keys(5)(6) <= release; -- Backspace 
                            when KEY_ENTER          => keys(5)(7) <= release; -- ENTER
                                                       osd_s(4) <= release;
                            
                            when KEY_ESC            => reset_key_n_s <= release;
                            
                        
                            when KEY_RCTRL          => keys_o(4) <= not release; 
                            
                            when X"7E"            => if (release = '0') then direct_video <= not direct_video; end if; --scroll lock
                            
                                                            
                    --      when KEY_LSHIFT     => keys(0)(0) <= release; -- Left shift (CAPS SHIFT)
                    --      when KEY_RSHIFT         => keys(0)(0) <= release; -- Right shift (CAPS SHIFT)
                            
                    --      when KEY_LCTRL      => keys(7)(1) <= release; -- Left CTRL (Symbol Shift)
                    --  
                    --  
--
                    --      
                    --      
                    --      -- Other special keys sent to the ULA as key combinations
                    --      when KEY_BACKSPACE  => keys(0)(0) <= release; -- Backspace (CAPS 0)
                    --                                      keys(4)(0) <= release;
                    --      when KEY_CAPSLOCK       => keys(0)(0) <= release; -- Caps lock (CAPS 2)
                    --                                      keys(3)(1) <= release;
                    --      when KEY_ESC            => keys(0)(0) <= release; -- Escape (CAPS SPACE)
                    --                                      keys(7)(0) <= release;
--
                    --      when KEY_BL             => keys(7)(1) <= release; -- ' (SYMBOL + 7)
                    --                                      keys(4)(3) <= release;
                    --      
                    --      
                    --      when KEY_COMMA       => keys(7)(1) <= release; -- , (SYMBOL + N)
                    --                                      keys(7)(3) <= release;
                    --      when KEY_KPCOMMA        => keys(7)(1) <= release; -- , (SYMBOL + N)
                    --                                      keys(7)(3) <= release;
                    --      
                    --      when KEY_KPPOINT        => keys(7)(1) <= release; -- . (SYMBOL + M)
                    --                                      keys(7)(2) <= release;
                    --      
                    --      when KEY_TWOPOINT       => keys(7)(1) <= release; -- ; (SYMBOL + O)
                    --                                      keys(5)(1) <= release;
                        
                            

                            when others =>
                                null;
                        end case;
                    else
                        -- Extended scancodes
                        case keyb_data_s is

                            when KEY_KPENTER        =>  osd_s(4) <= release; -- ENTER
                            when KEY_LEFT           =>  osd_s(2) <= release; keys_o(1) <= not release; -- Left (CAPS 5)
                            when KEY_DOWN           =>  osd_s(1) <= release; keys_o(2) <= not release; -- Down (CAPS 6)
                            when KEY_UP             =>  osd_s(0) <= release; keys_o(3) <= not release; -- Up (CAPS 7)
                            when KEY_RIGHT          =>  osd_s(3) <= release; keys_o(0) <= not release; -- Right (CAPS 8)

                        --  when KEY_RCTRL          => keys(7)(1) <= release; -- Right CTRL (Symbol Shift)
--
                            when KEY_KPSLASH        => keys(5)(2) <= release; 
                        --                                  

                            when others =>
                                null;
                        end case;
                    end if;
                end if;
            end if;
        end if;
    end process;

end architecture;

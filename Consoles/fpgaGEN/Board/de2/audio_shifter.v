/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//* audio_shifter.v */


module audio_shifter(
  input  wire           clk,    //32MHz
  input  wire           nreset,
  input  wire           mix,
  input  wire [ 15:0] rdata,
  input  wire [ 15:0] ldata,
  input  wire           exchan,
  output wire           aud_bclk,
  output wire           aud_daclrck,
  output wire           aud_dacdat,
  output wire           aud_xck
);


//// L-R mixer ////

wire [ 16:0] rdata_mix;
wire [ 16:0] ldata_mix;

assign rdata_mix = {rdata[15], rdata} + {{2{ldata[15]}}, ldata[15:1]};
assign ldata_mix = {ldata[15], ldata} + {{2{rdata[15]}}, rdata[15:1]};

// data mux
reg [15:0] rdata_mux;
reg [15:0] ldata_mux;

always @ (posedge clk) begin
  rdata_mux <= #1 (mix) ? rdata_mix[15:0] : rdata;
  ldata_mux <= #1 (mix) ? ldata_mix[15:0] : ldata;
end


//// audio output shifter ////

reg  [  9-1:0] shiftcnt;
reg  [ 16-1:0] shift;

always @(posedge clk, negedge nreset) begin
  if(~nreset)
    shiftcnt <= 9'd0;
  else
    shiftcnt <= shiftcnt - 9'd1;
end

always @ (posedge clk) begin
  if(~|shiftcnt[2:0]) begin
    if (~|shiftcnt[6:3])
      shift <= #1 (exchan ^ shiftcnt[7]) ? ldata_mux : rdata_mux;
    else
      shift <= #1 {shift[14:0], 1'b0};
  end
end


//// output ////
assign aud_daclrck = shiftcnt[7];
assign aud_bclk    = ~shiftcnt[2];
assign aud_xck     = shiftcnt[0];
assign aud_dacdat  = shift[15];


endmodule


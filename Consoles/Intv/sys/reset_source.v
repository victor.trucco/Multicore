/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/// reset_source.v

// This file was auto-generated as a prototype implementation of a module
// created in component editor.  It ties off all outputs to ground and
// ignores all inputs.  It needs to be edited to make it do something
// useful.
// 
// This file will not be automatically regenerated.  You should check it in
// to your version control system if you want to keep it.

`timescale 1 ps / 1 ps
module reset_source
(
	input  wire  clk,        //      clock.clk
	input  wire  reset_hps,  //  reset_hps.reset
	output wire  reset_sys,  //  reset_sys.reset
	output wire  reset_cold, // reset_cold.reset
	input  wire  cold_req,   //  reset_ctl.cold_req
	output wire  reset,      //           .reset
	input  wire  reset_req,  //           .reset_req
	input  wire  reset_vip,  //           .reset_vip
	input  wire  warm_req,   //           .warm_req
	output wire  reset_warm  // reset_warm.reset
);

assign reset_cold = cold_req;
assign reset_warm = warm_req;

wire   reset_m    = sys_reset | reset_hps | reset_req;
assign reset      = reset_m;
assign reset_sys  = reset_m | reset_vip;

reg  sys_reset = 1;
always @(posedge clk) begin
	integer timeout = 0;
	reg reset_lock = 0;

	reset_lock <= reset_lock | cold_req;

	if(timeout < 2000000) begin
		sys_reset <= 1;
		timeout <= timeout + 1;
		reset_lock <= 0;
	end
	else begin 
		sys_reset <= reset_lock;
	end
end

endmodule

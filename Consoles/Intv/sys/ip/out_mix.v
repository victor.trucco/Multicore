/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/// out_mix.v

`timescale 1 ps / 1 ps
module out_mix (
		input  wire        clk,             // Output.clk
		output reg         de,              //       .de
		output reg         h_sync,          //       .h_sync
		output reg         v_sync,          //       .v_sync
		output reg  [23:0] data,            //       .data
		output reg         vid_clk,         //  input.vid_clk
		input  wire [1:0]  vid_datavalid,   //       .vid_datavalid
		input  wire [1:0]  vid_h_sync,      //       .vid_h_sync
		input  wire [1:0]  vid_v_sync,      //       .vid_v_sync
		input  wire [47:0] vid_data,        //       .vid_data
		input  wire        underflow,       //       .underflow
		input  wire        vid_mode_change, //       .vid_mode_change
		input  wire [1:0]  vid_std,         //       .vid_std
		input  wire [1:0]  vid_f,           //       .vid_f
		input  wire [1:0]  vid_h,           //       .vid_h
		input  wire [1:0]  vid_v            //       .vid_v
	);

	reg        r_de;
	reg        r_h_sync;
	reg        r_v_sync;
	reg [23:0] r_data;
	
	always @(posedge clk) begin
		vid_clk <= ~vid_clk;
		
		if(~vid_clk) begin
			{r_de,de} <= vid_datavalid;
			{r_h_sync, h_sync} <= vid_h_sync;
			{r_v_sync, v_sync} <= vid_v_sync;
			{r_data, data} <= vid_data;
		end else begin
			de <= r_de;
			h_sync <= r_h_sync;
			v_sync <= r_v_sync;
			data <= r_data;
		end
	end

endmodule

--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
    

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity phoenix is
  port (
    -- Clocks
        clock_50_i          : in    std_logic;

        -- Buttons
        btn_n_i             : in    std_logic_vector(4 downto 1);

        -- SRAMs (AS7C34096)
        sram_addr_o         : out   std_logic_vector(18 downto 0)   := (others => '0');
        sram_data_io        : inout std_logic_vector(7 downto 0)    := (others => 'Z');
        sram_we_n_o         : out   std_logic                       := '1';
        sram_oe_n_o         : out   std_logic                       := '1';

        sram3_addr_o        : out   std_logic_vector(18 downto 0)   := (others => '0');
        sram3_data_io       : inout std_logic_vector(7 downto 0)    := (others => 'Z');
        sram3_we_n_o        : out   std_logic                       := '1';
        sram3_oe_n_o        : out   std_logic                       := '1';
        
        -- SDRAM    (H57V256)
        SDRAM_A             : out std_logic_vector(12 downto 0);
        SDRAM_DQ            : inout std_logic_vector(15 downto 0);

        SDRAM_BA            : out std_logic_vector(1 downto 0);
        SDRAM_DQMH          : out std_logic;
        SDRAM_DQML          : out std_logic;

        SDRAM_nRAS          : out std_logic;
        SDRAM_nCAS          : out std_logic;
        SDRAM_CKE           : out std_logic;
        SDRAM_CLK           : out std_logic;
        SDRAM_nCS           : out std_logic;
        SDRAM_nWE           : out std_logic;

        -- PS2
        ps2_clk_io          : inout std_logic                       := 'Z';
        ps2_data_io         : inout std_logic                       := 'Z';
        ps2_mouse_clk_io    : inout std_logic                       := 'Z';
        ps2_mouse_data_io   : inout std_logic                       := 'Z';

        -- SD Card
        sd_cs_n_o           : out   std_logic                       := 'Z';
        sd_sclk_o           : out   std_logic                       := 'Z';
        sd_mosi_o           : out   std_logic                       := 'Z';
        sd_miso_i           : in    std_logic                       := 'Z';

        -- Joysticks
        joy1_up_i           : in    std_logic;
        joy1_down_i         : in    std_logic;
        joy1_left_i         : in    std_logic;
        joy1_right_i        : in    std_logic;
        joy1_p6_i           : in    std_logic;
        joy1_p9_i           : in    std_logic;
        joy2_up_i           : in    std_logic;
        joy2_down_i         : in    std_logic;
        joy2_left_i         : in    std_logic;
        joy2_right_i        : in    std_logic;
        joy2_p6_i           : in    std_logic;
        joy2_p9_i           : in    std_logic;
        joyX_p7_o           : out   std_logic                       := '1';

        -- Audio
        AUDIO_L             : out   std_logic                       := '0';
        AUDIO_R             : out   std_logic                       := '0';
        ear_i               : in    std_logic;
        mic_o               : out   std_logic                       := '0';

        -- VGA
        VGA_R               : out   std_logic_vector(3 downto 0)    := (others => '0');
        VGA_G               : out   std_logic_vector(3 downto 0)    := (others => '0');
        VGA_B               : out   std_logic_vector(3 downto 0)    := (others => '0');
        VGA_HS              : out   std_logic                       := '1';
        VGA_VS              : out   std_logic                       := '1';

        -- HDMI
        tmds_o              : out   std_logic_vector(7 downto 0)    := (others => '0');

        --STM32
        stm_rx_o            : out std_logic         := 'Z'; -- stm RX pin, so, is OUT on the slave
        stm_tx_i            : in  std_logic         := 'Z'; -- stm TX pin, so, is IN on the slave
        stm_rst_o           : out std_logic         := 'Z'; -- '0' to hold the microcontroller reset line, to free the SD card
        
        stm_a15_io          : inout std_logic;
        stm_b8_io           : inout std_logic       := 'Z';
        stm_b9_io           : inout std_logic       := 'Z';
        
        SPI_SCK             : inout std_logic       := 'Z';
        SPI_DO              : inout std_logic       := 'Z';
        SPI_DI              : inout std_logic       := 'Z';
        SPI_SS2             : inout std_logic       := 'Z'

    );

end phoenix;

architecture rtl of phoenix is

 constant CONF_STR : string := "P,phoenix.dat;"&
                --                "S1,COL/BIN/ROM,Load COL BIN ROM;"&
                --                "S2,SG,Load SG;"&
                --        --        "O9,Wait,On,Off;"&
                --                "O23,Scanlines,Off,25%,50%,75%;"&
                --                "O8,Megacart,Off,On;"&
                --                "O9A,CPU Turbo,Off,7Mhz,12Mhz;"&
                --                "O7,Exp mod. 2,Off,On;"&
                --                 "O6,Joystick swap,Off,On;"&
                                "TB,Reset;";

  function to_slv(s: string) return std_logic_vector is 
    constant ss: string(1 to s'length) := s; 
    variable rval: std_logic_vector(1 to 8 * s'length); 
    variable p: integer; 
    variable c: integer; 
  
  begin 
    for i in ss'range loop
      p := 8 * i;
      c := character'pos(ss(i));
      rval(p - 7 to p) := std_logic_vector(to_unsigned(c,8)); 
    end loop; 
    return rval; 

  end function; 
  
  component data_io 
      generic ( STRLEN : integer := 0 );
    port (
        sck         : in std_logic;
        ss          : in std_logic;
        sdi         : in std_logic;
        sdo         : out std_logic;

        data_in     : in std_logic_vector(7 downto 0);   
        conf_str    : in std_logic_vector((8*STRLEN)-1 downto 0);   
        status      : out std_logic_vector(31 downto 0);   

        downloading : out std_logic;
        index       : out std_logic_vector(7 downto 0);
        clk         : in std_logic;
        clkref      : in std_logic;
        wr          : out std_logic;
        a           : out std_logic_vector(24 downto 0);
        d           : out std_logic_vector(7 downto 0)
    );
  end component data_io;


  signal hdmi_p_s       : std_logic_vector( 3 downto 0);
  signal hdmi_n_s       : std_logic_vector( 3 downto 0);

  signal clk_25m_s      : std_logic;
  signal clk_100m_s     : std_logic;
  signal clk_125m_s     : std_logic;
  signal pll_locked     : std_logic;

  signal romwr_a        : std_logic_vector(24 downto 0);
  signal ioctl_dout     : std_logic_vector(7 downto 0);
  signal rom_wr         : std_logic;
  signal index          : std_logic_vector(7 downto 0);
  signal downl          : std_logic := '0';
  signal BIOS_wr        : std_logic;

  signal bios_rom_a_s   : std_logic_vector(12 downto 0);
  signal bios_rom_d_s   : std_logic_vector( 7 downto 0);

  signal status         : std_logic_vector(31 downto 0);
  signal power_on_s     : std_logic_vector(15 downto 0) := (others=>'1');
  signal por_reset_s    : std_logic;
  signal osd_s          : std_logic_vector(7 downto 0) := (others=>'1');
     
begin


    SDRAM_nCS       <= '1';


  pll : entity work.pll
    port map (
      inclk0 => clock_50_i,
      c0     => clk_25m_s,
      c1     => clk_100m_s,
      c2     => SDRAM_CLK,
      c3     => clk_125m_s,
      locked => pll_locked
      );

 phoenix_ent : entity work.phoenix_top
 port map
   ( --clock_50m0_i_net   => clock_50_i

     clk_25m0_o         => clk_25m_s
   , clk_100m0_o        => clk_100m_s
   , clk_125m0_o        => clk_125m_s

   -- Buttons
   , btn_reset_n_i      => btn_n_i(4) and not(por_reset_s or downl)

   -- External IO port
--   ; exp_io1_net        : inout  std_logic_vector(12 downto 2) := (others => 'Z')
--   ; exp_io2_net        : inout  std_logic_vector(24 downto 15):= (others => 'Z')

   -- SRAM
   , sram_addr_o        => sram_addr_o(18 downto 0)
   , sram_data_io       => sram_data_io
   , sram_ce_n_o        => open
   , sram_oe_n_o        => sram_oe_n_o
   , sram_we_n_o        => sram_we_n_o

   -- PS2
   , ps2_clk_io         => ps2_clk_io
   , ps2_data_io        => ps2_data_io

   -- SD Card
   , sd_cs_n_o          => sd_cs_n_o
   , sd_sclk_o          => sd_sclk_o
   , sd_mosi_o          => sd_mosi_o
   , sd_miso_i          => sd_miso_i
   , sd_cd_n_i          => '0'

   -- Flash
-- Unused. MH June 2020.
--   ; flash_cs_n_o       : out    std_logic := '1'
--   ; flash_sclk_o       : out    std_logic := '0'
--   ; flash_mosi_o       : out    std_logic := '0'
--   ; flash_miso_i       : in     std_logic
--   ; flash_wp_o         : out    std_logic := '0'
--   ; flash_hold_o       : out    std_logic := '1'

   -- Joystick
   , joy_p5_o           => open
   , joy_p8_o           => open --joyX_p7_o
   , joy1_p1_i          => '1' --joy1_up_i,
   , joy1_p2_i          => '1' --joy1_down_i
   , joy1_p3_i          => '1' --joy1_left_i
   , joy1_p4_i          => '1' --joy1_right_i
   , joy1_p6_i          => '1' --joy1_p6_i
   , joy1_p7_i          => '1' --
   , joy1_p9_i          => '1' --joy1_p9_i
   , joy2_p1_i          => '1'
   , joy2_p2_i          => '1'
   , joy2_p3_i          => '1'
   , joy2_p4_i          => '1'
   , joy2_p6_i          => '1'
   , joy2_p7_i          => '1'
   , joy2_p9_i          => '1'

   -- SNES controller
   , snesjoy_clock_o    => open
   , snesjoy_latch_o    => open
   , snesjoy_data_i     => '1'

   -- LVDS
   , lvds_p_o           => hdmi_p_s
   , lvds_n_o           => hdmi_n_s

   -- Cartridge
   , cart_addr_o        => open
   , cart_data_io       => open
   , cart_dir_o         => open
   , cart_oe_n_o        => open
   , cart_en_80_n_o     => open
   , cart_en_A0_n_o     => open
   , cart_en_C0_n_o     => open
   , cart_en_E0_n_o     => open

   -- LED and RGB-LED
   , led_o              => open
   , rgb_led_o          => open

   -- external BIOS
   , bios_rom_a_o       => bios_rom_a_s
   , bios_rom_d_i       => bios_rom_d_s       
);


  tmds_o(7)   <= hdmi_p_s(2); -- 2+       
  tmds_o(6)   <= hdmi_n_s(2); -- 2-       
  tmds_o(5)   <= hdmi_p_s(1); -- 1+           
  tmds_o(4)   <= hdmi_n_s(1); -- 1-       
  tmds_o(3)   <= hdmi_p_s(0); -- 0+       
  tmds_o(2)   <= hdmi_n_s(0); -- 0-   
  tmds_o(1)   <= hdmi_p_s(3); -- CLK+ 
  tmds_o(0)   <= hdmi_n_s(3); -- CLK- 

  --BIOS on SRAM

  data_io_inst: data_io
  generic map (STRLEN => CONF_STR'length)
  port map
  (
      sck         => SPI_SCK, 
      ss          => SPI_SS2, 
      sdi         => SPI_DI, 
      sdo         => SPI_DO, 
      
      data_in     => osd_s,-- and keys_s,
      conf_str    => to_slv(CONF_STR), 
      status      => status, 

      downloading => downl, 
      index       => index, 
      clk         => clk_100m_s, 
      clkref      => '1', 
      wr          => rom_wr, 
      a           => romwr_a, 
      d           => ioctl_dout
  );
 
  sram3_addr_o  <= romwr_a(18 downto 0) when BIOS_wr = '1' else "000000" & bios_rom_a_s;
  sram3_data_io <= ioctl_dout when BIOS_wr ='1' else (others=>'Z');
  sram3_oe_n_o <= '0';
  sram3_we_n_o <= not BIOS_wr;
  bios_rom_d_s <= sram3_data_io;  

  BIOS_wr  <= '1' when romwr_a(24 downto 13) = "000000000000" and downl = '1' and index = x"00" else '0'; -- first 8kb of pumped data

--  --------------------------------------------------------------------------------
--  -- Power on Reset
--  ----------------------------------------------------------------------------
    process (clk_25m_s)
    begin
      if rising_edge(clk_25m_s) then

        if power_on_s /= x"0000" then
          power_on_s <= power_on_s - 1;
          por_reset_s <= '1';

        else 
          por_reset_s <= '0';
        end if;

      end if;
    end process;

-- stm_rst_o       <= btn_n_i(1); 

  --------------------------------------------------------------------------------
  -- start the microcontroller pump after the power on
  ----------------------------------------------------------------------------
    process (clk_25m_s, osd_s)
    variable edge_v : std_logic_vector (1 downto 0);
    begin
      if rising_edge(clk_25m_s) then
        edge_v := edge_v(0) & downl;

        if power_on_s /= x"0000" then
          osd_s <= "00111111"; 
          stm_rst_o       <= 'Z'; 
        end if;
        
        if downl = '1' and osd_s <= "00111111" then
          osd_s <= "11111111";
        end if;

        if edge_v = "10" and osd_s = "11111111" and power_on_s = x"0000" then
            stm_rst_o       <= '0';           
        end if;
        
      end if;
    end process;

end rtl;
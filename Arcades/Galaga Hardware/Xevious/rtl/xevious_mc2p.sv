//============================================================================
//
//  Multicore 2+ Top by Victor Trucco
//
//============================================================================

`default_nettype none

module Xevious_MC2p
(
 // Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAM (IS61WV20488FBLL-10)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM (W9825G6KH-6)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io        = 1'bz,
    inout wire  ps2_data_io       = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o         = 1'bZ,
    output wire sd_sclk_o         = 1'bZ,
    output wire sd_mosi_o         = 1'bZ,
    input wire  sd_miso_i,

    // Joysticks
    output wire joy_clock_o       = 1'b1,
    output wire joy_load_o        = 1'b1,
    input  wire joy_data_i,
    output wire joy_p7_o          = 1'b1,

    // Audio
    output      AUDIO_L,
    output      AUDIO_R,
    input wire  ear_i,
    output wire mic_o             = 1'b0,

    // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

    //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
   
    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output wire   SPI_nWAIT        = 1'b1, // '0' to hold the microcontroller data streaming

    inout [31:0] GPIO,

    output LED                    = 1'b1 // '0' is LED on
);


//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------
assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign stm_rst_o    = 1'bZ;
assign stm_rx_o = 1'bZ;

//no SRAM for this core
assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;

//all the SD reading goes thru the microcontroller for this core
assign sd_cs_n_o = 1'bZ;
assign sd_sclk_o = 1'bZ;
assign sd_mosi_o = 1'bZ;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

joystick_serial  joystick_serial 
(
    .clk_i           ( clk_18 ),
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

// ROM Data Pump
reg [7:0] pump_s = 8'b11111111;
PumpSignal PumpSignal (clk_sys, ~pll_locked, ioctl_downl, pump_s);

//-----------------------------------------------------------------

localparam CONF_STR = {
    "P,CORE_NAME.dat;",
    "S,DAT,Alternative ROM...;",
    "O34,Scanlines,Off,25%,50%,75%;",
    "O5,Blend,Off,On;",

    "O2,Double Video Buffer,On,Off;",
    "O67,Screen Rotation,0,90,180,270;",
    "O1,Scandoubler,On,Off;",

    "T0,Reset;",
    "V,v1.00.",`BUILD_DATE
};

wire  [1:0] scanlines = status[4:3];
wire        blend     = status[5];
wire  [7:0] dipA      = status[15:8];
wire  [7:0] dipB      = status[23:16]; // {1'b1,status[22:16]};

wire  [6:0] core_mod;

assign LED = ~ioctl_downl;
assign AUDIO_R = AUDIO_L;
//assign SDRAM_CLK = clk_72;
assign SDRAM_CKE = 1;

wire clk_18, clk_72;
wire pll_locked;
pll pll(
    .inclk0(clock_50_i),
    .c0(clk_18),
    .c1(clk_72),
    .c2(SDRAM_CLK),
    .locked(pll_locked)
    );

wire clk_25m2,clk_40;
pll_vga pll_vga(
    .inclk0(clock_50_i),
    .c0(clk_25m2),
    .c1(clk_40)
    );

wire        clk_sys = clk_18;
wire        clk_mem = clk_72;

reg         rom_loaded = 0;
reg         reset = 1;

always @(posedge clk_sys) begin
    reg ioctl_downlD;
    ioctl_downlD <= ioctl_downl;

    if (ioctl_downlD & ~ioctl_downl) rom_loaded <= 1;
    reset <= status[0] | ~btn_n_o[4] | ioctl_downl | ~rom_loaded;
end

wire [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;

wire        ypbpr;
wire        no_csync;
wire        key_strobe;
wire        key_pressed;
wire  [7:0] key_code;
/*
user_io #(
    .STRLEN($size(CONF_STR)>>3))
user_io(
    .clk_sys        (clk_sys        ),
    .conf_str       (CONF_STR       ),
    .SPI_CLK        (SPI_SCK        ),
    .SPI_SS_IO      (CONF_DATA0     ),
    .SPI_MISO       (SPI_DO         ),
    .SPI_MOSI       (SPI_DI         ),
    .buttons        (buttons        ),
    .switches       (switches       ),
    .scandoubler_disable (scandoublerD    ),
    .ypbpr          (ypbpr          ),
    .no_csync       (no_csync       ),
    .core_mod       (core_mod       ),
    .key_strobe     (key_strobe     ),
    .key_pressed    (key_pressed    ),
    .key_code       (key_code       ),
    .joystick_0     (joystick_0     ),
    .joystick_1     (joystick_1     ),
    .status         (status         )
    );
*/
wire        ioctl_downl;
wire  [7:0] ioctl_index;
wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire  [7:0] ioctl_dout;

data_io #(
    .STRLEN(($size(CONF_STR)>>3)))
data_io(
    .clk_sys       ( clk_sys      ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),
    .SPI_DO        ( SPI_DO       ),
    
    .data_in       ( pump_s & keys_s ),
    .conf_str      ( CONF_STR     ),
    .status        ( status       ),
    .core_mod      ( core_mod     ),
    
    .ioctl_download( ioctl_downl  ),
    .ioctl_index   ( ioctl_index  ),
    .ioctl_wr      ( ioctl_wr     ),
    .ioctl_addr    ( ioctl_addr   ),
    .ioctl_dout    ( ioctl_dout   )
);

reg port1_req, port2_req;
wire [15:0] cpu1_addr;
wire [15:0] cpu1_q;
wire [15:0] cpu2_addr;
wire [15:0] cpu2_q;
wire [15:0] cpu3_addr;
wire [15:0] cpu3_q;
wire [16:0] fg_addr;
wire [15:0] fg_q;
wire [16:0] bg0_addr;
wire [15:0] bg0_q;
wire [16:0] bg1_addr;
wire [15:0] bg1_q;
wire [16:0] sp1_addr;
wire [15:0] sp1_q;
wire [16:0] sp2_addr;
wire [15:0] sp2_q;

sdram sdram(
    .*,
    .init_n        ( pll_locked   ),
    .clk           ( clk_mem      ),

    // port1 used for CPU 1-2-3-fg-bg
    .port1_req     ( port1_req    ),
    .port1_ack     ( ),
    .port1_a       ( ioctl_addr[23:1] ),
    .port1_ds      ( {ioctl_addr[0], ~ioctl_addr[0]} ),
    .port1_we      ( ioctl_downl ),
    .port1_d       ( {ioctl_dout, ioctl_dout} ),
    .port1_q       ( ),

    .cpu1_addr     ( cpu1_addr[15:1] ),
    .cpu1_q        ( cpu1_q    ),
    .cpu2_addr     ( cpu2_addr[15:1] ),
    .cpu2_q        ( cpu2_q    ),
    .cpu3_addr     ( cpu3_addr[15:1] ),
    .cpu3_q        ( cpu3_q    ),
    .fg_addr       ( fg_addr[16:1] ),
    .fg_q          ( fg_q    ),
    .bg0_addr      ( bg0_addr[16:1] ),
    .bg0_q         ( bg0_q    ),
    .bg1_addr      ( bg1_addr[16:1] ),
    .bg1_q         ( bg1_q    ),

    // port2 for sprite graphx
    .port2_req     ( port2_req ),
    .port2_ack     ( ),
    .port2_a       ( ioctl_addr[23:1] ),
    .port2_ds      ( {ioctl_addr[0], ~ioctl_addr[0]} ),
    .port2_we      ( ioctl_downl ),
    .port2_d       ( {ioctl_dout, ioctl_dout} ),
    .port2_q       ( ),

    .sp1_addr      ( sp1_addr[16:1] ),
    .sp1_q         ( sp1_q    ),
    .sp2_addr      ( sp2_addr[16:1] ),
    .sp2_q         ( sp2_q    )

);

// data upload controller
always @(posedge clk_sys) begin
    reg        ioctl_wr_last = 0;

    ioctl_wr_last <= ioctl_wr;
    if (ioctl_downl) begin
        if (~ioctl_wr_last && ioctl_wr) begin
            port1_req <= ~port1_req;
            port2_req <= ~port2_req;
        end
    end
end

wire [10:0] audio;
wire hs, vs;
wire blankn;
wire [3:0] r,g,b;

xevious xevious(
    .clock_18          ( clk_sys ),
    .reset             ( reset ),
    .cpu1_addr_o       ( cpu1_addr ),
    .cpu1_rom_do       ( cpu1_addr[0] ? cpu1_q[15:8] : cpu1_q[7:0] ),
    .cpu2_addr_o       ( cpu2_addr ),
    .cpu2_rom_do       ( cpu2_addr[0] ? cpu2_q[15:8] : cpu2_q[7:0] ),
    .cpu3_addr_o       ( cpu3_addr ),
    .cpu3_rom_do       ( cpu3_addr[0] ? cpu3_q[15:8] : cpu3_q[7:0] ),
    .fg_addr_o         ( fg_addr ),
    .fg_rom_do         ( fg_addr[0] ? fg_q[15:8] : fg_q[7:0] ),
    .bg0_addr_o        ( bg0_addr ),
    .bg0_rom_do        ( bg0_addr[0] ? bg0_q[15:8] : bg0_q[7:0] ),
    .bg1_addr_o        ( bg1_addr ),
    .bg1_rom_do        ( bg1_addr[0] ? bg1_q[15:8] : bg1_q[7:0] ),
    .sp_grphx_1_addr_o ( sp1_addr ),
    .sp_grphx_1_do     ( sp1_addr[0] ? sp1_q[15:8] : sp1_q[7:0] ),
    .sp_grphx_2_addr_o ( sp2_addr ),
    .sp_grphx_2_do     ( sp2_addr[0] ? sp2_q[15:8] : sp2_q[7:0] ),

    .video_r(r),
    .video_g(g),
    .video_b(b),
    .video_hs(hs),
    .video_vs(vs),
    .video_hb(hb),
    .video_vb(vb),
    .video_clock(video_clock),
    .video_blankn(blankn),
    .audio(audio),
    .dipA(dipA),
    .dipB(dipB),
    .coin(btn_coin | m_coin2),
    .start1(btn_one_player),

    //you can´t press opposite directions on Xevious at the same time
    .left  ((m_left  && ~m_right) ? 1:0), 
    .right ((m_right && ~m_left)  ? 1:0),
    .up    ((m_up    && ~m_down)  ? 1:0),
    .down  ((m_down  && ~m_up)    ? 1:0),

    .fire(m_fireA),
    .bomb(m_fireB),
    .start2(btn_two_players),
    .b_test(),
    .b_svce(),
    .dl_addr(ioctl_addr[16:0]),
    .dl_wr(ioctl_wr),
    .dl_data(ioctl_dout)
    );
    
wire scandoublerD = ~status[1] ^ direct_video;
wire hb, vb, video_clock, vga_hs_s, vga_vs_s;
wire [8:0] vga_col_s;

framebuffer #(288,224,9,1) framebuffer
(
        .clk_sys    ( clk_sys ),
        .clk_i      ( video_clock ),
        .RGB_i      ((blankn) ? {r[3:1],g[3:1],b[3:1]} : 9'b000000000 ),
        .hblank_i   ( hb ),
        .vblank_i   ( vb ),
        .dis_db_i   ( status[2] ),
        .rotate_i   ( status[7:6] ), 

        .clk_vga_i  ( (status[6]) ? clk_40 : clk_25m2 ), //800x600 or 640x480
        .RGB_o      ( vga_col_s ),
        .hsync_o    ( vga_hs_s ),
        .vsync_o    ( vga_vs_s ),
        .blank_o    (  ),

        .odd_line_o (  )
);


mist_video #(.COLOR_DEPTH(4), .SD_HCNT_WIDTH(10),.USE_FRAMEBUFFER(1)) mist_video(

    .clk_sys ( scandoublerD ? clk_sys : (status[6]) ? clk_40 : clk_25m2 ),
    .SPI_SCK(SPI_SCK),
    .SPI_SS3(SPI_SS2),
    .SPI_DI(SPI_DI),

    .R      ( (scandoublerD) ? (blankn) ? r  : 4'b0000 : {vga_col_s[8:6], vga_col_s[6]} ),
    .G      ( (scandoublerD) ? (blankn) ? g  : 4'b0000 : {vga_col_s[5:3], vga_col_s[3]} ),
    .B      ( (scandoublerD) ? (blankn) ? b  : 4'b0000 : {vga_col_s[2:0], vga_col_s[0]} ),
    .HSync  ( (scandoublerD) ? hs : vga_hs_s ),
    .VSync  ( (scandoublerD) ? vs : vga_vs_s ),

    .VGA_R(VGA_R),
    .VGA_G(VGA_G),
    .VGA_B(VGA_B),
    .VGA_VS(VGA_VS),
    .VGA_HS(VGA_HS),

    .ce_divider(1'b1),
    .rotate(osd_rotate),
    .scanlines(scanlines),
    .blend(blend),
    .scandoubler_disable(scandoublerD),
    .osd_enable(osd_enable),
    .no_csync(~scandoublerD)
    );

dac #(
    .C_bits(11))
dac(
    .clk_i(clk_sys),
    .res_n_i(1),
    .dac_i(audio),
    .dac_o(AUDIO_L)
    );


wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player  = ~btn_n_o[1] | m_one_player;
wire btn_two_players = ~btn_n_o[2] | m_two_players;
wire btn_coin        = ~btn_n_o[3] | m_coin1;
wire [7:0] keys_s;
wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable;
wire direct_video;
wire [1:0]osd_rotate;
wire [4:1]btn_n_o;

//translate scancode to joystick
MC2_HID #( .OSD_CMD( 3'b011 ), .USE_VKP( 1'b1), .CLK_SPEED(18000)) k_hid
(
    .clk          ( clk_18 ),
    .kbd_clk      ( ps2_clk_io ),
    .kbd_dat      ( ps2_data_io ),
    .joystick_0   ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1   ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
      
    //-- joystick_0 and joystick_1 should be swapped
    .joyswap      ( 0 ),

    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer    ( 1 ),

    //-- tilt, coin4-1, start4-1
    .controls     ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

    //-- fire12-1, up, down, left, right

    .player1      ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2      ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),

    .direct_video ( direct_video ),
    .osd_rotate   ( osd_rotate ),

    //-- keys to the OSD
    .osd_o        ( keys_s ),
    .osd_enable   ( osd_enable ),
    
    //-- sega joystick
    .sega_strobe  ( joy_p7_o ),

     //-- Front buttons
     .front_buttons_i (btn_n_i),
     .front_buttons_o (btn_n_o)        
);


endmodule

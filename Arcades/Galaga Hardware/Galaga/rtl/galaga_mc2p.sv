/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///============================================================================
//
//  Multicore 2+ Top by Victor Trucco
//
//============================================================================

`default_nettype none

module galaga_mc2p
(
// Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAM (IS61WV20488FBLL-10)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM (W9825G6KH-6)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io        = 1'bz,
    inout wire  ps2_data_io       = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o         = 1'bZ,
    output wire sd_sclk_o         = 1'bZ,
    output wire sd_mosi_o         = 1'bZ,
    input wire  sd_miso_i,

    // Joysticks
    output wire joy_clock_o       = 1'b1,
    output wire joy_load_o        = 1'b1,
    input  wire joy_data_i,
    output wire joy_p7_o          = 1'b1,

    // Audio
    output      AUDIO_L,
    output      AUDIO_R,
    input wire  ear_i,
    output wire mic_o             = 1'b0,

    // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

    //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
   
    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output wire   SPI_nWAIT         = 1'b1, // '0' to hold the microcontroller data streaming

    inout [31:0] GPIO,

    output LED                      = 1'b1 // '0' is LED on
);

`include "..\rtl\build_id.v" 

localparam CONF_STR = {
    "P,Galaga.ini;", //just read the INI file
	 "O2,Double Video Buffer,On,Off;",
    "O78,Screen Rotation,0,90,180,270;",
    "O34,Scanlines,Off,25%,50%,75%;",
    "O5,Blend,Off,On;",
    "O9,Scandoubler,On,Off;",
    "T6,Reset;",
    "V,v1.21.",`BUILD_DATE
};


assign AUDIO_R = AUDIO_L;



//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------
assign LED = 1;
assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign stm_rst_o    = 1'bZ;
assign stm_rx_o = 1'bZ;

//no SRAM for this core
assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;

//no SDRAM for this core
assign SDRAM_nCS = 1'b1;

//all the SD reading goes thru the microcontroller for this core
assign sd_cs_n_o = 1'bZ;
assign sd_sclk_o = 1'bZ;
assign sd_mosi_o = 1'bZ;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

joystick_serial  joystick_serial 
(
    .clk_i           ( clk_40 ),
    
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

// ROM Data Pump
reg [7:0] osd_s = 8'b11111111;
PumpSignal PumpSignal (clk_18, ~pll_locked, ioctl_download, osd_s);

//-- END defaults -------------------------------------------------------

wire clk_18,pll_locked;
pll pll(
    .inclk0(clock_50_i),
    .c0(clk_18),
    .locked(pll_locked)
    );

wire clk_25m2,clk_40;
pll_vga pll_vga(
    .inclk0(clock_50_i),
    .c0(clk_25m2),
    .c1(clk_40)
    );

wire ioctl_download;
wire [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [9:0] kbjoy;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire        scandoublerD;
wire        ypbpr;
wire [10:0] ps2_key;
wire [9:0] audio;
wire hs, vs;
wire blankn = ~(hb | vb);
wire hb, vb;
wire [2:0] r,g;
wire [1:0] b;
wire video_clock;

galaga galaga(
    .clock_18(clk_18),
    .reset( status[6] | ~btn_n_o[4] ),
    .video_clk(video_clock),
    .video_r(r),
    .video_g(g),
    .video_b(b),
    .video_hb(hb),
    .video_vb(vb),
    .video_hs(hs),
    .video_vs(vs),
    .audio(audio),
    .coin(btn_coin),
    .start1(btn_one_player),
    .left1(m_left),
    .right1(m_right),
    .fire1(m_fireG | m_fireF | m_fireE | m_fireD | m_fireC | m_fireB | m_fireA),
    .start2(btn_two_players),
    .left2(m_left),
    .right2(m_right),
    .fire2(m_fireG | m_fireF | m_fireE | m_fireD | m_fireC | m_fireB | m_fireA)
    );


//=================================
wire [7:0] vga_col_s;
wire vga_hs_s, vga_vs_s;

wire [1:0] rotate = status[8:7];

framebuffer #(288,224,8,1) framebuffer
(
        .clk_sys    ( clk_18 ),
        .clk_i      ( video_clock ),
        .RGB_i      ((blankn) ? {r,g,b} : 8'b00000000 ),//idx_color_s ),
        .hblank_i   ( hb ),
        .vblank_i   ( vb ),
        .dis_db_i   ( status[2] ),
        .rotate_i   ( rotate ), 

        .clk_vga_i  ( (rotate[0]) ? clk_40 : clk_25m2 ), //800x600 or 640x480
        .RGB_o      ( vga_col_s ),
        .hsync_o    ( vga_hs_s ),
        .vsync_o    ( vga_vs_s ),
        .blank_o    (  ),

        .odd_line_o (  )
);


//=================================
/*
assign VGA_R = {r, 2'b00};
assign VGA_G = {g, 2'b00};
assign VGA_B = {b, 3'b000};
assign VGA_HS = hs;
assign VGA_VS = vs;
*/

wire direct_video_s = ~status[9] ^ direct_video;

mist_video #(.COLOR_DEPTH(3), .SD_HCNT_WIDTH(10), .USE_FRAMEBUFFER(1)) mist_video
(
    .clk_sys ( direct_video_s ? clk_18 : (rotate[0]) ? clk_40 : clk_25m2 ),
    .SPI_SCK ( SPI_SCK ),
    .SPI_SS3 ( SPI_SS2 ),
    .SPI_DI  ( SPI_DI  ),

    // video input 
    .R      ( (direct_video_s) ? (blankn) ? r  : 3'b000 : vga_col_s[7:5] ),
    .G      ( (direct_video_s) ? (blankn) ? g  : 3'b000 : vga_col_s[4:2] ),
    .B      ( (direct_video_s) ? (blankn) ? {b,b[0]}  : 3'b000 : {vga_col_s[1:0], vga_col_s[0]} ),
    .HSync  ( (direct_video_s) ? hs : vga_hs_s ),
    .VSync  ( (direct_video_s) ? vs : vga_vs_s ),

    // video output 
    .VGA_R  ( VGA_R  ),
    .VGA_G  ( VGA_G  ),
    .VGA_B  ( VGA_B  ),
    .VGA_HS ( VGA_HS ),
    .VGA_VS ( VGA_VS ),

    .ce_divider ( 1'b1 ),
    .blend      ( status[5] ),
    .rotate     ( osd_rotate ),
    .scanlines  ( status[4:3] ),
    .scandoubler_disable( direct_video_s ),
	 .no_csync   ( ~direct_video_s ),
    .osd_enable ( osd_enable )
);


data_io #(
    .STRLEN(($size(CONF_STR)>>3)))
data_io(
    .clk_sys       ( clk_18   ),
    .SPI_SCK       ( SPI_SCK  ),
    .SPI_SS2       ( SPI_SS2  ),
    .SPI_DI        ( SPI_DI   ),
    .SPI_DO        ( SPI_DO   ),
    
    .data_in       ( osd_s & keys_s ),
    .conf_str      ( CONF_STR ),
    .status        ( status   ),

     .ioctl_download (ioctl_download)

);


dac #(
    .C_bits(10))
dac(
    .clk_i(clk_18),
    .res_n_i(1),
    .dac_i(audio),
    .dac_o(AUDIO_L)
    );


wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player  = ~btn_n_o[1] | m_one_player;
wire btn_two_players = ~btn_n_o[2] | m_two_players;
wire btn_coin        = ~btn_n_o[3] | m_coin1;
wire [7:0] keys_s;
wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable;
wire direct_video;
wire [1:0]osd_rotate;
wire [4:1]btn_n_o;

//translate scancode to joystick
MC2_HID #( .OSD_CMD( 3'b011 ), .USE_VKP( 1'b1), .CLK_SPEED(18000)) k_hid
(
    .clk          ( clk_18 ),
    .kbd_clk      ( ps2_clk_io ),
    .kbd_dat      ( ps2_data_io ),
    .joystick_0   ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1   ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
      
    //-- joystick_0 and joystick_1 should be swapped
    .joyswap      ( 0 ),

    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer    ( 1 ),

    //-- tilt, coin4-1, start4-1
    .controls     ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

    //-- fire12-1, up, down, left, right

    .player1      ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2      ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),

    .direct_video ( direct_video ),
    .osd_rotate   ( osd_rotate ),

    //-- keys to the OSD
    .osd_o        ( keys_s ),
    .osd_enable   ( osd_enable ),
    
    //-- sega joystick
    .sega_strobe  ( joy_p7_o ),

	 //-- Front buttons
	 .front_buttons_i (btn_n_i),
	 .front_buttons_o (btn_n_o)        
);

endmodule 
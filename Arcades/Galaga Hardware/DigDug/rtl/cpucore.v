/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/module CPUCORE
(
	input				RESET,
	input				CLK,
	input				IRQ,
	input				NMI,
	output			RD,
	output			WR,
	output [15:0]	AD,
	input	  			DV,
	input	  [7:0]	DI,
	input	  [7:0]	IR,
	output  [7:0]	DO
);
	
wire  [7:0] m_do;
wire [15:0] m_ad;
wire        m_irq, m_nmi, m_me, m_ie, m_rd, m_wr;

wire        m_mx = (~m_me);
wire        m_mr = (~m_rd) & m_mx;
wire        m_mw = (~m_wr) & m_mx;

wire        cs_mrom = ( m_ad[15:14] ==  2'b00 );
wire			cs_nodv = cs_mrom;

wire [7:0]	m_di = cs_mrom ? IR : DV ? DI : 8'hFF;

assign m_irq = ~IRQ;
assign m_nmi = ~NMI;
tv80s core(
	.mreq_n(m_me),
	.iorq_n(m_ie),
	.rd_n(m_rd),
	.wr_n(m_wr),
	.A(m_ad),
	.dout(m_do),

	.reset_n(~RESET),
	.clk(CLK),
	.wait_n(1'b1),
	.int_n(m_irq),
	.nmi_n(m_nmi),
	.busrq_n(1'b1),
	.di(m_di)
);

assign RD = m_mr & ~cs_nodv;
assign WR = m_mw & ~cs_nodv;
assign AD = m_ad;
assign DO = m_do;

endmodule


//-----------------------------------------------
//  NMI Ack Control
//-----------------------------------------------
module CPUNMIACK
(
	input				RST,
	input				CL,
	input	[15:0]	AD,
	input				NMI,
	output reg		NMIo
);

reg  pNMI   = 1'b0;
wire NMIACK = ( AD == 16'h0066 );
always @( negedge CL or posedge RST ) begin
	if (RST) begin
		pNMI <= 1'b0;
		NMIo <= 1'b0;
	end
	else begin
		if (NMIACK) NMIo <= 0;
		else if ((pNMI^NMI) & NMI) NMIo <= 1'b1;
		pNMI <= NMI;
	end
end

endmodule


//============================================================================
//
//  Multicore 2+ / Neptuno Top by Victor Trucco
//
//============================================================================

`default_nettype none

module TheTowerofDruaga_mc2p (
     // Clocks
    input wire  clock_50_i,

    `ifdef NEPTUNO
    // SRAM (IS61WV102416BLL-10TLI)
    output [19:0]sram_addr_o = 20'b00000000000000000000,
    inout  [15:0]sram_data_io = 8'hZZZZ,
    output sram_we_n_o = 1'b1,
    output sram_oe_n_o = 1'b1,
    output sram_ub_n_o = 1'b1,
    output sram_lb_n_o = 1'b1,
    `else
    // SRAM (IS61WV20488FBLL-10)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1, 

    //external buttons
    input wire [4:1]    btn_n_i,
    `endif
        
    // SDRAM (W9825G6KH-6)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io        = 1'bz,
    inout wire  ps2_data_io       = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o         = 1'bZ,
    output wire sd_sclk_o         = 1'bZ,
    output wire sd_mosi_o         = 1'bZ,
    input wire  sd_miso_i,

    // Joysticks
    output wire joy_clock_o       = 1'b1,
    output wire joy_load_o        = 1'b1,
    input  wire joy_data_i,
    output wire joy_p7_o          = 1'b1,

    // Audio
    output      AUDIO_L,
    output      AUDIO_R,
    input wire  ear_i,
    output wire mic_o             = 1'b0,

    // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

    //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
   
    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output wire   SPI_nWAIT         = 1'b1, // '0' to hold the microcontroller data streaming

    inout [31:0] GPIO,

    output LED                      = 1'b1 // '0' is LED on

);

`include "..\..\rtl\build_id.v"

`define CORE_NAME "DRUAGA"
wire  [6:0] core_mod;

localparam CONF_STR = {
    `ifdef NEPTUNO
    "P,CORE_NAME.dat;",
    `else
    "P,CORE_NAME.dat;",
//    "P1,The Tower of Druaga.dat;",
    `endif
    "O12,Screen Rotate,0,90,180,270;",
    "O34,Scanlines,Off,25%,50%,75%;",
    "O6,Scandoubler,On,Off;",
    "O5,Blend,Off,On;",
    "OU,Service Mode,Off,On;",
    "OT,Freeze,Off,On;",
    "T0,Reset;",
    "V,v1.00.",`BUILD_DATE
};


//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------
assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign stm_rst_o = 1'bZ;
assign stm_rx_o  = 1'bZ;

//no SRAM for this core
assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;

//all the SD reading goes thru the microcontroller for this core
assign sd_cs_n_o = 1'bZ;
assign sd_sclk_o = 1'bZ;
assign sd_mosi_o = 1'bZ;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

`ifdef NEPTUNO
    wire [4:1] btn_n_i = 4'b1111;

    neptuno_joydecoder  neptuno_joydecoder (
`else 
    joystick_serial  joystick_serial (
`endif 

    .clk_i           ( clock_50_i ),
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

// ROM Data Pump
reg [7:0] pump_s = 8'b11111111;
PumpSignal PumpSignal (clock_48, ~pll_locked, ioctl_downl, pump_s);

//-----------------------------------------------------------------

wire        rotate    = 1'b0; //status[2];
wire  [1:0] scanlines = status[4:3];
wire        blend     = status[5];

wire        dcFreeze   = status[29];
wire        dcService  = status[30];
wire        dcCabinet  = 1'b0; // (upright only)

// The Tower of Druaga [t]
wire  [1:0] dtLives = status[9:8];

// Mappy [m]
wire        dmRoundP   = status[6];
wire  [2:0] dmRank       = status[12:10];
wire        dmDemoSnd    = status[13];
wire  [2:0] dmExtend     = status[16:14];
wire  [1:0] dmLives    = status[18:17];

// DigDug2 [d]
wire        ddLives    = status[19];
wire  [1:0] ddExtend   = status[21:20];
wire        ddLevelSel = status[22];

// Motos [o]
wire       doLives    = status[23];
wire       doRank     = status[24];
wire [1:0] doExtend   = status[26:25];
wire       doDemoSnd  = status[27];

reg   [7:0] DSW0;
reg   [7:0] DSW1;
reg   [7:0] DSW2;

reg   [5:0] INP0;
reg   [5:0] INP1;
reg   [2:0] INP2;

always @(*) begin

   // core_mod = 7'd0;

    INP0 = { m_fireB, m_fireA, m_left, m_down, m_right, m_up};
    INP1 = { m_fire2B, m_fire2A, m_left2, m_down2, m_right2, m_up2 };
    INP2 = { m_coin1 | m_coin2, m_two_players, m_one_player };
    DSW0 = 0;
    DSW1 = 0;
    DSW2 = 0;

    case (core_mod)
	7'h0, 7'h1, 7'h3, 7'h6: // DRUAGA, DIGDUG2, GROBDA
	begin
		DSW0 = status[15:8];
		DSW1 = status[23:16];
		DSW2 = { status[19:16], status[27:24] };
	end
	7'h2: // MAPPY
	begin
		DSW0 = status[15:8];
		DSW1 = status[23:16];
		DSW2 = { {2{status[27:24]}} };
	end
	default:
	begin
		DSW0 = status[15:8];
		DSW1 = status[23:16];
		DSW2 = status[31:24];
	end

	endcase
end

assign      LED = ~ioctl_downl;
assign      AUDIO_R = AUDIO_L;
//assign      SDRAM_CLK = clock_48;
assign    SDRAM_CKE = 1;

wire clock_48, clock_6, pll_locked;
pll pll(
    .inclk0(clock_50_i),
    .c0(clock_48),//49.147727
    .c1(clock_6),
    .c2(SDRAM_CLK),
    .locked(pll_locked)
    );

wire clk_25m2,clk_40;
pll_vga pll_vga(
    .inclk0(clock_50_i),
    .c0(clk_25m2),
    .c1(clk_40)
    );

wire [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire        scandoublerD;
wire        ypbpr;
wire        no_csync;
wire        key_strobe;
wire        key_pressed;
wire  [7:0] key_code;

// assign core_mod=7'd5;
/*
user_io #(.STRLEN($size(CONF_STR)>>3))user_io(
    .clk_sys        (clock_48       ),
    .conf_str       (CONF_STR       ),
    .SPI_CLK        (SPI_SCK        ),
    .SPI_SS_IO      (CONF_DATA0     ),
    .SPI_MISO       (SPI_DO         ),
    .SPI_MOSI       (SPI_DI         ),
    .buttons        (buttons        ),
    .switches       (switches       ),
    .scandoubler_disable (scandoublerD    ),
    .ypbpr          (ypbpr          ),
    .no_csync       (no_csync       ),
    .core_mod       (core_mod       ),
    .key_strobe     (key_strobe     ),
    .key_pressed    (key_pressed    ),
    .key_code       (key_code       ),
    .joystick_0     (joystick_0     ),
    .joystick_1     (joystick_1     ),
    .status         (status         )
    );
*/
wire        ioctl_downl;
wire  [7:0] ioctl_index;
wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire  [7:0] ioctl_dout;

/*
ROM map
00000-07FFF   cpu0     32k 3.1d+1.1b (+2.1c in Mappy)
08000-0BFFF   spchip0  16k 6.3m
0C000-0FFFF   spchip1  16k 7.3m
10000-11FFF   cpu1      8k 4.1k
12000-12FFF   bgchip    4k 5.3b
13000-133FF   spclut    1k 7.5k
13400-134FF   bgclut  256b 6.4c
13500-135FF   wave    256b 3.3m
13600-1361F   palet    32b 5.5b
*/

data_io #(.STRLEN(($size(CONF_STR)>>3)))
data_io(
    .clk_sys       ( clock_48     ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),
    .SPI_DO        ( SPI_DO       ),
    
    .data_in       ( pump_s & keys_s ),
    .conf_str      ( CONF_STR     ),
    .status        ( status       ),
    .core_mod      ( core_mod     ),

    .ioctl_download( ioctl_downl  ),
    .ioctl_index   ( ioctl_index  ),
    .ioctl_wr      ( ioctl_wr     ),
    .ioctl_addr    ( ioctl_addr   ),
    .ioctl_dout    ( ioctl_dout   )
);

reg port1_req, port2_req;
wire [14:0] rom_addr;
wire [15:0] rom_do;
wire [12:0] snd_addr;
wire [15:0] snd_do;

sdram sdram(
    .*,
    .init_n        ( pll_locked   ),
    .clk           ( clock_48     ),

    // port1 used for main CPU
    .port1_req     ( port1_req    ),
    .port1_ack     ( ),
    .port1_a       ( ioctl_addr[23:1] ),
    .port1_ds      ( {ioctl_addr[0], ~ioctl_addr[0]} ),
    .port1_we      ( ioctl_downl ),
    .port1_d       ( {ioctl_dout, ioctl_dout} ),
    .port1_q       ( ),

    .cpu1_addr     ( ioctl_downl ? 15'h7fff : {1'b0, rom_addr[14:1]} ),
    .cpu1_q        ( rom_do ),

    // port2 for sound CPU
    .port2_req     ( port2_req ),
    .port2_ack     ( ),
    .port2_a       ( ioctl_addr[23:1] - 16'h8000 ),
    .port2_ds      ( {ioctl_addr[0], ~ioctl_addr[0]} ),
    .port2_we      ( ioctl_downl ),
    .port2_d       ( {ioctl_dout, ioctl_dout} ),
    .port2_q       ( ),

    .snd_addr      ( ioctl_downl ? 15'h7fff : {3'b000, snd_addr[12:1]} ),
    .snd_q         ( snd_do )
);

always @(posedge clock_48) begin
    reg        ioctl_wr_last = 0;

    ioctl_wr_last <= ioctl_wr;
    if (ioctl_downl) begin
        if (~ioctl_wr_last && ioctl_wr) begin
            port1_req <= ~port1_req;
            port2_req <= ~port2_req;
        end
    end
end

reg reset = 1;
reg rom_loaded = 0;
always @(posedge clock_48) begin
    reg ioctl_downlD;
    ioctl_downlD <= ioctl_downl;

    if (ioctl_downlD & ~ioctl_downl) rom_loaded <= 1;
    reset <= status[0] | ~btn_n_o[4] | ioctl_downl | ~rom_loaded;
end

wire            PCLK, PCLK_EN;
wire  [8:0] HPOS,VPOS;

wire  [7:0] audio;
wire        hs, vs;
wire        hb, vb;
wire        blankn = ~(hb | vb);
wire [2:0]  r, g;
wire [1:0]  b;

fpga_druaga fpga_druaga(
    .MCLK(clock_48),
    .CLKCPUx2(clock_6),
    .RESET(reset),
    .SOUT(audio),
    .rom_addr(rom_addr),
    .rom_data(rom_addr[0] ? rom_do[15:8] : rom_do[7:0]),
    .snd_addr(snd_addr),
    .snd_data(snd_addr[0] ? snd_do[15:8] : snd_do[7:0]),
    .PH(HPOS),
    .PV(VPOS),
    .PCLK(PCLK),
    .PCLK_EN(PCLK_EN),
    .POUT({b,g,r}),
    .INP0(INP0),
    .INP1(INP1),
    .INP2(INP2),
    .DSW0(DSW0),
    .DSW1(DSW1),
    .DSW2(DSW2),

    .ROMAD(ioctl_addr[16:0]),
    .ROMDT(ioctl_dout),
    .ROMEN(ioctl_wr),
    .MODEL(core_mod[2:0])
    );

hvgen hvgen(
    .MCLK(clock_48),
    .HPOS(HPOS),
    .VPOS(VPOS),
    .PCLK(PCLK),
    .PCLK_EN(PCLK_EN),
    .HBLK(hb),
    .VBLK(vb),
    .HSYN(hs),
    .VSYN(vs)
);

wire direct_video_s = ~status[6] ^ direct_video;
wire [7:0] vga_col_s;
wire vga_hs_s, vga_vs_s;

framebuffer #(288,224,8,1) framebuffer
(
        .clk_sys    ( clock_48 ),
        .clk_i      ( PCLK ),
        .RGB_i      ((blankn) ? {r,g,b} : 8'b00000000 ),
        .hblank_i   ( hb ),
        .vblank_i   ( vb ),
        
        .rotate_i   ( status[2:1] ), 

        .clk_vga_i  ( (status[1]) ? clk_40 : clk_25m2 ), //800x600 or 640x480
        .RGB_o      ( vga_col_s ),
        .hsync_o    ( vga_hs_s ),
        .vsync_o    ( vga_vs_s )
);



mist_video #(.COLOR_DEPTH(3), .SD_HCNT_WIDTH(10), .USE_FRAMEBUFFER(1)) mist_video(
    
    .SPI_SCK        ( SPI_SCK          ),
    .SPI_SS3        ( SPI_SS2          ),
    .SPI_DI         ( SPI_DI           ),

    .clk_sys        ( (direct_video_s) ? clock_48 : (status[1]) ? clk_40 : clk_25m2),
    .R              ( (direct_video_s) ? blankn ? r : 0 : vga_col_s[7:5] ),
    .G              ( (direct_video_s) ? blankn ? g : 0 : vga_col_s[4:2] ),
    .B              ( (direct_video_s) ? blankn ? {b ,b[0]} : 0 : {vga_col_s[1:0],vga_col_s[0]} ),
    .HSync          ( (direct_video_s) ? hs : vga_hs_s ),
    .VSync          ( (direct_video_s) ? vs : vga_vs_s ),

/*
    .clk_sys        ( clock_48         ),
    .R              ( blankn ? r : 0   ),
    .G              ( blankn ? g : 0   ),
    .B              ( blankn ? {b,b[1]} : 0   ),
    .HSync          ( hs               ),
    .VSync          ( vs               ),
*/

    .VGA_R          ( VGA_R            ),
    .VGA_G          ( VGA_G            ),
    .VGA_B          ( VGA_B            ),
    .VGA_VS         ( VGA_VS           ),
    .VGA_HS         ( VGA_HS           ),

    .rotate         ( osd_rotate       ),
    .scandoubler_disable( direct_video_s ),
    .scanlines      ( scanlines        ),
    .blend          ( blend            ),
    .osd_enable     ( osd_enable       ),
    .no_csync       ( ~direct_video_s  )
    );

dac #(.C_bits(16))dac(
    .clk_i(clock_48),
    .res_n_i(1),
    .dac_i({audio,audio}),
    .dac_o(AUDIO_L)
    );


wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player  = ~btn_n_o[1] | m_one_player;
wire btn_two_players = ~btn_n_o[2] | m_two_players;
wire btn_coin        = ~btn_n_o[3] | m_coin1;
wire [7:0] keys_s;
wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable;
wire direct_video;
wire [1:0]osd_rotate;
wire [4:1]btn_n_o;

//translate scancode to joystick
MC2_HID #( .OSD_CMD( 3'b011 ), .USE_VKP( 1'b1), .CLK_SPEED(48000)) k_hid
(
    .clk          ( clock_48 ),
    .kbd_clk      ( ps2_clk_io ),
    .kbd_dat      ( ps2_data_io ),
    .joystick_0   ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1   ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
      
    //-- joystick_0 and joystick_1 should be swapped
    .joyswap      ( 0 ),

    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer    ( 0 ),

    //-- tilt, coin4-1, start4-1
    .controls     ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

    //-- fire12-1, up, down, left, right

    .player1      ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2      ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),

    .direct_video ( direct_video ),
    .osd_rotate   ( osd_rotate ),

    //-- keys to the OSD
    .osd_o        ( keys_s ),
    .osd_enable   ( osd_enable ),
    
    //-- sega joystick
    .sega_strobe  ( joy_p7_o ),

     //-- Front buttons
     .front_buttons_i (btn_n_i),
     .front_buttons_o (btn_n_o)        
);

endmodule

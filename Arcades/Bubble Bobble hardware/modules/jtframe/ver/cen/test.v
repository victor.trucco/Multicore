/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/`timescale 1ns/1ps

module test;

wire [1:0] cen, cenb;
reg        clk;
wire       cen_3p57, cen_1p78;
wire       cen6, cen3;

localparam [9:0] N=10'd5;
localparam [9:0] M=10'd12;

jtframe_frac_cen uut(
    .clk    ( clk   ),
    .n      ( N     ),         // numerator
    .m      ( M     ),         // denominator
    .cen    ( cen   ),
    .cenb   ( cenb  )  // 180 shifted
);

jtframe_cen3p57 #(1) uut3p57(
    .clk     ( clk      ),
    .cen_3p57( cen_3p57 ),
    .cen_1p78( cen_1p78 )
);

jtframe_cen24 u_cen(
    .clk    ( clk       ),
    .cen12  (           ),
    .cen12b (           ),
    .cen6   ( cen6      ),
    .cen6b  (           ),
    .cen3   ( cen3      ),
    .cen1p5 (           )
);

initial begin
    clk = 0;
    forever #20.834 clk = ~clk;
end

initial begin
    $dumpfile("test.lxt");
    $dumpvars;
    #2000 $finish;
end

endmodule

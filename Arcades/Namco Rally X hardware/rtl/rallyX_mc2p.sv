/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///============================================================================
//
//  Multicore 2+ Top by Victor Trucco
//
//============================================================================

`default_nettype none

module rallyX_mc2p (
     // Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAM (IS61WV20488FBLL-10)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM (W9825G6KH-6)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io        = 1'bz,
    inout wire  ps2_data_io       = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o         = 1'bZ,
    output wire sd_sclk_o         = 1'bZ,
    output wire sd_mosi_o         = 1'bZ,
    input wire  sd_miso_i,

    // Joysticks
    output wire joy_clock_o       = 1'b1,
    output wire joy_load_o        = 1'b1,
    input  wire joy_data_i,
    output wire joy_p7_o          = 1'b1,

    // Audio
    output      AUDIO_L,
    output      AUDIO_R,
    input wire  ear_i,
    output wire mic_o             = 1'b0,

    // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

    //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
   
    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output wire   SPI_nWAIT        = 1'b1, // '0' to hold the microcontroller data streaming

    inout [31:0] GPIO,

    output LED                    = 1'b1 // '0' is LED on
);



localparam CONF_STR = {
//    "P,CORE_NAME.dat;",
    "P,New Rally X.dat;",
    "S,DAT,Alternative ROM...;",
    "OHI,Screen Rotation,0,90,180,270;",
    "O34,Scanlines,None,CRT 25%,CRT 50%,CRT 75%;",
    "O5,Blend,Off,On;",
    "OG,Scandoubler,On,Off;",
    "DIP;",
    "T0,Reset;",
    "V,v1.00." };

//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------

assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign stm_rst_o    = 1'bZ;
assign stm_rx_o = 1'bZ;

//no SRAM for this core
assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;

//all the SD reading goes thru the microcontroller for this core
assign sd_cs_n_o = 1'bZ;
assign sd_sclk_o = 1'bZ;
assign sd_mosi_o = 1'bZ;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

joystick_serial  joystick_serial 
(
    .clk_i           ( clock_14 ),
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

reg [7:0] pump_s = 8'b11111111;
PumpSignal PumpSignal (clock_24, ~pll_locked, ioctl_downl, pump_s);

//-- END defaults -------------------------------------------------------

wire       rotate = status[2];
wire [1:0] scanlines = status[4:3];
wire       blend = status[5];
wire [1:0] orientation = {core_mod[2], core_mod[0]};

assign      LED = ~ioctl_downl;
assign      AUDIO_R = AUDIO_L;
assign    SDRAM_CLK = clock_24;
assign    SDRAM_CKE = 1;

wire pll_locked, clock_24, clock_14;
pll pll(
    .inclk0(clock_50_i),
    .c0(clock_24), //24.576MHz
    .c1(clock_14),
    .locked(pll_locked)
    );

wire clk_25m2,clk_40;
pll_vga pll_vga(
    .inclk0(clock_50_i),
    .c0(clk_25m2),
    .c1(clk_40)
    );

wire  [6:0] core_mod;
wire [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire        scandoublerD;
wire        ypbpr;
wire        no_csync;
wire  [7:0] audio;
wire        hs, vs;
wire        hb, vb;
wire        blankn = ~(hb | vb);
wire  [2:0] r, g;
wire  [1:0] b;
wire        key_strobe;
wire        key_pressed;
wire  [7:0] key_code;

reg   [7:0] iDSW1, iDSW2, iCTR1, iCTR2;

always @(*) begin
    iDSW1 = ~status[15:8];
    iDSW2 = ~status[23:16];
    iCTR1 = ~{ btn_coin, btn_one_player,  m_up,  m_down,  m_right,  m_left,  (m_fireA  | m_fireB  | m_fireC),  1'b0 };
    iCTR2 = ~{ m_coin2,  btn_two_players, m_up2, m_down2, m_right2, m_left2, (m_fire2A | m_fire2B | m_fire2C), 1'b0 };

    if (core_mod[0]) begin
        //Jungler, Loco-Motion, Tactician
        iCTR1 = ~{ btn_coin, m_coin2,  m_right, m_left, m_fireA, 1'b0, m_fireB, m_up2 };
        iCTR2 = ~{ btn_one_player, btn_two_players, m_left2, m_right2, m_fire2A, m_fire2B, m_down2, m_up };
        iDSW1[7] = ~m_down;
    end
    if (core_mod[3]) begin
        //Commando
        iCTR1 = ~{ btn_coin, m_coin2,  m_right, m_left, m_fireB, 2'b00, m_up };
        iCTR2 = ~{ btn_one_player, btn_two_players, m_left2, m_right2, m_fire2B, m_fire2A, m_down2, m_up };
        iDSW1[7] = ~m_down;
        iDSW1[6] = ~m_fireA;
    end
end

wire        ioctl_downl;
wire  [7:0] ioctl_index;
wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire  [7:0] ioctl_dout;
reg         port1_req;
reg  [15:0] rom_dout;
reg  [14:0] rom_addr;

data_io #(
    .STRLEN(($size(CONF_STR)>>3)))
data_io(
    .clk_sys       ( clock_24     ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),
    .SPI_DO        ( SPI_DO       ),
    
    .data_in       ( pump_s & keys_s ),
    .conf_str      ( CONF_STR     ),
    .status        ( status       ),
    .core_mod      ( core_mod     ),
    
    .ioctl_download( ioctl_downl  ),
    .ioctl_index   ( ioctl_index  ),
    .ioctl_wr      ( ioctl_wr     ),
    .ioctl_addr    ( ioctl_addr   ),
    .ioctl_dout    ( ioctl_dout   )
);
        
sdram #(.MHZ(24)) sdram(
    .*,
    .init_n        ( pll_locked   ),
    .clk           ( clock_24     ),
        
    // ROM upload
    .port1_req     ( port1_req    ),
    .port1_ack     ( ),
    .port1_a       ( ioctl_addr[22:1] ),
    .port1_ds      ( { ioctl_addr[0], ~ioctl_addr[0] } ),
    .port1_we      ( ioctl_downl ),
    .port1_d       ( {ioctl_dout, ioctl_dout} ),
      
    // CPU
    .cpu1_addr     ( ioctl_downl ? 17'h1ffff : {3'b000, rom_addr[14:1] } ),
    .cpu1_q        ( rom_dout  )
);

always @(posedge clock_24) begin
    reg        ioctl_wr_last = 0;
 
    ioctl_wr_last <= ioctl_wr;
    if (ioctl_downl) begin
        if (~ioctl_wr_last && ioctl_wr) begin
            port1_req <= ~port1_req;
        end
    end
end

fpga_nrx fpga_nrx(
    .RESET(status[0] | ~btn_n_i[4]),
    .CLK24M(clock_24),
    .CLK14M(clock_14),
    .mod_jungler(core_mod[0]),
    .mod_loco(core_mod[1]),
    .mod_tact(core_mod[2]),
    .mod_comm(core_mod[3]),
    .hsync(hs),
    .vsync(vs),
    .hblank(hb),
    .vblank(vb),
    .r(r),
    .g(g),
    .b(b),
    .PCLK(PCLK),
    .cpu_rom_addr(rom_addr),
    .cpu_rom_data(rom_addr[0] ? rom_dout[15:8] : rom_dout[7:0]),
    .SND(audio),
    .DSW1(iDSW1),
    .DSW2(iDSW2),
    .CTR1(iCTR1),
    .CTR2(iCTR2),
    .LAMP(),
    // ROM download
    .ROMCL(clock_24),
    .ROMAD(ioctl_addr[15:0]),
    .ROMDT(ioctl_dout),
    .ROMEN(ioctl_wr)
    );


//=================================
wire [7:0] vga_col_s;
wire vga_hs_s, vga_vs_s, PCLK;

framebuffer #(288,224,8) framebuffer
(
        .clk_sys    ( clock_24 ),
        .clk_i      ( PCLK ),
        .RGB_i      ((blankn) ? {r,g,b} : 8'b00000000 ),
        .hblank_i   ( hb ),
        .vblank_i   ( vb ),
        
        .rotate_i   ( status[18:17] ), 

        .clk_vga_i  ( (status[17]) ? clk_40 : clk_25m2 ), //800x600 or 640x480
        .RGB_o      ( vga_col_s ),
        .hsync_o    ( vga_hs_s ),
        .vsync_o    ( vga_vs_s ),
        .blank_o    (  ),

        .odd_line_o (  )
);

assign scandoublerD = ~status[16] ^ direct_video;
wire [1:0] osd_rotate;

mist_video #(.COLOR_DEPTH(6), .SD_HCNT_WIDTH(11), .USE_FRAMEBUFFER(1)) mist_video(
    .clk_sys        ( (scandoublerD) ? clock_24 :  (status[17]) ? clk_40 : clk_25m2       ),
    .SPI_SCK        ( SPI_SCK          ),
    .SPI_SS3        ( SPI_SS2          ),
    .SPI_DI         ( SPI_DI           ),

    .R              ( (scandoublerD) ? blankn ? {r,r}   : 0 : {vga_col_s[7:5],vga_col_s[7:5]}  ),
    .G              ( (scandoublerD) ? blankn ? {g,g}   : 0 : {vga_col_s[4:2],vga_col_s[4:2]}  ),
    .B              ( (scandoublerD) ? blankn ? {b,b,b} : 0 : {vga_col_s[1:0],vga_col_s[1:0],vga_col_s[1:0]}),
    .HSync          ( (scandoublerD) ? hs : vga_hs_s               ),
    .VSync          ( (scandoublerD) ? vs : vga_vs_s               ),

    .VGA_R          ( VGA_R            ),
    .VGA_G          ( VGA_G            ),
    .VGA_B          ( VGA_B            ),
    .VGA_HS         ( vga_hs_o         ),
    .VGA_VS         ( vga_vs_o         ),

    .ce_divider     ( 1'b1             ),
    .blend          ( blend            ),
    .rotate         ( osd_rotate ),
    .scandoubler_disable( scandoublerD ),
    .scanlines      ( scanlines        ),
    .osd_enable     ( osd_enable       )
    );

wire vga_vs_o, vga_hs_o;

assign VGA_VS = (scandoublerD) ? 1'b1                   : vga_vs_o; //teste 1 e 2
assign VGA_HS = (scandoublerD) ? ~(vga_hs_o ^ vga_vs_o) : vga_hs_o;     //teste 1
//assign VGA_HS = (scandoublerD) ? (vga_hs_o && vga_vs_o) : vga_hs_o;     //teste 2
//assign VGA_VS = vga_vs_o; //teste 3
//assign VGA_HS = vga_hs_o; //teste 3


    /*
user_io #(.STRLEN(($size(CONF_STR)>>3)))user_io(
    .clk_sys        (clock_24       ),
    .conf_str       (CONF_STR       ),
    .SPI_CLK        (SPI_SCK        ),
    .SPI_SS_IO      (CONF_DATA0     ),
    .SPI_MISO       (SPI_DO         ),
    .SPI_MOSI       (SPI_DI         ),
    .buttons        (buttons        ),
    .switches       (switches       ),
    .scandoubler_disable (scandoublerD    ),
    .ypbpr          (ypbpr          ),
    .no_csync       (no_csync       ),
    .key_strobe     (key_strobe     ),
    .key_pressed    (key_pressed    ),
    .key_code       (key_code       ),
    .joystick_0     (joystick_0     ),
    .joystick_1     (joystick_1     ),
    .status         (status         ),
    .core_mod       (core_mod       )
    );
    */
dac #(.C_bits(16))dac(
    .clk_i(core_mod[0] ? clock_14 : clock_24),
    .res_n_i(1),
    .dac_i({audio,audio}),
    .dac_o(AUDIO_L)
    );


//-----------------------

wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player =   ~btn_n_i[1] | m_one_player;
wire btn_two_players =  ~btn_n_i[2] | m_two_players;
wire btn_coin  =        ~btn_n_i[3] | m_coin1;

wire kbd_intr;
wire [7:0] kbd_scancode;
wire [7:0] keys_s;

//get scancode from keyboard
io_ps2_keyboard keyboard 
 (
  .clk       ( clock_14 ),
  .kbd_clk   ( ps2_clk_io ),
  .kbd_dat   ( ps2_data_io ),
  .interrupt ( kbd_intr ),
  .scancode  ( kbd_scancode )
);

wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable, direct_video;

//translate scancode to joystick
kbd_joystick #( .OSD_CMD    ( 3'b011 ), .USE_VKP( 1'b1), .CLK_SPEED (14000)) k_joystick
(
    .clk          ( clock_14 ),
    .kbdint       ( kbd_intr ),
    .kbdscancode  ( kbd_scancode ), 

    .joystick_0     ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1     ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
      
    //-- joystick_0 and joystick_1 should be swapped
    .joyswap        ( 0 ),

    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer      ( 0 ),

    .direct_video   ( direct_video ),
    .osd_rotate     ( osd_rotate ),

    //-- tilt, coin4-1, start4-1
    .controls    ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

    //-- fire12-1, up, down, left, right

    .player1     ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2     ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),

    //-- keys to the OSD
    .osd_o         ( keys_s ),
    .osd_enable     ( osd_enable ),

    //-- sega joystick
    .sega_strobe    ( joy_p7_o )
        
        
);

endmodule

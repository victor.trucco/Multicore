
# PlanAhead Launch Script for Post-Synthesis floorplanning, created by Project Navigator

create_project -name SpaceInvaders -dir "D:/Documentacao/Eletronica/FPGA/Multicore/SRC/Multicore/Arcades/Midway-Taito 8080 Hardware/Space Invaders/synth/ZX Next/SpaceInvaders/planAhead_run_1" -part xa6slx16ftg256-2
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "D:/Documentacao/Eletronica/FPGA/Multicore/SRC/Multicore/Arcades/Midway-Taito 8080 Hardware/Space Invaders/synth/ZX Next/SpaceInvaders/spaceinvaders_next.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {D:/Documentacao/Eletronica/FPGA/Multicore/SRC/Multicore/Arcades/Midway-Taito 8080 Hardware/Space Invaders/synth/ZX Next/SpaceInvaders} {ipcore_dir} }
set_property target_constrs_file "D:/Documentacao/Eletronica/FPGA/Multicore/SRC/Multicore/Arcades/Midway-Taito 8080 Hardware/Space Invaders/synth/ZX Next/zxnext_pins_issue2.ucf" [current_fileset -constrset]
add_files [list {D:/Documentacao/Eletronica/FPGA/Multicore/SRC/Multicore/Arcades/Midway-Taito 8080 Hardware/Space Invaders/synth/ZX Next/zxnext_pins_issue2.ucf}] -fileset [get_property constrset [current_run]]
link_design

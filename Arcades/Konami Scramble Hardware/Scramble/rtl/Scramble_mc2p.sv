/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///============================================================================
//  Scramble Arcade HW top-level for MiST
//
//  Scramble/Amidar/Frogger/Super Cobra/Tazzmania/Armored Car
//  Moon War/Speed Coin/Calipso/Dark Planet/Anteater/Lost Tomb
//  Mars/Battle Of Attlantis/Strategy X/Turtles/Rescue/Minefield
//  Mighty Monkey
//
//  This program is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation; either version 2 of the License, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//============================================================================
//============================================================================
//
//  Multicore 2+ Top by Victor Trucco
//
//============================================================================

`default_nettype none

module Scramble_mc2p
(
  // Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAM (IS61WV20488FBLL-10)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM (W9825G6KH-6)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io        = 1'bz,
    inout wire  ps2_data_io       = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o         = 1'bZ,
    output wire sd_sclk_o         = 1'bZ,
    output wire sd_mosi_o         = 1'bZ,
    input wire  sd_miso_i,

    // Joysticks
    output wire joy_clock_o       = 1'b1,
    output wire joy_load_o        = 1'b1,
    input  wire joy_data_i,
    output wire joy_p7_o          = 1'b1,

    // Audio
    output      AUDIO_L,
    output      AUDIO_R,
    input wire  ear_i,
    output wire mic_o             = 1'b0,

    // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

    //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
   
    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output wire   SPI_nWAIT        = 1'b1, // '0' to hold the microcontroller data streaming

    inout [31:0] GPIO,

    output LED                    = 1'b1 // '0' is LED on
);

`include "..\rtl\build_id.v"

`define CORE_NAME "SCRAMBLE"
wire [6:0] core_mod;

localparam CONF_STR = {
//    "P,Scramble.dat;",
    "P,CORE_NAME.dat;",
    "S,DAT,Alternative ROM...;",
    "OGH,Screen Rotation,0,90,180,270;",
    "O34,Scanlines,Off,25%,50%,75%;",
	 "OF,Double Video Buffer,On,Off;",
    "O5,Blending,Off,On;",
    "OI,Scandoubler,On,Off;",
    "O6,Joystick Swap,Off,On;",
    "DIP;",
    "T0,Reset;",
    "V,v1.20.",`BUILD_DATE
};


//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------
assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign stm_rst_o    = 1'bZ;
assign stm_rx_o = 1'bZ;
assign LED  = ~ioctl_downl;

//no SRAM for this core
assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;

//all the SD reading goes thru the microcontroller for this core
assign sd_cs_n_o = 1'bZ;
assign sd_sclk_o = 1'bZ;
assign sd_mosi_o = 1'bZ;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

joystick_serial  joystick_serial 
(
    .clk_i           ( clk_sys ),
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

// ROM Data Pump
reg [7:0] pump_s = 8'b11111111;
PumpSignal PumpSignal (clk_sys, ~pll_locked, ioctl_downl, pump_s);

//-----------------------------------------------------------------

integer hwsel = 0;
reg  [7:0] input0;
reg  [7:0] input1;
reg  [7:0] input2;
reg  [1:0] orientation;

always @(*) begin
    orientation = 2'b11; // portrait, left
    input0 = ~{ m_coin1, m_coin2, m_left, m_right, m_fireA, /*service*/1'b0, m_fireB, m_up2 };
    input1 = ~{ m_one_player, m_two_players, m_left2, m_right2, m_fire2A, m_fire2B, /*lives*/~status[8:7] };
    input2 = ~{ 1'b1, m_down, 1'b1, m_up, /*cabinet*/1'b1, /*coinage*/2'b11, m_down2 };

    //core_mod <= 7'h0; //debug
    
    case (core_mod)
        7'h0: // SCRAMBLE
        begin
            hwsel = 0;
        end
        7'h1: // AMIDAR
        begin
            hwsel = 0;
            input1[1:0] = ~status[8:7]; // lives345unl
            //input2[1] = status[10]; // demo sounds - no effect
        end
        7'h2: // FROGGER
        begin
            hwsel = 1;
        end
        7'h3: // SCOBRA
        begin
            hwsel = 2;
            input1[0] = status[9]; // allow continue
            input1[1] = status[7]; // lives34
        end
        7'h4: // TAZMANIA
        begin
            hwsel = 2;
            input0 = ~{ m_coin1, m_coin2, m_left, m_right, m_down, m_up, m_fireA, m_fireB };
            input1 = ~{ m_fire2A, m_fire2B, m_left2, m_right2, m_up2, m_down2, /*demosnd*/status[10], /*lives35*/status[7] };
            input2 = ~{ 1'b1, m_two_players, 2'b10, 3'b111, m_one_player }; // unknown, start2, 2xunknown, cabinet, 2xcoinage, start1
        end
        7'h5: // ARMORCAR
        begin
            hwsel = 2;
            input1[0] = ~status[7]; //lives35
            input1[1] = ~status[10]; // demo sounds
        end
        7'h6: // MOONWAR
        begin
            hwsel = 2;
            input0 = ~{ m_coin1, m_coin2, 1'b0, dial };
            input1 = ~{ m_fireA, m_fireB, m_fireC, m_fireD, m_two_players, m_one_player, /*live345*/~status[8:7] };
            input2 = ~{ 4'h0, 1'b1, 2'b11, 1'b0 }; // 4xunused, cabinet, coinage, p2fire(cocktail)
        end
        7'h7: // SPDCOIN
        begin
            hwsel = 2;
            input0 = ~{ m_coin1, m_coin2, m_left, m_right, m_two_players, 1'b0, m_one_player, 1'b0 };
            input1 = { 4'hf, 2'b00, 1'b0, 1'b0 };     // 6xunused, freeplay, freeze
            input2 = { 4'hf, ~status[7], status[11], 1'b1, 1'b1}; // 4xunused, lives35, difficulty, unknown, unused
        end
        7'h8: // CALIPSO
        begin
            hwsel = 3;
            input0 = ~{ m_coin1, m_coin2, m_left, m_right, m_down, m_up, 1'b1, m_two_players|m_fire2A }; // coin1, coin2, left, right, down, up, unused, start 2p / player2 fire
            input1 = ~{ 1'b1, 1'b1, m_left2, m_right2, m_down2, m_up2, status[10], status[7] };          // unused, unused, left, right, down, up, demo sounds, lives 3/5
            input2 = ~{ 5'b0, 2'b10, m_fireA | m_one_player };                                           // unused[7:3], coin dip[2:1], start 1p / player1 fire
        end
        7'h9: // DARKPLNT
        begin
            hwsel = 4;
            input0 = ~{ m_coin1, m_coin2, 3'b000, m_two_players | m_fireB, m_one_player | m_fireA, m_fireC };
            input1 = { darkplnt_dial_scrambled, /*lives*/status[7], /*bonus*/1'b0 };
            input2 = { /*unk*/4'hf, /*bonus life*/1'b0, /*coinage*/ 2'b10, /*unk*/1'b1 };
        end
        7'hA: // ANTEATER
        begin
            hwsel = 6;
            input0 = ~{ m_coin1, m_coin2, m_left, m_right, m_down, m_up, m_fireA, m_fireB };
            input1 = ~{ m_fire2A, m_fire2B, m_left2, m_right2, m_up2, m_down2, /*demosdns*/status[10], /*lives35*/status[7] };
            input2 = ~{ 1'b1, m_two_players, 2'b10, 3'b111, m_one_player };
        end
        7'hB: // LOSTTOMB
        begin
            hwsel = 7;
            input0 = ~{ m_coin1, m_coin2, m_left, m_right, m_down, m_up, m_one_player, m_two_players };
            input1 = ~{ 1'b0, m_fireA, m_left2, m_right2, m_down2, m_up2, /*lives35/free play/invulnerability*/~(status[8:7]+1'd1) };
            input2 = ~{ 4'h0, status[10], 2'b10, 1'b0 }; //4xunused, demo sounds, 2xcoinage, unused
        end
        7'hC: // MARS
        begin
            hwsel = 10;
            input0 = ~{ m_coin1, m_coin2, m_left, m_right, m_left2 | m_fireA, m_right2 | m_fireB, 1'b0, 1'b0 };
            input1 = ~{ m_one_player, m_two_players, 4'h0, /*coinage*/2'b11 };
            input2 = ~{ m_up2 | m_fireC, m_down, m_down2 | m_fireD, m_up, /*lives*/status[7], /*unk*/1'b0, /*cabinet*/1'b1, 1'b0 };
        end
        7'hD: // ATLANTIS
        begin
            hwsel = 0;
            input1[0] = 1'b0; // upright
            input1[1] = ~status[7]; // lives35
        end
        7'hE: // STRATGYX
        begin
            hwsel = 5;
            orientation = 2'b10;
            input0 = ~{ m_coin1, m_coin2, m_left, m_right, m_fireA, 1'b0, m_fireB, m_up2 };
            input1 = ~{ m_one_player, m_two_players, m_left2, m_right2, m_fire2A, m_fire2B, ~status[8:7] };
            input2 = ~{ m_fire2C, m_down, m_fireC, m_up, /*upright*/1'b1, /*coinage*/2'b00, m_down2 };
        end
        7'hF: // TURTLES
        begin
            hwsel = 11;
            input0 = ~{ m_coin1, m_coin2, m_left, m_right, m_fireA, 1'b0, 1'b0, m_up2 };
            input1 = ~{ m_one_player, m_two_players, m_left2, m_right2, m_fire2A, 1'b0, ~status[8:7] };
            input2 = ~{ 1'b0, m_down, 1'b0, m_up, /*upright*/1'b1, /*coinage*/2'b00, m_down2 };
        end
        7'h10: // MINEFLD
        begin
            hwsel = 8;
            input0 = ~{ m_coin1, m_coin2, m_left, m_right, m_down, m_up, /*start level*/status[11], m_fireA };
            input1 = ~{ /*2xunk*/2'b00, m_left2, m_right2, m_down2, m_up2, /*demosnd*/status[10], /*lives35*/status[7] };
            input2 = ~{ /*unk*/1'b0, m_two_players, /*2xunk*/2'b00, /*difficulty*/status[9:8], /*coinage*/1'b0, m_one_player };
        end
        7'h11: // RESCUE
        begin
            hwsel = 9;
            input0 = ~{ m_coin1, m_coin2, m_left, m_right, m_down, m_up, /*start level*/status[9], m_fireA };
            input1 = ~{ /*2xunk*/2'b00, m_left2, m_right2, m_down2, m_up2, /*demosnd*/status[10], /*lives35*/status[7] };
            input2 = ~{ /*unk*/1'b0, m_two_players, /*2xunk*/2'b00, /*difficulty*/~status[8], /*coinage*/2'b11, m_one_player };
        end
        7'h12: // MIMONKEY
        begin
            hwsel = 12;
            input2[5] = status[9]; // infinite lives
        end

        default:
        begin
            hwsel = 0;
        end
    endcase
end

wire       rotate    = status[2];
wire [1:0] scanlines = status[4:3];
wire       blend     = status[5];
wire       joyswap   = status[6];

assign AUDIO_R = AUDIO_L;
assign SDRAM_CLK = clk_sys;
assign SDRAM_CKE = 1;

wire clk_sys;
wire pll_locked;
pll pll(
    .inclk0(clock_50_i),
    .areset(0),
    .c0(clk_sys),
    .locked(pll_locked)
    );

wire clk_25m2,clk_40;
pll_vga pll_vga(
    .inclk0(clock_50_i),
    .c0(clk_25m2),
    .c1(clk_40)
    );

// reset generation
reg reset = 1;
reg rom_loaded = 0;
always @(posedge clk_sys) begin
    reg ioctl_downlD;
    ioctl_downlD <= ioctl_downl;

    if (ioctl_downlD & ~ioctl_downl) rom_loaded <= 1;
    reset <= status[0] | ~btn_n_i[4] | ~rom_loaded | ioctl_downl;
end

// clock enables
reg ce_6p, ce_6n, ce_12, ce_1p79;
always @(negedge clk_sys) begin
    reg [1:0] div = 0;
    reg [3:0] div179 = 0;

    div <= div + 1'd1;
    ce_12 <= div[0];
    ce_6p <= div[0] & ~div[1];
    ce_6n <= div[0] &  div[1];  
    ce_1p79 <= 0;
    div179 <= div179 - 1'd1;
    if(!div179) begin
        div179 <= 13;
        ce_1p79 <= 1;
    end
end

// ARM connection
wire [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire        scandoublerD;
wire        ypbpr;
wire        no_csync;
wire        key_strobe;
wire        key_pressed;
wire  [7:0] key_code;

/*
user_io #(
    .STRLEN($size(CONF_STR)>>3))
user_io(
    .clk_sys        (clk_sys        ),
    .conf_str       (CONF_STR       ),
    .SPI_CLK        (SPI_SCK        ),
    .SPI_SS_IO      (CONF_DATA0     ),
    .SPI_MISO       (SPI_DO         ),
    .SPI_MOSI       (SPI_DI         ),
    .buttons        (buttons        ),
    .switches       (switches       ),
    .scandoubler_disable (scandoublerD ),
    .ypbpr          (ypbpr          ),
    .no_csync       (no_csync       ),
    .core_mod       (core_mod       ),
    .key_strobe     (key_strobe     ),
    .key_pressed    (key_pressed    ),
    .key_code       (key_code       ),
    .joystick_0     (joystick_0     ),
    .joystick_1     (joystick_1     ),
    .status         (status         )
    );
*/

/* ROM structure
0000-7FFF 32k PGM ROM 
8000-9FFF  8k SND ROM

A000-A7FF  2k gfx1 5H
A800-AFFF  2k gfx2 5F
B000-B01F 32b palette LUT

Calipso, Mighty Monkey:
A000-BFFF  8k gfx1 5H
C000-DFFF  8k gfx2 5F
E000-E01F 32b palette LUT
*/

wire        ioctl_downl;
wire  [7:0] ioctl_index;
wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire  [7:0] ioctl_dout;

data_io #(
    .STRLEN(($size(CONF_STR)>>3)))
data_io(
    .clk_sys       ( clk_sys      ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),
    .SPI_DO        ( SPI_DO       ),
    
    .data_in       ( pump_s & keys_s ),
    .conf_str      ( CONF_STR     ),
    .status        ( status       ),
    .core_mod      ( core_mod     ),

    .ioctl_download( ioctl_downl  ),
    .ioctl_index   ( ioctl_index  ),
    .ioctl_wr      ( ioctl_wr     ),
    .ioctl_addr    ( ioctl_addr   ),
    .ioctl_dout    ( ioctl_dout   )
);

reg      port1_req;
reg [15:0] rom_dout;
reg [14:0] rom_addr;

sdram #(.MHZ(24)) sdram(
    .*,
    .init_n        ( pll_locked   ),
    .clk           ( clk_sys      ),

    // ROM upload
    .port1_req     ( port1_req    ),
    .port1_ack     ( ),
    .port1_a       ( ioctl_addr[22:1] ),
    .port1_ds      ( { ioctl_addr[0], ~ioctl_addr[0] } ),
    .port1_we      ( ioctl_downl ),
    .port1_d       ( {ioctl_dout, ioctl_dout} ),

    // CPU
    .cpu1_addr     ( ioctl_downl ? 17'h1ffff : {3'b000, rom_addr[14:1] } ),
    .cpu1_q        ( rom_dout  )
);

always @(posedge clk_sys) begin
    reg        ioctl_wr_last = 0;

    ioctl_wr_last <= ioctl_wr;
    if (ioctl_downl) begin
        if (~ioctl_wr_last && ioctl_wr) begin
            port1_req <= ~port1_req;
        end
    end
end

wire  [9:0] audio;
wire        hs, vs;
//wire        blankn = ~(hb | vb);
wire        blankn = ~vb;
wire        hb, vb;
wire  [5:0] r,b,g;

scramble_top scramble(
    .O_VIDEO_R(r),
    .O_VIDEO_G(g),
    .O_VIDEO_B(b),
    .O_HSYNC(hs),
    .O_VSYNC(vs),
    .O_HBLANK(hb),
    .O_VBLANK(vb),
    .O_AUDIO(audio),
    .I_HWSEL(hwsel),
    .I_PA(input0),
    .I_PB(input1),
    .I_PC(input2),
    .RESET(reset),
    .clk(clk_sys),
    .ena_12(ce_12),
    .ena_6(ce_6p),
    .ena_6b(ce_6n),
    .ena_1_79(ce_1p79),

    .rom_addr(rom_addr),
    .rom_dout(rom_addr[0] ? rom_dout[15:8] : rom_dout[7:0]),

    .dl_addr(ioctl_addr[15:0]),
    .dl_wr(ioctl_wr),
    .dl_data(ioctl_dout)
    );

//=================================
wire [7:0] vga_col_s;
wire vga_hs_s, vga_vs_s,vga_blank_s;

framebuffer #(256,224,8,1) framebuffer
(
        .clk_sys    ( clk_sys ),
        .clk_i      ( ce_6p ),
        .RGB_i      ((blankn) ? {r[5:3],g[5:3],b[5:4]} : 8'b00000000 ),//idx_color_s ),
        .hblank_i   ( hb ),
        .vblank_i   ( vb ),
        .dis_db_i   ( status[15] ),
        .rotate_i   ( status[17:16] ), 

        .clk_vga_i  ( (status[16]) ? clk_40 : clk_25m2 ), //800x600 or 640x480
        .RGB_o      ( vga_col_s ),
        .hsync_o    ( vga_hs_s ),
        .vsync_o    ( vga_vs_s ),
        .blank_o    ( vga_blank_s ),

        .odd_line_o (  )
);


//=================================

assign scandoublerD = ~status[ 18 ] ^ direct_video;

mist_video #(.COLOR_DEPTH(6),.SD_HCNT_WIDTH(10),.USE_FRAMEBUFFER(1)) mist_video(

    .clk_sys( scandoublerD ? clk_sys : (status[16]) ? clk_40 : clk_25m2 ),

    .SPI_SCK ( SPI_SCK ),
    .SPI_SS3 ( SPI_SS2 ),
    .SPI_DI  ( SPI_DI  ),

//    .R(blankn ? r : 6'd0),
//    .G(blankn ? g : 6'd0),
//    .B(blankn ? b : 6'd0),
//    .HSync(~hs),
//    .VSync(~vs),

    .R     ( scandoublerD ? blankn ? r : 6'd0 : { vga_col_s[7:5],3'b000 } ),
    .G     ( scandoublerD ? blankn ? g : 6'd0 : { vga_col_s[4:2],3'b000 } ),
    .B     ( scandoublerD ? blankn ? b : 6'd0 : { vga_col_s[1:0],vga_col_s[0],3'b000 } ),
    .HSync ( scandoublerD ? ~hs :    vga_hs_s ),
    .VSync ( scandoublerD ? ~vs :    vga_vs_s ),

    .VGA_R  ( VGA_R  ),
    .VGA_G  ( VGA_G  ),
    .VGA_B  ( VGA_B  ),
    .VGA_VS ( VGA_VS ),
    .VGA_HS ( VGA_HS ),

    .rotate(osd_rotate),
    .ce_divider(1'b1),
    .blend(blend),
    .scandoubler_disable(scandoublerD),
    .scanlines(scanlines),
    .osd_enable      ( osd_enable )
    );
    


dac #(10) dac(
    .clk_i(clk_sys),
    .res_n_i(1),
    .dac_i(audio),
    .dac_o(AUDIO_L)
    );

wire [4:0] dial;
moonwar_dial moonwar_dial (
    .clk(clk_sys),
    .moveleft(m_left | m_up),
    .moveright(m_right | m_down),
    .dialout(dial)
);

wire [6:0] darkplnt_dial;
spinner spinner (
    .clock(clk_sys),
    .reset(reset),
    .btn_left(m_left | m_up),
    .btn_right(m_right | m_down),
    .strobe(vs),
    .spin_angle(darkplnt_dial)
);

wire [5:0] dp_remap[64] = 
'{
    6'h03, 6'h02, 6'h00, 6'h01, 6'h21, 6'h20, 6'h22, 6'h23,
    6'h33, 6'h32, 6'h30, 6'h31, 6'h11, 6'h10, 6'h12, 6'h13,
    6'h17, 6'h16, 6'h14, 6'h15, 6'h35, 6'h34, 6'h36, 6'h37,
    6'h3f, 6'h3e, 6'h3c, 6'h3d, 6'h1d, 6'h1c, 6'h1e, 6'h1f,
    6'h1b, 6'h1a, 6'h18, 6'h19, 6'h39, 6'h38, 6'h3a, 6'h3b,
    6'h2b, 6'h2a, 6'h28, 6'h29, 6'h09, 6'h08, 6'h0a, 6'h0b,
    6'h0f, 6'h0e, 6'h0c, 6'h0d, 6'h2d, 6'h2c, 6'h2e, 6'h2f,
    6'h27, 6'h26, 6'h24, 6'h25, 6'h05, 6'h04, 6'h06, 6'h07
};

wire [5:0] darkplnt_dial_scrambled = dp_remap[darkplnt_dial[6:1]];
    

//-----------------------


wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, btn_coin, m_coin2, m_coin3, m_coin4, btn_one_player, btn_two_player, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire m_one_player  = ~btn_n_i[1] | btn_one_player;
wire m_two_players = ~btn_n_i[2] | btn_two_player;
wire m_coin1         = ~btn_n_i[3] | btn_coin;

wire kbd_intr;
wire [7:0] kbd_scancode;
wire [7:0] keys_s;

//get scancode from keyboard
io_ps2_keyboard #(.CLK_SPEED(24576)) keyboard 
 (
  .clk       ( clk_sys ),
  .kbd_clk   ( ps2_clk_io ),
  .kbd_dat   ( ps2_data_io ),
  .interrupt ( kbd_intr ),
  .scancode  ( kbd_scancode )
);

wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable,direct_video;
wire [1:0]osd_rotate;

//translate scancode to joystick
kbd_joystick #( .OSD_CMD    ( 3'b011 ), .USE_VKP( 1'b1), .CLK_SPEED(24576)) k_joystick
(
  .clk          ( clk_sys ),
  .kbdint       ( kbd_intr ),
  .kbdscancode  ( kbd_scancode ), 
  .direct_video ( direct_video ),
  .osd_rotate   ( osd_rotate ),

    .joystick_0     ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1     ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
          
    //-- joystick_0 and joystick_1 should be swapped
    .joyswap        ( joyswap ),
        
    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer      ( 0 ),

    //-- tilt, coin4-1, start4-1
    .controls    ( {m_tilt, m_coin4, m_coin3, m_coin2, btn_coin, m_four_players, m_three_players, btn_two_player, btn_one_player} ),
        
    //-- fire12-1, up, down, left, right

    .player1     ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2     ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),
        
    //-- keys to the OSD
    .osd_o         ( keys_s ),
    .osd_enable     ( osd_enable ),
    
    //-- sega joystick
    .sega_strobe    ( joy_p7_o )
        
        
);


endmodule 
/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///============================================================================
//
//  Multicore 2+ Top by Victor Trucco
//
//============================================================================

`default_nettype none

module Segasys1_MC2p(
    // Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAM (IS61WV20488FBLL-10)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM (W9825G6KH-6)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io        = 1'bz,
    inout wire  ps2_data_io       = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o         = 1'bZ,
    output wire sd_sclk_o         = 1'bZ,
    output wire sd_mosi_o         = 1'bZ,
    input wire  sd_miso_i,

    // Joysticks
    output wire joy_clock_o       = 1'b1,
    output wire joy_load_o        = 1'b1,
    input  wire joy_data_i,
    output wire joy_p7_o          = 1'b1,

    // Audio
    output      AUDIO_L,
    output      AUDIO_R,
    input wire  ear_i,
    output wire mic_o             = 1'b0,

    // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

    //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
   
    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output wire   SPI_nWAIT        = 1'b1, // '0' to hold the microcontroller data streaming

    inout [31:0] GPIO,

    output LED                    = 1'b1 // '0' is LED on
);


//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------
assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign stm_rst_o    = 1'bZ;
assign stm_rx_o = 1'bZ;

//no SRAM for this core
assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;

//all the SD reading goes thru the microcontroller for this core
assign sd_cs_n_o = 1'bZ;
assign sd_sclk_o = 1'bZ;
assign sd_mosi_o = 1'bZ;


wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

joystick_serial  joystick_serial 
(
    .clk_i           ( clock_50_i ),
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

// ROM Data Pump
reg [7:0] pump_s = 8'b11111111;
PumpSignal PumpSignal (clk_sys, ~pll_locked, ioctl_downl, pump_s);

//-----------------------------------------------------------------

localparam CONF_STR = {
    "P,CORE_NAME.dat;",
//    "P,Wonder Boy.dat;",
//    "P,Choplifter.dat;",
    "O34,Scanlines,Off,25%,50%,75%;",
    "O5,Blend,Off,On;",
    "T0,Reset;",
    "V,v1.0."
};

assign SDRAM_CKE = 1;
assign AUDIO_R = AUDIO_L;
assign LED = ~ioctl_downl;

wire        rotate    = status[2];
wire  [1:0] scanlines = status[4:3];
wire        blend = status[5];

reg   [7:0] INP0, INP1, INP2;
always @(*) begin
	INP0 = ~{m_left, m_right,m_up, m_down,1'b0,m_fireB,m_fireA,m_fireC};
	INP1 = ~{m_left2,m_right2,m_up2, m_down2,1'b0,m_fire2B,m_fire2A,m_fire2C};
	INP2 = ~{2'b00,m_two_players, m_one_player,3'b000, m_coin1};
	if (core_mod[5]) begin
		// Block Gal
		INP0 = ~spin[8:1];
		INP1 = ~spin[8:1];
		INP2 = ~{m_fire2A | |mouse_flags[2:0], m_fireA | |mouse_flags[2:0], m_two_players, m_one_player, 2'b00, m_coin2, m_coin1};
	end else
	if (core_mod[3]) begin
		//WaterMatch
		INP0 = ~{m_left, m_right, m_up, m_down, m_left2,m_right2,m_up2,m_down2};
		INP1 = ~{m_left3,m_right3,m_up3,m_down3,m_left4,m_right4,m_up4,m_down4};
		INP2 = ~{m_fire3A | m_fire4A,m_fireA | m_fire2A,m_two_players, m_one_player,3'b000, m_coin1};
	end
end

wire signed [8:0] spin;
wire signed [8:0] spin_next = spin + mouse_x;
always @(posedge clk_sys) begin
	if (mouse_strobe) begin
		if (spin[8] != mouse_x[8] || spin[8] == spin_next[8])
			spin <= spin_next;
		else
			spin <= {spin[8], {8{~spin[8]}}};
	end
end

wire  [7:0] DSW0 = status[15: 8];
wire  [7:0] DSW1 = status[23:16];

wire  [6:0] core_mod;  // [0]=SYS1/SYS2,[1]=H/V,[2]=H256/H240,[3]=4controllers,[4]=CW/CCW,[5]=spinner,[6]=SYS2 rowscroll,
wire  [1:0] orientation = { core_mod[4], core_mod[1] };

wire CLOCK_27;
pll_mc_to_27mhz pll_mc_to_27mhz(
    .inclk0(clock_50_i),
    .c0(CLOCK_27)
    );

wire clk_sys, sdram_clk;
wire pll_locked;
pll_mist pll(
    .inclk0(CLOCK_27),
    .c0(sdram_clk),//80
    .c1(clk_sys),//40
    .c2(SDRAM_CLK),
    .locked(pll_locked)
    );
//assign SDRAM_CLK = sdram_clk;


wire [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire  [7:0] joystick_2;
wire  [7:0] joystick_3;
wire        key_pressed;
wire        key_strobe;
wire  [7:0] key_code;
wire        scandoublerD;
wire        ypbpr;
wire        no_csync;

wire signed [8:0] mouse_x;
wire signed [8:0] mouse_y;
wire  [7:0] mouse_flags;
wire 			mouse_strobe;

/*
user_io #(
    .STRLEN(($size(CONF_STR)>>3)))
user_io(
    .clk_sys        (clk_sys        ),
    .conf_str       (CONF_STR       ),
    .SPI_CLK        (SPI_SCK        ),
    .SPI_SS_IO      (CONF_DATA0     ),
    .SPI_MISO       (SPI_DO         ),
    .SPI_MOSI       (SPI_DI         ),
    .buttons        (buttons        ),
    .switches       (switches       ),
    .scandoubler_disable (scandoublerD ),
    .ypbpr          (ypbpr          ),
    .core_mod       (core_mod       ),
    .no_csync       (no_csync       ),
    .key_strobe     (key_strobe     ),
    .key_pressed    (key_pressed    ),
    .key_code       (key_code       ),
    .joystick_0     (joystick_0     ),
    .joystick_1     (joystick_1     ),
    .status         (status         )
    );
*/
wire [15:0] audio;
wire [16:0] cpu_rom_addr;
wire [15:0] cpu_rom_do;
wire [16:0] spr_rom_addr;
wire [15:0] spr_rom_do;
wire [14:0] snd_rom_addr;
wire [15:0] snd_rom_do;
wire [15:0] tile_rom_addr;
wire [23:0] tile_rom_do;
wire        ioctl_downl;
wire  [7:0] ioctl_index;
wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire  [7:0] ioctl_dout;

data_io #(
    .STRLEN(($size(CONF_STR)>>3)))
data_io(
    .clk_sys       ( clk_sys      ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),
    .SPI_DO        ( SPI_DO       ),
    
    .data_in       ( pump_s & keys_s ),
    .conf_str      ( CONF_STR     ),
    .status        ( status       ),
    .core_mod       (core_mod       ),

    .ioctl_download( ioctl_downl  ),
    .ioctl_index   ( ioctl_index  ),
    .ioctl_wr      ( ioctl_wr     ),
    .ioctl_addr    ( ioctl_addr   ),
    .ioctl_dout    ( ioctl_dout   )
);

reg port1_req, port2_req;
wire [24:0] tl_ioctl_addr = ioctl_addr - 20'h40000;
sdram #(80) sdram(
	.*,
	.init_n        ( pll_locked   ),
	.clk           ( sdram_clk    ),

	// port1 used for main + sound CPUs
	.port1_req     ( port1_req    ),
	.port1_ack     ( ),
	.port1_a       ( ioctl_addr[23:1] ),
	.port1_ds      ( {ioctl_addr[0], ~ioctl_addr[0]} ),
	.port1_we      ( ioctl_downl ),
	.port1_d       ( {ioctl_dout, ioctl_dout} ),
	.port1_q       ( ),

	.cpu1_addr     ( ioctl_downl ? 17'h1ffff : (17'h4000 + cpu_rom_addr[16:1]) ), // offset 8000h
	.cpu1_q        ( cpu_rom_do ),
	.cpu2_addr     ( ioctl_downl ? 17'h1ffff : snd_rom_addr[14:1] ), // offset 0
	.cpu2_q        ( snd_rom_do ),
	.cpu3_addr     ( ioctl_downl ? 17'h1ffff : (17'h10000 + spr_rom_addr[16:1]) ), // offset 20000h
	.cpu3_q        ( spr_rom_do ),

	// port2 for backround tiles
	.port2_req     ( port2_req ),
	.port2_ack     ( ),
	.port2_a       ( tl_ioctl_addr[23:1] ),
	.port2_ds      ( {tl_ioctl_addr[0], ~tl_ioctl_addr[0]} ),
	.port2_we      ( ioctl_downl ),
	.port2_d       ( {ioctl_dout, ioctl_dout} ),
	.port2_q       ( ),

	.sp_addr       ( ioctl_downl ? 16'hffff : tile_rom_addr ),
	.sp_q          ( tile_rom_do )
);

always @(posedge clk_sys) begin
	reg        ioctl_wr_last = 0;
	ioctl_wr_last <= ioctl_wr;
	if (ioctl_downl) begin
		if (~ioctl_wr_last && ioctl_wr) begin
			port1_req <= ~port1_req;
			port2_req <= ~port2_req;
		end
	end
end

reg reset = 1;
reg rom_loaded = 0;
always @(posedge sdram_clk) begin
	reg ioctl_downlD;
	ioctl_downlD <= ioctl_downl;

	if (ioctl_downlD & ~ioctl_downl) rom_loaded <= 1;
	reset <= status[0] | ~btn_n_o[4] | ~rom_loaded;
end

reg [24:0] power_on_s   = 16'b1111111111111111;

reg reset_por;

always @(posedge clk_sys) 
begin

        if (reset == 1'b1)
            power_on_s = 25'b1111111111111111111111111;
        else if (power_on_s != 0)
			  begin
					power_on_s = power_on_s - 1'b1;
					reset_por = 1;
			  end 
				else
					reset_por = 0;
end 


SEGASYSTEM1 System1_Top(
	.clk40M(clk_sys),
	.reset(reset_por),

	.INP0(INP0),
	.INP1(INP1),
	.INP2(INP2),

	.DSW0(DSW0),
	.DSW1(DSW1),

	.SYSTEM2(core_mod[0]),
	.SYSTEM2_ROWSCROLL(core_mod[6]),

	.PH(HPOS),
	.PV(VPOS),
	.PCLK_EN(PCLK_EN),
	.POUT(POUT),

	.cpu_rom_addr(cpu_rom_addr),
	.cpu_rom_do(cpu_rom_addr[0] ? cpu_rom_do[15:8] : cpu_rom_do[7:0] ),

	.snd_rom_addr(snd_rom_addr),
	.snd_rom_do(snd_rom_addr[0] ? snd_rom_do[15:8] : snd_rom_do[7:0] ),

	.spr_rom_addr(spr_rom_addr),
	.spr_rom_do(spr_rom_addr[0] ? spr_rom_do[15:8] : spr_rom_do[7:0] ),

	.tile_rom_addr(tile_rom_addr),
	.tile_rom_do(tile_rom_do),

	.ROMCL(clk_sys),
	.ROMAD(ioctl_addr),
	.ROMDT(ioctl_dout),
	.ROMEN(ioctl_wr),
	.SOUT(audio)
);

wire        PCLK_EN;
wire  [8:0] HPOS,VPOS;
wire [11:0] POUT;
wire  [7:0] HOFFS = 8'd2;
wire  [7:0] VOFFS = 8'd2;
wire        hs, vs;
wire  [3:0] b, g, r;

HVGEN hvgen
(
	.HPOS(HPOS),.VPOS(VPOS),.CLK(clk_sys),.PCLK_EN(PCLK_EN),.iRGB(POUT),
	.oRGB({b,g,r}),.HBLK(),.VBLK(),.HSYN(hs),.VSYN(vs),
	.H240(core_mod[2]),.HOFFS(HOFFS),.VOFFS(VOFFS)
);

mist_video #(.COLOR_DEPTH(4), .SD_HCNT_WIDTH(10)) mist_video(
    .clk_sys        ( clk_sys          ),
    .SPI_SCK        ( SPI_SCK          ),
    .SPI_SS3        ( SPI_SS2          ),
    .SPI_DI         ( SPI_DI           ),
    .R              ( r                ),
    .G              ( g                ),
    .B              ( b                ),
    .HSync          ( hs               ),
    .VSync          ( vs               ),
    .VGA_R          ( VGA_R            ),
    .VGA_G          ( VGA_G            ),
    .VGA_B          ( VGA_B            ),
    .VGA_VS         ( VGA_VS           ),
    .VGA_HS         ( VGA_HS           ),
    .ce_divider     ( 1'b0             ),
    .blend          ( blend            ),
    .rotate         ( {1'b0, rotate}   ),
    .scandoubler_disable(scandoublerD  ),
    .scanlines      ( scanlines        ),
    .osd_enable     ( osd_enable )
);

dac #(
    .C_bits(16)) 
dac(
    .clk_i(clk_sys),
    .res_n_i(1),
    .dac_i(audio),
    .dac_o(AUDIO_L)
);

//------------------------------------------

wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;
wire m_fire3A, m_fire4A;

wire btn_one_player  = ~btn_n_o[1] | m_one_player;
wire btn_two_players = ~btn_n_o[2] | m_two_players;
wire btn_coin        = ~btn_n_o[3] | m_coin1;
wire [7:0] keys_s;
wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable;
wire direct_video;
wire [1:0]osd_rotate;
wire [4:1]btn_n_o;

MC2_HID #(.CLK_SPEED(50000), .use_usb_g ( 1'b1 )) k_hid
(
    .clk_i          ( clock_50_i ),
    .kbd_clk_io     ( ps2_clk_io ),
    .kbd_dat_io     ( ps2_data_io ),
    .usb_rx_i       ( ps2_mouse_clk_io ),

    .joystick_0_i   ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1_i   ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
      
    //-- joystick_0 and joystick_1 should be swapped
    .joyswap_i      ( 0 ),

    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer_i    ( 0 ),

    //-- tilt, coin4-1, start4-1
    .controls_o     ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

    //-- fire12-1, up, down, left, right

    .player1_o      ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2_o      ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),

    .direct_video_o ( direct_video ),
    .osd_rotate_o   ( osd_rotate ),

    //-- keys to the OSD
    .osd_o          ( keys_s ),
    .osd_enable_i   ( osd_enable ),
    
    //-- sega joystick
    .sega_strobe_o  ( joy_p7_o ),

     //-- Front buttons
     .front_buttons_i ( btn_n_i ),
     .front_buttons_o ( btn_n_o )        
);

endmodule 

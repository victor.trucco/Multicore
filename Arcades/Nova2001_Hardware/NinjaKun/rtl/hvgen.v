/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/module hvgen
(
	input         CLK,
	input         PCLK_EN,
	output  [8:0]		HPOS,
	output  [8:0]		VPOS,
	input	 [11:0]		iRGB,
	output reg [11:0]	oRGB,
	output reg			HBLK = 1,
	output reg			VBLK = 1,
	output reg			HSYN = 1,
	output reg			VSYN = 1
);

reg [8:0] hcnt = 0;
reg [8:0] vcnt = 0;

assign HPOS = hcnt-9'd16;
assign VPOS = vcnt-9'd16;

always @(posedge CLK) begin
	if (PCLK_EN) begin
		case (hcnt)
			 15: begin HBLK <= 0; hcnt <= hcnt+1'd1; end
			272: begin HBLK <= 1; hcnt <= hcnt+1'd1; end
			311: begin HSYN <= 0; hcnt <= hcnt+1'd1; end
			342: begin HSYN <= 1; hcnt <= 471;    end
			511: begin hcnt <= 0;
				case (vcnt)
					 15: begin VBLK <= 0; vcnt <= vcnt+1'd1; end
					207: begin VBLK <= 1; vcnt <= vcnt+1'd1; end
					226: begin VSYN <= 0; vcnt <= vcnt+1'd1; end
					233: begin VSYN <= 1; vcnt <= 483;	  end
					511: begin vcnt <= 0; end
					default: vcnt <= vcnt+1'd1;
				endcase
			end
			default: hcnt <= hcnt+1'd1;
		endcase
		oRGB <= (HBLK|VBLK) ? 12'h0 : iRGB;
	end
end

endmodule 

//============================================================================
//
//  Multicore 2+ / Neptuno Top by Victor Trucco
//
//============================================================================

`default_nettype none


module bagman_mc2p(
     // Clocks
    input wire  clock_50_i,

    `ifdef NEPTUNO
    // SRAM (IS61WV102416BLL-10TLI)
    output [19:0]sram_addr_o = 20'b00000000000000000000,
    inout  [15:0]sram_data_io = 8'hZZZZ,
    output sram_we_n_o = 1'b1,
    output sram_oe_n_o = 1'b1,
    output sram_ub_n_o = 1'b1,
    output sram_lb_n_o = 1'b1,
    `else
    // SRAM (IS61WV20488FBLL-10)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1, 

    //external buttons
    input wire [4:1]    btn_n_i,
    `endif
        
    // SDRAM (W9825G6KH-6)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io        = 1'bz,
    inout wire  ps2_data_io       = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o         = 1'bZ,
    output wire sd_sclk_o         = 1'bZ,
    output wire sd_mosi_o         = 1'bZ,
    input wire  sd_miso_i,

    // Joysticks
    output wire joy_clock_o       = 1'b1,
    output wire joy_load_o        = 1'b1,
    input  wire joy_data_i,
    output wire joy_p7_o          = 1'b1,

    // Audio
    output      AUDIO_L,
    output      AUDIO_R,
    input wire  ear_i,
    output wire mic_o             = 1'b0,

    // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

    //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
   
    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output wire   SPI_nWAIT         = 1'b1, // '0' to hold the microcontroller data streaming

    inout [31:0] GPIO,

    output LED                      = 1'b1 // '0' is LED on
);

`include "..\..\rtl\build_id.v" 

localparam CONF_STR = {
    
//       "P,squash.dat;",
    "P,CORE_NAME.dat;",
    "O12,Screen Rotate,0,90,180,270;",
    "O34,Scanlines,Off,25%,50%,75%;",
    "O5,Blend,Off,On;",
    "O7,Scandoubler,On,Off;",
    "T0,Reset;",
    "V,v1.25.",`BUILD_DATE
};


//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------
assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign stm_rst_o = 1'bZ;
assign stm_rx_o  = 1'bZ;

//no SRAM for this core
assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;

//all the SD reading goes thru the microcontroller for this core
assign sd_cs_n_o = 1'bZ;
assign sd_sclk_o = 1'bZ;
assign sd_mosi_o = 1'bZ;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

`ifdef NEPTUNO
    wire [4:1] btn_n_i = 4'b1111;

    neptuno_joydecoder  neptuno_joydecoder (
`else 
    joystick_serial  joystick_serial (
`endif 

    .clk_i           ( clock_50_i ),
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

// ROM Data Pump
reg [7:0] pump_s = 8'b11111111;
PumpSignal PumpSignal (clock_48, ~pll_locked, ioctl_downl, pump_s);

//-----------------------------------------------------------------

wire        rotate = 1'b0; //status[2];
wire  [1:0] scanlines = status[4:3];
wire        blend     = status[5];

wire        pickin = core_mod[1];
wire        sbagman = !pickin & core_mod[0]; // 1
wire        squash = pickin & core_mod[0]; // 3

wire  [7:0] dipsw = status[15:8];
wire  [7:0] player1 = ~{ m_fireA, squash ? dial1[1] : m_down, squash ? dial1[0] : m_up, m_right, m_left, (sbagman & m_fireB) | m_one_player, m_coin2, m_coin1 };
wire  [7:0] player2 = ~{ m_fire2A, squash ? dial2[1] : m_down2, squash ? dial2[0] : m_up2, m_right2, m_left2, (sbagman & m_fire2B) | m_two_players, m_coin4, m_coin3 };

assign      LED = ~ioctl_downl;
assign      AUDIO_R = AUDIO_L;
//assign      SDRAM_CLK = clock_48;
assign      SDRAM_CKE = 1;

//assign core_mod = 7'd3;

wire clock_48, clock_12, pll_locked;
pll pll(
    .inclk0(clock_50_i),
    .c0(clock_48),
    .c1(clock_12),
    .c2(SDRAM_CLK),
    .locked(pll_locked)
    );

wire clk_25m2,clk_40;    
pll_vga pll_vga(
    .inclk0(clock_50_i),
    .c0(clk_25m2),
    .c1(clk_40)
);

wire [31:0] status;
wire  [6:0] core_mod;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire        scandoublerD;
wire        ypbpr;
wire        no_csync;
wire [12:0] audio;
wire        hs, vs;
wire        hb, vb;
wire        blankn = ~(hb | vb);
wire [2:0]  r, g;
wire [1:0]  b;

wire [15:0] rom_addr;
wire [15:0] rom_do;
wire        rom_rd;
wire        ioctl_downl;
wire  [7:0] ioctl_index;
wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire  [7:0] ioctl_dout;
wire        key_strobe;
wire        key_pressed;
wire  [7:0] key_code;

data_io #(.STRLEN(($size(CONF_STR)>>3)))
data_io(
    .clk_sys       ( clock_48     ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),
    .SPI_DO        ( SPI_DO       ),
    
    .data_in       ( pump_s & keys_s ),
    .conf_str      ( CONF_STR     ),
    .status        ( status       ),
    .core_mod      ( core_mod     ),
    
    .ioctl_download( ioctl_downl  ),
    .ioctl_index   ( ioctl_index  ),
    .ioctl_wr      ( ioctl_wr     ),
    .ioctl_addr    ( ioctl_addr   ),
    .ioctl_dout    ( ioctl_dout   )
);

reg port1_req;

sdram #(.MHZ(48)) sdram(
    .*,
    .init_n        ( pll_locked   ),
    .clk           ( clock_48     ),

    // ROM upload
    .port1_req     ( port1_req    ),
    .port1_ack     ( ),
    .port1_a       ( ioctl_addr[23:1] ),
    .port1_ds      ( { ioctl_addr[0], ~ioctl_addr[0] } ),
    .port1_we      ( ioctl_downl ),
    .port1_d       ( { ioctl_dout, ioctl_dout } ),
    .port1_q       ( ),

    // CPU
    .cpu1_addr     ( ioctl_downl ? 17'h1ffff : { 2'b00, rom_addr[15:1] } ),
    .cpu1_q        ( rom_do )
);

always @(posedge clock_48) begin
    reg        ioctl_wr_last = 0;

    ioctl_wr_last <= ioctl_wr;
    if (ioctl_downl) begin
        if (~ioctl_wr_last && ioctl_wr) port1_req <= ~port1_req;
    end
end

reg reset = 1;
reg rom_loaded = 0;
always @(posedge clock_48) begin
    reg ioctl_downlD;
    ioctl_downlD <= ioctl_downl;

    if (ioctl_downlD & ~ioctl_downl) rom_loaded <= 1;
    reset <= status[0] | ~btn_n_o[4] | ~rom_loaded;
end

bagman bagman(
    .clock_12(clock_12),
    .reset(reset),
    .pickin(pickin),
    .video_r(r),
    .video_g(g),
    .video_b(b),
    .video_hblank(hb),
    .video_vblank(vb),
    .video_hs(hs),
    .video_vs(vs),
    .clk_pix(clk_pix),
    .audio_out(audio),
    .player1(player1),
    .player2(player2),
    .dipsw(dipsw),
    .roms_addr ( rom_addr ),
    .roms_do   ( rom_addr[0] ? rom_do[15:8] : rom_do[7:0] ),
    .dl_clk(clock_48),
    .dl_addr(ioctl_addr[15:0]),
    .dl_we(ioctl_wr),
    .dl_data(ioctl_dout),
    .core_mod      ( core_mod     ),
    );

wire direct_video_s = ~status[7] ^ direct_video;

wire [8:0] vga_col_s;
wire vga_hs_s, vga_vs_s, clk_pix;

framebuffer #(256,224,9) framebuffer
(
        .clk_sys    ( clock_48 ),
        .clk_i      ( clk_pix ),
        .RGB_i      ((blankn) ? {r, g, b,b[0] } : 9'b000000000 ),
        .hblank_i   ( hb ),
        .vblank_i   ( vb ),
        
        .rotate_i   ( status[2:1] ), 

        .clk_vga_i  ( (status[1]) ? clk_40 : clk_25m2 ), //800x600 or 640x480
        .RGB_o      ( vga_col_s ),
        .hsync_o    ( vga_hs_s ),
        .vsync_o    ( vga_vs_s ),
        .blank_o    (  ),

        .odd_line_o (  )
);

mist_video #(.COLOR_DEPTH(3), .SD_HCNT_WIDTH(10), .USE_FRAMEBUFFER(1)) mist_video(

    .clk_sys( direct_video_s ? clock_48 :(status[1]) ? clk_40 : clk_25m2),

    .SPI_SCK        ( SPI_SCK          ),
    .SPI_SS3        ( SPI_SS2          ),
    .SPI_DI         ( SPI_DI           ),

    .R       ( (direct_video_s) ? (blankn) ? r          : 3'b000 : vga_col_s[8:6] ),
    .G       ( (direct_video_s) ? (blankn) ? g          : 3'b000 : vga_col_s[5:3] ),
    .B       ( (direct_video_s) ? (blankn) ? {b, b[0]}  : 3'b000 : vga_col_s[2:0] ),
    .HSync   ( (direct_video_s) ? hs : vga_hs_s ),
    .VSync   ( (direct_video_s) ? vs : vga_vs_s ),

/*
    .clk_sys        ( clock_48         ),
    .R              ( blankn ? r : 0   ),
    .G              ( blankn ? g : 0   ),
    .B              ( blankn ? {b,b[0]} : 0   ),
    .HSync          ( hs               ),
    .VSync          ( vs               ),
*/
    .VGA_R          ( VGA_R            ),
    .VGA_G          ( VGA_G            ),
    .VGA_B          ( VGA_B            ),
    .VGA_VS         ( VGA_VS           ),
    .VGA_HS         ( VGA_HS           ),

    .rotate         ( osd_rotate ),
    .scandoubler_disable( direct_video_s ),
    .scanlines      ( scanlines        ),
    .blend          ( blend            ),
    .osd_enable     ( osd_enable       ),
    .no_csync       ( ~direct_video_s  )
    );


dac #(.C_bits(16))dac(
    .clk_i(clock_12),
    .res_n_i(1),
    .dac_i({audio,4'b0}),
    .dac_o(AUDIO_L)
    );

// Squash spinners
reg [1:0] dial1, dial2;
reg [4:0] dial_cnt;

always @(posedge clock_12) begin
    dial_cnt <= dial_cnt + 1'd1;
    if (dial_cnt == 0) begin
        if (dial1 != 2'b11) dial1 <= 2'b11;
        else if (m_up)      dial1 <= 2'b10;
        else if (m_down)    dial1 <= 2'b01;
        else                dial1 <= 2'b11;

        if (dial2 != 2'b11) dial2 <= 2'b11;
        else if (m_down2)   dial2 <= 2'b10;
        else if (m_up2)     dial2 <= 2'b01;
        else                dial2 <= 2'b11;
    end
end


wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player  = ~btn_n_o[1] | m_one_player;
wire btn_two_players = ~btn_n_o[2] | m_two_players;
wire btn_coin        = ~btn_n_o[3] | m_coin1;
wire [7:0] keys_s;
wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable;
wire direct_video;
wire [1:0]osd_rotate;
wire [4:1]btn_n_o;

//translate scancode to joystick
MC2_HID #( .OSD_CMD( 3'b011 ), .USE_VKP( 1'b1), .CLK_SPEED(12000)) k_hid
(
    .clk          ( clock_12 ),
    .kbd_clk      ( ps2_clk_io ),
    .kbd_dat      ( ps2_data_io ),
    .joystick_0   ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1   ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
      
    //-- joystick_0 and joystick_1 should be swapped
    .joyswap      ( 0 ),

    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer    ( !squash ),

    //-- tilt, coin4-1, start4-1
    .controls     ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

    //-- fire12-1, up, down, left, right

    .player1      ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2      ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),

    .direct_video ( direct_video ),
    .osd_rotate   ( osd_rotate ),

    //-- keys to the OSD
    .osd_o        ( keys_s ),
    .osd_enable   ( osd_enable ),
    
    //-- sega joystick
    .sega_strobe  ( joy_p7_o ),

     //-- Front buttons
     .front_buttons_i (btn_n_i),
     .front_buttons_o (btn_n_o)        
);

endmodule

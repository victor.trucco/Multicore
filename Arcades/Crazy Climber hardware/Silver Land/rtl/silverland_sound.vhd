--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
---------------------------------------------------------------------------------
-- Silver Land sound AY-3-8910 - Dar - June 2018
---------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all,ieee.numeric_std.all;

entity silverland_sound is
port	(
	cpu_clock    : in std_logic;
	cpu_addr     : in std_logic_vector(15 downto 0);
	cpu_data     : in std_logic_vector( 7 downto 0);
	cpu_iorq_n   : in std_logic;
	reg4_we_n    : in std_logic;
	reg5_we_n    : in std_logic;
	reg6_we_n    : in std_logic;
	ym_2149_data : out std_logic_vector(7 downto 0);
	sound_sample : out std_logic_vector(7 downto 0)
);
end silverland_sound;

architecture struct of silverland_sound is

signal hdiv         : std_logic_vector(1 downto 0);
signal clock_1_5mhz : std_logic; -- 1.50Mhz

signal ym_2149_audio : std_logic_vector(7 downto 0);


begin

clock_1_5mhz <= hdiv(0);

process(cpu_clock)
begin
	if falling_edge(cpu_clock) then

		if hdiv = "11" then
			hdiv <= "00";
		else
			hdiv <= std_logic_vector(unsigned(hdiv) + 1);
		end if;

	end if;
end process;

ym2149 : entity work.ym2149
port map (
-- data bus
	I_DA            => cpu_data,     --: in  std_logic_vector(7 downto 0);
	O_DA            => ym_2149_data, --: out std_logic_vector(7 downto 0);
	O_DA_OE_L       => open,         --: out std_logic;
-- control
	I_A9_L          => '0', --scs_n,	--: in  std_logic;
	I_A8            =>     cpu_iorq_n or cpu_addr(3),  --: in  std_logic;
	I_BDIR          => not(cpu_iorq_n or cpu_addr(2)), --: in  std_logic;
	I_BC2           => not(cpu_iorq_n or cpu_addr(1)), --: in  std_logic;
	I_BC1           => not(cpu_iorq_n or cpu_addr(0)), --: in  std_logic;
	I_SEL_L         => '1',                            --: in  std_logic;
-- audio
	O_AUDIO         => sound_sample, --: out std_logic_vector(7 downto 0);
-- port a
	I_IOA           => "11111111",    --: in  std_logic_vector(7 downto 0);
	O_IOA           => open,          --: out std_logic_vector(7 downto 0);
	O_IOA_OE_L      => open,          --: out std_logic;
-- port b
	I_IOB           => "11111111",    --: in  std_logic_vector(7 downto 0);
	O_IOB           => open,          --: out std_logic_vector(7 downto 0);
	O_IOB_OE_L      => open,          --: out std_logic;

	ENA             => '1',           --: in  std_logic; -- clock enable for higher speed operation
	RESET_L         => '1',           --: in  std_logic;
	CLK             => clock_1_5mhz   --: in  std_logic  -- note 6 Mhz!
);

end architecture;
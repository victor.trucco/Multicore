--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-------------------------------------------------------------------------------
--
-- $Id: ladybug_dip_pack-p.vhd,v 1.4 2005/10/10 20:52:04 arnim Exp $
--
-- Copyright (c) 2005, Arnim Laeuger (arnim.laeuger@gmx.net)
--
-- All rights reserved
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

package ladybug_dip_pack is

  -----------------------------------------------------------------------------
  -- DIP switch settings for Lady Bug
  -----------------------------------------------------------------------------
  constant lb_dip_block_1_c : std_logic_vector(7 downto 0) :=
    -- Lives ------------------------------------------------------------------
    -- 0 = 5 Lives
    -- 1 = 3 Lives
    '0' &
    -- Free Play --------------------------------------------------------------
    -- 0 = Free Play
    -- 1 = No Free Play
    '1' &
    -- Cabinet ----------------------------------------------------------------
    -- 0 = Upright
    -- 1 = Cocktail
    '0' &
    -- Screen Freeze ----------------------------------------------------------
    -- 0 = Freeze
    -- 1 = No Freeze
    '1' &
    -- Rack Test (Cheat) ------------------------------------------------------
    -- 0 = On
    -- 1 = Off
    '1' &
    -- High Score Initials ----------------------------------------------------
    -- 0 = 3-Letter Initials
    -- 1 = 10-Letter Initials
    '1' &
    -- Difficulty -------------------------------------------------------------
    -- 11 = Easy
    -- 10 = Medium
    -- 01 = Hard
    -- 00 = Hardest
    "10";

  -----------------------------------------------------------------------------
  -- DIP switch settings for Dorodon
  -----------------------------------------------------------------------------
  constant do_dip_block_1_c : std_logic_vector(7 downto 0) :=
    -- Lives ------------------------------------------------------------------
    -- 0 = 5 Lives
    -- 1 = 3 Lives
    '0' &
    -- Free Play --------------------------------------------------------------
    -- 0 = Free Play
    -- 1 = No Free Play
    '1' &
    -- Cabinet ----------------------------------------------------------------
    -- 0 = Upright
    -- 1 = Cocktail
    '0' &
    -- Screen Freeze ----------------------------------------------------------
    -- 0 = Freeze
    -- 1 = No Freeze
    '1' &
    -- Rack Test (Cheat) ------------------------------------------------------
    -- 0 = On
    -- 1 = Off
    '1' &
    -- Bonus Life -------------------------------------------------------------
    -- 0 = 40000
    -- 1 = 20000
    '1' &
    -- Difficulty -------------------------------------------------------------
    -- 11 = Easy
    -- 10 = Medium
    -- 01 = Hard
    -- 00 = Hardest
    "10";


  -----------------------------------------------------------------------------
  -- DIP switch settings for Cosmic Avenger
  -----------------------------------------------------------------------------
  constant ca_dip_block_1_c : std_logic_vector(7 downto 0) :=
    -- Lives per Game ---------------------------------------------------------
    -- 00 = 2 Lives
    -- 11 = 3 Lives
    -- 10 = 4 Lives
    -- 01 = 5 Lives
    "01" &
    -- Initial High Score -----------------------------------------------------
    -- 00 = 0
    -- 11 = 5000
    -- 10 = 8000
    -- 01 = 10000
    "11" &
    -- Cabinet ----------------------------------------------------------------
    -- 0 = Upright
    -- 1 = Cocktail
    '0' &
    -- High Score Names -------------------------------------------------------
    -- 0 = 3 Letters
    -- 1 = 10 Letters
    '1' &
    -- Difficulty -------------------------------------------------------------
    -- 11 = Easy
    -- 10 = Medium
    -- 01 = Hard
    -- 00 = Hardest
    "10";

  constant price_dip_block_2_c : std_logic_vector(7 downto 0) :=
    -- Pricing Options --------------------------------------------------------
    -- 1111 = 1 coin  1 credit
    -- 1110 = 1 coin  2 credits
    -- 1101 = 1 coin  3 credits
    -- 1100 = 1 coin  4 credits
    -- 1011 = 1 coin  5 credits
    -- 1010 = 2 coins 1 credit
    -- 1001 = 2 coins 3 credits
    -- 1000 = 3 coins 1 credit
    -- 0111 = 3 coins 2 credit
    -- 0110 = 4 coins 1 credit
    -- 0101 = 1 coin  1 credit
    -- 0100 = 1 coin  1 credit
    -- 0011 = 1 coin  1 credit
    -- 0010 = 1 coin  1 credit
    -- 0001 = 1 coin  1 credit
    -- 0000 = 1 coin  1 credit
    --
    -- Left Chute
    "1111" &
    -- Right Chute
    "1111";

end ladybug_dip_pack;

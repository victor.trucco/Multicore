/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///============================================================================
//  Arcade: TraverseUSA
//
//  DarFPGA's core ported to Multicore 2 by Victor Trucco
//
//  This program is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation; either version 2 of the License, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//============================================================================

`default_nettype none

module TraverseUSA_MC2(
// Clocks
	input wire	clock_50_i,

	// Buttons
	input wire [4:1]	btn_n_i,

	// SRAMs (AS7C34096)
	output wire	[18:0]sram_addr_o  = 18'b0000000000000000000,
	inout wire	[7:0]sram_data_io	= 8'bzzzzzzzz,
	output wire	sram_we_n_o		= 1'b1,
	output wire	sram_oe_n_o		= 1'b1,
		
	// SDRAM	(H57V256)
	output wire	[12:0]sdram_ad_o,
	inout wire	[15:0]sdram_da_io,
	output wire	[1:0]sdram_ba_o,
	output wire	[1:0]sdram_dqm_o,
	output wire	sdram_ras_o,
	output wire	sdram_cas_o,
	output wire	sdram_cke_o,
	output wire	sdram_clk_o,
	output wire	sdram_cs_o,
	output wire	sdram_we_o,

	// PS2
	inout wire	ps2_clk_io			= 1'bz,
	inout wire	ps2_data_io			= 1'bz,
	inout wire	ps2_mouse_clk_io  = 1'bz,
	inout wire	ps2_mouse_data_io = 1'bz,

	// SD Card
	output wire	sd_cs_n_o			= 1'b1,
	output wire	sd_sclk_o			= 1'b0,
	output wire	sd_mosi_o			= 1'b0,
	input wire	sd_miso_i,

	// Joysticks
	input wire	joy1_up_i,
	input wire	joy1_down_i,
	input wire	joy1_left_i,
	input wire	joy1_right_i,
	input wire	joy1_p6_i,
	input wire	joy1_p9_i,
	input wire	joy2_up_i,
	input wire	joy2_down_i,
	input wire	joy2_left_i,
	input wire	joy2_right_i,
	input wire	joy2_p6_i,
	input wire	joy2_p9_i,
	output wire	joyX_p7_o			= 1'b1,

	// Audio
	output wire	dac_l_o				= 1'b0,
	output wire	dac_r_o				= 1'b0,
	input wire	ear_i,
	output wire	mic_o					= 1'b0,

		// VGA
	output wire	[4:0]vga_r_o,
	output wire	[4:0]vga_g_o,
	output wire	[4:0]vga_b_o,
	output wire	vga_hsync_n_o,
	output wire	vga_vsync_n_o,

		// HDMI
	output wire	[7:0]tmds_o			= 8'b00000000,

		//STM32
	input wire	stm_tx_i,
	output wire	stm_rx_o,
	output wire	stm_rst_o			= 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
		
	inout wire	stm_b8_io, // CONF DATA0
	inout wire	stm_b9_io, //SPI SS2
	inout wire	stm_b12_io, // SPI SS3
	inout wire	stm_b13_io, // SPI SCK
	inout wire	stm_b14_io, // SPI DO
	inout wire	stm_b15_io  // SPI DI
	

);


localparam CONF_STR = {	"P,traverse.dat"};
localparam STRLEN = 14;

assign stm_rst_o = 1'bz;
assign dac_r_o = dac_l_o;
assign sdram_clk_o = clk_sys;

wire clk_sys, clk_aud;
wire pll_locked;
pll pll1(
	.inclk0( clock_50_i ),
	.areset(0),
	.c0(clk_sys),
	.c1(clk_aud),
	.locked(pll_locked)
	);

wire [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire        scandoublerD;
wire        ypbpr;
wire [10:0] ps2_key;
wire [10:0] audio;
wire        hs, vs;
wire        blankn;
wire  [2:0] g,b;
wire  [1:0] r;

wire [14:0] cart_addr;
wire [15:0] sdram_do;
wire        cart_rd;
wire        ioctl_downl;
wire  [7:0] ioctl_index;
wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire  [7:0] ioctl_dout;

data_io data_io (
	.clk_sys       ( clk_sys      ),
	.SPI_SCK       ( stm_b13_io      ),
	.SPI_SS2       ( stm_b9_io      ),
	.SPI_DI        ( stm_b15_io       ),
	.ioctl_download( ioctl_downl  ),
	.ioctl_index   ( ioctl_index  ),
	.ioctl_wr      ( ioctl_wr     ),
	.ioctl_addr    ( ioctl_addr   ),
	.ioctl_dout    ( ioctl_dout   )
);
		
sdram cart
(
    .SDRAM_DQ       ( sdram_da_io    ),
    .SDRAM_A        ( sdram_ad_o     ),
    .SDRAM_DQML     ( sdram_dqm_o[0] ),
    .SDRAM_DQMH     ( sdram_dqm_o[1] ),
    .SDRAM_nWE      ( sdram_we_o     ),
    .SDRAM_nCAS     ( sdram_cas_o    ),
    .SDRAM_nRAS     ( sdram_ras_o    ),
    .SDRAM_nCS      ( sdram_cs_o     ),
    .SDRAM_BA       ( sdram_ba_o     ),
    .SDRAM_CKE      ( sdram_cke_o    ),
	 
	.init          ( ~pll_locked  ),
	.clk           ( clk_sys      ),
	.wtbt          ( 2'b00        ),
	.dout          ( sdram_do     ),
	.din           ( {ioctl_dout, ioctl_dout} ),
	.addr          ( ioctl_downl ? ioctl_addr : cart_addr ),
	.we            ( ioctl_downl & ioctl_wr ),
	.rd            ( !ioctl_downl & cart_rd ),
	.ready()
);

reg reset = 1;
reg rom_loaded = 0;
always @(posedge clk_sys) begin
	reg ioctl_downlD;
	ioctl_downlD <= ioctl_downl;

	if (ioctl_downlD & ~ioctl_downl) rom_loaded <= 1;
//	reset <= status[0] | status[1] | buttons[1] | ~rom_loaded;
	reset <= btn_n_i[4] | ~rom_loaded;
end

//Coinage_B(7-4) / Cont. play(3) / Fuel consumption(2) / Fuel lost when collision (1-0)
wire [7:0] dip1 = 8'hff;
//Diag(7) / Demo(6) / Zippy(5) / Freeze (4) / M-Km(3) / Coin mode (2) / Cocktail(1) / Flip(0)
wire [7:0] dip2 = { ~status[9], ~status[8], ~status[7], ~status[6], ~status[5], 3'b110 };

// Traverse_usa
traverse_usa traverse_usa (
	.clock_36     ( clk_sys         ),
	.clock_0p895  ( clk_aud         ),
	.reset        ( reset 				),

	.video_r      ( r               ),
	.video_g      ( g               ),
	.video_b      ( b               ),
    .video_hs     ( hs              ),
	.video_vs     ( vs              ),
	.video_blankn ( blankn          ),

	.audio_out    ( audio           ),
	
	.cpu_rom_addr ( cart_addr       ),
	.cpu_rom_do   ( sdram_do[7:0]   ),
	.cpu_rom_rd   ( cart_rd         ),
	
	.dip_switch_1 ( dip1            ),  
	.dip_switch_2 ( dip2            ),

	.start2       ( btn_two_players ),
	.start1       ( btn_one_player  ),
	.coin1        ( btn_coin        ),

	.right1       ( m_right         ),
	.left1        ( m_left          ),
	.brake1       ( m_down          ),
	.accel1       ( m_up            ),

	.right2       ( m_right         ),
	.left2        ( m_left          ),
	.brake2       ( m_down          ),
	.accel2       ( m_up            )
);

video #(.COLOR_DEPTH(3), .SD_HCNT_WIDTH(10)) video(
	.clk_sys        ( clk_sys          ),
	.R              ( blankn ? {r, r[1] } : 0 ),
	.G              ( blankn ? g : 0   ),
	.B              ( blankn ? b : 0   ),
	.HSync          ( hs               ),
	.VSync          ( vs               ),
	.VGA_R          ( vga_r_o            ),
	.VGA_G          ( vga_g_o            ),
	.VGA_B          ( vga_b_o            ),
	.VGA_VS         ( vga_vsync_n_o           ),
	.VGA_HS         ( vga_hsync_n_o           ),
	.scandoubler_disable( scandoublerD ),
	.scanlines      ( status[4:3]      )
	);

user_io #(
	.STRLEN(($size(CONF_STR)>>3)))
user_io(
	.clk_sys        (clk_sys        ),
	.conf_str       (CONF_STR       ),
	.SPI_CLK        (stm_b13_io        ),
	.SPI_SS_IO      (stm_b8_io     ),
	.SPI_MISO       (stm_b14_io         ),
	.SPI_MOSI       (stm_b15_io         ),
	.buttons        (buttons        ),
	.switches       (switches       ),
	.scandoubler_disable (scandoublerD	  ),
	.ypbpr          (ypbpr          ),
	.key_strobe     (key_strobe     ),
	.key_pressed    (key_pressed    ),
	.key_code       (key_code       ),
	.joystick_0     (joystick_0     ),
	.joystick_1     (joystick_1     ),
	.status         (status         )
	);

dac #(
	.C_bits(11))
dac(
	.clk_i(clk_aud),
	.res_n_i(1),
	.dac_i(audio),
	.dac_o(dac_l_o)
	);

//											Rotated														Normal
wire m_up     = ~status[2] ? btn_left | joystick_0[1] | joystick_1[1] : btn_up | joystick_0[3] | joystick_1[3];
wire m_down   = ~status[2] ? btn_right | joystick_0[0] | joystick_1[0] : btn_down | joystick_0[2] | joystick_1[2];
wire m_left   = ~status[2] ? btn_down | joystick_0[2] | joystick_1[2] : btn_left | joystick_0[1] | joystick_1[1];
wire m_right  = ~status[2] ? btn_up | joystick_0[3] | joystick_1[3] : btn_right | joystick_0[0] | joystick_1[0];
wire m_fire   = btn_fire1 | joystick_0[4] | joystick_1[4];
wire m_bomb   = btn_fire2 | joystick_0[5] | joystick_1[5];

reg btn_one_player = 0;
reg btn_two_players = 0;
reg btn_left = 0;
reg btn_right = 0;
reg btn_down = 0;
reg btn_up = 0;
reg btn_fire1 = 0;
reg btn_fire2 = 0;
reg btn_fire3 = 0;
reg btn_coin  = 0;
wire       key_pressed;
wire [7:0] key_code;
wire       key_strobe;

always @(posedge clk_sys) begin
	if(key_strobe) begin
		case(key_code)
			'h75: btn_up          <= key_pressed; // up
			'h72: btn_down        <= key_pressed; // down
			'h6B: btn_left        <= key_pressed; // left
			'h74: btn_right       <= key_pressed; // right
			'h76: btn_coin        <= key_pressed; // ESC
			'h05: btn_one_player  <= key_pressed; // F1
			'h06: btn_two_players <= key_pressed; // F2
			'h14: btn_fire3       <= key_pressed; // ctrl
			'h11: btn_fire2       <= key_pressed; // alt
			'h29: btn_fire1       <= key_pressed; // Space
		endcase
	end
end

wire  [18:0] sram_addr_s;
wire  [ 7:0] sram_data_s;
wire  sram_we_n_s;
wire  pump_active_s;
wire  [15:0] power_on_s;
wire  [ 7:0] osd_s;
		

	osd 	#(			.STRLEN 			( 14 )		)
	osd 	
		(
			.pclk        ( clk_sys ),

			//-- spi for OSD
			.sdi         ( stm_b15_io  ),
			.sck         ( stm_b13_io ),
			.ss          ( stm_b12_io ),
			.sdo         ( stm_b14_io  ),
			
			.red_in      ( ),
			.green_in    ( ),
			.blue_in     ( ),
			.hs_in       ( ),
			.vs_in       ( ),

			.red_out     ( ),
			.green_out   ( ),
			.blue_out    ( ),
			.hs_out      ( ),
			.vs_out      ( ),

			.data_in		 ( osd_s ),
			.conf_str	 ( CONF_STR ),
						
			.pump_active_o		 ( pump_active_s ),
			.sram_a_o			 ( sram_addr_s ),
			.sram_d_o			 ( sram_data_s ),
			.sram_we_n_o		 ( sram_we_n_s ),
			.config_buffer_o	 ( )		
		);
		
		assign sram_addr_o   = pump_active_s ? sram_addr_s : { 2'b00 , cpu_addr_s };
		assign sram_data_io  = pump_active_s ? sram_data_s :  8'bZZZZZZZZ;
		assign rom_data_s  	= sram_data_io;
		assign sram_oe_n_o   = 1'b0;
		assign sram_we_n_o   = sram_we_n_s;


		//--start the microcontroller OSD menu after the power on
		process (clk, reset_n, osd_s)
		begin
			if rising_edge(clock_11) then
				if reset = '1' then
					power_on_s <= (others=>'1');
				elsif power_on_s /= x"0000" then
					power_on_s <= power_on_s - 1;
					power_on_reset <= '1';
					osd_s <= "00111111";
				else
					power_on_reset <= '0';
					
				end if;
				
				if pump_active_s = '1' and osd_s <= "00111111" then
					osd_s <= "11111111";
				end if;
				
			end if;
		end process;
		

endmodule 

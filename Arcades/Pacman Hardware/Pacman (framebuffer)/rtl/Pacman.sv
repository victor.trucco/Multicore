/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///============================================================================
//  Pacman
//
//  Version With Framebuffer and HDMI
//  Copyright (C) 2020 Victor Trucco
//
//  This program is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation; either version 2 of the License, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//============================================================================

//============================================================================
//
//  Multicore 2 Top by Victor Trucco
//
//============================================================================

`default_nettype none

module Pacman
(
    // Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAMs (AS7C34096)
    output wire [18:0]sram_addr_o  = 18'b0000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM    (H57V256)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io          = 1'bz,
    inout wire  ps2_data_io         = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o           = 1'b1,
    output wire sd_sclk_o           = 1'b0,
    output wire sd_mosi_o           = 1'b0,
    input wire  sd_miso_i,

    // Joysticks
    input wire  joy1_up_i,
    input wire  joy1_down_i,
    input wire  joy1_left_i,
    input wire  joy1_right_i,
    input wire  joy1_p6_i,
    input wire  joy1_p9_i,
    input wire  joy2_up_i,
    input wire  joy2_down_i,
    input wire  joy2_left_i,
    input wire  joy2_right_i,
    input wire  joy2_p6_i,
    input wire  joy2_p9_i,
    output wire joyX_p7_o           = 1'b1,

    // Audio
    output        AUDIO_L,
    output        AUDIO_R,
    input wire  ear_i,
    output wire mic_o                   = 1'b0,

        // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

        // HDMI
    output wire [7:0]tmds_o         = 8'b00000000,

        //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
        
    inout wire  stm_b8_io, 
    inout wire  stm_b9_io,

    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output        SPI_nWAIT

);

`include "rtl\build_id.v"

localparam CONF_STR = {
//  "P,JumpShot.dat;", //mod 16
// "P,TheGlob.dat;", //mod 15
//  "P,DreamShopper.dat;", //mod 14
//  "P,PacManiacMiner.dat;", //mod 13
//    "P,VanVanCar.dat;", //mod 12
//  "P,Ponpoko.dat;", //mod 11
//  "P,Alibaba.dat;", //mod 10
//  "P,Eeekk.dat;", //mod 9
//  "P,Woodpecker.dat;", //mod 8
//  "P,MrTnt.dat;", //mod 7
//  "P,Gorkans.dat;", //mod 6
//  "P,MsPacman.dat;", // mod 5
//  "P,Birdiy.dat;", // mod 4
//  "P,PacmanClub.dat;", // mod 2
//  "P,PacManPlus.dat;", // mod 1
//  "P,PacMan.dat;", // mod 0
  "P,CORE_NAME.dat;", 
    "S,DAT,Alternative ROM...;",
    "O34,Scanlines,Off,25%,50%,75%;", 
    //"O5,Blend,Off,On;", //16
    "O78,Rotate Screen,Off,90,180,270;", 
    
    /*
    "OF,Cabinet,Cocktail,Upright;",
    "OGH,Coinage,Free Play,1c/1cr,1c/2cr,2c/1cr;",
    "OIJ,Lives,1,2,3,5;",
    "OKL,Bonus Life After,10000,15000,20000,None;",
    "OM,Difficulty,Hard,Normal;",
    "ON,Ghosts Names,Off,On;",
    */
    
    "T6,Reset;", // 9
    "V,v1.30." // 8
};

reg [6:0] mod;
reg mod_plus = 0;
reg mod_jmpst= 0;
reg mod_club = 0;
reg mod_orig = 0;
reg mod_bird = 0;
reg mod_ms   = 0;
reg mod_gork = 0;
reg mod_mrtnt= 0;
reg mod_woodp= 0;
reg mod_eeek = 0;
reg mod_alib = 0;
reg mod_ponp = 0;
reg mod_van  = 0;
reg mod_pmm  = 0;
reg mod_dshop= 0;
reg mod_glob = 0;

wire mod_gm = mod_gork | mod_mrtnt;

always @(posedge clk_sys) 
begin   
    mod_orig <= (mod == 0);
    mod_plus <= (mod == 1);
    mod_club <= (mod == 2);
    mod_bird <= (mod == 4);
    mod_ms   <= (mod == 5);
    mod_gork <= (mod == 6);
    mod_mrtnt<= (mod == 7);
    mod_woodp<= (mod == 8);
    mod_eeek <= (mod == 9);
    mod_alib <= (mod == 10);
    mod_ponp <= (mod == 11);
    mod_van  <= (mod == 12);
    mod_pmm  <= (mod == 13);
    mod_dshop<= (mod == 14);
    mod_glob <= (mod == 15);
    mod_jmpst<= (mod == 16);
end

//assign LED = 1;
assign AUDIO_R = AUDIO_L;

assign stm_rst_o = 1'bz;
assign SPI_nWAIT = 1'b1;
assign SDRAM_nCS = 1'b1;
assign SDRAM_nWE = 1'b1;


wire clk_sys, clk_snd, clk_25, clk_pixel,clk_dvi;

wire [1:0] pll_locked_in;
wire pll_locked;
wire pll_areset;
wire pll_scanclk;
wire pll_scandata;
wire pll_scanclkena ;
wire pll_configupdate;
wire pll_scandataout;
wire pll_scandone;
wire [7:0]pll_rom_address;
wire pll_write_rom_ena;
wire pll_write_from_rom;
wire pll_reconfig_s;
wire pll_reconfig_busy;
wire pll_reconfig_reset;
reg [1:0]pll_reconfig_state = 2'b00;
integer pll_reconfig_timeout;
wire pll_rom_q;

pll pll
(
    .inclk0         ( clock_50_i ),
    .c0             ( clk_sys ),
    .c2             ( clk_pixel ),
    .c3             ( clk_dvi ),
    .locked         ( pll_locked ),

    .areset         ( pll_areset ),
    .scanclk        ( pll_scanclk ),
    .scandata       ( pll_scandata ),
    .scanclkena     ( pll_scanclkena ),
    .configupdate   ( pll_configupdate ),
    .scandataout    ( pll_scandataout ),
    .scandone       ( pll_scandone )

);

pll_reconfig pll_reconfig
(
    .busy               ( pll_reconfig_busy ),
    .clock              ( clock_50_i ),
    .counter_param      ( 3'b000 ),
    .counter_type       ( 4'b0000 ),
    .data_in            ( 9'b000000000 ),
    .pll_areset         ( pll_areset ),
    .pll_areset_in      ( 0 ),
    .pll_configupdate   ( pll_configupdate),
    .pll_scanclk        ( pll_scanclk ),
    .pll_scanclkena     ( pll_scanclkena ),
    .pll_scandata       ( pll_scandata ),
    .pll_scandataout    ( pll_scandataout ),
    .pll_scandone       ( pll_scandone ),
    .read_param         ( 0 ),
    .reconfig           ( pll_reconfig_s ),
    .reset              ( pll_reconfig_reset ),
    .reset_rom_address  ( 0 ),
    .rom_address_out    ( pll_rom_address ),
    .rom_data_in        ( pll_rom_q ),
    .write_from_rom     ( pll_write_from_rom ),
    .write_param         ( 0 ),
    .write_rom_ena      ( pll_write_rom_ena )
);

wire q_reconfig_27, q_reconfig_40;
    
reconfig_27  reconfig_27  ( .address ( pll_rom_address ), .clock ( clock_50_i ), .rden ( pll_write_rom_ena ), .q ( q_reconfig_27 ));
reconfig_40  reconfig_40  ( .address ( pll_rom_address ), .clock ( clock_50_i ), .rden ( pll_write_rom_ena ), .q ( q_reconfig_40 ));

wire img_rotate = status[7];
reg  old_rotate = 1'b1;
reg  recfg = 1'b0;

always @(*) 
begin
    case(img_rotate)
        1'b0: begin pll_rom_q <= q_reconfig_27; end
        1'b1: begin pll_rom_q <= q_reconfig_40; end
    endcase
end   

always @(posedge clock_50_i) 
begin

    pll_write_from_rom <= 0;
    pll_reconfig_s <= 0;
    pll_reconfig_reset <= 0;

    case(pll_reconfig_state)

        0: begin
                if (recfg)
                begin
                    pll_write_from_rom <= 1;
                    pll_reconfig_state <= 1;
                end
            end


        1: begin
                pll_reconfig_state <= 2;
            end


        2: begin
        
                if (pll_reconfig_busy == 0)
                begin   
                    pll_reconfig_s <= 1;
                    pll_reconfig_state <= 3;
                    pll_reconfig_timeout <= 1000;
                end;
                
            end


        3: begin
        
                pll_reconfig_timeout <= pll_reconfig_timeout - 1;
                
                if (pll_reconfig_timeout == 1) 
                begin
                    pll_reconfig_reset <= 1; // sometimes pll reconfig stuck in busy state
                    pll_reconfig_state <= 0;
                    recfg <= 0;
                end
                
                if (pll_reconfig_s == 0 && pll_reconfig_busy == 0)
                begin
                    pll_reconfig_state <= 0;
                    recfg <= 0;
                end
                
            end
    endcase

    if( img_rotate != old_rotate ) 
    begin
        old_rotate <= img_rotate;
        recfg <= 1;
    end

end  


reg ce_6m;
always @(posedge clk_sys) begin
    reg [1:0] div;
    div <= div + 1'd1;
    ce_6m <= !div;
end

reg ce_4m;
always @(posedge clk_sys) begin
    reg [2:0] div;
    
    div <= div + 1'd1;
    if(div == 5) div <= 0;
    ce_4m <= !div;
end

reg ce_1m79;
always @(posedge clk_sys) begin
    reg [3:0] div;
    
    div <= div + 1'd1;
    if(div == 12) div <= 0;
    ce_1m79 <= !div;
end

wire [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire        scandoublerD;
wire        ypbpr;
wire [10:0] ps2_key;
wire  [9:0] audio;
wire            hs, vs;
wire            hb, vb;
wire        blankn = ~(hb | vb);
wire  [2:0] r,g;
wire  [1:0] b;
wire  [3:0] idx_col;

wire [7:0] in0xor = mod_ponp ? 8'hE0 : 8'hFF;
wire [7:0] in1xor = mod_ponp ? 8'h00 : 8'hFF;
reg           m_cheat = 1'b0;

pacmant pacmant
(
    .O_VIDEO_COL    ( idx_col ),
    .O_HSYNC        ( hs ),
    .O_VSYNC        ( vs ),
    .O_HBLANK       ( hb ),
    .O_VBLANK       ( vb ),
    .O_AUDIO        ( audio ),
    
    .in0( (in0xor ^ 
                                {
                                    mod_eeek & m_fire2A,
                                    mod_alib & m_fireA,
                                    btn_coin, 
                                    (( mod_orig | mod_plus | mod_ms | mod_bird | mod_alib | mod_woodp) & m_cheat ) | (( mod_ponp | mod_van | mod_dshop ) & m_fireA ),
                                    m_down,
                                    m_right,
                                    m_left,
                                    m_up
                                })),

    .in1({status[15], 7'b1111111} & (in1xor ^ 
                                {
                                    mod_gm & m_fire2A,
                                    btn_two_players         | ( mod_eeek  & m_fireA ) | ( mod_jmpst & m_fire2A ),
                                    btn_one_player          | ( mod_jmpst & m_fireA ),
                                    ( mod_gm & m_fireA ) | (( mod_alib | mod_ponp | mod_van | mod_dshop ) & m_fire2A ),
                                    m_down2,
                                     mod_pmm ? m_fireA : m_right2,
                                    ~mod_pmm & m_left2,
                                     m_up2
                                })),
                            

    .dipsw1         ( status[23:16]),
    .dipsw2         ( (mod_ponp | mod_van | mod_dshop) ? status[31:24] : 8'hFF ),
    
    //.in0(8'hff),
//  .in1(8'hff),
//  .dipsw1(8'hff),
    //.dipsw2(8'hff),
    
    .RESET              ( status[6] | ~btn_n_i[4] | ioctl_downl ),
    .CLK                ( clk_sys ),
    .ENA_6              ( ce_6m ),
    .ENA_4              ( ce_4m ),
    .ENA_1M79           ( ce_1m79 ),
    
    // ---ROMS----
   
    .cpu_addr_o         ( cpu_addr_s ),
    .char_addr_o        ( char_addr_s ),
    .char_rom_5ef_dout  ( char_rom_5ef_dout ),

    .rom_addr           ( rom_addr_s ), 
    .rom_lo             ( rom_lo ),
    .rom_hi             ( rom_hi ),
    
    .video_x_o          ( video_x_s ),
    .video_y_o          ( video_y_s ),
    
    .mod_plus           ( mod_plus  ),
    .mod_jmpst          ( mod_jmpst ),
    .mod_bird           ( mod_bird  ),
    .mod_ms             ( mod_ms    ),
    .mod_mrtnt          ( mod_mrtnt ),
    .mod_woodp          ( mod_woodp ),
    .mod_eeek           ( mod_eeek ),
    .mod_alib           ( mod_alib ),
    .mod_ponp           ( mod_ponp | mod_van | mod_dshop ),
    .mod_van            ( mod_van | mod_dshop ),
    .mod_dshop          ( mod_dshop ),
    .mod_glob           ( mod_glob ),
    .mod_club           ( mod_club ),
    
    .dn_addr            ( ioctl_addr[15:0] ),
    .dn_data            ( ioctl_dout ),
    .dn_wr              ( ioctl_wr )
);

data_io #(.STRLEN(($size(CONF_STR)>>3))) 
data_io
(
    .clk_sys       ( clk_sys      ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),
    .SPI_DO        ( SPI_DO       ),
    
    .data_in       ( osd_s & keys_s ),
    .conf_str      ( CONF_STR ),
    .status        ( status ),
    .core_mod      ( mod ),
    
    .ioctl_download( ioctl_downl  ),
    .ioctl_index   ( ioctl_index  ),
    .ioctl_wr      ( ioctl_wr     ),
    .ioctl_addr    ( ioctl_addr   ),
    .ioctl_dout    ( ioctl_dout   )
    
);
    
dac #(.C_bits(11))
dac
(
    .clk_i   ( clk_sys ),
    .res_n_i ( 1 ),
    .dac_i   ( {1'b0, audio}), //audio} ),
    .dac_o   ( AUDIO_L )
);

//-----------------------

wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player  = ~btn_n_i[1] | m_one_player;
wire btn_two_players = ~btn_n_i[2] | m_two_players;
wire btn_coin        = ~btn_n_i[3] | m_coin1;

wire kbd_intr;
wire [7:0] kbd_scancode;
wire [7:0] keys_s;

//get scancode from keyboard
io_ps2_keyboard keyboard 
 (
  .clk       ( clk_sys ),
  .kbd_clk   ( ps2_clk_io ),
  .kbd_dat   ( ps2_data_io ),
  .interrupt ( kbd_intr ),
  .scancode  ( kbd_scancode )
);

wire [15:0] joy1_s;
wire [15:0] joy2_s;
wire [8:0]  controls_s;
wire        osd_enable;

//translate scancode to joystick
kbd_joystick #( .OSD_CMD    ( 3'b011 )) k_joystick
(
    .clk          ( clk_sys ),
    .kbdint       ( kbd_intr ),
    .kbdscancode  ( kbd_scancode ), 

    .joystick_0   ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1   ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),

    //-- joystick_0 and joystick_1 should be swapped
    .joyswap      ( 0 ),

    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer    ( ~(mod_club | mod_pmm | mod_jmpst) ),

    //-- tilt, coin4-1, start4-1
    .controls     ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

    //-- fire12-1, up, down, left, right

    .player1      ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2      ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),

    //-- keys to the OSD
    .osd_o        ( keys_s ),
    .osd_enable   ( osd_enable ),

    //-- sega joystick
    .sega_clk     ( hs ),
    .sega_strobe  ( joyX_p7_o )
);

    //--------- RGB OUTPUT ----------------------------------------------------
    
    wire [7:0] rgb_out;
    
//  PROM7_DST   col_rom_rgb
//  (
//      .CLK   ( clk_sys ),
//      .ADDR  ( idx_col ),
//      .DATA  ( rgb_out )
//  );
    
    dpram #(4,8) col_rom_rgb
    (
        .clk_a_i    ( clk_pixel ),
        .en_a_i     ( 1'b1 ),
        .we_i       ( ioctl_wr & rom7_cs & prom_cs ),
        .addr_a_i   ( ioctl_addr[3:0] ), 
        .data_a_i   ( ioctl_dout ),
    
        .clk_b_i    ( clk_sys ),
        .addr_b_i   ( idx_col ),
        .data_b_o   ( rgb_out )
  );
  
    
    wire scandblctrl = 1'b1; //btn_n_i[2];
                
    assign VGA_R  = ( scandblctrl ) ? vga_r_out_s[5:1] : {rgb_out[2:0], 2'b00};
    assign VGA_G  = ( scandblctrl ) ? vga_g_out_s[5:1] : {rgb_out[5:3], 2'b00};
    assign VGA_B  = ( scandblctrl ) ? vga_b_out_s[5:1] : {rgb_out[7:6], rgb_out[7], 2'b00};
    assign VGA_HS = ( scandblctrl ) ? vga_hs_s          : hs;
    assign VGA_VS = ( scandblctrl ) ? vga_vs_s          : vs;
    
    //--------- VGA/HDMI OUTPUT ----------------------------------------------------
    wire [8:0] video_x_s;
    wire [7:0] video_y_s;

    wire [3:0] vga_col_s;
    wire vga_blank_s;
    
    //-- contador I_HCNT de 128 a 511 = 48 + 288 pixels horizontais
    //-- hsync de 175 a 207
    //-- hblank de 143 a 239
    //-- imagem de 239 a 511 (272 pixels) e de 128 a 143 (16 pixels)
    
    //-- contador I_VCNT de 248 a 511 = 39 + 224 pixels verticais   
    
    vga vga
    (
        .I_CLK      ( clk_sys ),
        .I_CLK_VGA  ( clk_pixel ),
        .I_COLOR    ( idx_col ),
        .I_HCNT     ( video_x_s ),
        .I_VCNT     ( video_y_s ),
        
        .I_ROTATE   ( status[8:7] ),
        
        .O_HSYNC    ( vga_hs_s ),
        .O_VSYNC    ( vga_vs_s ),
        .O_COLOR    ( vga_col_s ),
        .O_BLANK    ( vga_blank_s )
    );
    

    wire [7:0] vga_out;
        
//  PROM7_DST   col_rom_vga
//  (
//      .CLK   ( clk_pixel ),
//      .ADDR  ( vga_col_s ),
//      .DATA  ( vga_out )
//  );
    
    wire prom_cs;
    wire rom7_cs;

    assign prom_cs = (ioctl_addr[15:14] == 2'b11);   //0xc000-ffff 
    assign rom7_cs = (ioctl_addr[9:4] == 6'b110000); //0xc300 

    dpram #(4,8) col_rom_7f
    (
        .clk_a_i    ( clk_pixel ),
        .en_a_i     ( 1'b1 ),
        .we_i       ( ioctl_wr & rom7_cs & prom_cs ),
        .addr_a_i   ( ioctl_addr[3:0] ), 
        .data_a_i   ( ioctl_dout ),

        .clk_b_i    ( clk_pixel ),
        .addr_b_i   ( vga_col_s ),
        .data_b_o   ( vga_out )
    );
  
    assign {b,g,r} = vga_out;
    
    wire [5:0] vga_r_s; 
    wire [5:0] vga_g_s; 
    wire [5:0] vga_b_s; 
    wire vga_hs_s;
    wire vga_vs_s; 

    mist_video #(.COLOR_DEPTH(3),.SD_HCNT_WIDTH(10)) mist_video
    (
        .clk_sys        ( clk_pixel  ),
        .SPI_SCK        ( SPI_SCK ),
        .SPI_SS3        ( SPI_SS2 ),
        .SPI_DI         ( SPI_DI  ),
        .R              ( r       ),
        .G              ( g       ),
        .B              ( {b,b[1]}),
        .HSync          ( vga_hs_s  ),
        .VSync          ( vga_vs_s  ),

        .VGA_R          ( vga_r_s ),
        .VGA_G          ( vga_g_s ),
        .VGA_B          ( vga_b_s ),
        .vga_vs_s       ( ),
        .vga_hs_s       ( ),
        
        .rotate         ( (mod_ponp) ? {1'b1,status[7]} : ~status[8:7] ),
        .scandoubler_disable(1'b1),
        .scanlines      ( 2'd0 ),
        .ce_divider     ( 1'b1 ),
        .blend          ( 1'b0 ),
        .osd_enable     ( osd_enable )
    );    
        
    wire [5:0] vga_r_out_s; 
    wire [5:0] vga_g_out_s; 
    wire [5:0] vga_b_out_s; 
    
    wire [1:0] scanlines = status[4:3];
    reg scanline = 0;
    
    always @(posedge vga_hs_s or negedge vga_vs_s)
    begin
        if (vga_vs_s == 1'b0 )
            scanline <= 1'b0;
        else //if (vga_hs_s == 1'b1 )
            scanline <= ~scanline;
    end
    
    //scanlines 
    always @(posedge clk_pixel) 
    begin

        // if no scanlines or not a scanline
        if(!scanline || !scanlines) 
        begin
            vga_r_out_s <= vga_r_s;
            vga_g_out_s <= vga_g_s;
            vga_b_out_s <= vga_b_s;
        end else 
        begin
            case(scanlines)
                2'b01: begin // reduce 25% = 1/2 + 1/4
                    vga_r_out_s <= {1'b0, vga_r_s[5:1]} + {2'b00, vga_r_s[5:2] };
                    vga_g_out_s <= {1'b0, vga_g_s[5:1]} + {2'b00, vga_g_s[5:2] };
                    vga_b_out_s <= {1'b0, vga_b_s[5:1]} + {2'b00, vga_b_s[5:2] };
                end

                2'b10: begin // reduce 50% = 1/2
                    vga_r_out_s <= {1'b0, vga_r_s[5:1]};
                    vga_g_out_s <= {1'b0, vga_g_s[5:1]};
                    vga_b_out_s <= {1'b0, vga_b_s[5:1]};
                end

                2'b11: begin // reduce 75% = 1/4
                    vga_r_out_s <= {2'b00, vga_r_s[5:2]};
                    vga_g_out_s <= {2'b00, vga_g_s[5:2]};
                    vga_b_out_s <= {2'b00, vga_b_s[5:2]};
                end
            endcase
        end
    end
    
//  assign VGA_B = { vga_b_s, 2'b00 };
//  assign VGA_G = { vga_g_s, 2'b00 };
//  assign VGA_R = { vga_r_s, 2'b00 };
    
//  assign vga_b_s = {vga_out[7:6], vga_out[7]};
//  assign vga_g_s = vga_out[5:3];
//  assign vga_r_s = vga_out[2:0];
    
//  reg [2:0] vga_r_s;
//  reg [2:0] vga_g_s;
//  reg [2:0] vga_b_s;
    
    wire    [15:0] sound_hdmi_s;
    
    wire [9:0] tdms_r_s;
    wire [9:0] tdms_g_s;
    wire [9:0] tdms_b_s;
    wire [3:0] hdmi_p_s;
    wire [3:0] hdmi_n_s;
    
 //-- HDMI
    hdmi 
    # (
        .FREQ ( 40000000 ),   //-- pixel clock frequency 
        .FS   ( 48000 ),      //-- audio sample rate - should be 32000, 41000 or 48000 = 48KHz
        .CTS  ( 40000 ),      //-- CTS = Freq(pixclk) * N / (128 * Fs)
        .N    ( 6144 )        //-- N = 128 * Fs /1000,  128 * Fs /1500 <= N <= 128 * Fs /300 (Check HDMI spec 7.2 for details)
    ) inst_dvid
    (
        .I_CLK_PIXEL    ( clk_pixel ),
        .I_R            ( {vga_r_out_s, vga_r_out_s[5:4]} ),
        .I_G            ( {vga_g_out_s, vga_g_out_s[5:4]} ),
        .I_B            ( {vga_b_out_s, vga_b_out_s[5:4]} ),
        .I_BLANK        ( vga_blank_s ),
        .I_HSYNC        ( vga_hs_s ),
        .I_VSYNC        ( vga_vs_s ),
        
        //-- PCM audio
        .I_AUDIO_ENABLE ( 1'b1 ),
        .I_AUDIO_PCM_L  ( sound_hdmi_s ),
        .I_AUDIO_PCM_R  ( sound_hdmi_s ),
        
        //-- TMDS parallel pixel synchronous outputs (serialize LSB first)
        .O_RED          ( tdms_r_s ),
        .O_GREEN        ( tdms_g_s ),
        .O_BLUE         ( tdms_b_s )
    );
    
    hdmi_out_altera hdmio
    (
        .clock_pixel_i  ( clk_pixel ),
        .clock_tdms_i   ( clk_dvi ),
        .red_i          ( tdms_r_s ),
        .green_i        ( tdms_g_s ),
        .blue_i         ( tdms_b_s ),
        .tmds_out_p     ( hdmi_p_s ),
        .tmds_out_n     ( hdmi_n_s )
    );

    wire [15:0] pcm_s;
    audio_pcm
    #( .CLK_RATE (40000000))
    audio_pcm
    (
        .clk         ( clk_pixel ),
        .sample_rate ( 0 ), //48khz
        .left_in     ( {audio, 6'b000000} ),
        .pcm_l       ( pcm_s ),
    );
 
    assign sound_hdmi_s =  {1'b0, ~pcm_s[15], pcm_s[14:1]}; 
    
    assign tmds_o[7] = hdmi_p_s[2]; //-- 2+     
    assign tmds_o[6] = hdmi_n_s[2]; //-- 2-     
    assign tmds_o[5] = hdmi_p_s[1]; //-- 1+         
    assign tmds_o[4] = hdmi_n_s[1]; //-- 1-     
    assign tmds_o[3] = hdmi_p_s[0]; //-- 0+     
    assign tmds_o[2] = hdmi_n_s[0]; //-- 0- 
    assign tmds_o[1] = hdmi_p_s[3]; //-- CLK+   
    assign tmds_o[0] = hdmi_n_s[3]; //-- CLK-   
        
    
    //--------- ROM DATA PUMP ----------------------------------------------------
    
        reg [15:0] power_on_s   = 16'b1111111111111111;
        reg [7:0] osd_s = 8'b11111111;
        
        reg first_reset = 1'b1;
        
        //--start the microcontroller OSD menu after the power on
        always @(posedge clk_sys) 
        begin
        
                if (first_reset == 1)
                begin
                    power_on_s = 16'b1111111111111111;
                    first_reset = 1'b0;
                end
                else if (power_on_s != 0)
                begin
                    power_on_s = power_on_s - 1;
                    osd_s = 8'b00111111;
                end 
                    
                
                if (ioctl_downl == 1 && osd_s == 8'b00111111)
                    osd_s = 8'b11111111;
            
        end 

    //-----------------------
    
    
    //-------------------
    // ROMS
    
    wire ioctl_downl;
    wire [7:0]ioctl_index;
    wire ioctl_wr;
    wire [24:0]ioctl_addr;
    wire [7:0]ioctl_dout;
    
    assign sram_oe_n_o  = 1'b0; 
    //assign program_rom_dinl = sram_data_io;   
    
//  assign sram_addr_o  = (ioctl_wr) ? ioctl_addr[18:0] : {5'b00000,cpu_addr_s[13:0] }; 
//  assign sram_data_io = (ioctl_wr) ? ioctl_dout : 8'hZZ;
//  assign program_rom_dinl = sram_data_io;
//  assign sram_we_n_o  = ~ioctl_wr;

     
    reg [7:0]rom_lo;
    reg [7:0]rom_hi;
    reg [15:0]rom_addr_s;
     
     
    always @(*)
    begin
        if (char_oe) char_rom_5ef_dout <= sram_data_io; 
    
        if (rom_addr_s[15]) rom_hi <= sram_data_io; else rom_lo <= sram_data_io; 
    end 
    
    always @(posedge clk_sys)
    begin                                                           //0x8000 (graphics)         0x4000-7fff (HI rom)   0x0000-3fff (LO rom)
        sram_addr_o  <= (ioctl_wr) ? ioctl_addr[18:0] : (ce_6m) ? {6'b000100,char_addr_s } : {4'b0000,rom_addr_s[15],rom_addr_s[13:0] };    
        //sram_addr_o  <= (ioctl_wr) ? ioctl_addr[18:0] : {5'b00000,cpu_addr_s[13:0] }; 
        sram_data_io <= (ioctl_wr) ? ioctl_dout : 8'hZZ;
        sram_we_n_o  <= ~ioctl_wr;
        
        char_oe <= ce_6m; //delay the clock signal one cycle to make the OE for video data
        
    end

    
    wire [15:0] cpu_addr_s;
    wire [12:0] char_addr_s;
    wire [7:0]  char_rom_5ef_dout;
    wire [7:0]  gfx_dout;
    reg char_oe;

endmodule 

--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library IEEE;
use IEEE.std_logic_1164.all;

package jt12 is 

component jt12
port
(
    rst        : in  std_logic;
    clk        : in  std_logic;         -- CPU clock
    cen        : in  std_logic := '1';  -- optional clock enable, if not needed leave as '1'
    din        : in  std_logic_vector(7 downto 0);
    addr       : in  std_logic_vector(1 downto 0);
    cs_n       : in  std_logic;
    wr_n       : in  std_logic;

    dout       : out std_logic_vector(7 downto 0);
    irq_n      : out std_logic;
    en_hifi_pcm: in  std_logic;     -- set high to use interpolation on PCM samples

    -- combined output
    snd_right  : out std_logic_vector(15 downto 0); -- signed
    snd_left   : out std_logic_vector(15 downto 0); -- signed
    snd_sample : out std_logic
);
end component;

component jt12_genmix
port 
(
    rst     : in  std_logic;
    clk     : in  std_logic;    -- expects 54 MHz clock
    fm_left : in  std_logic_vector(15 downto 0); -- FM at 55kHz
    fm_right: in  std_logic_vector(15 downto 0); -- FM at 55kHz
    psg_snd : in  std_logic_vector(10 downto 0); -- PSG at 220kHz
    fm_en   : in  std_logic;
    psg_en  : in  std_logic;
    -- Mixed sound at 54 MHz
    snd_left  : out std_logic_vector(15 downto 0);
    snd_right : out std_logic_vector(15 downto 0)
);
end component;

end;

/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/`timescale 1ns / 1ps

module test;

wire signed [15:0] pcm;

reg clk;
initial begin
    clk = 1'b0;
    #100_000;
    forever clk = #(54054/2) ~clk;
end

reg rst_n;
initial begin
    rst_n = 1;
    rst_n = #20000 0;
    rst_n = #50000 1;
end

reg [7:0] mem[0:8*1024*1024];
integer file, maxcnt, cnt;
initial begin
    // 2.rom is obtained by running the verilator simulation
    // on track 02 of Metal Slug
    file=$fopen("2.rom","rb");
    if( file==0 ) begin
        $display("Cannot open file 2.rom");
        $finish;
    end
    maxcnt = $fread( mem, file );
    $display("INFO 0x%X bytes read", maxcnt);
    cnt    = 32'h2b1b00;
    maxcnt = 32'h2b2e00;
    //$fclose(file);
end

reg nibble=1;

reg [3:0] data=4'd0;

integer chcnt=0;
reg cen=1;
always @(posedge clk) if(cen)
    chcnt <= chcnt==5 ? 0 : chcnt+1;

reg chon=1'b0;
wire [7:0] memcnt = mem[cnt];

// if cen is toggled adpcma_single instance will fail.
//always @(negedge clk) cen <= ~cen;

always @(posedge clk) if(cen) begin
    if(chcnt==0) begin
        data <= nibble ? mem[cnt][7:4] : mem[cnt][3:0];
        nibble <= ~nibble;
        if( !nibble ) cnt <= cnt+1;
        if( cnt == maxcnt ) $finish;    
        // $display("%X -> %X", cnt, mem[cnt] );
    end
    chon <= chcnt==0;
end

jt10_adpcm uut(
    .rst_n      ( rst_n ),
    .clk        ( clk   ),
    .cen        ( cen  ),
    .data       ( data  ),
    .chon       ( chon  ),
    .pcm        ( pcm   )
);

wire signed [15:0] pcm_single;

reg cen6=0;

always @(negedge clk ) cen6= chcnt==0;

adpcma_single single(
    .clk        ( clk && cen6         ),
    .data       ( data         ),
    .pcm        ( pcm_single   )
);

integer filepipe;
initial begin
    filepipe=$fopen("pipeline.val","w");
end

reg signed [15:0] pcm0=0;
always @(posedge clk) begin
    if(chcnt==2) begin
        pcm0 <= pcm;
        $fdisplay(filepipe, "%d\n",pcm);
    end
end
`ifdef NCVERILOG
initial begin
    $shm_open("test.shm");
    $shm_probe(test,"AS");
end
`else 
initial begin
    $dumpfile("test.fst");
    $dumpvars;
end
`endif

endmodule
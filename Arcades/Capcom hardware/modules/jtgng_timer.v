/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//*  This file is part of JT_GNG.
    JT_GNG program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JT_GNG program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JT_GNG.  If not, see <http://www.gnu.org/licenses/>.

    Author: Jose Tejada Gomez. Twitter: @topapate
    Version: 1.0
    Date: 27-10-2017 */

module jtgng_timer(
    input               clk,
    input               cen6,   //  6 MHz
    input               rst,
    output  reg [8:0]   V,
    output  reg [8:0]   H,
    output  reg         Hinit,
    output  reg         Vinit,
    output  reg         LHBL,
    output  reg         LHBL_obj,
    output  reg         LVBL,
    output  reg         LVBL_obj,
    output  reg         HS,
    output  reg         VS
);

parameter obj_offset=10'd3;

//reg LHBL_short;
//reg G4_3H;  // high on 3/4 H transition
//reg G4H;    // high on 4H transition
//reg OH;     // high on 0H transition

// H counter
always @(posedge clk, posedge rst) begin
    if( rst ) begin
        { Hinit, H } <= 10'd135;
    end else if(cen6) begin
        Hinit <= H == 9'h86;
        if( H == 9'd511 ) begin
            //Hinit <= 1'b1;
            H <= 9'd128;
        end
        else begin
            //Hinit <= 1'b0;
            H <= H + 9'b1;
        end
    end
end

// V Counter
always @(posedge clk, posedge rst) begin
    if( rst ) begin
        V     <= 9'd496;
        Vinit <= 1'b1;
    end else if(cen6) begin
        if( H == 9'd511 ) begin
            Vinit <= &V;
            V <= &V ? 9'd250 : V + 1'd1;
        end
    end
end

wire [9:0] LHBL_obj0 = 10'd135-obj_offset >= 10'd128 ? 10'd135-obj_offset : 10'd135-obj_offset+10'd512-10'd128;
wire [9:0] LHBL_obj1 = 10'd263-obj_offset;

// L Horizontal/Vertical Blanking
// Objects are drawn using a 2-line buffer
// so they are calculated two lines in advanced
// original games use a small ROM to generate
// control signals for the object buffers.
// I do not always use that ROM in my games,
// I often just generates the signals with logic
// LVBL_obj is such a signal. In CAPCOM schematics
// this is roughly equivalent to BLTM (1943) or BLTIMING (GnG)
always @(posedge clk, posedge rst)
    if( rst ) begin
        LHBL <= 1'b0;
        LVBL <= 1'b0;
        LVBL_obj <= 1'b0;
        VS <= 1'b0;
        HS <= 1'b0;
    end
    else if(cen6) begin
        if( H==LHBL_obj1[8:0] ) LHBL_obj<=1'b1;
        if( H==LHBL_obj0[8:0] ) LHBL_obj<=1'b0;
        if( &H[2:0] ) begin
            LHBL <= H[8];
            case( V )
                9'd496: LVBL <= 1'b0; // h1F0
                9'd272: LVBL <= 1'b1; // h110
                // OBJ LVBL is two lines ahead
                9'd494: LVBL_obj <= 1'b0;
                9'd270: LVBL_obj <= 1'b1;

                9'd507: VS <= 1;
                9'd510: VS <= 0;
            endcase // V
        end

        if (H==9'd178) HS <= 1;
        if (H==9'd206) HS <= 0;
        // if (H==9'd136) LHBL_short <= 1'b0;
        // if (H==9'd248) LHBL_short <= 1'b1;
    end

endmodule // jtgng_timer
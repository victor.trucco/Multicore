/*  This file is part of JT_GNG.
    JT_GNG program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JT_GNG program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JT_GNG.  If not, see <http://www.gnu.org/licenses/>.

    Author: Jose Tejada Gomez. Twitter: @topapate
    Version: 1.0
    Date: 22-2-2019 
     
     
     Multicore 2+ top by Victor Trucco
     
     */

`timescale 1ns/1ps

`default_nettype none

// This is the top level
// It will instantiate the appropriate game core according
// to the macro inside the QSF file
// the config string for the microcontroller is inside the same QSF

module `MC2TOP(

    // Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAM (IS61WV20488FBLL-10)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM (W9825G6KH-6)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io        = 1'bz,
    inout wire  ps2_data_io       = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o         = 1'bZ,
    output wire sd_sclk_o         = 1'bZ,
    output wire sd_mosi_o         = 1'bZ,
    input wire  sd_miso_i,

    // Joysticks
    output wire joy_clock_o       = 1'b1,
    output wire joy_load_o        = 1'b1,
    input  wire joy_data_i,
    output wire joy_p7_o          = 1'b1,

    // Audio
    output      AUDIO_L,
    output      AUDIO_R,
    input wire  ear_i,
    output wire mic_o             = 1'b0,

    // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

    //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
   
    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output wire   SPI_nWAIT        = 1'b1, // '0' to hold the microcontroller data streaming

    inout [31:0] GPIO,

    output LED                    = 1'b1 // '0' is LED on
    
    `ifdef SIMULATION
    ,output         sim_pxl_cen,
    output          sim_pxl_clk,
    output          sim_vs,
    output          sim_hs
    `endif
);


//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------
assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign stm_rst_o    = 1'bZ;
assign stm_rx_o = 1'bZ;
assign LED  = ~ioctl_download;

//no SRAM for this core
assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;

//all the SD reading goes thru the microcontroller for this core
assign sd_cs_n_o = 1'bZ;
assign sd_sclk_o = 1'bZ;
assign sd_mosi_o = 1'bZ;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

joystick_serial  joystick_serial 
(
    .clk_i           ( clk_vga ),
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

data_io #(.aw(22), .STRLEN($size(CONF_STR)/8)) u_datain (
    .sck                ( SPI_SCK       ),
    .ss                 ( SPI_SS2       ),
    .sdi                ( SPI_DI        ),
    .sdo                ( SPI_DO        ),
	 
	 //external data in to the microcontroller
	 .data_in				( pump_s & osd_keys & osd_kbd ),//& keys_i),
	 .conf_str				( CONF_STR 		 ),
    .status             ( status        ),
    .rst                ( ~pll_locked   ),
    .clk_sdram          ( clk_rom       ),
    .downloading_sdram  ( ioctl_download),
    .ioctl_addr         ( ioctl_addr    ),
    .ioctl_data         ( ioctl_data    ),
    .ioctl_wr           ( ioctl_wr      ),
    .index              ( /* unused*/   )
);

// ROM Data Pump
reg [7:0] pump_s = 8'b11111111;
PumpSignal PumpSignal (clk_rom, ~pll_locked, ioctl_download, pump_s);

//-----------------------------------------------------------------

assign stm_rst_o = 1'bz;

localparam CLK_SPEED=48;

localparam CONF_STR = {
    "P,CORE_NAME.dat;",  
    "O34,Scanlines,None,25%,50%,75%;",
    "O12,Screen Rotation,0,90,180,270;",
    "O5,Scandoubler,On,Off;",
    "O6,Double Video Buffer,On,Off;",
    "T0,RST;"
};

wire          rst, rst_n, clk_sys, clk_rom;
wire          cen12, cen6, cen3, cen1p5;
wire [31:0]   status, joystick1, joystick2;
reg  [31:0]   statusdips;
wire [21:0]   sdram_addr;
wire [31:0]   data_read;
wire          loop_rst;
wire [21:0]   ioctl_addr;
wire [ 7:0]   ioctl_data;
wire          ioctl_wr;
wire          ioctl_download;
wire sdram_req;

wire [21:0]   prog_addr;
wire [ 7:0]   prog_data;
wire [ 1:0]   prog_mask;
wire          prog_we;

wire [3:0] red;
wire [3:0] green;
wire [3:0] blue;

wire LHBL, LHBL_dly, LVBL, LVBL_dly, hs, vs;
wire [15:0] snd;

wire [9:0] game_joy1, game_joy2;
wire [1:0] game_coin, game_start;
wire game_rst;
wire [3:0] gfx_en;
// SDRAM
wire data_rdy, sdram_ack;
wire refresh_en;


// PLL's
// 24 MHz or 12 MHz base clock
wire clk_vga_in, clk_vga, clk_40, pll_locked;
jtgng_pll0 u_pll_game (
    .inclk0 ( clock_50_i ),
    .c1     ( clk_rom     ), // 48 MHz
    .c2     ( SDRAM_CLK   ),
   // .c3     ( clk_vga_in  ),
     .c3     ( clk_vga    ), // 25
	  .c4     ( clk_40     ),
    .locked ( pll_locked  )
);

// assign SDRAM_CLK = clk_rom;
assign clk_sys   = clk_rom;

//jtgng_pll1 u_pll_vga (
//    .inclk0 ( clk_vga_in ),
//    .c0     ( clk_vga    ) // 25
//);

wire [7:0] dipsw_a, dipsw_b;
wire [1:0] dip_fxlevel;
wire       enable_fm, enable_psg;
wire       dip_pause, dip_flip, dip_test;

`ifdef SIMULATION
assign sim_pxl_clk = clk_sys;
assign sim_pxl_cen = cen6;
assign sim_vs = ~LVBL_dly;
assign sim_hs = ~LHBL_dly;
`endif

wire [7:0] osd_kbd;
wire [1:0] osd_rotate;
wire       video_direct;
jtframe_mc2 #( .CONF_STR(CONF_STR),.CONF_STR_LEN($size(CONF_STR)/8 ),
    .SIGNED_SND(1'b1), .THREE_BUTTONS(1'b1))
u_frame(
    .clk_sys        ( clk_sys        ),
    .clk_rom        ( clk_rom        ),
    .clk_vga        ( clk_vga        ),
    .clk_40         ( clk_40         ),
    .pll_locked     ( pll_locked     ),
    .status         ( statusdips     ),
    .video_direct   ( video_direct   ),
    .osd_kbd        ( osd_kbd        ),
    .osd_rotate     ( osd_rotate     ),
    // Base video
    .game_r         ( red            ),
    .game_g         ( green          ),
    .game_b         ( blue           ),
    .LHBL           ( LHBL_dly       ),
    .LVBL           ( LVBL_dly       ),
    .hs             ( hs             ),
    .vs             ( vs             ),
    .pxl_cen        ( cen6           ),
    .pxl2_cen       ( cen12          ),
    // MiST VGA pins
    //.VGA_R          ( VGA_R        ),
    //.VGA_G          ( VGA_G        ),
    //.VGA_B          ( VGA_B        ),
    //.VGA_HS         ( VGA_HS  ),
    //.VGA_VS         ( VGA_VS  ),
     
    // SDRAM interface
    .SDRAM_CLK      ( SDRAM_CLK    ),
    .SDRAM_DQ       ( SDRAM_DQ    ),
    .SDRAM_A        ( SDRAM_A     ),
    .SDRAM_DQML     ( SDRAM_DQML ),
    .SDRAM_DQMH     ( SDRAM_DQMH ),
    .SDRAM_nWE      ( SDRAM_nWE     ),
    .SDRAM_nCAS     ( SDRAM_nCAS    ),
    .SDRAM_nRAS     ( SDRAM_nRAS    ),
    .SDRAM_nCS      ( SDRAM_nCS     ),
    .SDRAM_BA       ( SDRAM_BA     ),
    .SDRAM_CKE      ( SDRAM_CKE    ),
    
    // SPI interface to arm io controller
    .SPI_DI         ( SPI_DI      ),
    .SPI_SCK        ( SPI_SCK      ),
    .SPI_SS2        ( SPI_SS2      ),
     
    // ROM
    .prog_addr      ( prog_addr      ),
    .prog_data      ( prog_data      ),
    .prog_mask      ( prog_mask      ),
    .prog_we        ( prog_we        ),
    .downloading    ( ioctl_download ),
     
    // ROM access from game
    .loop_rst       ( loop_rst       ),
    .sdram_addr     ( sdram_addr     ),
    .sdram_req      ( sdram_req      ),
    .sdram_ack      ( sdram_ack      ),
    .data_read      ( data_read      ),
    .data_rdy       ( data_rdy       ),
    .refresh_en     ( refresh_en     ),
     
//////////// board
    .rst            ( rst            ), //outputs
    .rst_n          ( rst_n          ), // unused
    .game_rst       ( game_rst       ),
    .game_rst_n     (                ),
    // reset forcing signals:
    .rst_req        ( ~btn_n_i[4]|status[0]),
    // Sound
    .snd            ( snd            ),
    .AUDIO_L        ( AUDIO_L        ),
    .AUDIO_R        ( AUDIO_R        ),
     
    // joystick (output to game)
    .game_joystick1 ( game_joy1      ), // output
    .game_joystick2 ( game_joy2      ), // output
    .game_coin      ( game_coin      ), // output
    .game_start     ( game_start     ), // output
    .game_service   (                ), // unused
    .LED            (                ),
    // DIP and OSD settings
    .enable_fm      ( enable_fm      ),
    .enable_psg     ( enable_psg     ),
    .dip_test       ( dip_test       ),
    .dip_pause      ( dip_pause      ),
    .dip_flip       ( dip_flip       ),
    .dip_fxlevel    ( dip_fxlevel    ),
     
    // Debug
    .gfx_en         ( gfx_en         ),
     
     //keyboard
     .ps2_kbd_clk   ( ps2_clk_io     ),
     .ps2_kbd_data  ( ps2_data_io    ),
     
     //joysticks (connected to the FPGA)
     // .fpga_joystick1 ( ~{ 1'b1, 1'b1, 1'b1, 1'b1,  1'b1, 1'b1, btn_n_i[3], btn_n_i[2], btn_n_i[1], 1'b1, joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i })    
`ifdef GUNSMOKEJOY
     //-- joy_s format MXYZ SACB RLDU
     .fpga_joystick1 ( ~{ 1'b1, 1'b1, 1'b1, 1'b1,  1'b1, joyControls_s[3], btn_n_i[3]&joyControls_s[0], btn_n_i[2]&joyControls_s[2], btn_n_i[1]&joyControls_s[1], joy1_s[5], joy1_s[4], joy1_s[6], joy1_s[0], joy1_s[1], joy1_s[2], joy1_s[3] })
`else
     //-- joy_s format MXYZ SACB RLDU
     .fpga_joystick1 ( ~{ 1'b1, 1'b1, 1'b1, 1'b1,  1'b1, joyControls_s[3], btn_n_i[3]&joyControls_s[0], btn_n_i[2]&joyControls_s[2], btn_n_i[1]&joyControls_s[1], joy1_s[6], joy1_s[5], joy1_s[4], joy1_s[0], joy1_s[1], joy1_s[2], joy1_s[3] })
`endif
);

// include the on screen display
wire [5:0] osd_r_o;
wire [5:0] osd_g_o;
wire [5:0] osd_b_o;
wire [5:0] r_out;
wire [5:0] g_out;
wire [5:0] b_out;
wire [5:0] game_r6 = {vga_col_s[11:8], vga_col_s[8], vga_col_s[8]};
wire [5:0] game_g6 = {vga_col_s[7:4], vga_col_s[4], vga_col_s[4]};
wire [5:0] game_b6 = {vga_col_s[3:0], vga_col_s[0], vga_col_s[0]};
wire [11:0]vga_col_s;
wire       vga_hs_s,vga_vs_s;
wire       scan2xdis;
/*reg [1:0]  LHBL_edge;
reg [1:0]  LHBLndly_edge;
wire       framebuffer_LHBL;
reg        myLHBL = 1'b1;

always @(posedge clk_sys)
begin
	LHBL_edge <= {LHBL_edge[0], LHBL};
	LHBLndly_edge <= {LHBLndly_edge[0], LHBL_ndly};
	if (LHBL_edge == 2'b10) myLHBL <= 1'b0;
	if (LHBLndly_edge == 2'b01) myLHBL <= 1'b1;
end*/

`ifndef REDUCE_COLOR_DEPTH  
framebuffer #(256,224,12,1) framebuffer
(
		.clk_sys    ( clk_sys ),
		.clk_i      ( cen6 ),
		.RGB_i      ({red,green,blue}),
		.hblank_i   ( ~LHBL_dly ),
		.vblank_i   ( ~LVBL_dly ),
		.dis_db_i   ( status[6] ),
		.rotate_i   ( status[2:1] ), 

		.clk_vga_i  ((status[1]) ? clk_40 : clk_vga ), //800x600 or 640x480
		.RGB_o      ( vga_col_s ),
		.hsync_o    ( vga_hs_s ),
		.vsync_o    ( vga_vs_s ),
		.blank_o    (  ),

		.odd_line_o (  )
);
`else
framebuffer #(256,224,11,1) framebuffer
(
		.clk_sys    ( clk_sys ),
		.clk_i      ( cen6 ),
		.RGB_i      ({red,green,blue[3:1]}),
		.hblank_i   ( ~LHBL_dly ),
		.vblank_i   ( ~LVBL_dly ),
		.dis_db_i   ( status[6] ),
		.rotate_i   ( status[2:1] ), 

		.clk_vga_i  ((status[1]) ? clk_40 : clk_vga ), //800x600 or 640x480
		.RGB_o      ( vga_col_s[11:1] ),
		.hsync_o    ( vga_hs_s ),
		.vsync_o    ( vga_vs_s ),
		.blank_o    (  ),

		.odd_line_o (  )
);
assign vga_col_s[0] = 1'b0;
`endif
assign scan2xdis = status[5] ^ video_direct;

osd #(0,0,6'b01_11_01) osd (
		.clk_sys    ( (~scan2xdis) ? (status[1]) ? clk_40 : clk_vga : clk_sys ),

		// spi for OSD
		.SPI_DI     ( SPI_DI       ),
		.SPI_SCK    ( SPI_SCK      ),
		.SPI_SS3    ( SPI_SS2      ),

		.rotate     ( osd_rotate   ),

		.R_in       ( (~scan2xdis) ? game_r6  : {red, red[3:2]} ),
		.G_in       ( (~scan2xdis) ? game_g6  : {green, green[3:2]} ),
		.B_in       ( (~scan2xdis) ? game_b6  : {blue, blue[3:2]} ),
		.HSync      ( (~scan2xdis) ? vga_hs_s : ~hs ), //HSync        ),
		.VSync      ( (~scan2xdis) ? vga_vs_s : ~vs ), //VSync        ),

		.R_out      ( osd_r_o      ),
		.G_out      ( osd_g_o      ),
		.B_out      ( osd_b_o      )
);

scanlines scanlines
(
		.clk_sys   ( (~scan2xdis) ? (status[1]) ? clk_40 : clk_vga : clk_sys),

		.scanlines ( status[4:3]) ,
		.ce_x2     ( 1'b1 ),

		.r_in     ( osd_r_o      ),
		.g_in     ( osd_g_o      ),
		.b_in     ( osd_b_o      ),
		.hs_in    ( (~scan2xdis) ? vga_hs_s : ~hs    ),
		.vs_in    ( (~scan2xdis) ? vga_vs_s : ~vs    ),

		.r_out ( r_out ),
		.g_out ( g_out ),
		.b_out ( b_out )
);

assign VGA_R  = r_out[5:1];
assign VGA_G  = g_out[5:1];
assign VGA_B  = b_out[5:1];

// a minimig vga->scart cable expects a composite sync signal on the VIDEO_HS output.
// and VCC on VIDEO_VS (to switch into rgb mode)
assign VGA_HS = (~scan2xdis) ? vga_hs_s : ~hs;
assign VGA_VS = (~scan2xdis) ? vga_vs_s : ~vs;
 


`ifdef SIMULATION
`ifdef TESTINPUTS
    test_inputs u_test_inputs(
        .loop_rst       ( loop_rst       ),
        .LVBL           ( LVBL           ),
        .game_joystick1 ( game_joy1[6:0] ),
        .button_1p      ( game_start[0]  ),
        .coin_left      ( game_coin[0]   )
    );
    assign game_start[1] = 1'b1;
    assign game_coin[1]  = 1'b1;
    assign game_joystick2 = ~10'd0;
    assign game_joystick1[9:7] = 3'b111;
    assign sim_vs = vs;
    assign sim_hs = hs;
`endif
`endif

`GAMETOP #(.CLK_SPEED(CLK_SPEED))
u_game(
    .rst         ( game_rst       ),
    .clk         ( clk_sys        ),
    .cen12       ( cen12          ),
    .cen6        ( cen6           ),
    .cen3        ( cen3           ),
    .cen1p5      ( cen1p5         ),
    .red         ( red            ),
    .green       ( green          ),
    .blue        ( blue           ),
    .LHBL        ( LHBL           ),
    .LVBL        ( LVBL           ),
    .LHBL_dly    ( LHBL_dly       ),
    .LVBL_dly    ( LVBL_dly       ),
    .HS          ( hs             ),
    .VS          ( vs             ),

    .start_button( game_start     ), // input
    .coin_input  ( game_coin      ), // input
    .joystick1   ( game_joy1[6:0] ), // inputs
    .joystick2   ( game_joy2[6:0] ), // inputs

    // Sound control
    .enable_fm   ( enable_fm      ),
    .enable_psg  ( enable_psg     ),
     
    // PROM programming
    .ioctl_addr  ( ioctl_addr     ),
    .ioctl_data  ( ioctl_data     ),
    .ioctl_wr    ( ioctl_wr       ),
    .prog_addr   ( prog_addr      ),
    .prog_data   ( prog_data      ),
    .prog_mask   ( prog_mask      ),
    .prog_we     ( prog_we        ),

    // ROM load
    .downloading ( ioctl_download ),
    .loop_rst    ( loop_rst       ),
    .sdram_req   ( sdram_req      ),
    .sdram_addr  ( sdram_addr     ),
    .data_read   ( data_read      ),
    .sdram_ack   ( sdram_ack      ),
    .data_rdy    ( data_rdy       ),
    .refresh_en  ( refresh_en     ),

    // DIP switches
    .status      ( statusdips     ),
    .dip_pause   ( dip_pause      ),
    .dip_flip    ( dip_flip       ),
    .dip_test    ( dip_test       ),
    .dip_fxlevel ( dip_fxlevel    ),  

    // sound
    .snd         ( snd            ),
    .sample      (                ),
    // Debug
    .gfx_en      ( gfx_en         )
);


 //--- Joystick read with sega 6 button support----------------------
reg  [7:0]  osd_keys = 8'b11111111;
reg clk_sega_s;
localparam CLOCK = 48000; // clk_rom speed
localparam TIMECLK = (9 * (CLOCK / 1000)); //calculate 9us state time based on input clock
reg [9:0] delay = TIMECLK;

always@(posedge clk_rom)
begin
    delay <= delay - 10'd1;
    
    if (delay == 10'd0) 
        begin
            clk_sega_s <= ~clk_sega_s;
            delay <= TIMECLK;
        end
end

    reg [3:0]joyControls_s = 4'b1111; //-- Pause, 2P Start, 1P Start, Coin
    reg [11:0]joy1_s = 12'b111111111111;   
    reg [11:0]joy2_s = 12'b111111111111; 
    reg joyP7_s;

    reg [7:0]state_v = 8'd0;
    reg [7:0]count_cycles = 8'd100;
    reg j1_sixbutton_v = 1'b0;
    reg j2_sixbutton_v = 1'b0;
    reg [1:0]  sega_edge;

    always @(posedge clk_rom) 
    begin
        sega_edge <= {sega_edge[0], clk_sega_s};
        if (sega_edge == 2'b01)
        begin
            state_v <= state_v + 1;            
            case (state_v)          //-- joy_s format MXYZ SACB RLDU
                8'd0:  
                begin
                    if (count_cycles != 8'd0)
                        count_cycles <= count_cycles - 8'd1;
                    joyP7_s <=  1'b0;
                end

                8'd1:
                    joyP7_s <=  1'b1;

                8'd2:
                    begin
                        joy1_s[3:0] <= {joy1_right_i, joy1_left_i, joy1_down_i, joy1_up_i}; //-- R, L, D, U
                        joy2_s[3:0] <= {joy2_right_i, joy2_left_i, joy2_down_i, joy2_up_i}; //-- R, L, D, U
                        joy1_s[5:4] <= {joy1_p9_i, joy1_p6_i}; //-- C, B
                        joy2_s[5:4] <= {joy2_p9_i, joy2_p6_i}; //-- C, B                    
                        joyP7_s <= 1'b0;
                        j1_sixbutton_v <= 1'b0; //-- Assume it's not a six-button controller
                        j2_sixbutton_v <= 1'b0; //-- Assume it's not a six-button controller
                    end
                    
                8'd3:
                    begin
                        if (joy1_right_i == 1'b0 && joy1_left_i == 1'b0) // it's a megadrive controller
                                joy1_s[7:6] <= { joy1_p9_i , joy1_p6_i }; //-- Start, A
                        else
                                joy1_s[7:4] <= { 1'b1, 1'b1, joy1_p9_i, joy1_p6_i }; //-- read A/B as master System
                            
                        if (joy2_right_i == 1'b0 && joy2_left_i == 1'b0) // it's a megadrive controller
                                joy2_s[7:6] <= { joy2_p9_i , joy2_p6_i }; //-- Start, A
                        else
                                joy2_s[7:4] <= { 1'b1, 1'b1, joy2_p9_i, joy2_p6_i }; //-- read A/B as master System

                            
                        joyP7_s <= 1'b1;
                    end
                    
                8'd4:  
                    joyP7_s <= 1'b0;

                8'd5:
                    begin
                        if (joy1_right_i == 1'b0 && joy1_left_i == 1'b0 && joy1_down_i == 1'b0 && joy1_up_i == 1'b0 )
                            j1_sixbutton_v <= 1'b1; // --it's a six button
                        
                        
                        if (joy2_right_i == 1'b0 && joy2_left_i == 1'b0 && joy2_down_i == 1'b0 && joy2_up_i == 1'b0 )
                            j2_sixbutton_v <= 1'b1; // --it's a six button
                        
                        
                        joyP7_s <= 1'b1;
                    end
                    
                8'd6:
                    begin
                        if (j1_sixbutton_v == 1'b1)
                            joy1_s[11:8] <= { joy1_right_i, joy1_left_i, joy1_down_i, joy1_up_i }; //-- Mode, X, Y e Z
                        
                        
                        if (j2_sixbutton_v == 1'b1)
                            joy2_s[11:8] <= { joy2_right_i, joy2_left_i, joy2_down_i, joy2_up_i }; //-- Mode, X, Y e Z
                        
                        
                        joyP7_s <= 1'b0;
                    end 
                    
                default:
                    joyP7_s <= 1'b1;
                    
            endcase
            joyControls_s[0] <= (joy1_s[11]|joy1_s[4]); //-- Coin: Joy1 Mode + B
            joyControls_s[1] <= (joy1_s[11]|joy1_s[10]); //-- 1p start: Joy1 Mode + X
            joyControls_s[2] <= (joy1_s[11]|joy1_s[9]); //-- 2p start: Joy1 Mode + Y
            if (count_cycles == 8'd0)
            begin
                joyControls_s[3] <= (joy1_s[7]|joy1_s[6]); //-- Pause: Start + A
                if (joy1_s[7] || joy1_s[11])
                    osd_keys[7:0] <= { 1'b1, 1'b1, 1'b1, joy1_s[4]&joy1_s[5]&joy1_s[6], joy1_s[3], joy1_s[2], joy1_s[1], joy1_s[0]};
                else
                    osd_keys[7:0] <= { 1'b0, 1'b1, 1'b1, joy1_s[4]&joy1_s[5]&joy1_s[6], joy1_s[3], joy1_s[2], joy1_s[1], joy1_s[0]};
            end
            if (ioctl_download) osd_keys[7:0] <= 8'b11111111;
        end
    end
    
    assign joy_p7_o = joyP7_s;
    
    //-------------------------------------------------------------

endmodule
/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/`timescale 1ns/1ps

/*

	Schematic sheet: 85606-A-2-6/8 Character video RAM

*/

module jt_gng_a7(
	input		G4H,		// from 5/8
	input		V4,
	input		V2,
	input		V1,
	input		H4,
	input		G6M,
	input		G4_3H,
	input		CHVFLIP,	// from 6/8
	input		CHHFLIP,
	input		CHHFLIPq,
	input [9:0] AC,
	input		FLIP,		// from 2/8
	output		CH6M,		// to 8/8
	output		CHARZ,
	output		CHARY
);

reg VV4,VV2,VV1;

assign CH6M = G6M;

always @(posedge G4H) {VV4,VV2,VV1} <= {V4,V2,V1};
wire vflip =  CHVFLIP ^ FLIP;
wire flip_9D = FLIP;
wire hflip = ~CHHFLIP ^ flip_9D;

wire [3:0] addr;
assign #2 addr = { {3{vflip}} ^ {VV4,VV2,VV1}, hflip^H4 };

reg [7:0] mem_11e[0:16383];

initial
	$readmemh("../../rom/mm01.11e.hex", mem_11e);

reg [7:0] data;

always @(AC,addr)
	data <= mem_11e[ {AC,addr} ];

wire [1:0] S;
wire [3:0] QZ, QY;

jt74194 u_10D(
	.D		( data[7:4] ),
	.S		( S 		),
	.clk	( CH6M 		),
	.cl_b	( 1'b1		),
	.L		( 1'b0		),
	.R		( 1'b0		),
	.Q		( QZ		)
);

jt74194 u_11D(
	.D		( data[3:0] ),
	.S		( S 		),
	.clk	( CH6M 		),
	.cl_b	( 1'b1		),
	.L		( 1'b0		),
	.R		( 1'b0		),
	.Q		( QY		)
);

jt74157 u_10C(
	.A	( {1'b1, G4_3H, QZ[0], QY[0]} ),
	.B	( {G4_3H, 1'b1, QZ[3], QY[3]} ),
	.sel( CHHFLIPq ^ ~flip_9D ), // according to schematic CHHFLIPq ^ flip_9D
	// but the output is corrupted unless I take ~flip_9D
	.st_l( 1'b0 ),
	.Y	( {S, CHARZ, CHARY} )
);

endmodule // jt_gng_a7
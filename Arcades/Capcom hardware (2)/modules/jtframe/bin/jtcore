#!/bin/bash

function show_usage() {
    cat << EOF
JTCORE compilation tool. (c) Jose Tejada 2019, @topapate
    First argument is the project name, like gng, or 1943

    -skip       skips compilation and goes directly to prepare the release file
                using the RBF file available.
    -d          Defines a verilog macro. Macros are removed from qsf file after
                compilation.
                Useful macros:
                    NOSOUND             the core is compiled without sound (faster)
                    MISTER_VIDEO_MIXER  uses the Mr video modules
                    JTFRAME_SCAN2X      uses simple scan doubler from JTFRAME
                    MISTER_NOHDMI       skips MiSTer HDMI related logic. Speeds up compilation.
    -qq         very quick compilation. Defines NOSUND and MISTER_NOHDMI. Does not alter compilation target.
    -q | -quick quick compilation. Defines NOSUND.
    -ftp-folder Destination folder in MiSTer FTP to which final RBF will be copied
                The full name is made by preceding this by a _
                and adding /cores after it.
    -git        adds the release file to git
    -mister     compiles MiSTer version. Default is MiST.
    -seed       sets random seed for compilation
    -sidi       compiles SIDI version. This uses MiST's folders and settings but different pinout
    -prog       programs the FPGA
    -prog-only  skips compilations and directly programs the FPGA
    -sim        prepare quartus simulation
    -rename     name used for copying the RBF file into the SD card
    -help       displays this message
EOF
   exit 0
}

# Is the root folder environment variable set

if [ "$JTROOT" = "" ]; then
    echo "ERROR: Missing JTROOT environment variable. Define it to"
    echo "point to the root folder of the project to compile."
    exit 1
fi

cd $JTROOT
MODULES=$JTROOT/modules
JTFRAME=$MODULES/jtframe
# This is the FTP host to which files will be copied
HOST=mr
FTP_FOLDER=Arcade

if [ ! -e "$JTFRAME" ]; then
    echo "ERROR: cannot locate jtframe folder."
    echo "       It should be at $JTFRAME"
    exit 1
fi

# Is the project defined?
PRJ=$1

case "$PRJ" in
    "")
        echo "ERROR: Missing project name."
        show_usage
        exit 1;;
    -help | -h | --help)
        show_usage;;
esac

if [ -e $JTROOT/cores/$PRJ ]; then
    PRJPATH=$JTROOT/cores/$PRJ
elif [ -e $JTROOT/$PRJ ]; then
    PRJPATH=$JTROOT/$PRJ
elif [ -e $JTROOT/hdl ]; then
    PRJPATH=$JTROOT
else
    echo "ERROR: Cannot find a valid project path"
    exit 1
fi
shift

SIM=FALSE
GIT=FALSE
PROG=FALSE
MIST=mist
SKIP_COMPILE=FALSE
RENAME=

# Verilog macros
MACROS=
SEED=1
OUTPUTDIR=output_files

function parse_args {
    while [ $# -gt 0 ]; do
        case "$1" in
            -skip | -s) SKIP_COMPILE=TRUE;;
            -git | -g) GIT=TRUE;;
            -prog | -p) PROG=TRUE;;
            -prog-only | -w)
                PROG=TRUE
                SKIP_COMPILE=TRUE;;
            -mister | -mr)
                MIST=mister;;
            -mist)
                MIST=mist;;
            -sidi)
                MIST=sidi;;
            -sim)
                SIM=TRUE;;
            -rename)
                shift
                RENAME=$1;;
            -ftp-folder)
                shift
                FTP_FOLDER=$1;;
            -seed | -s)
                shift
                SEED=$1;;
            -gngvga)
                MACROS="JTFRAME_VGA SCAN2X_TYPE=1 $MACROS";;
            -d)
                shift
                MACROS="$1 $MACROS"
                ;;
            -q|-quick)
                MACROS="$MACROS NOSOUND";;
            -qq|-veryquick)
                MACROS="$MACROS NOSOUND MISTER_NOHDMI";;
            -help | -h)
                show_usage;;
            *)  echo "ERROR: Unknown option $1";
                exit 1;;
        esac
        shift
    done
}

parse_args $JTCORE_DEFAULT
parse_args $*

OUTPUTDIR=output_${SEED}

# Check Quartus path
case $MIST in
    mister)
        if ! (which quartus_sh|grep 17 -q); then
            PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:.
            PATH=$PATH:$HOME/intelFPGA_lite/17.1/quartus/bin
            export PATH
        fi;;
    mist|sidi)
        if ! (which quartus_sh|grep 13 -q); then
            PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:.
            PATH=$PATH:$HOME/altera/13.1/quartus/bin
            export PATH
        fi;;
esac

function escape_bars {
    echo ${1//\//\\/}
}

# qsf line to disable SOUND synthesis
# set_global_assignment -name VERILOG_MACRO "NOSOUND=<None>"
function copy_templates {
    datestr=$(date "+%H:%M:%S %B %e, %Y")
    # Replace core name and path to modules
    # do not leave a space in the line below sedstr!
    sedstr='s/${CORENAME}/'${PRJ}'/g;s/${DATE}/'${datestr}'/g;s/${MODULES}/'$(escape_bars ${MODULES})'/g;s/${PRJPATH}/'$(escape_bars ${PRJPATH})'/g;'\
's/${OUTPUTDIR}/'${OUTPUTDIR}'/g'
    sed "$sedstr" $JTFRAME/hdl/$MIST/$MIST.qpf > $PRJPATH/$MIST/jt${PRJ}.qpf
    sed "$sedstr" $JTFRAME/hdl/$MIST/$MIST.qsf > $PRJPATH/$MIST/jt${PRJ}.qsf
    # TCL in HDL folder is always appended
    TCL=$PRJPATH/hdl/jt${PRJ}.tcl
    if [ ! -e $TCL ]; then
        echo "WARNING: No project TCL file. Creating one empty."
        touch $TCL
    fi
    cat $TCL >> $PRJPATH/$MIST/jt${PRJ}.qsf
    # TCL in platform folder is added if it exists
    if [ -e $PRJPATH/$MIST/jt${PRJ}.tcl ]; then
        cat $PRJPATH/$MIST/jt${PRJ}.tcl >> $PRJPATH/$MIST/jt${PRJ}.qsf            
    fi
}

function link_if_missing {
    if [ ! -e $1 ]; then
        if [ -e ../mist/$1 ]; then
            ln -sr ../mist/$1 && echo "Linked file $1" && git add $1 -f
        else 
            echo WARNING: Missing $1
        fi
    fi
}

echo ==============================================
echo jt$PRJ $MIST compilation starts at $(date +%T) with seed $SEED
if [ "$MACROS" != "" ]; then
    echo INFO: Macros used: $MACROS
else
    echo INFO: No macros.
fi

################ compilation
if [ $SKIP_COMPILE = FALSE ]; then
    if ! which quartus_sh; then 
        echo "ERROR: cannot find quartus_sh in the path. Please add the correct path"
        echo "to the PATH environment variable"
        exit 1
    fi
    # Create MiST(er) folders if they don't exist
    mkdir -p $PRJPATH/{mist,mister,sidi}
    # Update message file
    if [ -e bin/jt${PRJ}_msg.py ]; then
        cd bin
        jt${PRJ}_msg.py || echo "WARNING: No message file"
    fi
    cd $PRJPATH/$MIST || exit 1
    echo "Moved to folder " $(pwd)
    # Link files from MiST folder if missing
    for i in msg.hex conf_str.v jt${PRJ}.tcl; do
        link_if_missing $i
    done
    ############################################3
    # Credits via jtframe_credits:
    if [[ -e $PRJPATH/patrons/msg && ! -e msg ]]; then
        ln -sr $PRJPATH/patrons/msg
    fi
    if [ -e msg ]; then
        $JTFRAME/bin/msg2hex msg
    fi
    # Update Avatars
    if [ -e lut ]; then
        $JTFRAME/bin/lut2hex lut
        if [ -e $PRJPATH/patrons/avatars ]; then
            avatar.py $PRJ > /tmp/avatar_$PRJ.log || ( cat /tmp/avatar_$PRJ.log; exit 1 )
        fi
    fi
    if [ ! -e font0.hex ]; then
        ln -s $JTFRAME/bin/font0.hex
    fi
    ############################################3
    # Recompile
    rm -rf db incremental_db output_files
    mkdir -p $JTROOT/log/$MIST
    LOG="$JTROOT/log/$MIST/jt$PRJ.log"
    echo Log file: $LOG
    copy_templates
    # Prepare macros
    for m in $MACROS; do
        echo -e "\n" set_global_assignment -name VERILOG_MACRO \"$m\" >> jt$PRJ.qsf
    done
    echo -e "\nset_global_assignment -name seed $SEED" >> jt$PRJ.qsf
    if [ $SIM = FALSE ]; then
        # Compilation
        quartus_sh --flow compile jt$PRJ > "$LOG"
        if ! grep "Full Compilation was successful" "$LOG"; then
            grep -i error "$LOG" -A 2
            echo "ERROR while compiling the project. Aborting"
            exit 1
        fi
        grep Worst-case "$LOG"
    else
        # Simulation
        echo Preparing simulation netlist. Logfile:
        echo $LOG
        quartus_map jt$PRJ --64bit --parallel=1 --efort=fast > "$LOG"
        exit $?
    fi
fi

function append_if_exists {
    val=
    while [ $# -gt 0 ]; do
        if [ -e "$1" ]; then
            val="$val $1"
        fi
        shift
    done
    echo $val
}

################# Store output file
# Rename output file
cd $JTROOT
mkdir -p releases
RELEASE=jt${PRJ}_$(date +"%Y%m%d")
if [ $SEED != 1 ]; then
    SEED_SUFFIX=_$SEED
fi
if [ $MIST = mister ]; then
    RBF=$PRJPATH/${MIST}/$OUTPUTDIR/jt$PRJ.rbf
else
    RBF=$PRJPATH/${MIST}/jt$PRJ.rbf
fi
if [ ! -e $RBF ]; then
    echo "ERROR: file $RBF does not exist. You need to recompile."
    exit 1
fi
cp $RBF $RELEASE.rbf
ZIPFOLDER=$MIST
mkdir -p releases/$ZIPFOLDER
# zip --update --junk-paths releases/$ZIPFOLDER/${RELEASE}.zip ${RELEASE}.rbf README.txt $* \
#     $(append_if_exists rom/{$PRJ/build_rom.sh,$PRJ/build_rom.ini,build_rom_$PRJ.bat} doc/jt$PRJ.txt )
if [ -e $JTROOT/../jtbin ]; then
    BINFOLDER=$JTROOT/../jtbin/$ZIPFOLDER/$PRJ
    mkdir -p $BINFOLDER
    mkdir -p $BINFOLDER/releases
    cp $RELEASE.rbf $BINFOLDER/releases/${RELEASE}${SEED_SUFFIX}.rbf -v
    OTHER=$(append_if_exists rom/{$PRJ/build_rom.sh,$PRJ/build_rom.ini,build_rom_$PRJ.bat} doc/jt$PRJ.txt )
    if [ "$OTHER" != "" ]; then cp $OTHER $BINFOLDER; fi
    # If there is a mounted SD card with the right name, copy the file to it too
    if [ -d /media/$USER/${MIST^^} ]; then
        if [ -z $RENAME ]; then
            RENAME=JT${PRJ^^}.rbf
        else
            RENAME=$RENAME.rbf
        fi
        cp -v $RELEASE.rbf /media/$USER/${MIST^^}
        # Copy the file to the core folder so it can be used by ARC files
        if [ -d /media/$USER/${MIST^^}/JT${PRJ^^} ]; then
            cp -v $RELEASE.rbf /media/$USER/${MIST^^}/JT${PRJ^^}/$RENAME
        fi
    fi
    if [ $MIST = mister ]; then
        # If Mr is online, copy the file to the _Arcade/cores folder
        if ping $HOST -w 1 -c 1 > /dev/null; then
            ftp -inv $HOST <<EOF
user root 1
cd /media/fat/_${FTP_FOLDER}/cores
mput $RELEASE.rbf
bye
EOF
        fi
    fi
fi
rm $RELEASE.rbf

# Add to git
if [ $GIT = TRUE ]; then
    git add -f $PRJ/${MIST}/msg.hex
    git add -f releases/$MIST/$RELEASE.zip
fi

if [ $PROG = TRUE ]; then
    if [ $MIST = mister ]; then
        CABLE="DE-SoC"
        cd $PRJPATH/mister
        FILE=jtag.cdf
    else
        CABLE="USB-Blaster(Altera)"
        FILE=$PRJPATH/${MIST}/jt$PRJ.cdf
    fi
    quartus_pgm -c "$CABLE" $FILE
fi

echo completed at $(date)
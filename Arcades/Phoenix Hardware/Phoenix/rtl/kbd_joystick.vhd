--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

entity Kbd_Joystick is
port (
  Clk          : in std_logic;
  KbdInt       : in std_logic;
  KbdScanCode  : in std_logic_vector(7 downto 0);
  JoyBCPPFRLDU  : out std_logic_vector(12 downto 0)
);
end Kbd_Joystick;

architecture Behavioral of Kbd_Joystick is

signal IsReleased : std_logic;

begin 

process(Clk)
begin
  if rising_edge(Clk) then
  
    if KbdInt = '1' then
      if KbdScanCode = "11110000" then IsReleased <= '1'; else IsReleased <= '0'; end if; 
      if KbdScanCode = "01110101" then JoyBCPPFRLDU(0) <= not(IsReleased); end if; -- up    arrow : 0x75
      if KbdScanCode = "01110010" then JoyBCPPFRLDU(1) <= not(IsReleased); end if; -- down  arrow : 0x72
      if KbdScanCode = "01101011" then JoyBCPPFRLDU(2) <= not(IsReleased); end if; -- left  arrow : 0x6B
      if KbdScanCode = "01110100" then JoyBCPPFRLDU(3) <= not(IsReleased); end if; -- right arrow : 0x74
      if KbdScanCode = "00101001" then JoyBCPPFRLDU(4) <= not(IsReleased); end if; -- space : 0x29
      if KbdScanCode = "00000101" then JoyBCPPFRLDU(5) <= not(IsReleased); end if; -- F1 : 0x05
      if KbdScanCode = "00000110" then JoyBCPPFRLDU(6) <= not(IsReleased); end if; -- F2 : 0x06
      if KbdScanCode = "00000100" then JoyBCPPFRLDU(7) <= not(IsReleased); end if; -- F3 : 0x04
      if KbdScanCode = "00010100" then JoyBCPPFRLDU(8) <= not(IsReleased); end if; -- ctrl : 0x14		
	
      if KbdScanCode = x"1d" then JoyBCPPFRLDU(9)  <= not(IsReleased); end if; -- W
      if KbdScanCode = x"1b" then JoyBCPPFRLDU(10) <= not(IsReleased); end if; -- S
      if KbdScanCode = x"1c" then JoyBCPPFRLDU(11) <= not(IsReleased); end if; -- A
      if KbdScanCode = x"23" then JoyBCPPFRLDU(12) <= not(IsReleased); end if; -- D

    end if;
 
  end if;
end process;

end Behavioral;



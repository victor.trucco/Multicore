/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///============================================================================
//
//  Multicore 2 Top by Victor Trucco
//
//============================================================================

`default_nettype none

module LunarLander_MC2(
		// Clocks
	input wire	clock_50_i,

	// Buttons
	input wire [4:1]	btn_n_i,

	// SRAMs (AS7C34096)
	output wire	[18:0]sram_addr_o  = 18'b0000000000000000000,
	inout wire	[7:0]sram_data_io	= 8'bzzzzzzzz,
	output wire	sram_we_n_o		= 1'b1,
	output wire	sram_oe_n_o		= 1'b1,
		
	// SDRAM	(H57V256)
	output [12:0] SDRAM_A,
	output  [1:0] SDRAM_BA,
	inout  [15:0] SDRAM_DQ,
	output        SDRAM_DQMH,
	output        SDRAM_DQML,
	output        SDRAM_CKE,
	output        SDRAM_nCS,
	output        SDRAM_nWE,
	output        SDRAM_nRAS,
	output        SDRAM_nCAS,
	output        SDRAM_CLK,

	// PS2
	inout wire	ps2_clk_io			= 1'bz,
	inout wire	ps2_data_io			= 1'bz,
	inout wire	ps2_mouse_clk_io  = 1'bz,
	inout wire	ps2_mouse_data_io = 1'bz,

	// SD Card
	output wire	sd_cs_n_o			= 1'b1,
	output wire	sd_sclk_o			= 1'b0,
	output wire	sd_mosi_o			= 1'b0,
	input wire	sd_miso_i,

	// Joysticks
	input wire	joy1_up_i,
	input wire	joy1_down_i,
	input wire	joy1_left_i,
	input wire	joy1_right_i,
	input wire	joy1_p6_i,
	input wire	joy1_p9_i,
	input wire	joy2_up_i,
	input wire	joy2_down_i,
	input wire	joy2_left_i,
	input wire	joy2_right_i,
	input wire	joy2_p6_i,
	input wire	joy2_p9_i,
	output wire	joyX_p7_o			= 1'b1,

	// Audio
	output        AUDIO_L,
	output        AUDIO_R,
	input wire	ear_i,
	output wire	mic_o					= 1'b0,

		// VGA
	output  [4:0] VGA_R,
	output  [4:0] VGA_G,
	output  [4:0] VGA_B,
	output        VGA_HS,
	output        VGA_VS,

		// HDMI
	output wire	[7:0]tmds_o			= 8'b00000000,

		//STM32
	input wire	stm_tx_i,
	output wire	stm_rx_o,
	output wire	stm_rst_o			= 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
		
	inout wire	stm_b8_io, 
	inout wire	stm_b9_io,

	input         SPI_SCK,
	output        SPI_DO,
	input         SPI_DI,
	input         SPI_SS2

);

`include "rtl\build_id.v" 

localparam CONF_STR = {
	"P,LunarLander.dat;",
	"S,DAT,Alternative ROM...;",
	"O3,Test,Off,On;",
	"O45,Language,English,Spanish,French,German;",
	"O68,Fuel,450,600,750,900,1100,1300,1550,1800;",
	"T0,Reset;",
	"V,v1.00.",`BUILD_DATE
};

assign sram_we_n_o	= 1'b1;
assign sram_oe_n_o	= 1'b1;
assign stm_rst_o		= 1'bz;

assign AUDIO_R = AUDIO_L;
assign SDRAM_CLK = clk_72;
assign SDRAM_CKE = 1;

wire clk_72, clk_50, clk_6, locked;
pll pll(
	.inclk0(clock_50_i),
	.c0(clk_72), //memclk = 12x sysclk
	.c1(clk_50), //video clk
	.c2(clk_6),  //sysclk
	.locked(locked)
);

wire [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire        scandoublerD;
wire        ypbpr;
wire 			hs, vs, hso, vso;
wire 			hb, vb;
wire 			blankn = ~(hb | vb);
wire  [3:0] r, g, b;
wire  [7:0] ro, go, bo;
wire			vgade;
wire  [7:0] audio;
wire        key_strobe;
wire        key_pressed;
wire  [7:0] key_code;
wire        ioctl_downl;
wire  [7:0] ioctl_index;
wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire  [7:0] ioctl_dout;

data_io #(
	.STRLEN(($size(CONF_STR)>>3)))
data_io(
	.clk_sys       ( clk_72       ),
	.SPI_SCK       ( SPI_SCK      ),
	.SPI_SS2       ( SPI_SS2      ),
	.SPI_DI        ( SPI_DI       ),
	.SPI_DO        ( SPI_DO       ),
	
	.data_in			( osd_s & keys_s ),
	.conf_str		( CONF_STR 		),
	.status			( status 		),
	
	.ioctl_download( ioctl_downl  ),
	.ioctl_index   ( ioctl_index  ),
	.ioctl_wr      ( ioctl_wr     ),
	.ioctl_addr    ( ioctl_addr   ),
	.ioctl_dout    ( ioctl_dout   )
	);

wire [12:0] cpu_rom_addr;
wire [15:0] cpu_rom_data;
wire [12:0] vector_rom_addr;
wire [15:0] vector_rom_data;
wire  [9:0] vector_ram_addr;
wire [15:0] vector_ram_din;
wire [15:0] vector_ram_dout;
wire        vector_ram_we;
wire        vector_ram_cs1;
wire        vector_ram_cs2;

reg port1_req, port2_req;
sdram sdram(
	.*,
	.init_n        ( locked   ),
	.clk           ( clk_72      ),

	// port1 used for main CPU
	.port1_req     ( port1_req    ),
	.port1_ack     ( ),
	.port1_a       ( ioctl_addr[23:1] ),
	.port1_ds      ( {ioctl_addr[0], ~ioctl_addr[0]} ),
	.port1_we      ( ioctl_downl ),
	.port1_d       ( {ioctl_dout, ioctl_dout} ),
	.port1_q       ( ),

	.cpu1_addr     ( ioctl_downl ? 15'h7fff : {3'b001, vector_rom_addr[12:1]} ),
	.cpu1_q        ( vector_rom_data ),
	.cpu2_addr     ( ioctl_downl ? 15'h7fff : {3'b000, cpu_rom_addr[12:1]} ),
	.cpu2_q        ( cpu_rom_data ),

	// port2 is for vector RAM
	.port2_req     ( port2_req ),
	.port2_ack     ( ),
	.port2_a       ( vector_ram_addr_last ),
	.port2_ds      ( {vector_ram_cs2, vector_ram_cs1} ),
	.port2_we      ( vector_ram_we_last ),
	.port2_d       ( vector_ram_din ),
	.port2_q       ( vector_ram_dout )
	);

reg  [9:0] vector_ram_addr_last = 0;
reg        vector_ram_we_last = 0;

always @(posedge clk_72) begin
	reg        ioctl_wr_last = 0;

	ioctl_wr_last <= ioctl_wr;
	if (ioctl_downl) begin
		if (~ioctl_wr_last && ioctl_wr) begin
			port1_req <= ~port1_req;
		end
	end

	if ((vector_ram_cs1 || vector_ram_cs2) && (vector_ram_addr_last != vector_ram_addr || vector_ram_we_last != vector_ram_we)) begin
		vector_ram_addr_last <= vector_ram_addr;
		vector_ram_we_last <= vector_ram_we;
		port2_req <= ~port2_req;
	end
end

reg reset = 1;
reg rom_loaded = 0;
always @(posedge clk_6) begin
	reg ioctl_downlD;
	ioctl_downlD <= ioctl_downl;

	if (ioctl_downlD & ~ioctl_downl) rom_loaded <= 1;
	reset <= status[0] | ~btn_n_i[4] | ~rom_loaded;
end

	
LLANDER_TOP LLANDER_TOP (
	.ROT_LEFT_L(~m_left),
	.ROT_RIGHT_L(~m_right),
	.ABORT_L(~m_fireB),
	.GAME_SEL_L(~m_fireA),
	.START_L(~btn_one_player),
	.COIN1_L(~btn_coin),
	.COIN2_L(1'b1),
	.THRUST(thrust),
	.DIAG_STEP_L(1'b1),
	.SLAM_L(1'b1),
	.SELF_TEST_L(~status[3]), 
	.START_SEL_L(1'b1),
   .AUDIO_OUT(audio), 
   .VIDEO_R_OUT(r),
   .VIDEO_G_OUT(g),
   .VIDEO_B_OUT(b),
	.LAMP2(lamp2),
	.LAMP3(lamp3),
	.LAMP4(lamp4),
	.LAMP5(lamp5),
   .HSYNC_OUT(hs),
   .VSYNC_OUT(vs),
	.VID_HBLANK(hb),
	.VID_VBLANK(vb),
	.VGA_DE(vgade),
	.DIP({1'b0,1'b0,status[4],status[5],~status[6],1'b1,status[7],status[8]}),//todo dip full
   .RESET_L(~(reset)),
	.clk_6(clk_6),
	.clk_50(clk_50),
	.cpu_rom_addr    (cpu_rom_addr),
	.cpu_rom_data    (cpu_rom_addr[0] ? cpu_rom_data[15:8] : cpu_rom_data[7:0] ),
	.vector_rom_addr (vector_rom_addr), 
	.vector_rom_data (vector_rom_addr[0] ? vector_rom_data[15:8] : vector_rom_data[7:0]),
	.vector_ram_addr (vector_ram_addr),
	.vector_ram_din  (vector_ram_din),
	.vector_ram_dout (vector_ram_dout),
	.vector_ram_we   (vector_ram_we),
	.vector_ram_cs1  (vector_ram_cs1),
	.vector_ram_cs2  (vector_ram_cs2)
   );

reg ce_pix;
always @(posedge clk_50) ce_pix <= ~ce_pix;

ovo #(
	.COLS(1), 
	.LINES(1), 
	.RGB(24'hFF00FF)) 
diff (
	.i_r({r,r}),
	.i_g({g,g}),
	.i_b({b,b}),
	.i_hs(~hs),
	.i_vs(~vs),
	.i_de(vgade),
	.i_en(ce_pix),
	.i_clk(clk_50),

	.o_r(ro),
	.o_g(go),
	.o_b(bo),
	.o_hs(hso),
	.o_vs(vso),
	.o_de(),
	.ena(diff_count > 0),
	.in0(difficulty),
	.in1()
	);

reg [7:0] thrust = 0;

// 1 second = 6,000,000 cycles (duh)
// If we want to go from zero to full throttle in 1 second we tick every
// 23,529 cycles.
always @(posedge clk_6) begin :thrust_count
	int thrust_count;
	thrust_count <= thrust_count + 1'd1;
	if (thrust_count == 'd23529) begin
		thrust_count <= 0;
		if (m_down && thrust > 0)
			thrust <= thrust - 1'd1;

		if (m_up && thrust < 'd254)
			thrust <= thrust + 1'd1;
	end
end

int diff_count = 0;
always @(posedge clk_6) begin
	if (diff_count > 0)
		diff_count <= diff_count - 1;
	if (~m_fireB)
		diff_count <= 'd60_000_000; // 10 seconds
end

wire lamp2, lamp3, lamp4, lamp5;
wire [1:0] difficulty;
always_comb begin
	if(lamp5)
		difficulty = 2'd3;
	else if(lamp4)
		difficulty = 2'd2;
	else if(lamp3)
		difficulty = 2'd1;
	else
		difficulty = 2'd0;
end


wire [5:0] vga_r_s;	
wire [5:0] vga_g_s;	
wire [5:0] vga_b_s;	
	
mist_video #(.COLOR_DEPTH(6)) mist_video(
	.clk_sys        ( clk_50           ),
	.SPI_SCK        ( SPI_SCK          ),
	.SPI_SS3        ( SPI_SS2          ),
	.SPI_DI         ( SPI_DI           ),
	.R              ( blankn ? ro[7:2] : 0   ),
	.G              ( blankn ? go[7:2] : 0   ),
	.B              ( blankn ? bo[7:2] : 0   ),
	.HSync          ( ~hso             ),
	.VSync          ( ~vso             ),
	.VGA_R          ( vga_r_s          ),
	.VGA_G          ( vga_g_s          ),
	.VGA_B          ( vga_b_s          ),
	.VGA_VS         ( VGA_VS           ),
	.VGA_HS         ( VGA_HS           ),
	.scandoubler_disable(1),//scandoublerD ),
	.no_csync       ( 1'b1 ),
	.osd_enable 	 ( osd_enable )
	
	);

assign VGA_R = vga_r_s[5:1];
assign VGA_G = vga_g_s[5:1];
assign VGA_B = vga_b_s[5:1];
/*

user_io #(.STRLEN(($size(CONF_STR)>>3)))user_io(
	.clk_sys        (clk_6          ),
	.conf_str       (CONF_STR       ),
	.SPI_CLK        (SPI_SCK        ),
	.SPI_SS_IO      (CONF_DATA0     ),
	.SPI_MISO       (SPI_DO         ),
	.SPI_MOSI       (SPI_DI         ),
	.buttons        (buttons        ),
	.switches       (switches       ),
	.scandoubler_disable (scandoublerD	  ),
	.ypbpr          (ypbpr          ),
	.key_strobe     (key_strobe     ),
	.key_pressed    (key_pressed    ),
	.key_code       (key_code       ),
	.joystick_0     (joystick_0     ),
	.joystick_1     (joystick_1     ),
	.status         (status         )
	);
*/
dac #(
	.C_bits(8))
dac(
	.clk_i(clk_6),
	.res_n_i(1),
	.dac_i(audio),
	.dac_o(AUDIO_L)
	);

//--------- ROM DATA PUMP ----------------------------------------------------
	
		reg [15:0] power_on_s	= 16'b1111111111111111;
		reg [7:0] osd_s = 8'b11111111;
		
		wire hard_reset = ~locked;
		
		//--start the microcontroller OSD menu after the power on
		always @(posedge clk_72) 
		begin
		
				if (hard_reset == 1)
					power_on_s = 16'b1111111111111111;
				else if (power_on_s != 0)
				begin
					power_on_s = power_on_s - 1;
					osd_s = 8'b00111111;
				end 
					
				
				if (ioctl_downl == 1 && osd_s == 8'b00111111)
					osd_s = 8'b11111111;
			
		end 

//-----------------------

wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player = 	~btn_n_i[1] | m_one_player;
wire btn_two_players = 	~btn_n_i[2] | m_two_players;
wire btn_coin  = 			~btn_n_i[3] | m_coin1;

wire kbd_intr;
wire [7:0] kbd_scancode;
wire [7:0] keys_s;

//get scancode from keyboard
io_ps2_keyboard keyboard 
 (
  .clk       ( clk_6 ),
  .kbd_clk   ( ps2_clk_io ),
  .kbd_dat   ( ps2_data_io ),
  .interrupt ( kbd_intr ),
  .scancode  ( kbd_scancode )
);

wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable;

//translate scancode to joystick
kbd_joystick #( .OSD_CMD	( 3'b011 )) k_joystick
(
  .clk         	( clk_6 ),
  .kbdint      	( kbd_intr ),
  .kbdscancode 	( kbd_scancode ), 
  
	.joystick_0 	({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
	.joystick_1		({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
		  
	//-- joystick_0 and joystick_1 should be swapped
	.joyswap 		( 0 ),
		
	//-- player1 and player2 should get both joystick_0 and joystick_1
	.oneplayer		( 1 ),

	//-- tilt, coin4-1, start4-1
	.controls    ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),
		
	//-- fire12-1, up, down, left, right

	.player1     ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
	.player2     ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),
		
	//-- keys to the OSD
	.osd_o		   ( keys_s ),
	.osd_enable 	( osd_enable ),
	
	//-- sega joystick
	.sega_clk  		( hs ),
	.sega_strobe	( joyX_p7_o )
		
		
);

endmodule 
--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library work;
--use work.pace_pkg.all;

package body video_controller_pkg is

  function NULL_RGB return RGB_t is
  begin
    return (others => (others => '0'));
  end NULL_RGB;

  function NULL_TO_BITMAP_CTL return to_BITMAP_CTL_t is
  begin
    return (others => (others => '0'));
  end NULL_TO_BITMAP_CTL;

  function NULL_TO_TILEMAP_CTL return to_TILEMAP_CTL_t is
  begin
    return ((others => '0'), (others => '0'), (others => '0'), (others => (others => '0')));
  end NULL_TO_TILEMAP_CTL;
  
  function NULL_TO_GRAPHICS return to_GRAPHICS_t is
  begin
    return ((others => (others => '0')), 
            (others => (others => '0')), 
            (others => (others => '0')),
            '0', '0', NULL_RGB);
  end NULL_TO_GRAPHICS;

end package body video_controller_pkg;

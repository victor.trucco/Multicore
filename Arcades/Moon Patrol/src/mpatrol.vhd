--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

library work;
use work.pace_pkg.all;
use work.video_controller_pkg.all;

entity mpatrol is
  port
    (
	-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);

		-- SRAMs (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_oe_n_o			: out   std_logic								:= '1';
		
		-- SDRAM	(H57V256)
		sdram_ad_o			: out std_logic_vector(12 downto 0);
		sdram_da_io			: inout std_logic_vector(15 downto 0);

		sdram_ba_o			: out std_logic_vector(1 downto 0);
		sdram_dqm_o			: out std_logic_vector(1 downto 0);

		sdram_ras_o			: out std_logic;
		sdram_cas_o			: out std_logic;
		sdram_cke_o			: out std_logic;
		sdram_clk_o			: out std_logic;
		sdram_cs_o			: out std_logic;
		sdram_we_o			: out std_logic;
	

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joysticks
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p9_i			: in    std_logic;
		joyX_p7_o			: out   std_logic								:= '1';

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
		tmds_o				: out   std_logic_vector(7 downto 0)	:= (others => '0');

		--STM32
		stm_rx_o				: out std_logic		:= 'Z'; -- stm RX pin, so, is OUT on the slave
		stm_tx_i				: in  std_logic		:= 'Z'; -- stm TX pin, so, is IN on the slave
		stm_rst_o			: out std_logic		:= '0'; -- '0' to hold the microcontroller reset line, to free the SD card
		
		stm_a15_io			: inout std_logic;
		stm_b8_io			: inout std_logic		:= 'Z';
		stm_b9_io			: inout std_logic		:= 'Z';
		stm_b12_io			: inout std_logic		:= 'Z';
		stm_b13_io			: inout std_logic		:= 'Z';
		stm_b14_io			: inout std_logic		:= 'Z';
		stm_b15_io			: inout std_logic		:= 'Z'
  );    
  
end mpatrol;

architecture SYN of mpatrol is

  signal   SPI_SCK 			: std_logic;
  signal    SPI_DI 			: std_logic;
  signal SPI_SS2 			: std_logic;
  signal	SPI_SS3 			: std_logic;
  signal	SPI_SS4 			: std_logic;
  signal   SPI_DO 			: std_logic;
  signal    CONF_DATA0 		: std_logic;
  signal init       		: std_logic := '1';  
  signal clk_sys        : std_logic;
  signal clk_vid        : std_logic;
  signal clk_osd			: std_logic;  
  signal clkrst_i       : from_CLKRST_t;
  signal buttons_i      : from_BUTTONS_t;
  signal switches_i     : from_SWITCHES_t;
  signal leds_o         : to_LEDS_t;
  signal inputs_i       : from_INPUTS_t;
  signal video_i        : from_VIDEO_t;
  signal video_o        : to_VIDEO_t;
  --MIST
  signal audio       	: std_logic;
  signal status     		: std_logic_vector(31 downto 0); 
  signal joystick1      : std_logic_vector(7 downto 0);
  signal joystick2      : std_logic_vector(7 downto 0);
  signal kbd_joy 			: std_logic_vector(9 downto 0);	 
  signal switches       : std_logic_vector(1 downto 0);
  signal buttons        : std_logic_vector(1 downto 0);
  signal scan_disable   : std_logic;
  signal ypbpr   			: std_logic;
  signal r  				: std_logic_vector(5 downto 0);
  signal g  				: std_logic_vector(5 downto 0);
  signal b  				: std_logic_vector(5 downto 0);
  signal hs 				: std_logic;
  signal vs 				: std_logic;  
  signal reset 			: std_logic;  
  signal clock_3p58 		: std_logic;
  signal audio_out 		: std_logic_vector(11 downto 0);
  signal sound_data     : std_logic_vector(7 downto 0);
  
component keyboard
   port  (
		clk						:in STD_LOGIC;
		reset						:in STD_LOGIC;
		ps2_kbd_clk				:in STD_LOGIC;
		ps2_kbd_data			:in STD_LOGIC;
		joystick					:out STD_LOGIC_VECTOR(9 downto 0));
end component;
  
component mist_io
   port  (
		clk_sys 					:in STD_LOGIC;
      SPI_SCK 					:in STD_LOGIC;
      CONF_DATA0 				:in STD_LOGIC;
      SPI_DI 					:in STD_LOGIC;
      SPI_DO 					:out STD_LOGIC;
		SPI_SS2					:in STD_LOGIC;
      switches 				:out STD_LOGIC_VECTOR(1 downto 0);
      buttons  				:out STD_LOGIC_VECTOR(1 downto 0);
		scan_disable  			:out STD_LOGIC;
		ypbpr  					:out STD_LOGIC;
      joystick_1  			:out STD_LOGIC_VECTOR(7 downto 0);
      joystick_0 				:out STD_LOGIC_VECTOR(7 downto 0);
      status					:out STD_LOGIC_VECTOR(31 downto 0));

end component;

component video_mist
   port  (
		clk_sys					:in STD_LOGIC;
		ce_pix					:in STD_LOGIC;
		ce_pix_actual			:in STD_LOGIC;
		SPI_SCK					:in STD_LOGIC;
		SPI_SS3					:in STD_LOGIC;
		SPI_DI					:in STD_LOGIC;
		R							:in STD_LOGIC_VECTOR(5 downto 0);
		G							:in STD_LOGIC_VECTOR(5 downto 0);
		B							:in STD_LOGIC_VECTOR(5 downto 0);
		HSync						:in STD_LOGIC;
		VSync						:in STD_LOGIC;		
		VGA_R						:out STD_LOGIC_VECTOR(5 downto 0);
		VGA_G						:out STD_LOGIC_VECTOR(5 downto 0);
		VGA_B						:out STD_LOGIC_VECTOR(5 downto 0);
		VGA_HS					:out STD_LOGIC;
		VGA_VS					:out STD_LOGIC;
		scan_disable			:in STD_LOGIC;
		scanlines				:in STD_LOGIC_VECTOR(1 downto 0);
		hq2x						:in STD_LOGIC;
		ypbpr_full				:in STD_LOGIC;
		line_start				:in STD_LOGIC;
		mono						:in STD_LOGIC);
end component;


begin
--CLOCK
Clock_inst : entity work.Clock
	port map (
		inclk0  			=> clock_50_i,
		c0					=> clock_3p58,--3.58
		c1 				=> clk_osd,--10
		c2 				=> clk_sys,--30
		c3 				=> clk_vid--40
	);	

    clkrst_i.clk_ref <= clock_50_i;
	 clkrst_i.clk(0)	<=	clk_sys;
	 clkrst_i.clk(1)	<=	clk_vid;
	 
--RESET
	process (clk_sys)
		variable count : std_logic_vector (11 downto 0) := (others => '0');
	begin
		if rising_edge(clk_sys) then
			if count = X"FFF" then
				init <= '0';
			else
				count := count + 1;
				init <= '1';
			end if;
		end if;
	end process;

  clkrst_i.arst 		<= init or status(5) or buttons(1);
  clkrst_i.arst_n 	<= not clkrst_i.arst;

  GEN_RESETS : for i in 0 to 3 generate

    process (clkrst_i)
      variable rst_r : std_logic_vector(2 downto 0) := (others => '0');
    begin
      if clkrst_i.arst = '1' then
        rst_r := (others => '1');
      elsif rising_edge(clkrst_i.clk(i)) then
        rst_r := rst_r(rst_r'left-1 downto 0) & '0';
      end if;
      clkrst_i.rst(i) <= rst_r(rst_r'left);
    end process;

  end generate GEN_RESETS;	 
	 
mist_io_inst : mist_io
   port map (
		clk_sys 			=> clk_sys,
      SPI_SCK 			=> SPI_SCK,
      CONF_DATA0 		=> CONF_DATA0,
      SPI_DI 			=> SPI_DI,
      SPI_DO 			=> SPI_DO,
		SPI_SS2			=> SPI_SS2,
      switches 		=> switches,
      buttons  		=> buttons,
		scan_disable 	=> scan_disable,
		ypbpr  			=> ypbpr,
      joystick_1  	=> joystick2,
      joystick_0 		=> joystick1,
      status			=> status
--      ps2_kbd_clk 	=> ps2_kbd_clk,
--      ps2_kbd_data	=> ps2_kbd_data
    );
	 
video_mist_inst : video_mist	 
   port map (
		clk_sys 			=> clk_sys,
		ce_pix			=> clk_osd,
		ce_pix_actual	=> clk_osd,
		SPI_SCK			=> SPI_SCK,
		SPI_SS3			=> SPI_SS3,
		SPI_DI			=> SPI_DI,
		R					=> video_o.rgb.r(9 downto 4),
		G					=> video_o.rgb.g(9 downto 4),
		B					=> video_o.rgb.b(9 downto 4),
		HSync				=> video_o.hsync,
		VSync				=> video_o.vsync,
		VGA_R(5 downto 1)=> vga_r_o,
		VGA_G(5 downto 1)=> vga_g_o,
		VGA_B(5 downto 1)=> vga_b_o,
		VGA_HS			=> vga_hsync_n_o,
		VGA_VS			=> vga_vsync_n_o,
		--ToDo
		scan_disable	=> '1',--scan_disable,
		scanlines		=> status(4 downto 3),
		hq2x				=> status(2),
		ypbpr_full		=> '1',
		line_start		=> '0',
		mono				=> '0'
	 );

    video_i.clk 		<= clk_vid;
    video_i.clk_ena 	<= '1';
    video_i.reset 	<= clkrst_i.rst(1);

		
u_keyboard : keyboard
	port  map(
		clk 				=> clk_sys,
		reset 			=> '0',
		ps2_kbd_clk 	=> ps2_clk_io,
		ps2_kbd_data 	=> ps2_data_io,
		joystick 		=> kbd_joy
);






		inputs_i.jamma_n.coin(1) <= not btn_n_i(3); --kbd_joy(3) or status(1);--ESC
		inputs_i.jamma_n.p(1).start <= btn_n_i(1); --not kbd_joy(1);--TECLA 1	
		inputs_i.jamma_n.p(1).up <= joy1_up_i and joy2_up_i;
		inputs_i.jamma_n.p(1).down <= joy1_down_i and joy2_down_i;
		inputs_i.jamma_n.p(1).left <= not kbd_joy(6) and joy1_left_i and joy2_left_i;
		inputs_i.jamma_n.p(1).right <= not kbd_joy(7) and joy1_right_i and joy2_right_i;		
		inputs_i.jamma_n.p(1).button(1) <=not kbd_joy(0) and joy1_p6_i and joy2_p6_i;--Fire
		inputs_i.jamma_n.p(1).button(2) <= not kbd_joy(4) and joy1_p9_i and joy2_p9_i;--Jump
		inputs_i.jamma_n.p(1).button(3) <= '1';
		inputs_i.jamma_n.p(1).button(4) <= '1';
		inputs_i.jamma_n.p(1).button(5) <= '1';		
		
		inputs_i.jamma_n.p(2).start <= btn_n_i(2); --not kbd_joy(2);-- TECLA 2		

		inputs_i.jamma_n.p(2).button(3) <= '1';
		inputs_i.jamma_n.p(2).button(4) <= '1';
		inputs_i.jamma_n.p(2).button(5) <= '1';	

		-- not currently wired to any inputs
		inputs_i.jamma_n.coin_cnt <= (others => '1');
		inputs_i.jamma_n.coin(2) <= '1';
		inputs_i.jamma_n.service <= '1';
		inputs_i.jamma_n.tilt <= '1';
		inputs_i.jamma_n.test <= '1';


  
moon_patrol_sound_board : entity work.moon_patrol_sound_board
	port map(
		clock_3p58    	=> clock_3p58,
		reset     		=> clkrst_i.arst, 
		select_sound  	=> sound_data,
		audio_out     	=> audio_out,
		dbg_cpu_addr  	=> open
	);  
  
dac : entity work.dac
   port map (
      clk_i     		=> clk_sys,
		res_n_i   		=> '1',
      dac_i   			=> audio_out,
      dac_o  			=> audio
   );
		
  dac_l_o <= audio;
  dac_r_o <= audio; 		
		


 pace_inst : entity work.pace                                            
   port map
   (
		clkrst_i				=> clkrst_i,
		buttons_i         => buttons_i,
		switches_i        => switches_i,
		leds_o            => open,
		inputs_i          => inputs_i,
		video_i           => video_i,
		video_o           => video_o,
		sound_data_o 		=> sound_data
    );
end SYN;

/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//*  This file is part of JTCPS1.
    JTCPS1 program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JTCPS1 program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JTCPS1.  If not, see <http://www.gnu.org/licenses/>.

    Author: Jose Tejada Gomez. Twitter: @topapate
    Version: 1.0
    Date: 12-3-2020 */
    
`timescale 1ns/1ps

// Star field generator
// Based on the circuit used for Side Arms, but without the ROM

module jtcps1_stars(
    input              rst,
    input              clk,
    input              pxl_cen,

    input              VB,
    input              HB,
    input      [ 8:0]  vdump,
    // control registers
    input      [15:0]  hpos0,
    input      [15:0]  vpos0,
    input      [15:0]  hpos1,
    input      [15:0]  vpos1,

    output     [ 8:0]  star0,
    output     [ 8:0]  star1
);

wire [22:0] poly1, poly0;
wire        load = HB | VB;

jtcps1_lfsr #(0) u_lfsr0(
    .clk        ( clk           ),
    .pxl_cen    ( pxl_cen       ),
    .load       ( load          ),
    .hpos       ( hpos0[ 8:0]   ),
    .vpos       ( vpos0[ 8:0]   ),
    .vdump      ( vdump         ),
    .poly       ( poly0         )
);

jtcps1_lfsr #(1) u_lfsr1(
    .clk        ( clk           ),
    .pxl_cen    ( pxl_cen       ),
    .load       ( load          ),
    .hpos       ( hpos1[ 8:0]   ),
    .vpos       ( vpos1[ 8:0]   ),
    .vdump      ( vdump         ),
    .poly       ( poly1         )
);

function bright;
    input [22:0] poly;
    bright = &poly[15:7];
endfunction

// Bits 8:7 must be zero
assign star0[8:4] = { 2'd0, poly0[6:4] };
assign star1[8:4] = { 2'd0, poly1[6:4] };

assign star0[3:0] = bright(poly0) ? poly0[3:0] : 4'hf;
assign star1[3:0] = bright(poly1) ? poly1[3:0] : 4'hf;

`ifdef SIMULATION
wire s0 = star0[3:0]!=4'hf;
wire s1 = star1[3:0]!=4'hf;
`endif

endmodule

module jtcps1_lfsr (
    input              clk,
    input              pxl_cen,
    input              load,
    input      [ 8:0]  hpos,
    input      [ 8:0]  vpos,
    input      [ 8:0]  vdump,
    output reg [22:0]  poly
);

parameter B=0;
wire bb = B;

reg last_load;
reg [8:0] cnt;
wire      cnthi = |cnt;
wire [8:0] v = vpos+vdump;

always @(posedge clk) begin
    last_load <= load;
    if( load && !last_load ) begin
        poly <= { {bb,~bb,~bb,bb}^{v[3:2],v[7:6]}, v[3:0], v[8:4], 10'h55 ^ {10{bb}} };
        cnt  <= hpos;
    end else if( (!load && pxl_cen) || (load&&cnthi) ) begin
        if(cnthi) cnt<=cnt-9'd1;
        poly <= { poly[21:0], ~(poly[21]^poly[17])};
    end
end

endmodule
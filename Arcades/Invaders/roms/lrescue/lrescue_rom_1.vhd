--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-- generated with romgen by MikeJ
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity LRESCUE_ROM_1 is
  port (
    CLK         : in    std_logic;
    ADDR        : in    std_logic_vector(10 downto 0);
    DATA        : out   std_logic_vector(7 downto 0)
    );
end;

architecture RTL of LRESCUE_ROM_1 is


  type ROM_ARRAY is array(0 to 2047) of std_logic_vector(7 downto 0);
  constant ROM : ROM_ARRAY := (
    x"00",x"00",x"00",x"C3",x"4B",x"16",x"00",x"00", -- 0x0000
    x"F5",x"C5",x"D5",x"E5",x"C3",x"E7",x"05",x"00", -- 0x0008
    x"F5",x"C5",x"D5",x"E5",x"3E",x"80",x"32",x"BE", -- 0x0010
    x"20",x"21",x"C0",x"20",x"35",x"CD",x"4B",x"0A", -- 0x0018
    x"DB",x"01",x"0F",x"DA",x"66",x"00",x"3A",x"EA", -- 0x0020
    x"20",x"A7",x"CA",x"42",x"00",x"3A",x"EB",x"20", -- 0x0028
    x"FE",x"09",x"D2",x"3E",x"00",x"C6",x"01",x"27", -- 0x0030
    x"32",x"EB",x"20",x"CD",x"AC",x"0A",x"AF",x"32", -- 0x0038
    x"EA",x"20",x"3A",x"E9",x"20",x"A7",x"C2",x"60", -- 0x0040
    x"00",x"3A",x"EF",x"20",x"A7",x"C2",x"E6",x"0E", -- 0x0048
    x"3A",x"EB",x"20",x"A7",x"C2",x"6B",x"00",x"3A", -- 0x0050
    x"BF",x"21",x"A7",x"C2",x"E6",x"0E",x"00",x"00", -- 0x0058
    x"E1",x"D1",x"C1",x"F1",x"FB",x"C9",x"3E",x"01", -- 0x0060
    x"C3",x"3F",x"00",x"3A",x"58",x"20",x"A7",x"C2", -- 0x0068
    x"60",x"00",x"3E",x"01",x"CD",x"DC",x"08",x"31", -- 0x0070
    x"00",x"24",x"FB",x"CD",x"A4",x"01",x"CD",x"BD", -- 0x0078
    x"0A",x"21",x"13",x"30",x"11",x"50",x"1D",x"0E", -- 0x0080
    x"04",x"CD",x"2A",x"05",x"D3",x"06",x"3A",x"EB", -- 0x0088
    x"20",x"3D",x"CD",x"97",x"4A",x"0E",x"14",x"C2", -- 0x0090
    x"89",x"01",x"11",x"6A",x"0B",x"CD",x"2A",x"05", -- 0x0098
    x"DB",x"01",x"E6",x"04",x"CA",x"8E",x"00",x"06", -- 0x00A0
    x"99",x"AF",x"32",x"EE",x"20",x"3A",x"EB",x"20", -- 0x00A8
    x"80",x"27",x"32",x"EB",x"20",x"CD",x"AC",x"0A", -- 0x00B0
    x"CD",x"18",x"4C",x"22",x"F8",x"20",x"22",x"FC", -- 0x00B8
    x"20",x"CD",x"D4",x"02",x"CD",x"DF",x"02",x"CD", -- 0x00C0
    x"EA",x"02",x"CD",x"C5",x"48",x"06",x"20",x"CD", -- 0x00C8
    x"00",x"49",x"CD",x"A4",x"01",x"CD",x"98",x"05", -- 0x00D0
    x"CD",x"AC",x"4C",x"21",x"01",x"01",x"22",x"E5", -- 0x00D8
    x"20",x"CD",x"B4",x"47",x"CD",x"4C",x"4C",x"CD", -- 0x00E0
    x"8F",x"02",x"CD",x"E3",x"49",x"CD",x"D4",x"02", -- 0x00E8
    x"00",x"00",x"00",x"00",x"00",x"CD",x"F2",x"02", -- 0x00F0
    x"CD",x"CA",x"01",x"CD",x"61",x"02",x"CD",x"79", -- 0x00F8
    x"02",x"CD",x"46",x"02",x"CD",x"A9",x"01",x"CD", -- 0x0100
    x"78",x"1F",x"3A",x"27",x"20",x"A7",x"D3",x"06", -- 0x0108
    x"CA",x"0A",x"01",x"CD",x"12",x"10",x"C2",x"23", -- 0x0110
    x"01",x"3A",x"28",x"20",x"FE",x"02",x"D3",x"06", -- 0x0118
    x"C2",x"13",x"01",x"21",x"29",x"20",x"36",x"00", -- 0x0120
    x"CD",x"30",x"04",x"00",x"00",x"00",x"2A",x"2D", -- 0x0128
    x"20",x"3A",x"2C",x"20",x"FE",x"FE",x"01",x"00", -- 0x0130
    x"14",x"C4",x"85",x"01",x"09",x"22",x"1D",x"20", -- 0x0138
    x"CD",x"6D",x"0D",x"21",x"13",x"20",x"36",x"01", -- 0x0140
    x"21",x"5E",x"20",x"7E",x"A7",x"C2",x"30",x"4C", -- 0x0148
    x"21",x"14",x"20",x"7E",x"A7",x"C2",x"AA",x"04", -- 0x0150
    x"21",x"5D",x"20",x"7E",x"A7",x"C2",x"62",x"03", -- 0x0158
    x"CD",x"77",x"0C",x"D3",x"06",x"3A",x"1D",x"20", -- 0x0160
    x"FE",x"B0",x"DC",x"7F",x"02",x"3A",x"1D",x"20", -- 0x0168
    x"FE",x"B0",x"D4",x"58",x"0D",x"3A",x"16",x"20", -- 0x0170
    x"A7",x"CC",x"9D",x"0D",x"CD",x"05",x"4C",x"CD", -- 0x0178
    x"7B",x"48",x"C3",x"48",x"01",x"01",x"00",x"10", -- 0x0180
    x"C9",x"11",x"55",x"0B",x"CD",x"2A",x"05",x"DB", -- 0x0188
    x"01",x"0F",x"0F",x"DA",x"9D",x"01",x"0F",x"DA", -- 0x0190
    x"A7",x"00",x"C3",x"8E",x"00",x"06",x"98",x"3E", -- 0x0198
    x"01",x"C3",x"AA",x"00",x"3E",x"01",x"C3",x"AA", -- 0x01A0
    x"01",x"AF",x"32",x"E9",x"20",x"C9",x"00",x"00", -- 0x01A8
    x"00",x"00",x"00",x"00",x"C9",x"C5",x"E5",x"1A", -- 0x01B0
    x"77",x"2B",x"1B",x"0D",x"C2",x"B7",x"01",x"E1", -- 0x01B8
    x"01",x"20",x"00",x"09",x"C1",x"05",x"C2",x"B5", -- 0x01C0
    x"01",x"C9",x"21",x"02",x"24",x"11",x"91",x"18", -- 0x01C8
    x"01",x"03",x"18",x"CD",x"1B",x"0C",x"21",x"05", -- 0x01D0
    x"24",x"01",x"03",x"0A",x"CD",x"1B",x"0C",x"21", -- 0x01D8
    x"04",x"3D",x"11",x"D8",x"18",x"01",x"03",x"18", -- 0x01E0
    x"CD",x"B5",x"01",x"21",x"C7",x"3E",x"11",x"F6", -- 0x01E8
    x"18",x"01",x"03",x"0A",x"CD",x"B5",x"01",x"21", -- 0x01F0
    x"02",x"27",x"0E",x"17",x"11",x"31",x"18",x"D5", -- 0x01F8
    x"C5",x"06",x"08",x"CD",x"00",x"0A",x"C1",x"D1", -- 0x0200
    x"0D",x"C3",x"5B",x"1D",x"11",x"EB",x"19",x"C5", -- 0x0208
    x"06",x"08",x"CD",x"00",x"0A",x"C1",x"0D",x"C2", -- 0x0210
    x"0C",x"02",x"CD",x"22",x"17",x"2E",x"00",x"0E", -- 0x0218
    x"06",x"7E",x"E6",x"80",x"C4",x"30",x"02",x"11", -- 0x0220
    x"05",x"00",x"19",x"0D",x"C2",x"21",x"02",x"C9", -- 0x0228
    x"E5",x"C5",x"7E",x"E6",x"7F",x"47",x"23",x"5E", -- 0x0230
    x"23",x"56",x"23",x"4E",x"23",x"7E",x"67",x"69", -- 0x0238
    x"CD",x"00",x"0A",x"C1",x"E1",x"C9",x"CD",x"22", -- 0x0240
    x"17",x"2E",x"AC",x"C3",x"CA",x"4B",x"00",x"21", -- 0x0248
    x"01",x"27",x"11",x"17",x"18",x"C5",x"06",x"10", -- 0x0250
    x"CD",x"00",x"0A",x"C1",x"0D",x"C2",x"52",x"02", -- 0x0258
    x"C9",x"21",x"1C",x"25",x"0E",x"0E",x"11",x"F3", -- 0x0260
    x"19",x"C5",x"E5",x"06",x"05",x"CD",x"00",x"0A", -- 0x0268
    x"E1",x"24",x"24",x"C1",x"0D",x"C2",x"66",x"02", -- 0x0270
    x"C9",x"11",x"13",x"1A",x"C3",x"A9",x"0D",x"3A", -- 0x0278
    x"13",x"20",x"A7",x"C8",x"21",x"26",x"20",x"7E", -- 0x0280
    x"A7",x"C0",x"36",x"01",x"C3",x"6D",x"0D",x"CD", -- 0x0288
    x"22",x"17",x"2E",x"AE",x"4E",x"79",x"A7",x"C8", -- 0x0290
    x"21",x"1B",x"3B",x"C5",x"11",x"0B",x"1A",x"E5", -- 0x0298
    x"06",x"08",x"CD",x"00",x"0A",x"E1",x"25",x"25", -- 0x02A0
    x"C1",x"0D",x"C2",x"9B",x"02",x"C9",x"00",x"00", -- 0x02A8
    x"00",x"C9",x"00",x"00",x"00",x"C9",x"CD",x"8C", -- 0x02B0
    x"05",x"23",x"11",x"F5",x"20",x"1A",x"BE",x"1B", -- 0x02B8
    x"2B",x"1A",x"CA",x"C9",x"02",x"D0",x"C3",x"CB", -- 0x02C0
    x"02",x"BE",x"D0",x"7E",x"12",x"13",x"23",x"7E", -- 0x02C8
    x"12",x"C3",x"00",x"47",x"06",x"C0",x"11",x"A3", -- 0x02D0
    x"1A",x"21",x"00",x"20",x"C3",x"04",x"0B",x"06", -- 0x02D8
    x"C0",x"21",x"00",x"21",x"11",x"A3",x"1B",x"C3", -- 0x02E0
    x"04",x"0B",x"06",x"C0",x"21",x"00",x"22",x"C3", -- 0x02E8
    x"E4",x"02",x"06",x"01",x"21",x"29",x"20",x"70", -- 0x02F0
    x"CD",x"22",x"17",x"2E",x"40",x"70",x"2E",x"56", -- 0x02F8
    x"70",x"C9",x"06",x"00",x"C3",x"F4",x"02",x"06", -- 0x0300
    x"01",x"21",x"29",x"20",x"70",x"CD",x"22",x"17", -- 0x0308
    x"2E",x"70",x"70",x"2E",x"90",x"70",x"21",x"70", -- 0x0310
    x"20",x"70",x"2E",x"7C",x"70",x"2E",x"88",x"70", -- 0x0318
    x"C9",x"06",x"00",x"C3",x"09",x"03",x"CD",x"A4", -- 0x0320
    x"01",x"CD",x"21",x"03",x"CD",x"02",x"03",x"21", -- 0x0328
    x"13",x"20",x"36",x"00",x"CD",x"9D",x"4A",x"CD", -- 0x0330
    x"14",x"49",x"2A",x"1D",x"20",x"01",x"03",x"10", -- 0x0338
    x"CD",x"30",x"0C",x"0E",x"04",x"11",x"23",x"1A", -- 0x0340
    x"C5",x"2A",x"1D",x"20",x"01",x"02",x"10",x"CD", -- 0x0348
    x"18",x"0C",x"3E",x"08",x"CD",x"D5",x"05",x"C1", -- 0x0350
    x"0D",x"C2",x"48",x"03",x"CD",x"19",x"49",x"C3", -- 0x0358
    x"CB",x"05",x"CD",x"26",x"03",x"00",x"00",x"C3", -- 0x0360
    x"A5",x"4A",x"2E",x"AC",x"35",x"00",x"CA",x"B0", -- 0x0368
    x"04",x"CD",x"42",x"4B",x"C3",x"C6",x"1E",x"3A", -- 0x0370
    x"EF",x"20",x"A7",x"C8",x"3A",x"C6",x"20",x"C3", -- 0x0378
    x"44",x"0A",x"00",x"00",x"36",x"00",x"CD",x"74", -- 0x0380
    x"4C",x"CD",x"22",x"4F",x"CD",x"21",x"03",x"C3", -- 0x0388
    x"B9",x"1F",x"CD",x"22",x"17",x"2E",x"AE",x"34", -- 0x0390
    x"E5",x"CD",x"8F",x"02",x"E1",x"2E",x"A0",x"7E", -- 0x0398
    x"A7",x"CA",x"D4",x"03",x"2E",x"AE",x"7E",x"FE", -- 0x03A0
    x"06",x"CC",x"0E",x"04",x"CD",x"CB",x"05",x"CD", -- 0x03A8
    x"22",x"17",x"2E",x"A2",x"5E",x"23",x"56",x"EB", -- 0x03B0
    x"CD",x"0A",x"4F",x"CD",x"A4",x"4C",x"CD",x"CB", -- 0x03B8
    x"05",x"CD",x"B5",x"4C",x"CD",x"22",x"17",x"2E", -- 0x03C0
    x"AB",x"34",x"2E",x"AF",x"34",x"2E",x"00",x"06", -- 0x03C8
    x"AA",x"CD",x"E4",x"02",x"CD",x"CB",x"05",x"C9", -- 0x03D0
    x"00",x"00",x"21",x"1B",x"3D",x"79",x"A7",x"C8", -- 0x03D8
    x"00",x"25",x"25",x"3D",x"D3",x"06",x"C2",x"E1", -- 0x03E0
    x"03",x"C5",x"06",x"10",x"CD",x"34",x"0A",x"CD", -- 0x03E8
    x"42",x"49",x"00",x"00",x"00",x"E5",x"CD",x"F5", -- 0x03F0
    x"49",x"CD",x"0A",x"4F",x"3E",x"10",x"CD",x"D5", -- 0x03F8
    x"05",x"E1",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0400
    x"C1",x"0D",x"C2",x"E9",x"03",x"C9",x"C5",x"21", -- 0x0408
    x"14",x"2A",x"11",x"AD",x"1E",x"0E",x"10",x"CD", -- 0x0410
    x"B3",x"05",x"CD",x"00",x"4D",x"00",x"00",x"00", -- 0x0418
    x"00",x"00",x"00",x"00",x"00",x"21",x"50",x"00", -- 0x0420
    x"CD",x"0A",x"4F",x"00",x"00",x"00",x"C1",x"C9", -- 0x0428
    x"CD",x"22",x"17",x"2E",x"2D",x"7E",x"FE",x"04", -- 0x0430
    x"21",x"06",x"25",x"D4",x"46",x"04",x"11",x"4A", -- 0x0438
    x"04",x"06",x"0D",x"C3",x"00",x"0A",x"21",x"06", -- 0x0440
    x"3D",x"C9",x"3E",x"08",x"3E",x"00",x"3E",x"2A", -- 0x0448
    x"00",x"3E",x"02",x"00",x"3E",x"28",x"10",x"00", -- 0x0450
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0458
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"36", -- 0x0460
    x"00",x"CD",x"02",x"03",x"21",x"13",x"20",x"36", -- 0x0468
    x"00",x"CD",x"D2",x"0D",x"3A",x"AE",x"20",x"A7", -- 0x0470
    x"C2",x"29",x"06",x"21",x"01",x"01",x"22",x"A0", -- 0x0478
    x"20",x"3A",x"A1",x"20",x"A7",x"D3",x"06",x"C2", -- 0x0480
    x"81",x"04",x"CD",x"83",x"16",x"21",x"13",x"20", -- 0x0488
    x"36",x"01",x"21",x"1B",x"20",x"36",x"01",x"CD", -- 0x0490
    x"07",x"03",x"21",x"16",x"20",x"36",x"01",x"2A", -- 0x0498
    x"9E",x"20",x"3A",x"9D",x"20",x"47",x"C3",x"34", -- 0x04A0
    x"0A",x"00",x"CD",x"67",x"04",x"C3",x"58",x"01", -- 0x04A8
    x"CD",x"B6",x"02",x"CD",x"D4",x"02",x"00",x"00", -- 0x04B0
    x"AF",x"32",x"EE",x"20",x"CD",x"A4",x"01",x"CD", -- 0x04B8
    x"02",x"03",x"CD",x"21",x"03",x"21",x"13",x"2D", -- 0x04C0
    x"11",x"63",x"4B",x"0E",x"0A",x"CD",x"B3",x"05", -- 0x04C8
    x"CD",x"97",x"4B",x"3A",x"E3",x"20",x"A7",x"C4", -- 0x04D0
    x"0A",x"47",x"CD",x"C0",x"4C",x"CD",x"F8",x"4C", -- 0x04D8
    x"CD",x"86",x"4B",x"CD",x"CB",x"05",x"CD",x"A9", -- 0x04E0
    x"01",x"C3",x"73",x"16",x"CD",x"8C",x"05",x"23", -- 0x04E8
    x"11",x"F5",x"20",x"1A",x"BE",x"1B",x"2B",x"1A", -- 0x04F0
    x"CA",x"C9",x"02",x"D0",x"C3",x"CB",x"02",x"00", -- 0x04F8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0500
    x"00",x"00",x"F3",x"78",x"D3",x"05",x"06",x"0A", -- 0x0508
    x"0E",x"00",x"0D",x"C2",x"12",x"05",x"05",x"C2", -- 0x0510
    x"10",x"05",x"FB",x"C9",x"4E",x"23",x"46",x"23", -- 0x0518
    x"79",x"86",x"77",x"23",x"78",x"86",x"77",x"C9", -- 0x0520
    x"0E",x"02",x"1A",x"D5",x"CD",x"36",x"05",x"D1", -- 0x0528
    x"13",x"0D",x"C2",x"2A",x"05",x"C9",x"CD",x"9B", -- 0x0530
    x"0B",x"06",x"05",x"D3",x"06",x"CD",x"00",x"0A", -- 0x0538
    x"C5",x"AF",x"77",x"01",x"20",x"00",x"09",x"77", -- 0x0540
    x"09",x"77",x"09",x"C1",x"C9",x"CD",x"8C",x"05", -- 0x0548
    x"3A",x"F1",x"20",x"A7",x"C8",x"AF",x"32",x"F1", -- 0x0550
    x"20",x"E5",x"2A",x"F2",x"20",x"EB",x"E1",x"7E", -- 0x0558
    x"83",x"27",x"77",x"5F",x"23",x"7E",x"8A",x"27", -- 0x0560
    x"77",x"57",x"23",x"7E",x"23",x"66",x"6F",x"7A", -- 0x0568
    x"CD",x"74",x"05",x"7B",x"D5",x"F5",x"0F",x"0F", -- 0x0570
    x"0F",x"0F",x"E6",x"0F",x"CD",x"87",x"05",x"F1", -- 0x0578
    x"E6",x"0F",x"CD",x"87",x"05",x"D1",x"C9",x"C6", -- 0x0580
    x"1C",x"C3",x"36",x"05",x"3A",x"C6",x"20",x"0F", -- 0x0588
    x"21",x"F8",x"20",x"D8",x"21",x"FC",x"20",x"C9", -- 0x0590
    x"21",x"01",x"24",x"0E",x"1C",x"06",x"DF",x"E5", -- 0x0598
    x"C5",x"36",x"00",x"11",x"20",x"00",x"19",x"05", -- 0x05A0
    x"C2",x"A1",x"05",x"C1",x"E1",x"23",x"0D",x"C2", -- 0x05A8
    x"9D",x"05",x"C9",x"D5",x"1A",x"CD",x"36",x"05", -- 0x05B0
    x"D1",x"3E",x"07",x"32",x"C0",x"20",x"3A",x"C0", -- 0x05B8
    x"20",x"3D",x"C2",x"BE",x"05",x"13",x"0D",x"C2", -- 0x05C0
    x"B3",x"05",x"C9",x"3E",x"40",x"C3",x"D5",x"05", -- 0x05C8
    x"3E",x"80",x"C3",x"D5",x"05",x"32",x"C0",x"20", -- 0x05D0
    x"3A",x"C0",x"20",x"A7",x"D3",x"06",x"C2",x"D8", -- 0x05D8
    x"05",x"C9",x"00",x"00",x"00",x"00",x"00",x"3A", -- 0x05E0
    x"DC",x"20",x"A7",x"CA",x"8C",x"0E",x"DB",x"01", -- 0x05E8
    x"E6",x"06",x"C4",x"16",x"06",x"CD",x"7C",x"03", -- 0x05F0
    x"E6",x"10",x"06",x"01",x"21",x"CB",x"20",x"C2", -- 0x05F8
    x"0B",x"06",x"7E",x"A7",x"23",x"C2",x"0B",x"06", -- 0x0600
    x"2B",x"06",x"00",x"70",x"2A",x"D9",x"20",x"23", -- 0x0608
    x"22",x"D9",x"20",x"C3",x"60",x"00",x"3E",x"01", -- 0x0610
    x"32",x"E1",x"20",x"C9",x"21",x"00",x"00",x"22", -- 0x0618
    x"CB",x"20",x"C9",x"CD",x"42",x"08",x"C3",x"A0", -- 0x0620
    x"06",x"3A",x"EF",x"20",x"A7",x"C8",x"E1",x"C3", -- 0x0628
    x"62",x"03",x"00",x"00",x"00",x"00",x"00",x"3E", -- 0x0630
    x"20",x"C3",x"D5",x"05",x"3E",x"05",x"C3",x"D5", -- 0x0638
    x"05",x"01",x"50",x"06",x"CD",x"7C",x"03",x"E6", -- 0x0640
    x"60",x"CA",x"55",x"06",x"0D",x"C2",x"44",x"06", -- 0x0648
    x"05",x"C2",x"44",x"06",x"C9",x"E1",x"C9",x"21", -- 0x0650
    x"CB",x"20",x"11",x"C2",x"08",x"06",x"1A",x"CD", -- 0x0658
    x"04",x"0B",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0660
    x"11",x"9A",x"08",x"21",x"17",x"27",x"0E",x"17", -- 0x0668
    x"CD",x"2A",x"05",x"21",x"12",x"27",x"11",x"5B", -- 0x0670
    x"1C",x"CD",x"04",x"07",x"21",x"10",x"27",x"CD", -- 0x0678
    x"04",x"07",x"21",x"0E",x"27",x"0E",x"05",x"CD", -- 0x0680
    x"06",x"07",x"21",x"0E",x"33",x"11",x"43",x"09", -- 0x0688
    x"06",x"28",x"CD",x"00",x"0A",x"11",x"B1",x"08", -- 0x0690
    x"0E",x"10",x"21",x"06",x"2A",x"CD",x"2A",x"05", -- 0x0698
    x"3A",x"DC",x"20",x"A7",x"CA",x"78",x"08",x"3A", -- 0x06A0
    x"DA",x"20",x"FE",x"20",x"D2",x"78",x"08",x"CD", -- 0x06A8
    x"16",x"07",x"CD",x"7C",x"03",x"E6",x"60",x"FE", -- 0x06B0
    x"40",x"CC",x"2F",x"07",x"FE",x"20",x"CC",x"85", -- 0x06B8
    x"07",x"3A",x"E1",x"20",x"A7",x"C2",x"78",x"08", -- 0x06C0
    x"3A",x"CC",x"20",x"A7",x"C2",x"CF",x"07",x"01", -- 0x06C8
    x"30",x"01",x"CD",x"26",x"07",x"CD",x"1E",x"07", -- 0x06D0
    x"3A",x"DA",x"20",x"FE",x"1D",x"C2",x"A0",x"06", -- 0x06D8
    x"06",x"05",x"C5",x"21",x"06",x"2A",x"06",x"20", -- 0x06E0
    x"CD",x"EB",x"09",x"CD",x"3C",x"06",x"11",x"B1", -- 0x06E8
    x"08",x"21",x"06",x"2A",x"0E",x"04",x"CD",x"2A", -- 0x06F0
    x"05",x"CD",x"3C",x"06",x"C1",x"05",x"C2",x"E2", -- 0x06F8
    x"06",x"C3",x"A0",x"06",x"0E",x"0B",x"06",x"05", -- 0x0700
    x"CD",x"00",x"0A",x"C5",x"01",x"60",x"01",x"09", -- 0x0708
    x"C1",x"0D",x"C2",x"06",x"07",x"C9",x"2A",x"DD", -- 0x0710
    x"20",x"3E",x"2A",x"C3",x"36",x"05",x"2A",x"DD", -- 0x0718
    x"20",x"3E",x"1B",x"C3",x"36",x"05",x"0D",x"C2", -- 0x0720
    x"26",x"07",x"05",x"C2",x"26",x"07",x"C9",x"CD", -- 0x0728
    x"41",x"06",x"CD",x"1E",x"07",x"2A",x"DD",x"20", -- 0x0730
    x"7D",x"FE",x"11",x"CA",x"60",x"07",x"FE",x"0F", -- 0x0738
    x"CA",x"69",x"07",x"7C",x"FE",x"33",x"CA",x"74", -- 0x0740
    x"07",x"24",x"24",x"7C",x"FE",x"35",x"D2",x"54", -- 0x0748
    x"07",x"C3",x"5A",x"07",x"CD",x"1E",x"07",x"21", -- 0x0750
    x"0D",x"36",x"22",x"DD",x"20",x"C3",x"16",x"07", -- 0x0758
    x"7C",x"FE",x"3B",x"D2",x"79",x"07",x"C3",x"6F", -- 0x0760
    x"07",x"7C",x"FE",x"3B",x"D2",x"7F",x"07",x"24", -- 0x0768
    x"24",x"C3",x"5A",x"07",x"26",x"34",x"C3",x"49", -- 0x0770
    x"07",x"21",x"0F",x"27",x"C3",x"5A",x"07",x"21", -- 0x0778
    x"0D",x"27",x"C3",x"5A",x"07",x"CD",x"41",x"06", -- 0x0780
    x"CD",x"1E",x"07",x"2A",x"DD",x"20",x"7D",x"FE", -- 0x0788
    x"11",x"CA",x"A9",x"07",x"FE",x"0F",x"CA",x"BA", -- 0x0790
    x"07",x"7C",x"FE",x"28",x"DA",x"C3",x"07",x"FE", -- 0x0798
    x"34",x"C2",x"AF",x"07",x"26",x"33",x"C3",x"AF", -- 0x07A0
    x"07",x"7C",x"FE",x"28",x"DA",x"B4",x"07",x"25", -- 0x07A8
    x"25",x"C3",x"5A",x"07",x"21",x"11",x"27",x"C3", -- 0x07B0
    x"5A",x"07",x"7C",x"FE",x"28",x"DA",x"C9",x"07", -- 0x07B8
    x"C3",x"AF",x"07",x"21",x"0F",x"3B",x"C3",x"5A", -- 0x07C0
    x"07",x"21",x"11",x"3B",x"C3",x"5A",x"07",x"01", -- 0x07C8
    x"FF",x"20",x"CD",x"26",x"07",x"3A",x"DB",x"20", -- 0x07D0
    x"FE",x"0A",x"D2",x"5D",x"08",x"3A",x"DD",x"20", -- 0x07D8
    x"FE",x"11",x"CA",x"FD",x"07",x"FE",x"0F",x"CA", -- 0x07E0
    x"03",x"08",x"3A",x"DE",x"20",x"FE",x"32",x"D2", -- 0x07E8
    x"0B",x"08",x"CD",x"3B",x"08",x"C6",x"16",x"CD", -- 0x07F0
    x"42",x"08",x"C3",x"A0",x"06",x"CD",x"3B",x"08"  -- 0x07F8
  );

begin

  p_rom : process
  begin
    wait until rising_edge(CLK);
     DATA <= ROM(to_integer(unsigned(ADDR)));
  end process;
end RTL;

--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-- generated with romgen by MikeJ
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity LRESCUE_ROM_3 is
  port (
    CLK         : in    std_logic;
    ADDR        : in    std_logic_vector(10 downto 0);
    DATA        : out   std_logic_vector(7 downto 0)
    );
end;

architecture RTL of LRESCUE_ROM_3 is


  type ROM_ARRAY is array(0 to 2047) of std_logic_vector(7 downto 0);
  constant ROM : ROM_ARRAY := (
    x"11",x"2C",x"18",x"CC",x"0E",x"10",x"2A",x"23", -- 0x0000
    x"20",x"06",x"05",x"C3",x"C8",x"09",x"11",x"27", -- 0x0008
    x"18",x"C9",x"CD",x"41",x"0A",x"E6",x"10",x"C9", -- 0x0010
    x"21",x"17",x"20",x"7E",x"A7",x"C3",x"67",x"49", -- 0x0018
    x"2A",x"23",x"20",x"06",x"05",x"11",x"2C",x"18", -- 0x0020
    x"C3",x"55",x"0C",x"21",x"1A",x"20",x"CD",x"FD", -- 0x0028
    x"0F",x"C3",x"F6",x"0F",x"21",x"17",x"20",x"7E", -- 0x0030
    x"A7",x"C2",x"41",x"10",x"36",x"01",x"CD",x"73", -- 0x0038
    x"4F",x"2A",x"23",x"20",x"7D",x"FE",x"30",x"C9", -- 0x0040
    x"2A",x"1D",x"20",x"01",x"F8",x"03",x"09",x"22", -- 0x0048
    x"23",x"20",x"C9",x"21",x"17",x"20",x"7E",x"A7", -- 0x0050
    x"C2",x"5E",x"10",x"CD",x"48",x"10",x"00",x"00", -- 0x0058
    x"00",x"00",x"00",x"00",x"CD",x"20",x"10",x"CD", -- 0x0060
    x"48",x"10",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0068
    x"00",x"00",x"21",x"98",x"20",x"CD",x"12",x"10", -- 0x0070
    x"CA",x"95",x"10",x"E5",x"21",x"1B",x"20",x"36", -- 0x0078
    x"02",x"E1",x"C3",x"FC",x"0F",x"00",x"00",x"00", -- 0x0080
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0088
    x"00",x"00",x"00",x"98",x"00",x"E5",x"23",x"34", -- 0x0090
    x"7E",x"E6",x"01",x"21",x"1B",x"20",x"C2",x"AB", -- 0x0098
    x"10",x"36",x"01",x"E1",x"34",x"7E",x"E6",x"04", -- 0x00A0
    x"C3",x"00",x"10",x"36",x"00",x"C3",x"A3",x"10", -- 0x00A8
    x"CD",x"22",x"17",x"2E",x"AD",x"7E",x"21",x"FB", -- 0x00B0
    x"19",x"A7",x"C4",x"C1",x"10",x"22",x"A7",x"20", -- 0x00B8
    x"C9",x"21",x"03",x"1A",x"C9",x"21",x"A2",x"20", -- 0x00C0
    x"7E",x"A7",x"C2",x"D6",x"10",x"CD",x"8B",x"1D", -- 0x00C8
    x"CD",x"B0",x"10",x"CD",x"FB",x"10",x"CD",x"FB", -- 0x00D0
    x"10",x"CD",x"22",x"17",x"2E",x"AD",x"7E",x"A7", -- 0x00D8
    x"C2",x"29",x"11",x"3A",x"A5",x"20",x"FE",x"19", -- 0x00E0
    x"DC",x"04",x"11",x"21",x"1E",x"20",x"3A",x"A6", -- 0x00E8
    x"20",x"BE",x"DA",x"0B",x"11",x"21",x"A3",x"20", -- 0x00F0
    x"CD",x"1C",x"05",x"21",x"A5",x"20",x"CD",x"0D", -- 0x00F8
    x"0B",x"C3",x"C8",x"09",x"21",x"00",x"FF",x"22", -- 0x0100
    x"A3",x"20",x"C9",x"21",x"01",x"00",x"22",x"A3", -- 0x0108
    x"20",x"21",x"1D",x"20",x"3A",x"A5",x"20",x"BE", -- 0x0110
    x"D2",x"1E",x"11",x"C3",x"F5",x"10",x"21",x"00", -- 0x0118
    x"00",x"22",x"A0",x"20",x"AF",x"32",x"A2",x"20", -- 0x0120
    x"C9",x"21",x"AD",x"20",x"7E",x"A7",x"C2",x"3C", -- 0x0128
    x"11",x"3A",x"A5",x"20",x"FE",x"19",x"DA",x"3C", -- 0x0130
    x"11",x"C3",x"F5",x"10",x"36",x"01",x"21",x"00", -- 0x0138
    x"01",x"22",x"A3",x"20",x"21",x"1E",x"20",x"3A", -- 0x0140
    x"A6",x"20",x"BE",x"D2",x"51",x"11",x"C3",x"F5", -- 0x0148
    x"10",x"21",x"01",x"00",x"22",x"A3",x"20",x"21", -- 0x0150
    x"1D",x"20",x"3A",x"A5",x"20",x"BE",x"D2",x"64", -- 0x0158
    x"11",x"C3",x"F5",x"10",x"CD",x"22",x"17",x"2E", -- 0x0160
    x"AD",x"36",x"00",x"C3",x"1E",x"11",x"CD",x"22", -- 0x0168
    x"17",x"2E",x"2D",x"4E",x"E5",x"2B",x"2B",x"2B", -- 0x0170
    x"23",x"CD",x"32",x"12",x"36",x"00",x"CD",x"37", -- 0x0178
    x"0D",x"22",x"A5",x"20",x"06",x"08",x"CD",x"E8", -- 0x0180
    x"09",x"E1",x"7E",x"A7",x"C8",x"35",x"7E",x"FE", -- 0x0188
    x"03",x"DA",x"95",x"11",x"C9",x"2E",x"AD",x"36", -- 0x0190
    x"01",x"C9",x"3A",x"16",x"20",x"A7",x"CC",x"17", -- 0x0198
    x"16",x"21",x"29",x"20",x"7E",x"A7",x"C4",x"46", -- 0x01A0
    x"12",x"CD",x"EB",x"48",x"00",x"00",x"00",x"21", -- 0x01A8
    x"88",x"20",x"7E",x"A7",x"C8",x"23",x"7E",x"A7", -- 0x01B0
    x"C2",x"00",x"12",x"36",x"01",x"2A",x"8A",x"20", -- 0x01B8
    x"4E",x"7D",x"FE",x"37",x"D4",x"21",x"12",x"22", -- 0x01C0
    x"8A",x"20",x"CD",x"22",x"17",x"2E",x"72",x"CD", -- 0x01C8
    x"3B",x"12",x"C2",x"ED",x"11",x"2A",x"8A",x"20", -- 0x01D0
    x"23",x"22",x"8A",x"20",x"CD",x"22",x"17",x"2E", -- 0x01D8
    x"8C",x"7E",x"FE",x"04",x"C2",x"2C",x"12",x"21", -- 0x01E0
    x"00",x"00",x"C3",x"6F",x"1E",x"E5",x"2A",x"8A", -- 0x01E8
    x"20",x"23",x"22",x"8A",x"20",x"E1",x"CD",x"37", -- 0x01F0
    x"0D",x"01",x"00",x"09",x"09",x"22",x"8E",x"20", -- 0x01F8
    x"21",x"8E",x"20",x"CD",x"0D",x"0B",x"CD",x"55", -- 0x0200
    x"0C",x"21",x"8E",x"20",x"7E",x"FE",x"40",x"DA", -- 0x0208
    x"24",x"12",x"21",x"8C",x"20",x"CD",x"1C",x"05", -- 0x0210
    x"21",x"8E",x"20",x"CD",x"0D",x"0B",x"C3",x"C8", -- 0x0218
    x"09",x"2E",x"27",x"C9",x"2A",x"8E",x"20",x"06", -- 0x0220
    x"03",x"CD",x"79",x"17",x"21",x"89",x"20",x"36", -- 0x0228
    x"00",x"C9",x"11",x"03",x"00",x"19",x"0D",x"C2", -- 0x0230
    x"35",x"12",x"C9",x"79",x"A7",x"CA",x"43",x"12", -- 0x0238
    x"CD",x"32",x"12",x"7E",x"A7",x"C9",x"11",x"2E", -- 0x0240
    x"20",x"CD",x"D8",x"0A",x"D8",x"21",x"2D",x"20", -- 0x0248
    x"CD",x"0D",x"0B",x"CD",x"18",x"0C",x"3A",x"2E", -- 0x0250
    x"20",x"FE",x"28",x"DC",x"9C",x"12",x"FE",x"C8", -- 0x0258
    x"D4",x"A2",x"12",x"3A",x"16",x"20",x"A7",x"CC", -- 0x0260
    x"76",x"12",x"21",x"2B",x"20",x"CD",x"1C",x"05", -- 0x0268
    x"21",x"27",x"20",x"36",x"01",x"C9",x"2A",x"2D", -- 0x0270
    x"20",x"01",x"00",x"10",x"09",x"11",x"F7",x"17", -- 0x0278
    x"01",x"02",x"10",x"CD",x"2C",x"0A",x"C5",x"E5", -- 0x0280
    x"1A",x"B6",x"77",x"23",x"13",x"0D",x"C2",x"88", -- 0x0288
    x"12",x"E1",x"01",x"20",x"00",x"09",x"C1",x"05", -- 0x0290
    x"C2",x"86",x"12",x"C9",x"21",x"2C",x"20",x"36", -- 0x0298
    x"02",x"C9",x"21",x"2C",x"20",x"36",x"FE",x"21", -- 0x02A0
    x"28",x"20",x"34",x"C9",x"3A",x"16",x"20",x"A7", -- 0x02A8
    x"C8",x"3A",x"1D",x"20",x"FE",x"B8",x"D0",x"CD", -- 0x02B0
    x"12",x"10",x"C2",x"CC",x"12",x"21",x"0D",x"20", -- 0x02B8
    x"7E",x"A7",x"C2",x"C8",x"12",x"36",x"01",x"C9", -- 0x02C0
    x"23",x"36",x"01",x"C9",x"21",x"0E",x"20",x"7E", -- 0x02C8
    x"A7",x"C8",x"36",x"00",x"2B",x"36",x"00",x"21", -- 0x02D0
    x"00",x"20",x"36",x"01",x"C9",x"21",x"00",x"20", -- 0x02D8
    x"7E",x"A7",x"C8",x"23",x"7E",x"A7",x"C2",x"F5", -- 0x02E0
    x"12",x"36",x"01",x"2A",x"1D",x"20",x"01",x"08", -- 0x02E8
    x"05",x"09",x"22",x"06",x"20",x"11",x"07",x"20", -- 0x02F0
    x"CD",x"D8",x"0A",x"D0",x"21",x"06",x"20",x"CD", -- 0x02F8
    x"0D",x"0B",x"CD",x"57",x"17",x"21",x"06",x"20", -- 0x0300
    x"7E",x"FE",x"B8",x"D2",x"4D",x"13",x"21",x"04", -- 0x0308
    x"20",x"CD",x"1C",x"05",x"CD",x"90",x"1D",x"CD", -- 0x0310
    x"2C",x"0A",x"C5",x"E5",x"1A",x"D3",x"04",x"DB", -- 0x0318
    x"03",x"A6",x"CA",x"2A",x"13",x"3E",x"01",x"32", -- 0x0320
    x"0C",x"20",x"DB",x"03",x"AE",x"77",x"23",x"13", -- 0x0328
    x"AF",x"D3",x"04",x"DB",x"03",x"A6",x"CA",x"3E", -- 0x0330
    x"13",x"3E",x"01",x"32",x"0C",x"20",x"DB",x"03", -- 0x0338
    x"AE",x"77",x"E1",x"01",x"20",x"00",x"09",x"C1", -- 0x0340
    x"05",x"C2",x"1A",x"13",x"C9",x"2A",x"06",x"20", -- 0x0348
    x"06",x"03",x"CD",x"79",x"17",x"21",x"00",x"00", -- 0x0350
    x"22",x"00",x"20",x"C9",x"3A",x"16",x"20",x"A7", -- 0x0358
    x"CC",x"33",x"16",x"CD",x"22",x"17",x"2E",x"40", -- 0x0360
    x"7E",x"A7",x"C2",x"5D",x"14",x"2E",x"70",x"7E", -- 0x0368
    x"A7",x"C8",x"C3",x"27",x"1F",x"C5",x"E5",x"7E", -- 0x0370
    x"F5",x"E6",x"80",x"CA",x"BD",x"13",x"F1",x"F5", -- 0x0378
    x"07",x"07",x"22",x"34",x"20",x"21",x"7B",x"19", -- 0x0380
    x"DC",x"49",x"14",x"22",x"3A",x"20",x"F1",x"00", -- 0x0388
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"2A", -- 0x0390
    x"34",x"20",x"CD",x"37",x"0D",x"22",x"38",x"20", -- 0x0398
    x"21",x"38",x"20",x"CD",x"0D",x"0B",x"CD",x"FD", -- 0x03A0
    x"09",x"21",x"39",x"20",x"7E",x"FE",x"E0",x"D4", -- 0x03A8
    x"4D",x"14",x"21",x"36",x"20",x"CD",x"1C",x"05", -- 0x03B0
    x"2A",x"38",x"20",x"EB",x"F5",x"F1",x"E1",x"23", -- 0x03B8
    x"73",x"23",x"72",x"23",x"C1",x"0D",x"C2",x"75", -- 0x03C0
    x"13",x"21",x"70",x"20",x"7E",x"A7",x"C8",x"23", -- 0x03C8
    x"7E",x"A7",x"C2",x"1A",x"14",x"36",x"01",x"2A", -- 0x03D0
    x"72",x"20",x"4E",x"7D",x"FE",x"56",x"D4",x"5A", -- 0x03D8
    x"14",x"22",x"72",x"20",x"CD",x"22",x"17",x"2E", -- 0x03E0
    x"7E",x"CD",x"3B",x"12",x"C2",x"07",x"14",x"2A", -- 0x03E8
    x"72",x"20",x"23",x"22",x"72",x"20",x"CD",x"22", -- 0x03F0
    x"17",x"2E",x"8A",x"7E",x"FE",x"04",x"C2",x"43", -- 0x03F8
    x"14",x"21",x"00",x"00",x"C3",x"73",x"1E",x"E5", -- 0x0400
    x"2A",x"72",x"20",x"23",x"22",x"72",x"20",x"E1", -- 0x0408
    x"CD",x"37",x"0D",x"01",x"00",x"09",x"09",x"22", -- 0x0410
    x"76",x"20",x"21",x"76",x"20",x"CD",x"0D",x"0B", -- 0x0418
    x"CD",x"55",x"0C",x"21",x"76",x"20",x"7E",x"FE", -- 0x0420
    x"40",x"DA",x"3B",x"14",x"21",x"74",x"20",x"CD", -- 0x0428
    x"1C",x"05",x"21",x"76",x"20",x"CD",x"0D",x"0B", -- 0x0430
    x"C3",x"C8",x"09",x"2A",x"76",x"20",x"06",x"03", -- 0x0438
    x"CD",x"79",x"17",x"21",x"71",x"20",x"36",x"00", -- 0x0440
    x"C9",x"21",x"8E",x"19",x"C9",x"E5",x"2A",x"38", -- 0x0448
    x"20",x"06",x"13",x"CD",x"E8",x"09",x"E1",x"36", -- 0x0450
    x"28",x"C9",x"2E",x"47",x"C9",x"CD",x"A5",x"14", -- 0x0458
    x"22",x"65",x"20",x"21",x"F7",x"18",x"22",x"69", -- 0x0460
    x"20",x"CD",x"22",x"17",x"2E",x"41",x"4E",x"23", -- 0x0468
    x"C5",x"E5",x"CD",x"38",x"0D",x"22",x"67",x"20", -- 0x0470
    x"CD",x"9B",x"1D",x"CD",x"FD",x"09",x"21",x"68", -- 0x0478
    x"20",x"7E",x"FE",x"EA",x"D4",x"98",x"14",x"21", -- 0x0480
    x"65",x"20",x"CD",x"1C",x"05",x"2A",x"67",x"20", -- 0x0488
    x"EB",x"C3",x"A1",x"1D",x"C2",x"70",x"14",x"C9", -- 0x0490
    x"E5",x"2A",x"67",x"20",x"06",x"0B",x"CD",x"E8", -- 0x0498
    x"09",x"E1",x"36",x"28",x"C9",x"CD",x"22",x"17", -- 0x04A0
    x"2E",x"AB",x"7E",x"A7",x"21",x"00",x"01",x"C8", -- 0x04A8
    x"FE",x"01",x"21",x"00",x"01",x"C8",x"C3",x"7B", -- 0x04B0
    x"1E",x"C9",x"00",x"73",x"23",x"72",x"23",x"00", -- 0x04B8
    x"00",x"C9",x"3A",x"16",x"20",x"A7",x"CC",x"3F", -- 0x04C0
    x"16",x"CD",x"22",x"17",x"2E",x"56",x"7E",x"A7", -- 0x04C8
    x"C2",x"B7",x"15",x"2E",x"90",x"7E",x"A7",x"C8", -- 0x04D0
    x"E5",x"CD",x"59",x"15",x"22",x"36",x"20",x"21", -- 0x04D8
    x"A1",x"19",x"22",x"3A",x"20",x"E1",x"23",x"4E", -- 0x04E0
    x"23",x"C5",x"E5",x"7E",x"A7",x"CA",x"12",x"15", -- 0x04E8
    x"CD",x"37",x"0D",x"22",x"38",x"20",x"21",x"38", -- 0x04F0
    x"20",x"CD",x"0D",x"0B",x"CD",x"FD",x"09",x"21", -- 0x04F8
    x"39",x"20",x"7E",x"FE",x"28",x"DC",x"6E",x"15", -- 0x0500
    x"21",x"36",x"20",x"CD",x"1C",x"05",x"2A",x"38", -- 0x0508
    x"20",x"EB",x"C3",x"B3",x"1D",x"C2",x"E9",x"14", -- 0x0510
    x"21",x"7C",x"20",x"7E",x"A7",x"C8",x"23",x"7E", -- 0x0518
    x"A7",x"C2",x"97",x"15",x"36",x"01",x"2A",x"7E", -- 0x0520
    x"20",x"4E",x"7D",x"FE",x"47",x"D4",x"56",x"15", -- 0x0528
    x"22",x"7E",x"20",x"CD",x"22",x"17",x"2E",x"92", -- 0x0530
    x"CD",x"3B",x"12",x"C2",x"89",x"15",x"2A",x"7E", -- 0x0538
    x"20",x"23",x"22",x"7E",x"20",x"CD",x"22",x"17", -- 0x0540
    x"2E",x"8B",x"7E",x"FE",x"04",x"C2",x"83",x"15", -- 0x0548
    x"21",x"00",x"00",x"C3",x"77",x"1E",x"2E",x"37", -- 0x0550
    x"C9",x"CD",x"22",x"17",x"2E",x"AB",x"7E",x"A7", -- 0x0558
    x"21",x"00",x"FF",x"C8",x"FE",x"01",x"21",x"00", -- 0x0560
    x"FE",x"C8",x"C3",x"62",x"1F",x"C9",x"E5",x"2A", -- 0x0568
    x"38",x"20",x"06",x"13",x"CD",x"E8",x"09",x"E1", -- 0x0570
    x"36",x"E0",x"C9",x"2A",x"82",x"20",x"06",x"03", -- 0x0578
    x"CD",x"79",x"17",x"21",x"7D",x"20",x"36",x"00", -- 0x0580
    x"C9",x"E5",x"2A",x"7E",x"20",x"C3",x"BE",x"1D", -- 0x0588
    x"01",x"00",x"09",x"09",x"22",x"82",x"20",x"21", -- 0x0590
    x"82",x"20",x"CD",x"0D",x"0B",x"CD",x"55",x"0C", -- 0x0598
    x"3A",x"82",x"20",x"FE",x"40",x"DA",x"7B",x"15", -- 0x05A0
    x"21",x"80",x"20",x"CD",x"1C",x"05",x"21",x"82", -- 0x05A8
    x"20",x"CD",x"0D",x"0B",x"C3",x"C8",x"09",x"CD", -- 0x05B0
    x"02",x"16",x"22",x"65",x"20",x"21",x"39",x"19", -- 0x05B8
    x"22",x"69",x"20",x"CD",x"22",x"17",x"2E",x"57", -- 0x05C0
    x"4E",x"23",x"C5",x"E5",x"CD",x"38",x"0D",x"22", -- 0x05C8
    x"67",x"20",x"21",x"67",x"20",x"CD",x"0D",x"0B", -- 0x05D0
    x"CD",x"FD",x"09",x"21",x"68",x"20",x"7E",x"FE", -- 0x05D8
    x"28",x"DC",x"F5",x"15",x"21",x"65",x"20",x"CD", -- 0x05E0
    x"1C",x"05",x"2A",x"67",x"20",x"EB",x"C3",x"AA", -- 0x05E8
    x"1D",x"C2",x"CA",x"15",x"C9",x"E5",x"2A",x"67", -- 0x05F0
    x"20",x"06",x"0B",x"CD",x"E8",x"09",x"E1",x"36", -- 0x05F8
    x"ED",x"C9",x"CD",x"22",x"17",x"2E",x"AB",x"7E", -- 0x0600
    x"A7",x"21",x"00",x"FF",x"C8",x"FE",x"01",x"21", -- 0x0608
    x"00",x"FE",x"C8",x"C3",x"91",x"1E",x"C9",x"21", -- 0x0610
    x"11",x"27",x"0E",x"05",x"E5",x"21",x"94",x"20", -- 0x0618
    x"34",x"7E",x"A1",x"06",x"03",x"C2",x"2F",x"16", -- 0x0620
    x"E1",x"11",x"DF",x"19",x"C3",x"00",x"0A",x"E1", -- 0x0628
    x"C3",x"34",x"0A",x"21",x"0F",x"3B",x"0E",x"01", -- 0x0630
    x"E5",x"21",x"95",x"20",x"C3",x"20",x"16",x"21", -- 0x0638
    x"08",x"36",x"0E",x"04",x"E5",x"21",x"96",x"20", -- 0x0640
    x"C3",x"20",x"16",x"31",x"00",x"24",x"AF",x"D3", -- 0x0648
    x"03",x"D3",x"04",x"21",x"00",x"20",x"11",x"A3", -- 0x0650
    x"1A",x"06",x"00",x"CD",x"04",x"0B",x"21",x"00", -- 0x0658
    x"21",x"11",x"A3",x"1B",x"06",x"C0",x"D5",x"CD", -- 0x0660
    x"04",x"0B",x"21",x"00",x"22",x"D1",x"06",x"C0", -- 0x0668
    x"CD",x"04",x"0B",x"AF",x"D3",x"03",x"D3",x"05", -- 0x0670
    x"CD",x"A9",x"01",x"00",x"00",x"00",x"00",x"FB", -- 0x0678
    x"C3",x"B5",x"46",x"CD",x"CB",x"05",x"21",x"F7", -- 0x0680
    x"18",x"22",x"63",x"20",x"00",x"00",x"00",x"21", -- 0x0688
    x"39",x"19",x"22",x"61",x"20",x"06",x"06",x"CD", -- 0x0690
    x"22",x"17",x"2E",x"41",x"4E",x"23",x"C5",x"E5", -- 0x0698
    x"CD",x"38",x"0D",x"22",x"67",x"20",x"2A",x"63", -- 0x06A0
    x"20",x"C3",x"CB",x"1D",x"C2",x"9E",x"16",x"2A", -- 0x06A8
    x"63",x"20",x"11",x"0B",x"00",x"19",x"22",x"63", -- 0x06B0
    x"20",x"C5",x"CD",x"47",x"0C",x"CD",x"22",x"17", -- 0x06B8
    x"2E",x"57",x"4E",x"23",x"C5",x"E5",x"CD",x"38", -- 0x06C0
    x"0D",x"22",x"67",x"20",x"2A",x"61",x"20",x"C3", -- 0x06C8
    x"D6",x"1D",x"C2",x"C4",x"16",x"2A",x"61",x"20", -- 0x06D0
    x"11",x"0B",x"00",x"19",x"22",x"61",x"20",x"C1", -- 0x06D8
    x"05",x"C2",x"97",x"16",x"CD",x"D6",x"4F",x"CD", -- 0x06E0
    x"F1",x"1E",x"CD",x"22",x"17",x"2E",x"41",x"7E", -- 0x06E8
    x"FE",x"0A",x"D2",x"F6",x"16",x"34",x"2E",x"57", -- 0x06F0
    x"7E",x"FE",x"0B",x"D2",x"FF",x"16",x"34",x"CD", -- 0x06F8
    x"F6",x"1D",x"CD",x"0E",x"17",x"21",x"08",x"36", -- 0x0700
    x"CD",x"0E",x"17",x"21",x"0F",x"3B",x"06",x"03", -- 0x0708
    x"C3",x"34",x"0A",x"EB",x"2A",x"67",x"20",x"06", -- 0x0710
    x"0B",x"C3",x"FD",x"09",x"00",x"00",x"00",x"00", -- 0x0718
    x"00",x"00",x"3A",x"C6",x"20",x"67",x"C9",x"00", -- 0x0720
    x"01",x"02",x"03",x"00",x"01",x"02",x"03",x"00", -- 0x0728
    x"01",x"02",x"03",x"00",x"01",x"02",x"03",x"00", -- 0x0730
    x"01",x"02",x"03",x"00",x"01",x"02",x"03",x"00", -- 0x0738
    x"01",x"02",x"03",x"01",x"01",x"02",x"03",x"00", -- 0x0740
    x"01",x"02",x"03",x"00",x"01",x"02",x"03",x"00", -- 0x0748
    x"01",x"02",x"03",x"00",x"01",x"02",x"03",x"CD", -- 0x0750
    x"2C",x"0A",x"C5",x"E5",x"1A",x"D3",x"04",x"DB", -- 0x0758
    x"03",x"2F",x"A6",x"77",x"23",x"13",x"AF",x"D3", -- 0x0760
    x"04",x"DB",x"03",x"2F",x"A6",x"77",x"E1",x"01", -- 0x0768
    x"20",x"00",x"09",x"C1",x"05",x"C2",x"5A",x"17", -- 0x0770
    x"C9",x"CD",x"2C",x"0A",x"C5",x"E5",x"AF",x"D3", -- 0x0778
    x"04",x"DB",x"03",x"77",x"23",x"00",x"AF",x"D3", -- 0x0780
    x"04",x"DB",x"03",x"77",x"E1",x"01",x"20",x"00", -- 0x0788
    x"09",x"C1",x"05",x"C2",x"7C",x"17",x"C9",x"00", -- 0x0790
    x"00",x"00",x"00",x"80",x"01",x"C0",x"03",x"E0", -- 0x0798
    x"47",x"E0",x"47",x"F0",x"4F",x"F0",x"4F",x"F8", -- 0x07A0
    x"5D",x"F8",x"5D",x"FC",x"7F",x"FC",x"5F",x"FE", -- 0x07A8
    x"5D",x"FF",x"5D",x"FF",x"0F",x"FF",x"0F",x"01", -- 0x07B0
    x"1C",x"01",x"18",x"01",x"38",x"01",x"70",x"01", -- 0x07B8
    x"70",x"01",x"70",x"01",x"F0",x"01",x"F0",x"01", -- 0x07C0
    x"F0",x"01",x"F0",x"01",x"70",x"01",x"70",x"01", -- 0x07C8
    x"70",x"01",x"30",x"01",x"38",x"01",x"1C",x"FF", -- 0x07D0
    x"0F",x"FF",x"0F",x"FF",x"5D",x"FE",x"5D",x"FC", -- 0x07D8
    x"5F",x"FC",x"7F",x"F8",x"5D",x"F8",x"5D",x"F0", -- 0x07E0
    x"4F",x"F0",x"4F",x"E0",x"47",x"E0",x"47",x"C0", -- 0x07E8
    x"03",x"80",x"01",x"00",x"00",x"00",x"00",x"00", -- 0x07F0
    x"00",x"00",x"00",x"02",x"01",x"9E",x"03",x"E2"  -- 0x07F8
  );

begin

  p_rom : process
  begin
    wait until rising_edge(CLK);
     DATA <= ROM(to_integer(unsigned(ADDR)));
  end process;
end RTL;

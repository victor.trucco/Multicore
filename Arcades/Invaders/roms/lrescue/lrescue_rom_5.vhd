--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-- generated with romgen by MikeJ
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity LRESCUE_ROM_5 is
  port (
    CLK         : in    std_logic;
    ADDR        : in    std_logic_vector(10 downto 0);
    DATA        : out   std_logic_vector(7 downto 0)
    );
end;

architecture RTL of LRESCUE_ROM_5 is


  type ROM_ARRAY is array(0 to 2047) of std_logic_vector(7 downto 0);
  constant ROM : ROM_ARRAY := (
    x"C3",x"3D",x"40",x"35",x"C0",x"36",x"10",x"3E", -- 0x0000
    x"01",x"32",x"40",x"20",x"3A",x"1E",x"20",x"FE", -- 0x0008
    x"90",x"D2",x"27",x"40",x"3E",x"FD",x"32",x"48", -- 0x0010
    x"20",x"21",x"1D",x"20",x"3E",x"A8",x"96",x"23", -- 0x0018
    x"86",x"32",x"4A",x"20",x"C3",x"38",x"40",x"3E", -- 0x0020
    x"02",x"32",x"48",x"20",x"21",x"1D",x"20",x"3A", -- 0x0028
    x"1E",x"20",x"86",x"D6",x"90",x"32",x"4A",x"20", -- 0x0030
    x"3E",x"FD",x"C3",x"60",x"42",x"3A",x"40",x"20", -- 0x0038
    x"A7",x"C2",x"4A",x"40",x"21",x"41",x"20",x"C3", -- 0x0040
    x"03",x"40",x"21",x"42",x"20",x"35",x"C2",x"7C", -- 0x0048
    x"40",x"36",x"02",x"23",x"7E",x"A7",x"C2",x"91", -- 0x0050
    x"40",x"CD",x"EA",x"40",x"21",x"44",x"20",x"7E", -- 0x0058
    x"A7",x"C2",x"73",x"40",x"23",x"7E",x"FE",x"4D", -- 0x0060
    x"C2",x"7C",x"40",x"21",x"44",x"20",x"36",x"01", -- 0x0068
    x"C3",x"7C",x"40",x"23",x"7E",x"FE",x"55",x"C2", -- 0x0070
    x"7C",x"40",x"36",x"49",x"21",x"43",x"20",x"7E", -- 0x0078
    x"A7",x"C2",x"8A",x"40",x"21",x"47",x"20",x"CD", -- 0x0080
    x"1C",x"05",x"CD",x"BA",x"40",x"CD",x"03",x"41", -- 0x0088
    x"C9",x"2A",x"45",x"20",x"11",x"FC",x"FF",x"19", -- 0x0090
    x"22",x"45",x"20",x"7D",x"FE",x"25",x"C2",x"8A", -- 0x0098
    x"40",x"21",x"40",x"20",x"36",x"00",x"21",x"43", -- 0x00A0
    x"20",x"36",x"00",x"23",x"36",x"00",x"23",x"36", -- 0x00A8
    x"25",x"21",x"49",x"20",x"36",x"C0",x"23",x"36", -- 0x00B0
    x"D0",x"C9",x"2A",x"45",x"20",x"4E",x"23",x"46", -- 0x00B8
    x"23",x"5E",x"23",x"56",x"23",x"3A",x"44",x"20", -- 0x00C0
    x"A7",x"CA",x"D4",x"40",x"3E",x"01",x"32",x"42", -- 0x00C8
    x"20",x"C3",x"DC",x"40",x"3A",x"42",x"20",x"FE", -- 0x00D0
    x"02",x"C2",x"E6",x"40",x"3A",x"43",x"20",x"A7", -- 0x00D8
    x"C2",x"E6",x"40",x"22",x"45",x"20",x"2A",x"49", -- 0x00E0
    x"20",x"C9",x"3A",x"49",x"20",x"FE",x"48",x"DA", -- 0x00E8
    x"FD",x"40",x"3A",x"4A",x"20",x"FE",x"E0",x"D2", -- 0x00F0
    x"FD",x"40",x"FE",x"2A",x"D0",x"21",x"43",x"20", -- 0x00F8
    x"36",x"01",x"C9",x"CD",x"2C",x"0A",x"C5",x"E5", -- 0x0100
    x"1A",x"D3",x"04",x"DB",x"03",x"77",x"23",x"13", -- 0x0108
    x"0D",x"C2",x"08",x"41",x"AF",x"D3",x"04",x"DB", -- 0x0110
    x"03",x"77",x"E1",x"01",x"20",x"00",x"09",x"C1", -- 0x0118
    x"05",x"C2",x"06",x"41",x"C9",x"01",x"05",x"55", -- 0x0120
    x"41",x"01",x"05",x"55",x"41",x"01",x"08",x"5A", -- 0x0128
    x"41",x"01",x"08",x"62",x"41",x"01",x"0A",x"6A", -- 0x0130
    x"41",x"01",x"0B",x"74",x"41",x"01",x"0E",x"7F", -- 0x0138
    x"41",x"02",x"10",x"8D",x"41",x"02",x"12",x"AD", -- 0x0140
    x"41",x"02",x"17",x"CD",x"41",x"02",x"18",x"F5", -- 0x0148
    x"41",x"02",x"18",x"25",x"42",x"00",x"00",x"00", -- 0x0150
    x"00",x"00",x"00",x"00",x"00",x"04",x"04",x"00", -- 0x0158
    x"00",x"00",x"00",x"00",x"00",x"0C",x"0C",x"00", -- 0x0160
    x"00",x"00",x"00",x"00",x"00",x"08",x"1C",x"1C", -- 0x0168
    x"08",x"00",x"00",x"00",x"00",x"00",x"00",x"0C", -- 0x0170
    x"1E",x"1E",x"1E",x"0E",x"00",x"00",x"00",x"00", -- 0x0178
    x"00",x"00",x"4C",x"1C",x"3E",x"3F",x"3E",x"1C", -- 0x0180
    x"0C",x"20",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0188
    x"00",x"00",x"00",x"8D",x"00",x"3C",x"00",x"7E", -- 0x0190
    x"00",x"FF",x"00",x"FF",x"00",x"7E",x"00",x"3E", -- 0x0198
    x"00",x"0C",x"00",x"C0",x"00",x"01",x"00",x"00", -- 0x01A0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x01A8
    x"00",x"00",x"00",x"35",x"04",x"79",x"05",x"7C", -- 0x01B0
    x"00",x"FC",x"01",x"FE",x"03",x"FF",x"01",x"FE", -- 0x01B8
    x"01",x"FE",x"04",x"FC",x"00",x"3A",x"04",x"00", -- 0x01C0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x01C8
    x"00",x"00",x"00",x"62",x"08",x"F8",x"08",x"FC", -- 0x01D0
    x"03",x"FE",x"07",x"EC",x"0F",x"FF",x"07",x"FF", -- 0x01D8
    x"0F",x"BE",x"07",x"BC",x"0F",x"FD",x"07",x"FD", -- 0x01E0
    x"03",x"F8",x"01",x"F2",x"08",x"A8",x"02",x"04", -- 0x01E8
    x"04",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x01F0
    x"00",x"00",x"00",x"12",x"20",x"C8",x"04",x"E8", -- 0x01F8
    x"03",x"F4",x"03",x"FC",x"0F",x"FC",x"07",x"78", -- 0x0200
    x"0F",x"7A",x"27",x"FF",x"17",x"FE",x"0E",x"F8", -- 0x0208
    x"17",x"FC",x"0F",x"E8",x"0F",x"FC",x"05",x"FC", -- 0x0210
    x"17",x"B2",x"24",x"28",x"01",x"20",x"00",x"00", -- 0x0218
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0220
    x"00",x"00",x"00",x"E1",x"01",x"FB",x"30",x"FC", -- 0x0228
    x"07",x"FC",x"0F",x"FE",x"07",x"FE",x"2E",x"ED", -- 0x0230
    x"0E",x"FC",x"1F",x"FE",x"07",x"FE",x"0F",x"FD", -- 0x0238
    x"0F",x"F8",x"1F",x"BC",x"0F",x"FC",x"07",x"F8", -- 0x0240
    x"03",x"F4",x"13",x"81",x"10",x"00",x"00",x"00", -- 0x0248
    x"00",x"00",x"00",x"00",x"00",x"FF",x"FF",x"FF", -- 0x0250
    x"FF",x"FF",x"FF",x"FF",x"FF",x"FF",x"FF",x"FF", -- 0x0258
    x"32",x"47",x"20",x"21",x"4A",x"20",x"7E",x"FE", -- 0x0260
    x"D0",x"D4",x"75",x"42",x"FE",x"30",x"DC",x"7A", -- 0x0268
    x"42",x"C3",x"4A",x"40",x"00",x"36",x"D0",x"C9", -- 0x0270
    x"00",x"00",x"36",x"30",x"C9",x"C9",x"00",x"00", -- 0x0278
    x"CD",x"BD",x"0A",x"CD",x"32",x"46",x"C3",x"96", -- 0x0280
    x"42",x"3E",x"10",x"CD",x"D5",x"05",x"1A",x"C3", -- 0x0288
    x"CF",x"42",x"40",x"CD",x"D5",x"05",x"21",x"19", -- 0x0290
    x"28",x"11",x"10",x"44",x"0E",x"15",x"CD",x"2A", -- 0x0298
    x"05",x"3E",x"50",x"CD",x"D5",x"05",x"CD",x"69", -- 0x02A0
    x"43",x"21",x"90",x"2B",x"0E",x"07",x"CD",x"15", -- 0x02A8
    x"43",x"3E",x"01",x"CD",x"22",x"43",x"3E",x"40", -- 0x02B0
    x"CD",x"D5",x"05",x"CD",x"86",x"46",x"21",x"16", -- 0x02B8
    x"2E",x"11",x"15",x"45",x"1A",x"FE",x"FF",x"CA", -- 0x02C0
    x"E8",x"42",x"FE",x"5B",x"CA",x"E2",x"42",x"E6", -- 0x02C8
    x"3F",x"D5",x"CD",x"36",x"05",x"D1",x"CD",x"A4", -- 0x02D0
    x"43",x"13",x"3E",x"05",x"CD",x"D5",x"05",x"C3", -- 0x02D8
    x"C4",x"42",x"21",x"14",x"2F",x"C3",x"89",x"42", -- 0x02E0
    x"3E",x"01",x"CD",x"22",x"43",x"3E",x"50",x"CD", -- 0x02E8
    x"D5",x"05",x"0E",x"07",x"21",x"90",x"2B",x"06", -- 0x02F0
    x"10",x"11",x"33",x"45",x"CD",x"00",x"0A",x"0D", -- 0x02F8
    x"C2",x"F7",x"42",x"21",x"10",x"31",x"11",x"43", -- 0x0300
    x"45",x"01",x"02",x"10",x"CD",x"1B",x"0C",x"3E", -- 0x0308
    x"60",x"CD",x"D5",x"05",x"C9",x"11",x"B5",x"44", -- 0x0310
    x"06",x"10",x"CD",x"00",x"0A",x"0D",x"C2",x"15", -- 0x0318
    x"43",x"C9",x"F5",x"21",x"10",x"31",x"11",x"C5", -- 0x0320
    x"44",x"01",x"02",x"10",x"CD",x"1B",x"0C",x"F1", -- 0x0328
    x"3D",x"C8",x"F5",x"21",x"90",x"33",x"11",x"E5", -- 0x0330
    x"44",x"06",x"10",x"CD",x"00",x"0A",x"F1",x"3D", -- 0x0338
    x"C8",x"F5",x"11",x"F5",x"44",x"06",x"10",x"CD", -- 0x0340
    x"00",x"0A",x"F1",x"3D",x"C8",x"F5",x"7C",x"FE", -- 0x0348
    x"3D",x"C2",x"58",x"43",x"26",x"31",x"2D",x"2D", -- 0x0350
    x"0E",x"01",x"CD",x"15",x"43",x"F1",x"FE",x"1B", -- 0x0358
    x"DA",x"65",x"43",x"3E",x"1B",x"F5",x"C3",x"4A", -- 0x0360
    x"43",x"11",x"6D",x"44",x"21",x"13",x"2D",x"01", -- 0x0368
    x"04",x"08",x"CD",x"1B",x"0C",x"16",x"70",x"21", -- 0x0370
    x"12",x"2E",x"06",x"00",x"E5",x"36",x"80",x"0E", -- 0x0378
    x"05",x"09",x"36",x"01",x"E1",x"0E",x"20",x"09", -- 0x0380
    x"15",x"C2",x"7C",x"43",x"11",x"8D",x"44",x"21", -- 0x0388
    x"13",x"3C",x"01",x"04",x"08",x"CD",x"1B",x"0C", -- 0x0390
    x"11",x"AD",x"44",x"21",x"12",x"32",x"06",x"08", -- 0x0398
    x"CD",x"00",x"0A",x"C9",x"C5",x"E5",x"D5",x"1A", -- 0x03A0
    x"E6",x"80",x"CA",x"B3",x"43",x"11",x"05",x"45", -- 0x03A8
    x"C3",x"B6",x"43",x"11",x"0D",x"45",x"21",x"90", -- 0x03B0
    x"31",x"06",x"08",x"CD",x"00",x"0A",x"D1",x"E1", -- 0x03B8
    x"C1",x"C9",x"CD",x"34",x"0B",x"CD",x"32",x"46", -- 0x03C0
    x"3E",x"04",x"CD",x"22",x"43",x"CD",x"69",x"43", -- 0x03C8
    x"CD",x"A0",x"46",x"3E",x"10",x"CD",x"D5",x"05", -- 0x03D0
    x"0E",x"09",x"21",x"15",x"30",x"11",x"25",x"44", -- 0x03D8
    x"06",x"08",x"CD",x"A4",x"43",x"CD",x"00",x"0A", -- 0x03E0
    x"3E",x"05",x"CD",x"D5",x"05",x"0D",x"C2",x"E0", -- 0x03E8
    x"43",x"3E",x"01",x"CD",x"22",x"43",x"3E",x"60", -- 0x03F0
    x"CD",x"D5",x"05",x"21",x"15",x"30",x"11",x"28", -- 0x03F8
    x"45",x"0E",x"09",x"CD",x"2A",x"05",x"3E",x"40", -- 0x0400
    x"CD",x"D5",x"05",x"C9",x"0F",x"0B",x"00",x"18", -- 0x0408
    x"29",x"29",x"29",x"1B",x"0B",x"14",x"0D",x"00", -- 0x0410
    x"11",x"1B",x"1B",x"11",x"04",x"12",x"02",x"14", -- 0x0418
    x"04",x"1B",x"29",x"29",x"29",x"80",x"52",x"38", -- 0x0420
    x"7E",x"38",x"52",x"80",x"00",x"00",x"10",x"10", -- 0x0428
    x"7E",x"10",x"10",x"00",x"00",x"92",x"44",x"28", -- 0x0430
    x"92",x"28",x"44",x"92",x"00",x"38",x"44",x"38", -- 0x0438
    x"10",x"38",x"44",x"38",x"00",x"FE",x"10",x"38", -- 0x0440
    x"54",x"38",x"10",x"FE",x"00",x"00",x"00",x"00", -- 0x0448
    x"00",x"00",x"00",x"00",x"00",x"00",x"18",x"52", -- 0x0450
    x"7E",x"52",x"18",x"00",x"00",x"00",x"18",x"52", -- 0x0458
    x"7E",x"52",x"18",x"00",x"00",x"00",x"18",x"52", -- 0x0460
    x"7E",x"52",x"18",x"00",x"00",x"F0",x"FF",x"FF", -- 0x0468
    x"0F",x"08",x"00",x"00",x"10",x"04",x"00",x"00", -- 0x0470
    x"20",x"02",x"00",x"00",x"40",x"02",x"00",x"00", -- 0x0478
    x"40",x"01",x"00",x"00",x"80",x"01",x"00",x"00", -- 0x0480
    x"80",x"01",x"00",x"00",x"80",x"01",x"00",x"00", -- 0x0488
    x"80",x"01",x"00",x"00",x"80",x"02",x"00",x"00", -- 0x0490
    x"40",x"02",x"00",x"00",x"40",x"04",x"00",x"00", -- 0x0498
    x"20",x"08",x"00",x"00",x"10",x"30",x"00",x"00", -- 0x04A0
    x"0C",x"C0",x"FF",x"FF",x"03",x"80",x"80",x"83", -- 0x04A8
    x"4D",x"32",x"04",x"08",x"F0",x"30",x"7A",x"DD", -- 0x04B0
    x"F4",x"F4",x"DD",x"7A",x"30",x"00",x"00",x"00", -- 0x04B8
    x"00",x"00",x"00",x"00",x"00",x"E0",x"03",x"E6", -- 0x04C0
    x"03",x"E7",x"03",x"FF",x"0F",x"79",x"0F",x"78", -- 0x04C8
    x"0F",x"6C",x"1F",x"EC",x"1F",x"EC",x"1F",x"6C", -- 0x04D0
    x"1F",x"78",x"0F",x"79",x"0F",x"FF",x"0F",x"E7", -- 0x04D8
    x"03",x"E6",x"03",x"E0",x"03",x"0E",x"18",x"BE", -- 0x04E0
    x"6D",x"3D",x"3C",x"3D",x"6D",x"BE",x"18",x"0E", -- 0x04E8
    x"00",x"00",x"00",x"00",x"00",x"19",x"3A",x"6D", -- 0x04F0
    x"FA",x"FA",x"6D",x"3A",x"19",x"00",x"00",x"00", -- 0x04F8
    x"00",x"00",x"00",x"00",x"00",x"79",x"7C",x"6E", -- 0x0500
    x"E6",x"E6",x"6E",x"7C",x"79",x"70",x"78",x"78", -- 0x0508
    x"F8",x"F8",x"78",x"78",x"79",x"8F",x"0B",x"84", -- 0x0510
    x"00",x"92",x"04",x"5B",x"88",x"0D",x"92",x"04", -- 0x0518
    x"91",x"13",x"1B",x"02",x"8E",x"08",x"8D",x"FF", -- 0x0520
    x"05",x"08",x"06",x"07",x"13",x"1B",x"30",x"30", -- 0x0528
    x"30",x"1B",x"FF",x"38",x"7C",x"EC",x"FE",x"B7", -- 0x0530
    x"E5",x"F8",x"60",x"34",x"0C",x"00",x"00",x"00", -- 0x0538
    x"00",x"00",x"00",x"10",x"00",x"B8",x"00",x"FC", -- 0x0540
    x"05",x"FC",x"0F",x"CC",x"1F",x"9F",x"1F",x"BF", -- 0x0548
    x"0F",x"FD",x"1F",x"C8",x"3C",x"98",x"19",x"F0", -- 0x0550
    x"3B",x"E0",x"7F",x"C0",x"3F",x"C4",x"1F",x"FC", -- 0x0558
    x"0D",x"38",x"00",x"CD",x"BD",x"0A",x"CD",x"32", -- 0x0560
    x"46",x"3E",x"30",x"CD",x"D5",x"05",x"21",x"18", -- 0x0568
    x"25",x"11",x"7B",x"19",x"01",x"01",x"13",x"CD", -- 0x0570
    x"1B",x"0C",x"21",x"18",x"29",x"11",x"8E",x"19", -- 0x0578
    x"01",x"01",x"13",x"CD",x"1B",x"0C",x"21",x"15", -- 0x0580
    x"25",x"11",x"A1",x"19",x"01",x"01",x"13",x"CD", -- 0x0588
    x"1B",x"0C",x"21",x"0C",x"27",x"11",x"39",x"18", -- 0x0590
    x"01",x"01",x"10",x"CD",x"1B",x"0C",x"21",x"0F", -- 0x0598
    x"26",x"11",x"49",x"18",x"01",x"01",x"20",x"CD", -- 0x05A0
    x"1B",x"0C",x"21",x"12",x"25",x"11",x"69",x"18", -- 0x05A8
    x"01",x"01",x"28",x"CD",x"1B",x"0C",x"CD",x"86", -- 0x05B0
    x"46",x"2E",x"18",x"26",x"2C",x"3E",x"1A",x"CD", -- 0x05B8
    x"36",x"05",x"3E",x"0A",x"CD",x"D5",x"05",x"3E", -- 0x05C0
    x"01",x"84",x"67",x"FE",x"33",x"DA",x"BD",x"45", -- 0x05C8
    x"0E",x"03",x"7D",x"FE",x"18",x"C2",x"DE",x"45", -- 0x05D0
    x"11",x"1C",x"46",x"C3",x"02",x"46",x"FE",x"15", -- 0x05D8
    x"C2",x"E9",x"45",x"11",x"1F",x"46",x"C3",x"02", -- 0x05E0
    x"46",x"FE",x"12",x"C2",x"F4",x"45",x"11",x"22", -- 0x05E8
    x"46",x"C3",x"02",x"46",x"FE",x"0F",x"C2",x"FF", -- 0x05F0
    x"45",x"11",x"25",x"46",x"C3",x"02",x"46",x"11", -- 0x05F8
    x"28",x"46",x"CD",x"2A",x"05",x"0E",x"07",x"11", -- 0x0600
    x"2B",x"46",x"CD",x"2A",x"05",x"7D",x"D6",x"03", -- 0x0608
    x"6F",x"FE",x"09",x"C2",x"BB",x"45",x"3E",x"60", -- 0x0610
    x"C3",x"D5",x"05",x"C9",x"1B",x"1F",x"1C",x"1B", -- 0x0618
    x"21",x"1C",x"1B",x"21",x"1C",x"1D",x"1C",x"1C", -- 0x0620
    x"1D",x"21",x"1C",x"1B",x"0F",x"0E",x"08",x"0D", -- 0x0628
    x"13",x"12",x"2E",x"07",x"26",x"3B",x"11",x"69", -- 0x0630
    x"46",x"D5",x"1A",x"3C",x"CD",x"36",x"05",x"D1", -- 0x0638
    x"13",x"25",x"25",x"7D",x"FE",x"09",x"CA",x"56", -- 0x0640
    x"46",x"7C",x"FE",x"2A",x"C2",x"39",x"46",x"2E", -- 0x0648
    x"09",x"26",x"33",x"C3",x"39",x"46",x"7C",x"FE", -- 0x0650
    x"2F",x"C2",x"39",x"46",x"26",x"29",x"2E",x"07", -- 0x0658
    x"11",x"7E",x"46",x"06",x"08",x"CD",x"00",x"0A", -- 0x0660
    x"C9",x"0C",x"0D",x"07",x"12",x"FF",x"10",x"0D", -- 0x0668
    x"0E",x"10",x"0D",x"01",x"1A",x"0D",x"12",x"07", -- 0x0670
    x"FF",x"12",x"24",x"22",x"24",x"1C",x"3C",x"42", -- 0x0678
    x"99",x"A5",x"A5",x"81",x"42",x"3C",x"21",x"47", -- 0x0680
    x"2B",x"01",x"47",x"2C",x"11",x"27",x"2E",x"1A", -- 0x0688
    x"D6",x"40",x"C0",x"0A",x"D6",x"44",x"C0",x"7E", -- 0x0690
    x"D6",x"7F",x"C0",x"AF",x"32",x"ED",x"20",x"C9", -- 0x0698
    x"26",x"2B",x"2E",x"47",x"11",x"00",x"02",x"7E", -- 0x06A0
    x"19",x"86",x"19",x"86",x"FE",x"3F",x"C2",x"62", -- 0x06A8
    x"03",x"C9",x"ED",x"20",x"C9",x"CD",x"80",x"42", -- 0x06B0
    x"00",x"00",x"00",x"CD",x"63",x"45",x"CD",x"CB", -- 0x06B8
    x"05",x"CD",x"DF",x"02",x"CD",x"D4",x"02",x"CD", -- 0x06C0
    x"BD",x"0A",x"CD",x"CA",x"01",x"CD",x"78",x"1F", -- 0x06C8
    x"CD",x"61",x"02",x"CD",x"79",x"02",x"21",x"BF", -- 0x06D0
    x"21",x"36",x"01",x"CD",x"F2",x"02",x"CD",x"A9", -- 0x06D8
    x"01",x"21",x"27",x"20",x"7E",x"A7",x"CA",x"E1", -- 0x06E0
    x"46",x"23",x"7E",x"FE",x"01",x"D3",x"06",x"C2", -- 0x06E8
    x"EA",x"46",x"3A",x"2E",x"20",x"FE",x"70",x"D3", -- 0x06F0
    x"06",x"DA",x"2F",x"47",x"C3",x"F2",x"46",x"00", -- 0x06F8
    x"CD",x"B7",x"0A",x"3A",x"C6",x"20",x"32",x"E3", -- 0x0700
    x"20",x"C9",x"CD",x"CB",x"05",x"CD",x"BD",x"0A", -- 0x0708
    x"3A",x"E3",x"20",x"32",x"C6",x"20",x"CD",x"76", -- 0x0710
    x"4B",x"00",x"00",x"00",x"CD",x"85",x"08",x"00", -- 0x0718
    x"00",x"00",x"C3",x"57",x"06",x"3A",x"BF",x"21", -- 0x0720
    x"A7",x"C2",x"AA",x"0E",x"C3",x"60",x"00",x"21", -- 0x0728
    x"29",x"20",x"36",x"00",x"2A",x"2D",x"20",x"3A", -- 0x0730
    x"2C",x"20",x"FE",x"FE",x"01",x"00",x"14",x"C4", -- 0x0738
    x"85",x"01",x"09",x"22",x"1D",x"20",x"CD",x"6D", -- 0x0740
    x"0D",x"21",x"13",x"20",x"36",x"01",x"CD",x"EC", -- 0x0748
    x"47",x"00",x"00",x"00",x"00",x"00",x"D3",x"06", -- 0x0750
    x"21",x"14",x"20",x"7E",x"A7",x"C2",x"A1",x"47", -- 0x0758
    x"CD",x"D8",x"47",x"21",x"5D",x"20",x"7E",x"A7", -- 0x0760
    x"C2",x"92",x"47",x"CD",x"77",x"0C",x"D3",x"06", -- 0x0768
    x"3A",x"1D",x"20",x"FE",x"B0",x"DC",x"7F",x"02", -- 0x0770
    x"3A",x"1D",x"20",x"FE",x"B0",x"D4",x"58",x"0D", -- 0x0778
    x"3A",x"16",x"20",x"A7",x"CC",x"9D",x"0D",x"D3", -- 0x0780
    x"06",x"CD",x"05",x"4C",x"CD",x"7B",x"48",x"C3", -- 0x0788
    x"4E",x"47",x"CD",x"29",x"03",x"CD",x"DF",x"02", -- 0x0790
    x"CD",x"D4",x"02",x"CD",x"CB",x"05",x"C3",x"73", -- 0x0798
    x"16",x"CD",x"67",x"04",x"3A",x"AE",x"20",x"A7", -- 0x07A0
    x"C2",x"92",x"47",x"21",x"AC",x"09",x"22",x"10", -- 0x07A8
    x"20",x"C3",x"60",x"47",x"CD",x"98",x"05",x"CD", -- 0x07B0
    x"22",x"17",x"2E",x"B0",x"7E",x"A7",x"C8",x"36", -- 0x07B8
    x"00",x"2E",x"AB",x"7E",x"FE",x"03",x"CC",x"CF", -- 0x07C0
    x"47",x"CD",x"3B",x"4A",x"C3",x"98",x"05",x"CD", -- 0x07C8
    x"C2",x"43",x"CD",x"B9",x"4D",x"C3",x"98",x"05", -- 0x07D0
    x"CD",x"7C",x"03",x"E6",x"76",x"FE",x"54",x"C0", -- 0x07D8
    x"2E",x"ED",x"26",x"20",x"7E",x"A7",x"C8",x"2B", -- 0x07E0
    x"2B",x"36",x"03",x"C9",x"3A",x"16",x"20",x"A7", -- 0x07E8
    x"C8",x"21",x"00",x"20",x"36",x"01",x"C9",x"00", -- 0x07F0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00"  -- 0x07F8
  );

begin

  p_rom : process
  begin
    wait until rising_edge(CLK);
     DATA <= ROM(to_integer(unsigned(ADDR)));
  end process;
end RTL;

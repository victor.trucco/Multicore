--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library IEEE;
use IEEE.std_logic_1164.all;

package fx68k is
COMPONENT fx68k
PORT
(
	clk             : in std_logic;
	extReset        : in std_logic; -- External sync reset on emulated system
	pwrUp           : in std_logic; -- Asserted together with reset on emulated system coldstart
	enPhi1          : in std_logic;
	enPhi2          : in std_logic; -- Clock enables. Next cycle is PHI1 or PHI2

	eRWn            : out std_logic;
	ASn             : out std_logic;
	LDSn            : out std_logic;
	UDSn            : out std_logic;
	E               : out std_logic;
	VMAn            : out std_logic;
	FC0             : out std_logic;
	FC1             : out std_logic;
	FC2             : out std_logic;
	BGn             : out std_logic;
	oRESETn         : out std_logic;
	oHALTEDn        : out std_logic;
	DTACKn          : in std_logic;
	VPAn            : in std_logic;
	BERRn           : in std_logic;
	BRn             : in std_logic;
	BGACKn          : in std_logic;
	IPL0n           : in std_logic;
	IPL1n           : in std_logic;
	IPL2n           : in std_logic;
	iEdb            : in std_logic_vector(15 downto 0);
	oEdb            : out std_logic_vector(15 downto 0);
	eab             : out std_logic_vector(23 downto 1)
);
END COMPONENT;
end package;
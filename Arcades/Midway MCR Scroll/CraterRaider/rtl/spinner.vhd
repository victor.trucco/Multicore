--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity spinner is
port(
 clock_40       : in std_logic;
 reset          : in std_logic;
 btn_left       : in std_logic;
 btn_right      : in std_logic;
 btn_acc        : in std_logic; -- speed up button
 ctc_zc_to_2    : in std_logic;
 spin_angle     : out std_logic_vector(7 downto 0)
);
end spinner;

architecture rtl of spinner is

signal ctc_zc_to_2_r : std_logic;
signal spin_count    : std_logic_vector(9 downto 0);

begin

spin_angle <= spin_count(9 downto 2);

process (clock_40, reset)
begin
	if reset = '1' then
		spin_count <= (others => '0');
	elsif rising_edge(clock_40) then
		ctc_zc_to_2_r <= ctc_zc_to_2;

		if ctc_zc_to_2_r ='0' and ctc_zc_to_2 = '1' then
			if btn_acc = '0' then  -- space -- speed up
				if btn_left = '1' then spin_count <= spin_count + 5; end if; -- left
				if btn_right = '1' then spin_count <= spin_count - 5; end if; -- right
			else
				if btn_left = '1' then spin_count <= spin_count + 10; end if;
				if btn_right = '1' then spin_count <= spin_count - 10; end if;
			end if;
		end if;
	end if;
end process;

end rtl;
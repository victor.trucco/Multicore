//============================================================================
//  Arcade: TraverseUSA, ShotRider
//
//  DarFPGA's core ported to MiST by (C) 2019 Szombathelyi György
//
//  This program is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation; either version 2 of the License, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//============================================================================

//============================================================================
//
//  Multicore 2+ / Neptuno Top by Victor Trucco
//
//============================================================================

`default_nettype none

module TraverseUSA_mc2p(
     // Clocks
    input wire  clock_50_i,

    `ifdef NEPTUNO
    // SRAM (IS61WV102416BLL-10TLI)
    output [19:0]sram_addr_o = 20'b00000000000000000000,
    inout  [15:0]sram_data_io = 8'hZZZZ,
    output sram_we_n_o = 1'b1,
    output sram_oe_n_o = 1'b1,
    output sram_ub_n_o = 1'b1,
    output sram_lb_n_o = 1'b1,
    `else
    // SRAM (IS61WV20488FBLL-10)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1, 

    //external buttons
    input wire [4:1]    btn_n_i,
    `endif
        
    // SDRAM (W9825G6KH-6)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io        = 1'bz,
    inout wire  ps2_data_io       = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o         = 1'bZ,
    output wire sd_sclk_o         = 1'bZ,
    output wire sd_mosi_o         = 1'bZ,
    input wire  sd_miso_i,

    // Joysticks
    output wire joy_clock_o       = 1'b1,
    output wire joy_load_o        = 1'b1,
    input  wire joy_data_i,
    output wire joy_p7_o          = 1'b1,

    // Audio
    output      AUDIO_L,
    output      AUDIO_R,
    input wire  ear_i,
    output wire mic_o             = 1'b0,

    // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

    //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
   
    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output wire   SPI_nWAIT         = 1'b1, // '0' to hold the microcontroller data streaming

    inout [31:0] GPIO,

    output LED                      = 1'b1 // '0' is LED on
);

`include "../../rtl/build_id.v" 

`define CORE_NAME "TRAVRUSA"
wire [6:0] core_mod;

localparam CONF_STR = {
    `ifdef NEPTUNO
    "P,CORE_NAME.dat;",
    `else
    "P,CORE_NAME.dat;",
    `endif
    "O12,Screen Rotate,0,90,180,270;",
    "O34,Scanlines,Off,25%,50%,75%;",
   // "OB,Video Timing,Original,Pal 50Hz;",
    "OA,Blending,Off,On;",
    "OC,Scandoubler,On,Off;",
    "O5,Units,MP,Km;",
    "O6,Freeze,Disable,Enable;",
    "T0,Reset;",
    "V,v1.0.",`BUILD_DATE
};


//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------
assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign stm_rst_o = 1'bZ;
assign stm_rx_o  = 1'bZ;

//no SRAM for this core
assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;

//all the SD reading goes thru the microcontroller for this core
assign sd_cs_n_o = 1'bZ;
assign sd_sclk_o = 1'bZ;
assign sd_mosi_o = 1'bZ;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

`ifdef NEPTUNO
    wire [4:1] btn_n_i = 4'b1111;

    neptuno_joydecoder  neptuno_joydecoder (
`else 
    joystick_serial  joystick_serial (
`endif 

    .clk_i           ( clock_50_i ),
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

// ROM Data Pump
reg [7:0] pump_s = 8'b11111111;
PumpSignal PumpSignal (clk_sys, ~pll_locked, ioctl_downl, pump_s);

//-----------------------------------------------------------------

wire       rotate = 1'b0; //status[2];
wire [1:0] scanlines = status[4:3];
wire       blend = status[10];
wire       pal = status[11];

reg        shtrider = 0;
wire [7:0] dip1 = 8'hff;
reg  [7:0] dip2;

always @(*) begin

    if (core_mod == 7'h1) begin
        shtrider = 1;
        // Cocktail(3) / M-Km(1) / Flip(0)
        dip2 = { 4'b1111, 2'b11, status[5], 1'b0 };
    end else begin
        shtrider = 0;
        // Diag(7) / Demo(6) / Zippy(5) / Freeze (4) / M-Km(3) / Coin mode (2) / Cocktail(1) / Flip(0)
        dip2 = { ~status[9], ~status[8], ~status[7], ~status[6], ~status[5], 3'b110 };
    end
end

assign LED = 1;
assign AUDIO_R = AUDIO_L;
//assign SDRAM_CLK = clk_sys;
assign SDRAM_CKE = 1;

wire clk_sys, clk_aud;
wire pll_locked;
pll_mist pll(
    .inclk0(clock_50_i),
    .c0(clk_sys),
    .c1(clk_aud),
    .c2(SDRAM_CLK),
    .locked(pll_locked)
    );

wire clk_25m2,clk_40;    
pll_vga pll_vga(
    .inclk0(clock_50_i),
    .c0(clk_25m2),
    .c1(clk_40)
);

wire [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire        scandoublerD;
wire        ypbpr;
wire        no_csync;
wire        key_pressed;
wire  [7:0] key_code;
wire        key_strobe;
/*
user_io #(
    .STRLEN(($size(CONF_STR)>>3)))
user_io(
    .clk_sys        (clk_sys        ),
    .conf_str       (CONF_STR       ),
    .SPI_CLK        (SPI_SCK        ),
    .SPI_SS_IO      (CONF_DATA0     ),
    .SPI_MISO       (SPI_DO         ),
    .SPI_MOSI       (SPI_DI         ),
    .buttons        (buttons        ),
    .switches       (switches       ),
    .scandoubler_disable (scandoublerD    ),
    .ypbpr          (ypbpr          ),
    .no_csync       (no_csync       ),
    .core_mod       (core_mod       ),
    .key_strobe     (key_strobe     ),
    .key_pressed    (key_pressed    ),
    .key_code       (key_code       ),
    .joystick_0     (joystick_0     ),
    .joystick_1     (joystick_1     ),
    .status         (status         )
    );
    */
wire [14:0] cart_addr;
wire [15:0] sdram_do;
wire        cart_rd;
wire [12:0] snd_addr;
wire [15:0] snd_do;

wire        ioctl_downl;
wire  [7:0] ioctl_index;
wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire  [7:0] ioctl_dout;

/* ROM structure
00000-07FFF CPU ROM  32k zr1-0.m3 zr1-5.l3 zr1-6a.k3 zr1-7.j3
08000-09FFF SND ROM   8k mr10.1a mr10.1a
0A000-0FFFF GFX1     24k zippyrac.001 mr8.3c mr9.3a
10000-15FFF GFX2     24k zr1-8.n3 zr1-9.l3 zr1-10.k3
16000-161FF CHR PAL 512b mmi6349.ij
16200-162FF SPR PAL 256b tbp24s10.3
16300-1631F SPR LUT  32b tbp18s.2
*/
data_io #(.STRLEN(($size(CONF_STR)>>3)))
data_io(
    .clk_sys       ( clk_sys      ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),

     .SPI_DO        ( SPI_DO       ),
    
    .data_in       ( pump_s & keys_s ),
    .conf_str      ( CONF_STR     ),
    .status        ( status       ),
    .core_mod      ( core_mod     ),

    .ioctl_download( ioctl_downl  ),
    .ioctl_index   ( ioctl_index  ),
    .ioctl_wr      ( ioctl_wr     ),
    .ioctl_addr    ( ioctl_addr   ),
    .ioctl_dout    ( ioctl_dout   )
);

reg port1_req, port2_req;
sdram sdram(
    .*,
    .init_n        ( pll_locked   ),
    .clk           ( clk_sys      ),

    // port1 used for main CPU
    .port1_req     ( port1_req    ),
    .port1_ack     ( ),
    .port1_a       ( ioctl_addr[23:1] ),
    .port1_ds      ( {ioctl_addr[0], ~ioctl_addr[0]} ),
    .port1_we      ( ioctl_downl ),
    .port1_d       ( {ioctl_dout, ioctl_dout} ),
    .port1_q       ( ),

    .cpu1_addr     ( ioctl_downl ? 15'h7fff : {1'b0, cart_addr[14:1]} ),
    .cpu1_q        ( sdram_do ),
 
    // port2 for sound board
    .port2_req     ( port2_req ),
    .port2_ack     ( ),
    .port2_a       ( ioctl_addr[23:1] - 16'h4000 ),
    .port2_ds      ( {ioctl_addr[0], ~ioctl_addr[0]} ),
    .port2_we      ( ioctl_downl ),
    .port2_d       ( {ioctl_dout, ioctl_dout} ),
    .port2_q       ( ),

    .snd_addr      ( ioctl_downl ? 15'h7fff : {3'b000, snd_addr[12:1]} ),
    .snd_q         ( snd_do )
);

always @(posedge clk_sys) begin
    reg        ioctl_wr_last = 0;

    ioctl_wr_last <= ioctl_wr;
    if (ioctl_downl) begin
        if (~ioctl_wr_last && ioctl_wr) begin
            port1_req <= ~port1_req;
            port2_req <= ~port2_req;
        end
    end
end

reg reset = 1;
reg rom_loaded = 0;
always @(posedge clk_sys) begin
    reg ioctl_downlD;
    ioctl_downlD <= ioctl_downl;

    if (ioctl_downlD & ~ioctl_downl) rom_loaded <= 1;
    reset <= status[0] | ~btn_n_o[4] | ~rom_loaded;
end

wire [10:0] audio;
wire        hs, vs;
wire        blankn;
wire  [2:0] g,b;
wire  [1:0] r;
wire        clk_pix, hb, vb;

// Traverse_usa
traverse_usa traverse_usa (
    .clock_36     ( clk_sys         ),
    .clock_0p895  ( clk_aud         ),
    .clk_pix      ( clk_pix         ),
    .reset        ( reset           ),

    .palmode      ( pal             ),
    .shtrider     ( shtrider        ),
    
    .video_r      ( r               ),
    .video_g      ( g               ),
    .video_b      ( b               ),
    .video_hs     ( hs              ),
    .video_vs     ( vs              ),
    .video_blankn ( blankn          ),
    .video_hb     ( hb              ),
    .video_vb     ( vb              ),

    .audio_out    ( audio           ),

    .dip_switch_1 ( dip1            ),  
    .dip_switch_2 ( dip2            ),

    .start2       ( m_two_players   ),
    .start1       ( m_one_player    ),
    .coin1        ( m_coin1         ),

    .right1       ( m_right         ),
    .left1        ( m_left          ),
    .brake1       ( m_down          ),
    .accel1       ( m_up            ),

    .right2       ( m_right2        ),
    .left2        ( m_left2         ),
    .brake2       ( m_down2         ),
    .accel2       ( m_up2           ),

    .cpu_rom_addr ( cart_addr       ),
    .cpu_rom_do   ( cart_addr[0] ? sdram_do[15:8] : sdram_do[7:0] ),
    .cpu_rom_rd   ( cart_rd         ),
    .snd_rom_addr ( snd_addr        ),
    .snd_rom_do   ( snd_addr[0] ? snd_do[15:8] : snd_do[7:0] ),
    .dl_addr      ( ioctl_addr[16:0]),
    .dl_data      ( ioctl_dout      ),
    .dl_wr        ( ioctl_wr        )
);

wire direct_video_s = ~status[12] ^ direct_video;

wire [8:0] vga_col_s;
wire vga_hs_s, vga_vs_s;

framebuffer #(256,240,9) framebuffer
(
        .clk_sys    ( clk_sys ),
        .clk_i      ( clk_pix ),
        .RGB_i      ((blankn) ? {r, r[1], g, b } : 9'b000000000 ),
        .hblank_i   ( hb ),
        .vblank_i   ( vb ),
        
        .rotate_i   ( status[2:1] ), 

        .clk_vga_i  ( (status[1]) ? clk_40 : clk_25m2 ), //800x600 or 640x480
        .RGB_o      ( vga_col_s ),
        .hsync_o    ( vga_hs_s ),
        .vsync_o    ( vga_vs_s ),
        .blank_o    (  ),

        .odd_line_o (  )
);

mist_video #(.COLOR_DEPTH(3), .SD_HCNT_WIDTH(10), .USE_FRAMEBUFFER(1)) mist_video(

    .clk_sys( direct_video_s ? clk_sys :(status[1]) ? clk_40 : clk_25m2),

    .SPI_SCK        ( SPI_SCK          ),
    .SPI_SS3        ( SPI_SS2          ),
    .SPI_DI         ( SPI_DI           ),
/*
    .clk_sys        ( clk_sys          ),
    .R              ( blankn ? {r, r[1] } : 0 ),
    .G              ( blankn ? g : 0   ),
    .B              ( blankn ? b : 0   ),
    .HSync          ( hs               ),
    .VSync          ( vs               ),
*/
    .R       ( (direct_video_s) ? (blankn) ? {r, r[1] } : 3'b000 : vga_col_s[8:6] ),
    .G       ( (direct_video_s) ? (blankn) ? g          : 3'b000 : vga_col_s[5:3] ),
    .B       ( (direct_video_s) ? (blankn) ? b          : 3'b000 : vga_col_s[2:0] ),
    .HSync   ( (direct_video_s) ? hs : vga_hs_s ),
    .VSync   ( (direct_video_s) ? vs : vga_vs_s ),

    .VGA_R          ( VGA_R            ),
    .VGA_G          ( VGA_G            ),
    .VGA_B          ( VGA_B            ),
    .VGA_VS         ( VGA_VS           ),
    .VGA_HS         ( VGA_HS           ),

    .rotate         ( osd_rotate       ),
    .scandoubler_disable( direct_video_s ),
    .scanlines      ( scanlines        ),
    .osd_enable     ( osd_enable       ),
    .no_csync       ( ~direct_video_s  ),
    .ce_divider     ( 1'b0             ),
    .blend          ( blend            )
    );

dac #(
    .C_bits(11))
dac(
    .clk_i(clk_aud),
    .res_n_i(~reset),
    .dac_i(audio),
    .dac_o(AUDIO_L)
    );

wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player  = ~btn_n_o[1] | m_one_player;
wire btn_two_players = ~btn_n_o[2] | m_two_players;
wire btn_coin        = ~btn_n_o[3] | m_coin1;
wire [7:0] keys_s;
wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable;
wire direct_video;
wire [1:0]osd_rotate;
wire [4:1]btn_n_o;

//translate scancode to joystick
MC2_HID #( .OSD_CMD( 3'b011 ), .USE_VKP( 1'b1), .CLK_SPEED(36000)) k_hid
(
    .clk          ( clk_sys ),
    .kbd_clk      ( ps2_clk_io ),
    .kbd_dat      ( ps2_data_io ),
    .joystick_0   ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1   ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
      
    //-- joystick_0 and joystick_1 should be swapped
    .joyswap      ( 0 ),

    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer    ( 0 ),

    //-- tilt, coin4-1, start4-1
    .controls     ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

    //-- fire12-1, up, down, left, right

    .player1      ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2      ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),

    .direct_video ( direct_video ),
    .osd_rotate   ( osd_rotate ),

    //-- keys to the OSD
    .osd_o        ( keys_s ),
    .osd_enable   ( osd_enable ),
    
    //-- sega joystick
    .sega_strobe  ( joy_p7_o ),

     //-- Front buttons
     .front_buttons_i (btn_n_i),
     .front_buttons_o (btn_n_o)        
);


endmodule 

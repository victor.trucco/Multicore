//============================================================================
//
//  Multicore 2+ / Neptuno Top by Victor Trucco
//
//============================================================================

`default_nettype none

module Arkanoid_MC2p (
    // Clocks
    input wire  clock_50_i,   

    `ifdef NEPTUNO
    // SRAM (IS61WV102416BLL-10TLI)
    output [19:0]sram_addr_o = 20'b00000000000000000000,
    inout  [15:0]sram_data_io = 8'hZZZZ,
    output sram_we_n_o = 1'b1,
    output sram_oe_n_o = 1'b1,
    output sram_ub_n_o = 1'b1,
    output sram_lb_n_o = 1'b1,
    `else
    // SRAM (IS61WV20488FBLL-10)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1, 

    //external buttons
    input wire [4:1]    btn_n_i,
    `endif
        
    // SDRAM (W9825G6KH-6)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io        = 1'bz,
    inout wire  ps2_data_io       = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o         = 1'bZ,
    output wire sd_sclk_o         = 1'bZ,
    output wire sd_mosi_o         = 1'bZ,
    input wire  sd_miso_i,

    // Joysticks
    output wire joy_clock_o       = 1'b1,
    output wire joy_load_o        = 1'b1,
    input  wire joy_data_i,
    output wire joy_p7_o          = 1'b1,

    // Audio
    output      AUDIO_L,
    output      AUDIO_R,
    input wire  ear_i,
    output wire mic_o             = 1'b0,

    // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

    //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
   
    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output wire   SPI_nWAIT         = 1'b1, // '0' to hold the microcontroller data streaming

    inout [31:0] GPIO,

    output LED                      = 1'b1 // '0' is LED on

);

`include "..\..\rtl\build_id.v" 

localparam CONF_STR = {
    "P,CORE_NAME.dat;",
    "S,DAT,Alternative ROM...;",
	 "O78,Screen Rotation,0,90,180,270;",
    "O34,Scanlines,Off,25%,50%,75%;",
    "O5,Blend,Off,On;",
    "O6,Service,Off,On;",
    "O9,Scandoubler,On,Off;",
    "T0,Reset;",
    "V,v1.00.",`BUILD_DATE
};

wire        rotate = status[2];
wire  [1:0] scanlines = status[4:3];
wire        blend = status[5];
wire        joyswap = 0;
wire        service = status[6];
wire  [1:0] orientation = 2'b11;
wire  [7:0] dip_sw = status[15:8];


//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------
assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign stm_rx_o = 1'bZ;

//no SRAM for this core
assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;

//all the SD reading goes thru the microcontroller for this core
assign sd_cs_n_o = 1'bZ;
assign sd_sclk_o = 1'bZ;
assign sd_mosi_o = 1'bZ;
assign stm_rst_o = 1'bz;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

`ifdef NEPTUNO
    wire [4:1] btn_n_i = 4'b1111;

    neptuno_joydecoder  neptuno_joydecoder (
`else 
    joystick_serial  joystick_serial (
`endif

    .clk_i           ( clock_50_i ),
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);
//-- END defaults -------------------------------------------------------

// ROM Data Pump
reg [7:0] osd_s = 8'b11111111;
PumpSignal PumpSignal (clock_96, ~pll_locked, ioctl_downl, osd_s);

assign      LED = ~ioctl_downl;
assign      SDRAM_CLK = clock_96;
assign      SDRAM_CKE = 1;
assign      AUDIO_R = AUDIO_L;

wire clock_48, clock_96, pll_locked;
pll pll(
    .inclk0(clock_50_i),
    .c0(clock_96),
    .c1(clock_48),
    .locked(pll_locked)
    );

wire clk_25m2,clk_40;
pll_vga pll_vga(
    .inclk0(clock_50_i),
    .c0(clk_25m2),
    .c1(clk_40)
    );
	 
wire [63:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire        scandoublerD;
wire        ypbpr;
wire        no_csync;
wire  [6:0] core_mod;
wire        key_strobe;
wire        key_pressed;
wire  [7:0] key_code;
wire        mouse_strobe;
wire  [8:0] mouse_x;
wire  [7:0] mouse_flags;

/*
user_io #(.STRLEN(($size(CONF_STR)>>3)))user_io(
    .clk_sys        (clock_48       ),
    .conf_str       (CONF_STR       ),
    .SPI_CLK        (SPI_SCK        ),
    .SPI_SS_IO      (CONF_DATA0     ),
    .SPI_MISO       (SPI_DO         ),
    .SPI_MOSI       (SPI_DI         ),
    .buttons        (buttons        ),
    .switches       (switches       ),
    .scandoubler_disable (scandoublerD),
    .ypbpr          (ypbpr          ),
    .no_csync       (no_csync       ),
    .core_mod       (core_mod       ),
    .key_strobe     (key_strobe     ),
    .key_pressed    (key_pressed    ),
    .key_code       (key_code       ),
    .mouse_strobe   (mouse_strobe   ),
    .mouse_flags    (mouse_flags    ),
    .mouse_x        (mouse_x        ),
    .joystick_0     (joystick_0     ),
    .joystick_1     (joystick_1     ),
    .status         (status         )
    );
*/

wire [15:0] cpu_rom_addr;
wire [15:0] cpu_rom_do;
wire [14:0] bg_addr;
wire [31:0] bg_do;

wire        ioctl_downl;
wire  [7:0] ioctl_index;
wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire  [7:0] ioctl_dout;

data_io #(.STRLEN(($size(CONF_STR)>>3))) data_io(
    .clk_sys       ( clock_48     ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),
    .SPI_DO        ( SPI_DO       ),

    .data_in       ( osd_s & keys_s ),
    .conf_str      ( CONF_STR ),
    .status        ( status ),

    .ioctl_download( ioctl_downl  ),
    .ioctl_index   ( ioctl_index  ),
    .ioctl_wr      ( ioctl_wr     ),
    .ioctl_addr    ( ioctl_addr   ),
    .ioctl_dout    ( ioctl_dout   )
);
wire [24:0] bg_ioctl_addr = ioctl_addr - 17'h10000;

reg port1_req, port2_req;

sdram #(96) sdram(
    .*,
    .init_n        ( pll_locked   ),
    .clk           ( clock_96     ),

    .port1_req     ( port1_req    ),
    .port1_ack     ( ),
    .port1_a       ( ioctl_addr[23:1] ),
    .port1_ds      ( {ioctl_addr[0], ~ioctl_addr[0]} ),
    .port1_we      ( ioctl_downl ),
    .port1_d       ( {ioctl_dout, ioctl_dout} ),
    .port1_q       ( ),

    .cpu1_addr     ( ioctl_downl ? 16'hffff : {1'b0, cpu_rom_addr[15:1]} ),
    .cpu1_q        ( cpu_rom_do ),
    .cpu2_addr     ( ),
    .cpu2_q        ( ),

    // port2 for sprite graphics
    .port2_req     ( port2_req ),
    .port2_ack     ( ),
    .port2_a       ( {bg_ioctl_addr[24:17], bg_ioctl_addr[14:0], bg_ioctl_addr[16]} ), // merge sprite roms to 32-bit wide words
    .port2_ds      ( {bg_ioctl_addr[15], ~bg_ioctl_addr[15]} ),
    .port2_we      ( ioctl_downl ),
    .port2_d       ( {ioctl_dout, ioctl_dout} ),
    .port2_q       ( ),

    .sp_addr       ( ioctl_downl ? 16'hffff : bg_addr ),
    .sp_q          ( bg_do )
);

// ROM download controller
always @(posedge clock_48) begin
    reg        ioctl_wr_last = 0;

    ioctl_wr_last <= ioctl_wr;
    if (ioctl_downl) begin
        if (~ioctl_wr_last && ioctl_wr) begin
            port1_req <= ~port1_req;
            port2_req <= ~port2_req;
        end
    end
end

reg reset = 1;
reg rom_loaded = 0;
always @(posedge clock_48) begin
    reg ioctl_downlD;
    ioctl_downlD <= ioctl_downl;
    if (ioctl_downlD & ~ioctl_downl) rom_loaded <= 1;
    reset <= status[0] | ~btn_n_o[4] | ~rom_loaded;
end

// quadrature encoder (spinner)
reg   [1:0] spinner_encoder;
reg  [11:0] position = 0;
wire [11:0] position_next = position + {{4{mouse_x[8]}}, mouse_x[7:0]};

always @(posedge clock_48) begin
    reg [15:0] spin_counter;
    reg  [2:0] ce_6m;
    reg [11:0] div_4k;

    ce_6m <= ce_6m + 3'd1;
    if(!ce_6m) begin

        div_4k <= div_4k + 1'd1;
        if(div_4k == 1499) div_4k <= 0;

        if(position != 0) begin //we need to drive position to 0 still;
            if(!div_4k) begin
                case({position[11] , spinner_encoder})
                    {1'b1, 2'b00}: spinner_encoder <= 2'b01;
                    {1'b1, 2'b01}: spinner_encoder <= 2'b11;
                    {1'b1, 2'b11}: spinner_encoder <= 2'b10;
                    {1'b1, 2'b10}: spinner_encoder <= 2'b00;
                    {1'b0, 2'b00}: spinner_encoder <= 2'b10;
                    {1'b0, 2'b10}: spinner_encoder <= 2'b11;
                    {1'b0, 2'b11}: spinner_encoder <= 2'b01;
                    {1'b0, 2'b01}: spinner_encoder <= 2'b00;
                endcase
                
                if(position[11]) position <= position + 1'b1;
                else position <= position - 1'b1;
            end
        end

        if (m_left | m_right) begin // 0.167us per cycle
            // DPAD left/right
            if (spin_counter == 'd48000) begin// roughly 8ms to emulate 125hz standard mouse poll rate
                position <= m_right ? (m_fireB ? 12'd9 : 12'd4) : (m_fireB ? -12'd9 : -12'd4);
                spin_counter <= 0;
            end else begin
                spin_counter <= spin_counter + 1'b1;
            end
        end else begin
            spin_counter <= 0;
        end
    end
    if(mouse_strobe) begin
        if (position[11] != mouse_x[8] || position[11] == position_next[11]) 
            position <= position_next;
        else
            position <= {position[11], {11{~position[11]}}};
    end
end

wire [15:0] audio;
wire        hs, vs, cs;
wire        hblank, vblank;
wire        blankn = ~(hblank | vblank);
wire  [3:0] r, g, b;

Arkanoid Arkanoid_inst
(
    .reset(~reset),                                   //input reset

    .clk_48m(clock_48),                               //input clk_48m

    .spinner(spinner_encoder),                        //input [1:0] spinner

    .coin1(m_coin1),                                  //input coin1
    .coin2(m_coin2),                                  //input coin2

    .btn_shot(~(m_fireA | |mouse_flags[1:0])),        //input btn_shot
    .btn_service(~service),                           //input btn_service

    .tilt(1),                                         //input tilt
    
    .btn_1p_start(~m_one_player),                     //input btn_1p_start
    .btn_2p_start(~m_two_players),                    //input btn_2p_start

    .dip_sw(~dip_sw),                                 //input [7:0] dip_sw

    .sound(audio),                                    //output [15:0] sound

    .h_center(),                                      //Screen centering
    .v_center(),

    .video_hsync(hs),                                 //output video_hsync
    .video_vsync(vs),                                 //output video_vsync
    .video_vblank(vblank),                            //output video_vblank
    .video_hblank(hblank),                            //output video_hblank
	 .video_clock      ( video_clock ),

    .video_r(r),                                      //output [3:0] video_r
    .video_g(g),                                      //output [3:0] video_g
    .video_b(b),                                      //output [3:0] video_b

    .ym2149_clk_div(1'b0),                            //Easter egg - controls the YM2149 clock divider for bootlegs with overclocked AY-3-8910s (default on)
    .vol_boost(1'b0),                                 //Audio volume boost option
    .overclock(1'b0),                                 //Flag to signal that Arkanoid has been overclocked to normalize video timings in order to maintain consistent sound pitch

    .ioctl_addr(ioctl_addr),
    .ioctl_wr(ioctl_wr && ioctl_index == 0),
    .ioctl_data(ioctl_dout),

    .pause(1'b0),

 //   .hs_address(hs_address),
 //   .hs_data_out(hs_data_out),
 //   .hs_data_in(hs_data_in),
 //   .hs_write(hs_write_enable),

    .cpu_rom_addr(cpu_rom_addr),
    .cpu_rom_do(cpu_rom_addr[0] ? cpu_rom_do[15:8] : cpu_rom_do[7:0]),
    .gfx_rom_addr(bg_addr),
    .gfx_rom_do(bg_do)
);

//=================================
wire [11:0] vga_col_s;
wire vga_hs_s, vga_vs_s;
wire video_clock;

wire direct_video_s = ~status[9] ^ direct_video;

framebuffer #(256,224,12) framebuffer
(
        .clk_sys    ( clock_48),
        .clk_i      ( video_clock ),
        .RGB_i      ((blankn) ? {r,g,b} : 12'b0000000000000 ),//idx_color_s ),
        .hblank_i   ( hblank ),
        .vblank_i   ( vblank ),
        
        .rotate_i   ( status[8:7] ), 

        .clk_vga_i  ( (status[7]) ? clk_40 : clk_25m2 ), //800x600 or 640x480
        .RGB_o      ( vga_col_s ),
        .hsync_o    ( vga_hs_s ),
        .vsync_o    ( vga_vs_s ),
        .blank_o    (  ),

        .odd_line_o (  )
);


mist_video #(.COLOR_DEPTH(4), .SD_HCNT_WIDTH(10), .USE_FRAMEBUFFER(1)) mist_video(
   // .clk_sys        ( clock_48         ),
	 .clk_sys ( direct_video_s ? clock_48 : (status[7]) ? clk_40 : clk_25m2 ),
    .SPI_SCK        ( SPI_SCK          ),
    .SPI_SS3        ( SPI_SS2          ),
    .SPI_DI         ( SPI_DI           ),
	
	/* 
    .R              ( blankn ? r : 0   ),
    .G              ( blankn ? g : 0   ),
    .B              ( blankn ? b : 0   ),
    .HSync          ( ~hs              ),
    .VSync          ( vs               ),
	 */
	 
	 // video input 
    .R      ( (direct_video_s) ? (blankn) ? r  : 4'b0000 : {vga_col_s[11:8]} ),
    .G      ( (direct_video_s) ? (blankn) ? g  : 4'b0000 : {vga_col_s[7:4]} ),
    .B      ( (direct_video_s) ? (blankn) ? b  : 4'b0000 : {vga_col_s[3:0]} ),
    .HSync  ( (direct_video_s) ? ~hs : vga_hs_s ),
    .VSync  ( (direct_video_s) ? vs : vga_vs_s ),
	 
    .VGA_R          ( VGA_R            ),
    .VGA_G          ( VGA_G            ),
    .VGA_B          ( VGA_B            ),
    .VGA_VS         ( VGA_VS           ),
    .VGA_HS         ( VGA_HS           ),
	 
    .ce_divider     ( 1'b1                ),
    .rotate         ( osd_rotate ),
    .blend          ( blend            ),
    .scandoubler_disable( direct_video_s ),
    .scanlines      ( scanlines        ),
    .no_csync   ( ~direct_video_s ),
    .osd_enable ( osd_enable )
    );

dac #(.C_bits(16))dac_l(
    .clk_i(clock_48),
    .res_n_i(1'b1),
    .dac_i({~audio[15], audio[14:0]}),
    .dac_o(AUDIO_L)
    );


wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player  = ~btn_n_o[1] | m_one_player;
wire btn_two_players = ~btn_n_o[2] | m_two_players;
wire btn_coin        = ~btn_n_o[3] | m_coin1;
wire [7:0] keys_s;
wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable;
wire direct_video;
wire [1:0]osd_rotate;
wire [4:1]btn_n_o;

MC2_HID #( .OSD_CMD( 3'b011 ), .CLK_SPEED(50000), .use_usb_g ( 1'b1 )) k_hid
(
    .clk_i          ( clock_50_i ),
    .kbd_clk_io     ( ps2_clk_io ),
    .kbd_dat_io     ( ps2_data_io ),
    .usb_rx_i       ( ps2_mouse_clk_io ),

    .joystick_0_i   ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1_i   ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
      
    //-- joystick_0 and joystick_1 should be swapped
    .joyswap_i      ( 0 ),

    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer_i    ( 0 ),

    //-- tilt, coin4-1, start4-1
    .controls_o     ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

    //-- fire12-1, up, down, left, right

    .player1_o      ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2_o      ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),

    .direct_video_o ( direct_video ),
    .osd_rotate_o   ( osd_rotate ),

    //-- keys to the OSD
    .osd_o          ( keys_s ),
    .osd_enable_i   ( osd_enable ),
    
    //-- sega joystick
    .sega_strobe_o  ( joy_p7_o ),

     //-- Front buttons
     .front_buttons_i ( btn_n_i ),
     .front_buttons_o ( btn_n_o )        
);


endmodule
